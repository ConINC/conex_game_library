﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using ConexGameLibrary;

namespace ConexGameLibrary
{
    public class Entity_Functions
    {
        //-------------------------------------------------------------------------------
       
        //WORK ON THAT 
        public static void loadMapEndtities(string fullPathOfMap,  EntityContainer entity_container_p,  List<MeshGroup> meshgroups_p,  List<Block> blocks_p)
        {
            String[] MapEntity_lines;
            try
            {
                MapEntity_lines = File.ReadAllLines(fullPathOfMap);
            }
            catch 
            {
                return;
            }
            Door_sliding temp_door_sliding;
            int entity_data_line_size = 6;
            int current_list_index = 0;
            for (int i = 0; i < MapEntity_lines.Length; i++)
            {
                if (MapEntity_lines[i].StartsWith("Mapname "))
                    entity_container_p.Mapname = MapEntity_lines[i].Substring(8);
                
                //Search for an Entity in the entity list first
                if (MapEntity_lines[i].Contains("Door_Entity") &&
                    MapEntity_lines[i + 1].StartsWith("{") &&
                    MapEntity_lines[i + entity_data_line_size].StartsWith("}")
                    )
                {
                    string door_sliding_name = MapEntity_lines[i].Substring(12);
                    //Now since we have the name, it must also be contained in the map file as a mesh, search for it by enumerating in both lists
                                       
                    for (int j = 0; j < meshgroups_p.Count; j++)
                    {
                        if (blocks_p[j].group == door_sliding_name && meshgroups_p[j].GroupName == door_sliding_name)
                        {
                            //Now we have a deal, create that particular Entity, since it is addressable now , it exists!
                            temp_door_sliding = new Door_sliding();

                            //is given by reference
                            //temp_door_sliding.block = blocks_p[j];
                            //temp_door_sliding.mesh = meshgroups_p[j];
                            temp_door_sliding.entity_name = door_sliding_name;

                            //now we need to read out the rest of the entity data
                            string dir_line = string.Empty;
                            string pos_line = string.Empty;
                            string amount_line = string.Empty;


                            if (MapEntity_lines[i + 2].Contains("Move_Direction"))
                                dir_line = MapEntity_lines[i + 2].Substring(16);

                            if (MapEntity_lines[i + 3].Contains("Move_Amount"))
                                amount_line = MapEntity_lines[i + 3].Substring(13);

                            if (MapEntity_lines[i + 4].Contains("Position"))
                                pos_line = MapEntity_lines[i + 4].Substring(10);

                            if (MapEntity_lines[i + 5].Contains("Speed"))
                                temp_door_sliding.speed = Convert.ToSingle(MapEntity_lines[i + 5].Substring(7), CultureInfo.InvariantCulture.NumberFormat);

                            string[] dir_segments = dir_line.Split(',');
                            string[] pos_segments = pos_line.Split(',');

                            //--------------------------------------------------------------------------------
                            //Convert the string data into vector
                            
                            temp_door_sliding.moveDirection = new Vector3(
                                Convert.ToSingle(dir_segments[0], CultureInfo.InvariantCulture.NumberFormat),
                                Convert.ToSingle(dir_segments[1], CultureInfo.InvariantCulture.NumberFormat),
                                Convert.ToSingle(dir_segments[2], CultureInfo.InvariantCulture.NumberFormat));
                            Console.WriteLine("\n\n"+temp_door_sliding.moveDirection + " as direction");

                            temp_door_sliding.centerPosition = new Vector3(
                                Convert.ToSingle(pos_segments[0], CultureInfo.InvariantCulture.NumberFormat),
                                Convert.ToSingle(pos_segments[1], CultureInfo.InvariantCulture.NumberFormat),
                                Convert.ToSingle(pos_segments[2], CultureInfo.InvariantCulture.NumberFormat));
                            Console.WriteLine(temp_door_sliding.centerPosition + " as centerPos");

                            temp_door_sliding.move_amount = Convert.ToSingle(
                                amount_line,
                                CultureInfo.InvariantCulture.NumberFormat);
                            Console.WriteLine(temp_door_sliding.move_amount + " as moveamount");

                            temp_door_sliding.destinationPosition = Vector3.Add(
                               temp_door_sliding.centerPosition,
                               Vector3.Multiply(temp_door_sliding.moveDirection, temp_door_sliding.move_amount));
                            Console.WriteLine(temp_door_sliding.destinationPosition + " as DestinationPos");
                            //--------------------------------------------------------------------------------

                            Console.WriteLine("Added object {0} to entity list", temp_door_sliding.entity_name);


                            entity_container_p.door_sliding_list.Add(new Door_sliding());
                            entity_container_p.door_sliding_list[current_list_index] = ObjectCopier.Clone<Door_sliding>(temp_door_sliding);
                            
                            Console.WriteLine(current_list_index + " is index");
                            current_list_index++;
                        }
                    }
                }
            }            
        }     
    }
    [SerializableAttribute]
    public class Door_sliding
    {        
        public Vector3 centerPosition;
        public Vector3 moveDirection;
        public float move_amount;
        public Vector3 destinationPosition;
        public Vector3 currentPosition;
        public String entity_name;
        bool door_locked = false;
        bool door_open = false;
        bool door_moving = false;

        public float speed;
        private int timer = 0;

        public void automaticOpening(  List<Block> block_p,   List<MeshGroup> mesh_p, Vector3 trigger_position, float radius)
        {
            if(!door_open && !door_moving && !door_locked && VectorMath.get_length(Vector3.Subtract(trigger_position, centerPosition)) < radius)
            {

            }
        }
                
        private void resetTimer()
        {
            timer = 180;
        }

        private void changePosition(float speed,   List<Block> block_p,   List<MeshGroup> mesh_p)
        {
            currentPosition = Vector3.Add(centerPosition, Vector3.Multiply(moveDirection, move_amount));
            for (int i = 0; i < mesh_p.Count; i++)
            {
                //detect if the meshgroup is the right entity
                //translate the mesh vertices positions. VERY unefficient..
                if (mesh_p[i].GroupName == entity_name)
                {
                    for (int j = 0; j < mesh_p[i].vertices.Length; j++)
                    {                       
                        Vector3 newval = Vector3.Add(
                            mesh_p[i].vertices[j].Position,
                                Vector3.Multiply(
                                moveDirection,
                                move_amount)
                            );
                        CustomVertex vert = new CustomVertex(
                            newval,
                            mesh_p[i].vertices[j].TextureCoordinate,
                            mesh_p[i].vertices[j].Normal,
                            Vector3.Zero
                            , Vector3.Zero
                            );

                        mesh_p[i].vertices[j] = vert;
                    }
                }

                //detect the right group too, then translate
                //block meshes
                if (block_p[i].group == entity_name)
                {
                    for (int j = 0; j < block_p[i].wallModel.vertices.Length; j++)
                    {
                        Vector3 newval = Vector3.Add(
                            block_p[i].wallModel.vertices[j].Position,
                                Vector3.Multiply(
                                moveDirection,
                                move_amount)
                            );
                        CustomVertex vert = new CustomVertex(
                            newval,
                            block_p[i].wallModel.vertices[j].TextureCoordinate,
                            block_p[i].wallModel.vertices[j].Normal
                            , Vector3.Zero,
                            Vector3.Zero
                            );

                        block_p[i].wallModel.vertices[j] = vert;
                    }

                    for (int j = 0; j < block_p[i].floorModel.vertices.Length; j++)
                    {
                        Vector3 newval = Vector3.Add(
                                block_p[i].floorModel.vertices[j].Position,
                                    Vector3.Multiply(
                                    moveDirection,
                                    move_amount)
                                );
                        CustomVertex vert = new CustomVertex(
                            newval,
                            block_p[i].floorModel.vertices[j].TextureCoordinate,
                            block_p[i].floorModel.vertices[j].Normal
                            , Vector3.Zero
                            , Vector3.Zero
                            );

                        block_p[i].floorModel.vertices[j] = vert;
                    }
                }
                //End of shifting
            }
        }
               
        public void lock_door()
        {
            door_locked = true;
        }

        public void unlock_door()
        {
            door_locked = false;
        }
    }

    [SerializableAttribute]
    public class Door_rotating
    {
        MeshGroup mesh;
        Block block;
        Vector3 centerPosition;
        Vector3 moveDirection;
        float move_amount;
        String entity_name;

        public Door_rotating()
        {
        }
    }

    public class EntityContainer
    {
        public string Mapname;
        public List<Door_sliding> door_sliding_list;
        public List<Door_rotating> door_rotating_list;

        public EntityContainer()
        {
            door_sliding_list = new List<Door_sliding>();
            door_rotating_list = new List<Door_rotating>();
        }
    }    

}
