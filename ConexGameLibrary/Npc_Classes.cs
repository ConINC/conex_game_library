﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ConexGameLibrary;
using System.IO;

namespace ConexGameLibrary
{
    //This will be part of the GameSegment, it holds all Npc relevant level data
    //such as:
    //----------------------------------------------
    //-Pathfinding nodes
    //-Guardroutes
    //-TriggerAreas
    //-ShootingSpots
    //-behaviour related areas
    //-climb or jump spots
    //
    //-----------------------------------

    public class NPC_Level
    {
        public Color activateColor;
        public Vector3 playerpos, targetpos;
        public MouseState input;
        public KeyboardState keyboard;
        public List<NPC_Node_Collection> Npc_Node_Collection_list;

        //Use this only for a single NPC, just for visualisation
        public List<NPC_PathLines> pathLines;

        public Texture2D line_texture;
        public Effect lineEffect;

        public Block npc_node_mesh;
        public float rotateY;

        public int activeNode = -1, passiveNode = -1; //Used to link nodes.

        public int export_node_timer = 0;

        public List<NPC_Follower> Npcs;

        public NPC_Level()
        {
            Npcs = new List<NPC_Follower>();
            this.pathLines = new List<NPC_PathLines>();
            Npc_Node_Collection_list = new List<NPC_Node_Collection>();            
        }

    }

    public struct Node_StartEndStruct
    {
        public int startNode;
        public int endNode;
    }

    public struct Node_PathIndicesStruct
    {
        public int[] node_indices;
    }
    
    public class NPC_Entity
    {
        public AnimatedModelMesh model;
        
        public String NPC_Name;
        public String NPC_Index;
        public Vector3 Position;
        public Vector3 Rotation;
    }

    /// <summary>
    /// Will be used the most, walks along a path and reacts to player
    /// </summary>
    public class NPC_Guard : NPC_Entity
    {
        
    }

    public class NPC_NOONE : NPC_Entity
    {
        
    }
    
    public class NPC_Follower : I_Targetable , I_GameEntity
    {
        //The animated Model for the NPC itself
        public AnimatedModelMesh model;
        public AnimatedModelMesh Hitbox_model;

        public AnimationController controller;

        //non-animated mesh to serve as Items/Held weapons/Backpacks.. anything
        public int[] ParentedModels_AttachedIndex;
        public AnimatedModelMesh[] ParentedModels;

        public string NPC_Type;
        public String NPC_Name;
        public String NPC_Index;
        public Vector3 Position;
        public Vector3 Rotation;
        public Vector3 AdditionalMovement = Vector3.Zero;

        //----------------------------------------------------------------------------------
        // Gun related stuff - Change this anyway later
        //----------------------------------------------------------------------------------
        public Vector3 CurrentGunPos; //relative height only, since parented to npc Pos
        public Vector3 currentGunLookAt;
        public Vector3 GunRotation;
        public bool CanShoot = false;
        public float AimError = 0.15f;
        //----------------------------------------------------------------------------------

        public NPC_PathFindRoute pathRoute;
        public bool standIdle = false;
        public float WalkSpeed;
        public float GetCloseValue = 1;
        
        public int currentPathNode;
        public bool ReachedGoal = false;
        public bool followPathWalking = false;
        public float CurrentWalkSpeed_Multiplier = 1;
        public float WalkOffTimer = 160;
        public float PassNodeRadius; //Standard
        public float ActOnSightRadius;
        public int NPC_BehaviourMode = 0; //0 OnSight, 1 = Path, 2 .. idle?

        public bool allow_CanDoPathFind = true;
        public bool Lock_CanDoPathFind = false;

        public SphereConvexHull sphereConvexHull;

        public Vector3 Get_Position()
        {
            return Position;
        }

        public NPC_Follower(
            GraphicsDevice g, 
            string NpcName, 
            GeneralGameData dat, 
            ref GameSegment seg,
            ref  AnimationContainer cont, 
            string idleAnim,
            string modelPathName,
            string hitboxPathName,  
            Vector3 pos, 
            Vector3 rot, 
            float speed,
            float getclose, 
            int parentedModelsCount,
            float nodeRadius,
            float actOnSightRadius)
        {
            this.GetCloseValue = getclose;
            this.ActOnSightRadius = actOnSightRadius;
            this.PassNodeRadius = nodeRadius;
            this.NPC_Name = NpcName;

            //MESH
            model = Animation_Functions.LoadAnimatedMeshFromFBX(dat, modelPathName);
            controller = new AnimationController(cont, idleAnim, "owner");

            controller.ControllerOwnerName = NpcName;

            var s = new FileStream(dat.GameDirectory + model.relTextureNames[0], FileMode.Open);
            model.DiffuseTexture = Texture2D.FromStream(g, s);
            s.Close();

            controller.AnimationTimer = 0;
            controller.AnimationEnded = true;
            controller.AnimationRunning = false;
            controller.PlayAnimationOnce = false;
                        
            this.ParentedModels = new AnimatedModelMesh[parentedModelsCount];
            this.ParentedModels_AttachedIndex = new int[parentedModelsCount];
            
            this.WalkSpeed = speed;
            Position = pos;
            Rotation = rot;                        
        }

        public I_ConvexHull getConvexHull()
        {
            return sphereConvexHull;
        }

        //Calculate the collision
        //Calculate the damage 
        public void get_Shot(Shooting_Data dat, AnimatedModelMesh hitbox, Matrix m)
        {
            List<string> list = CollisionDetection.Calculate_Ray_To_Hitbox(m, hitbox, dat.shoot_ray);

            foreach (String l in list)
            {
                Console.WriteLine(l);
            }
        }

        public Vector3 get_Position()
        {
            return Position;
        }

        public Vector3 get_Position_Targetable()
        {
            return Position;
        }

        public void updateConvexHull()
        {
            this.sphereConvexHull.Position = Position;
            this.sphereConvexHull.Position.Y += sphereConvexHull.Radius;            
        }

        public void updateNPCStuff()
        {
            this.Position += AdditionalMovement;
        }

        public void setConvexHull(SphereConvexHull hull)
        {
            sphereConvexHull = hull;
        }
    }

    public class NPC_PathLines
    {
        public String PathLines_Name;
        public int PathLines_Index;
        public float Rotation_Y = 0;
        public List<Line> Lines;

        public NPC_PathLines(int pathlineIndex, string pathlineName)
        {
            this.PathLines_Index = pathlineIndex;
            this.PathLines_Name = pathlineName;
            Lines = new List<Line>();
        }
    }

    #region Node-classes
    //The Node is used for Pathfinding calculations
    public class NPC_Node
    {
        public List<int> neighbourNodes;
        public Vector3 Position;
        
        public int NodeIndex;
        public string NodeName; //if ever used..

        public int CameFrom = -1; //Used for backtracking in pathfinding
        public int selected = 0;
        
        public NPC_Node()
        {
            this.neighbourNodes = new List<int>();
        }
    }

    public class NPC_Node_Collection
    {
        public List<NPC_Node> nodes;
        public String NodeColleciton_Type;
        public string NodeCollection_Name;
        public int NodeCollection_Index;
        public List<Block> nodeBlock;

        public List<int> shortest_path = new List<int>();
        
        /// <summary>
        /// Constructor, what else!
        /// </summary>
        /// <param name="nodeColleciton_Index">Index, use for whatever</param>
        /// <param name="nodeCollection_Name">Identifiy it by name</param>
        /// <param name="nodeCollection_Type">The typ defines for Which enemytype, Walk, Fly, or Drive</param>
        public NPC_Node_Collection(int nodeColleciton_Index, string nodeCollection_Name, string nodeCollection_Type)
        {
            this.nodes = new List<NPC_Node>();
            this.NodeCollection_Name = nodeCollection_Name;
            this.NodeCollection_Index = nodeColleciton_Index;
            this.NodeColleciton_Type = nodeCollection_Type;
        }
    }
    #endregion

    #region Pathing-classes
    //A Collection of Path_routes
    public class NPC_PathRoute_Collection
    {
        public List<NPC_PathRoute> Routes = new List<NPC_PathRoute>();
    }

    //A PathRoute consisting of pathnodes 
    public class NPC_PathRoute
    {
        public String PathName;
        public int PathIndex; //not sure if needed
        public string PathType; //Who uses this? "Enemy" or "Friend" 
        public List<NPC_PathNode> path;
        public int[] pathIndices;
        
        public NPC_PathRoute()
        {
            path = new List<NPC_PathNode>();
            this.PathIndex = -1;
            
        }
    }

    //Use this for the Guarding-route
    public class NPC_PathNode
    {
        public Vector3 Position;

        public int Current_NodeIndex;
        public int Parent_NodeIndex;
        public int Child_NodeIndex;
        public int WaitAtNode_Time; //in frames
        public bool IgnoreNode = false;

        public string NodeName; //if ever used..
    }

    public class NPC_PathFindRoute
    {
        public List<int> startpoints;
        public List<int> endpoints;
        public int[] path_indices;

        public NPC_PathFindRoute()
        {
            path_indices = new int[0];
            startpoints = new List<int>();
            endpoints = new List<int>();
        }
    }

    #endregion
    
    public class NPC_Functions
    {
        /// <summary>
        /// THis loads the Nodes for the desired Collection 
        /// </summary>
        /// <param name="Index"></param>
        /// <param name="Name"></param>
        /// <param name="Type"></param>
        /// <param name="FileEnding">map_(ending)  example: _NPC_Walk_Nodes / _NPC_Fly_Nodes /...</param>
        /// <param name="dat"></param>
        /// <param name="seg"></param>
        /// <returns></returns>
        public static NPC_Node_Collection Load_NPC_Nodes(int Index, string Name, string Type, string FileEnding, GeneralGameData dat,   GameSegment seg)
        {
            List<Block> blocks = seg.Level.maploader.get_SplitMapMeshesAsBlock(dat.GameDirectory, dat.RelativeMapFileAndPath + FileEnding);
            NPC_Node_Collection currentCollection = new NPC_Node_Collection(Index, Name,Type);
            
            /*----------------------------------------------------------------------------------
            //Since we the floor/wall blocks here, we merge them into a point to become a Node
            /*          \        /
            /*           \      /
            /*            \    /    becomes    x    (point)
            /*             \  /
            /*          ....\/....  
            /*__________________________________________________________________________________
            */
            
            for (int i = 0; i < blocks.Count; i++)
            {
                
                Vector3 mergedPoint_Floor = Vector3.Zero;

                //Lets merge the Floor vertices first
                for (int f = 0; f < blocks[i].floorModel.vertices.Length; f++)
                {
                    mergedPoint_Floor += blocks[i].floorModel.vertices[f].Position;
                }

                //Keep this one for later
                float dividor_floor = (float)(blocks[i].floorModel.vertices.Length);

                mergedPoint_Floor = mergedPoint_Floor / dividor_floor;
                

                Vector3 mergedPoint_Wall = Vector3.Zero;

                //Lets merge the Wall vertices now
                for (int w = 0; w < blocks[i].wallModel.vertices.Length; w++)
                {
                    mergedPoint_Wall = mergedPoint_Wall + blocks[i].wallModel.vertices[w].Position;
                }


                                
                float dividor_wall = (float)blocks[i].wallModel.vertices.Length;

                mergedPoint_Wall = mergedPoint_Wall / dividor_wall;

                //Finally, merge the 2 Points into one
                Vector3 NodePosition = (mergedPoint_Floor) / 1.0f; //Thx operator overload

                //At last, we want to create the Node and add it to the collection
                NPC_Node node = new NPC_Node();
                node.NodeIndex = i; //just use this index for now
                node.NodeName = string.Empty;
                node.Position = NodePosition;

                currentCollection.nodes.Add(node);
            }

            //Done
            return currentCollection;
        }

        //Same function as above, Just for the blocks!
        public static List<Block> Load_NPC_Nodes_BlocksMesh(string FileEnding, GeneralGameData dat,GameSegment seg)
        {
            List<Block> blocks = seg.Level.maploader.get_SplitMapMeshesAsBlock(dat.GameDirectory, dat.RelativeMapFileAndPath + FileEnding);
           
            //Done
            return blocks;
        }

        public static Node_StartEndStruct Get_StartAndEndNodeFromPositions(  GameSegment seg, Vector3 startPosition, Vector3 endPositon, int nodeCollectionIndex)
        {
            Node_StartEndStruct node;
            int startnode = -1;
            int endnode = -1;

            float dis_start_lowest = 999999;
            float dis_end_lowest = 999999;


            float dis_start = 0;
            float dis_end = 0;           

            int pass = 0;

            for (int i = 0; i < seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes.Count; i++)
            {
                //Get each direciton from end/start to each node   Since we try to find the closest node WHich is also visible!   
                //dir_start = Vector3.Subtract(seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes[i].Position, startPosition);
                dis_start = VectorMath.get_length(Vector3.Subtract(seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes[i].Position, startPosition));
                //raystart = new Ray(startPosition + new Vector3(0, 3, 0), dir_start);

                //Find the closest node!
                if (dis_start < dis_start_lowest + 0.5f)
                {
                    pass++;
                    dis_start_lowest = dis_start;
                    startnode = i;

                    //We identified the closest node, 

                    //Console.WriteLine("Node {0} is {1} away from player_start in pass {2}", startnode, dis_start, pass);
                }

                #region 1111
                /*
                for (int j = 0; j < seg.Level.MapBlocks.Count; j++)
                {
                    for (int x = 0; x < seg.Level.MapBlocks[j].wallModel.vertices.Length; x += 3)
                    {
                        Vector3 intersection_point_startRay = CollisionDetection.intersectionPointTriangleRay(
                            seg.Level.MapBlocks[j].wallModel.vertices[x].Position,
                            seg.Level.MapBlocks[j].wallModel.vertices[x + 1].Position,
                            seg.Level.MapBlocks[j].wallModel.vertices[x + 2].Position,
                            raystart);
                        startToWallIntersect = VectorMath.get_length(Vector3.Subtract(intersection_point_startRay, startPosition));

                        if (startToWallIntersect < dis_start-1)
                        {
                            canSee_start = false;
                            

                        }
                        else
                            canSee_start = true;

                    }
                }
                */
                #endregion
            }

            pass = 0;
            for (int i = 0; i < seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes.Count; i++)
            {
                //Get each direciton from end/start to each node   Since we try to find the closest node WHich is also visible!   
                //dir_start = Vector3.Subtract(seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes[i].Position, startPosition);
                dis_end = VectorMath.get_length(Vector3.Subtract(seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes[i].Position, endPositon));
                //raystart = new Ray(startPosition + new Vector3(0, 3, 0), dir_start);


                //Find the closest node!
                if (dis_end < dis_end_lowest + 0.5f)
                {
                    pass++;
                    dis_end_lowest = dis_end;
                    endnode = i;

                    //We identified the closest node, 

                   // Console.WriteLine("Node {0} is {1} away from player_end in pass {2}", endnode, dis_end, pass);
                }

                #region 1111
                /*
                for (int j = 0; j < seg.Level.MapBlocks.Count; j++)
                {
                    for (int x = 0; x < seg.Level.MapBlocks[j].wallModel.vertices.Length; x += 3)
                    {
                        Vector3 intersection_point_startRay = CollisionDetection.intersectionPointTriangleRay(
                            seg.Level.MapBlocks[j].wallModel.vertices[x].Position,
                            seg.Level.MapBlocks[j].wallModel.vertices[x + 1].Position,
                            seg.Level.MapBlocks[j].wallModel.vertices[x + 2].Position,
                            raystart);
                        startToWallIntersect = VectorMath.get_length(Vector3.Subtract(intersection_point_startRay, startPosition));

                        if (startToWallIntersect < dis_start-1)
                        {
                            canSee_start = false;
                            

                        }
                        else
                            canSee_start = true;

                    }
                }
                */
                #endregion
            }
            node.endNode = endnode;
            node.startNode = startnode;

            return node;

        }
        
        /// <summary>
        /// This one only explores the surroundings, thats all
        /// </summary>
        /// <param name="node_col"></param>
        /// <param name="startnode"></param>
        /// <param name="endnode"></param>
        public static void breadth_Algorithm_NoPath(NPC_Node_Collection node_col, int startnode, int endnode)
        {
            Queue<NPC_Node> nodes_front = new Queue<NPC_Node>();
            nodes_front.Enqueue(node_col.nodes[startnode]);

            List<int> visited = new List<int>();

            //While the front has nodes, keep going
            while (nodes_front.Count > 0)
            {
                //Console.WriteLine("Front contains {0} nodes", nodes_front.Count);
                //Get the first node in the queue
                NPC_Node node = nodes_front.Dequeue();

                //Console.WriteLine("Visiting node {0} right now", node.NodeIndex);
                //Now we loop throught all its neighbours
                for (int i = 0; i < node.neighbourNodes.Count; i++)
                {
                    //If that neighbour_node is not in the visited list, it must be processed
                    if (!visited.Contains(node.neighbourNodes[i]))                    
                    {
                        //Add it to the queue to be processed
                        nodes_front.Enqueue(node_col.nodes[node.neighbourNodes[i]]);
                        
                        //the node was visited add to visited list!
                        visited.Add(node.neighbourNodes[i]);
                        //Console.WriteLine("Visiting neighbour {0} of node {1}",node.neighbourNodes[i] , node.NodeIndex);
                    }
                    
                }
            }

        }

        /// <summary>
        /// Use this to find the path from a start to endnode, this algorithm only fills in "parental" info to
        /// each node around it, so that the only direction leads to the start again
        /// </summary>
        /// <param name="node_col"></param>
        /// <param name="startnode"></param>
        /// <param name="endnode"></param>
        /// <param name="foundLeave"></param>
        public static void breadth_Algorithm_wParentInfo(NPC_Node_Collection node_col, int startnode, int endnode, bool foundLeave)
        {
            Queue<NPC_Node> nodes_front = new Queue<NPC_Node>();
            nodes_front.Enqueue(node_col.nodes[startnode]);

            List<NPC_Node> came_from = new List<NPC_Node>();

            node_col.nodes[startnode].CameFrom = startnode;
            came_from.Add(node_col.nodes[startnode]);

            //While the front has nodes, keep going
            while (nodes_front.Count > 0)
            {
                //Console.WriteLine("Front contains {0} nodes", nodes_front.Count);
                //Get the first node in the queue
                NPC_Node node = nodes_front.Dequeue();

                //the node has been found, dont need to continue
                if (node.NodeIndex == endnode && foundLeave)
                    break;

                //Console.WriteLine("Visiting node {0} right now", node.NodeIndex);
                //Now we loop throught all its neighbours
                for (int i = 0; i < node.neighbourNodes.Count; i++)
                {
                    //If that neighbour_node is not in the visited list, it must be processed
                    if (!came_from.Contains( node_col.nodes[node.neighbourNodes[i]]))
                    {
                        //NEXT IS  node.neighbourNodes[i]!!!!!

                        //Add it to the queue to be processed
                        nodes_front.Enqueue(node_col.nodes[node.neighbourNodes[i]]);

                        //note where we came from, then add it to the "camefrom" list
                        node_col.nodes[node.neighbourNodes[i]].CameFrom = node.NodeIndex;
                        came_from.Add(node_col.nodes[node.neighbourNodes[i]]);


                        //Console.WriteLine("Node {0} came from {1}", node.neighbourNodes[i], node.NodeIndex);
                    }

                }
            }

        }

        public static void Create_NPC_Path_Lines(  GameSegment seg, GeneralGameData dat, NPC_PathFindRoute route)
        {            
            seg.npc_level.pathLines[0].Lines.Clear();
            for (int d = 0; d < route.startpoints.Count; d++)
            {
                try
                {
                    Line line = new Line(Vector3.Zero, Vector3.Zero, Color.Yellow);
                    line.LineVertices[0].Position = seg.npc_level.Npc_Node_Collection_list[0].nodes[route.startpoints[d]].Position + new Vector3(0, -0.5f, 0);
                    line.LineVertices[1].Position = seg.npc_level.Npc_Node_Collection_list[0].nodes[route.startpoints[d]].Position + new Vector3(0, 0.5f, 0);
                    line.LineVertices[2].Position = seg.npc_level.Npc_Node_Collection_list[0].nodes[route.endpoints[d]].Position + new Vector3(0, 0, 0);
                    seg.npc_level.pathLines[0].Lines.Add(line);
                }
                catch { }
            }
        }

        public static void Setup_NPC_Route_And_Pathfind(  GameSegment seg, GeneralGameData dat)
        {
            seg.npc_level.Npcs[0].ReachedGoal = false;
            //seg.npc_level.Npcs[0].Position = seg.Players[0].Position_Player;
            Node_StartEndStruct node = NPC_Functions.Get_StartAndEndNodeFromPositions(
                  seg,
                seg.npc_level.Npcs[0].Position,
                seg.Players[0].Position_Player, 0);

            NPC_Functions.breadth_Algorithm_NoPath(seg.npc_level.Npc_Node_Collection_list[0], node.startNode, 10);
            NPC_Functions.breadth_Algorithm_wParentInfo(seg.npc_level.Npc_Node_Collection_list[0], node.startNode, 10, false);


            seg.npc_level.Npcs[0].pathRoute = NPC_Functions.NPC_GeneratePathFromParents(
                seg.npc_level.Npc_Node_Collection_list[0],
                node.startNode,
                node.endNode);

            seg.npc_level.Npcs[0].followPathWalking = true;
            seg.npc_level.Npcs[0].ReachedGoal = false;
            seg.npc_level.Npcs[0].currentPathNode = 0;
            seg.npc_level.Npcs[0].currentPathNode = 0;
            //seg.npc_level.Npcs
            NPC_Functions.Create_NPC_Path_Lines(  seg, dat, seg.npc_level.Npcs[0].pathRoute);
        }

        public static NPC_PathFindRoute NPC_GeneratePathFromParents(NPC_Node_Collection nodecol, int startnode, int endnode)
        {
            
            NPC_PathFindRoute pathRoute = new NPC_PathFindRoute();
            return pathRoute;
            int currentNode = endnode;

            while (currentNode != startnode && currentNode >= 0)
            {
                pathRoute.endpoints.Add(nodecol.nodes[currentNode].NodeIndex);
                pathRoute.startpoints.Add(nodecol.nodes[currentNode].CameFrom);

                currentNode = nodecol.nodes[currentNode].CameFrom;
            }
            pathRoute.startpoints.Reverse();
            pathRoute.endpoints.Reverse();

            
            int[] pathIndices = new int[pathRoute.startpoints.Count + 1];
            pathIndices[0] = -1;
            if (pathRoute.endpoints.Count > 0)
            {
                                
                pathIndices[0] =  pathRoute.startpoints[0];

                for (int j = 0; j < pathRoute.endpoints.Count; j++)
                {
                    pathIndices[j + 1] = pathRoute.endpoints[j];

                }
            }

            pathRoute.path_indices = pathIndices;
    
            return pathRoute;
        } 
    }

    public class NPC_BehaviourFunctions
    {
        
        //This function uses the standard Idle function when theres no other animation playback requested
        public static void Animation_Behaviour(int npcIndex,   GameSegment seg)
        {
            if (seg.npc_level.Npcs.Count < 1)
                return;
            
            Matrix currentRot = Matrix.CreateRotationY(seg.npc_level.Npcs[npcIndex].Rotation.Y) * Matrix.CreateTranslation(seg.npc_level.Npcs[npcIndex].Position + new Vector3(0,0.4f,0));
                        
            //Play a specific animation over the idle first
            if (seg.npc_level.Npcs[npcIndex].controller.PlayAnimationOnce)
            {
                seg.npc_level.Npcs[npcIndex].controller.AnimationTimer++;    
            
                Animation_Functions.PlayAnimation_AnimColl(seg.shared_animation_human,
                    seg.npc_level.Npcs[npcIndex].controller.currentAnimationName,
                      seg.npc_level.Npcs[npcIndex].model,
                      seg.npc_level.Npcs[npcIndex].controller,
                      currentRot,
                    "", Matrix.Identity, "", Matrix.Identity, "", Matrix.Identity, null);                               
            }
            //This plays the idle Animation if noone is played else
            else if (seg.npc_level.Npcs[npcIndex].controller.PlayAnimationOnce == false)
            {
                Animation_Functions.PlayAnimation_AnimColl(seg.shared_animation_human,
                    seg.npc_level.Npcs[npcIndex].controller.idleAnimName,
                      seg.npc_level.Npcs[npcIndex].model,
                      seg.npc_level.Npcs[npcIndex].controller,
                      currentRot,
                    "", Matrix.Identity, "", Matrix.Identity, "", Matrix.Identity, null);                               

                seg.npc_level.Npcs[npcIndex].controller.AnimationTimer++;
            }                        
            
        }
        //React on sight/close range     //Ye I know, will change later
        
        public static void Decide_NPC_ActMode(int npcIndex,   GameSegment seg, GeneralGameData dat, I_Targetable targetObj)
        {
            return;
            
            Vector3 targetObject = targetObj.get_Position_Targetable() + new Vector3(0, 4, 0);
            Vector3 currentPos = seg.npc_level.Npcs[npcIndex].Position + new Vector3(0, 4, 0);

            seg.npc_level.targetpos = currentPos;
            seg.npc_level.playerpos = targetObject;
            Vector3 newRotation =  new Vector3(0, 0, 0);
            Vector3 AimDirection = currentPos - targetObject;
            Single DistancePlayerNpc = VectorMath.get_length(AimDirection);
            Vector3 AimDirNormalized = Vector3.Normalize(AimDirection);
            Ray ray = new Ray(targetObject, AimDirNormalized);

            RayTriangle_CollisionData data_new;
            RayTriangle_CollisionData data = new RayTriangle_CollisionData();
            data.distance = float.MaxValue;
            data.IntersectPoint = Vector3.Zero;
            data.uValue = float.MinValue;
            data.doesIntersect = false;

            seg.npc_level.Npcs[npcIndex].NPC_BehaviourMode = 0; // 0 = on sight

            //Check every wall/such, always try to find the closesest with positive rayfactor
            for (int i = 0; i < seg.Level.MapBlocks.Count; i++)
            {
                for (int j = 0; j < seg.Level.MapBlocks[i].wallModel.vertices.Length; j += 3)
                {
                    data_new = CollisionDetection.checkTriangleIntersect_returnFull(
                        seg.Level.MapBlocks[i].wallModel.vertices[j].Position,
                        seg.Level.MapBlocks[i].wallModel.vertices[j + 1].Position,
                        seg.Level.MapBlocks[i].wallModel.vertices[j + 2].Position,
                        ray);

                    if (data_new.distance < data.distance && data_new.uValue > 0)
                    {
                        data = data_new;
                    }
                }

                for (int j = 0; j < seg.Level.MapBlocks[i].floorModel.vertices.Length; j += 3)
                {
                    data_new = CollisionDetection.checkTriangleIntersect_returnFull(
                        seg.Level.MapBlocks[i].floorModel.vertices[j].Position,
                        seg.Level.MapBlocks[i].floorModel.vertices[j + 1].Position,
                        seg.Level.MapBlocks[i].floorModel.vertices[j + 2].Position,
                        ray);

                    if (data_new.distance < data.distance && data_new.uValue > 0)
                    {
                        data = data_new;
                    }
                }                             
            }

            //This must be true
            DistancePlayerNpc = VectorMath.roundf(DistancePlayerNpc, 1);

            bool blocked = (VectorMath.roundf(data.uValue,1) <= DistancePlayerNpc) && data.uValue > 0;

            if (!data.doesIntersect || (data.doesIntersect && !blocked))
            {
                seg.npc_level.Npcs[npcIndex].NPC_BehaviourMode = 0; //Allow Act on Sight
                seg.npc_level.activateColor = Color.Green;
                                
                seg.npc_level.Npcs[npcIndex].allow_CanDoPathFind = false;
                seg.npc_level.Npcs[npcIndex].Lock_CanDoPathFind = false;                
            }
            else
            {
                seg.npc_level.Npcs[npcIndex].NPC_BehaviourMode = 1; //Allow pathfind
                seg.npc_level.activateColor = Color.Red;

                seg.npc_level.Npcs[npcIndex].allow_CanDoPathFind = true;
                if (seg.npc_level.Npcs[npcIndex].allow_CanDoPathFind && !seg.npc_level.Npcs[npcIndex].Lock_CanDoPathFind)
                {
                    NPC_Functions.Setup_NPC_Route_And_Pathfind(  seg, dat);
                    seg.npc_level.Npcs[npcIndex].Lock_CanDoPathFind = true;
                }
            }
        }

        public static void ActOnSight(int npcIndex,GameSegment seg, float speedMultiply, I_Targetable targetObj, GeneralGameData dat)
        {         
            //Voila.. do the act code
            if (seg.npc_level.Npcs[npcIndex].NPC_BehaviourMode == 0)
            {
                NPC_Functions.Setup_NPC_Route_And_Pathfind(  seg, dat);
                Vector3 npcPos = seg.npc_level.Npcs[npcIndex].Position;
                Vector3 toTargetDir = targetObj.get_Position_Targetable() - npcPos;
                float dis = VectorMath.get_length(toTargetDir);

                //Hes standing, doing notihng
                //AnimationController.setAnimationPlayback_Once(  seg.npc_level.Npcs[npcIndex].model.AnimationController,seg.npc_level.Npcs[npcIndex].model.AnimationController.currentAnimationName);

                if (dis > seg.npc_level.Npcs[npcIndex].GetCloseValue)
                {
                    Vector3 toTargetDir_normalized = Vector3.Normalize(toTargetDir);

                    seg.npc_level.Npcs[npcIndex].Position += toTargetDir_normalized * seg.npc_level.Npcs[npcIndex].WalkSpeed * new Vector3(1, 0, 1) * speedMultiply;
                    float newRot = VectorMath.getAtan2OfVector(toTargetDir_normalized);

                    seg.npc_level.Npcs[npcIndex].Rotation.Y = newRot;

                    //Hes walking on ActSightMode
                    AnimationController.setAnimationPlayback_Once(  seg.npc_level.Npcs[npcIndex].controller, "Stand_Animation_ArmCrossed_Breathing");
                    
                }
                else
                {
                    AnimationController.setAnimationPlayback_Once(seg.npc_level.Npcs[npcIndex].controller, "Stand_Animation_ArmCrossed_Breathing");
                    
                }
            }
        }
                
        //WORK ON THIS LATER ON!!!!! Walking not done / Also animation playback when walking is not set
        public static void WalkAlongPath(int npcIndex,  GameSegment seg, float speedMultiply, GeneralGameData dat)
        {
            //1 is allowance for pathfind, if not != 1, return
            if (seg.npc_level.Npcs[npcIndex].NPC_BehaviourMode != 1)
                return;
            
            if (seg.npc_level.Npcs[npcIndex].pathRoute.path_indices[0] == -1)
                return;

            int curIndex = seg.npc_level.Npcs[npcIndex].currentPathNode;
            //Console.WriteLine("Index {0}",curIndex);
            Vector3 targetPosition = seg.npc_level.Npc_Node_Collection_list[0].nodes[seg.npc_level.Npcs[npcIndex].pathRoute.path_indices[curIndex]].Position;

            Vector3 walkDirection = Vector3.Subtract(targetPosition, seg.npc_level.Npcs[npcIndex].Position);
            Vector3 walkDirNormalized = Vector3.Normalize(walkDirection);
            walkDirNormalized.Y *= 1;

            float len = VectorMath.get_length(walkDirection);
            float newRot = (float)Math.Acos((double)VectorMath.getScalarProductBeforeCosinus(new Vector3(walkDirNormalized.X, walkDirNormalized.Y, walkDirNormalized.Z), new Vector3(-1, 0, 0)));

            if (len > 5 && seg.npc_level.Npcs[npcIndex].followPathWalking || !seg.npc_level.Npcs[npcIndex].ReachedGoal)
            {
                newRot = VectorMath.getAtan2OfVector(walkDirNormalized);

                if (seg.npc_level.Npcs[npcIndex].ReachedGoal || !seg.npc_level.Npcs[npcIndex].followPathWalking)
                    return;

                if (seg.npc_level.Npcs[npcIndex].Rotation.Y < newRot + 0.1f)
                    seg.npc_level.Npcs[npcIndex].Rotation.Y += 0.025f;
                if (seg.npc_level.Npcs[npcIndex].Rotation.Y > newRot - 0.1f)
                    seg.npc_level.Npcs[npcIndex].Rotation.Y -= 0.035f;
                               
                Matrix transformRot = Matrix.CreateRotationY(newRot);
                
                seg.npc_level.Npcs[npcIndex].Position += walkDirNormalized * seg.npc_level.Npcs[npcIndex].WalkSpeed * speedMultiply*5;

                //AnimationController.setAnimationPlayback_Once(  seg.npc_level.Npcs[npcIndex].model.AnimationController,seg.npc_level.Npcs[npcIndex].model.AnimationController.currentAnimationName);
            }

            //Path choosing
            if (len < seg.npc_level.Npcs[npcIndex].PassNodeRadius)
            {
                //Call this to update, each time the a node is passed, we want to update the path
                NPC_Functions.Setup_NPC_Route_And_Pathfind(  seg, dat);

                int currNodeIndex = seg.npc_level.Npcs[npcIndex].currentPathNode;
                
                int lastIndex = seg.npc_level.Npcs[npcIndex].pathRoute.path_indices.Length - 1;

                if (seg.npc_level.Npcs[npcIndex].currentPathNode < seg.npc_level.Npcs[npcIndex].pathRoute.path_indices.Length - 1)
                    seg.npc_level.Npcs[npcIndex].currentPathNode++;

                //AnimationController.setAnimationPlayback_Once(  seg.npc_level.Npcs[npcIndex].model.AnimationController,seg.npc_level.Npcs[npcIndex].model.AnimationController.idleAnimName);

                if (currNodeIndex == lastIndex)
                {
                    seg.npc_level.Npcs[npcIndex].ReachedGoal = true;
                    seg.npc_level.Npcs[npcIndex].followPathWalking = false;
                }
                
            }
        }

        public static void HandleNpcCollision(int npcIndex,   GameSegment seg)
        {
            CollisionDetection.HandleGameEntityCollision(seg.npc_level.Npcs[npcIndex],   seg, 0.2f, 1f);
        }

        public static void updateNPC(int npcIndex,   GameSegment seg)
        {
            return;
            seg.npc_level.Npcs[npcIndex].updateNPCStuff();
            seg.npc_level.Npcs[npcIndex].updateConvexHull();
        }

        public static void tempAnimationController(  GameSegment seg)
        {
            KeyboardState k = Keyboard.GetState();

            if (k.IsKeyDown(Keys.G))
            {
                AnimationController.setAnimationPlayback_Once(
                          seg.npc_level.Npcs[0].controller,
                        "Stand_Animation_Pistol_Aiming_RightArm");
            }
        }
    }
}
