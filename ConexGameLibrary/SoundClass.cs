﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using ConexGameLibrary;

namespace ConexGameLibrary
{
    public class Sound_Container
    {
        public string SoundNames; // "//Sounds//Gun1.wav" for example    
        SoundEffect[] sound_effect;
        SoundEffectInstance[] Instances;
        AudioEmitter emitter;
        AudioListener Listener;

        //Set n-size of soundpath's
        public Sound_Container(GeneralGameData gamedat,string names,String[] soundPaths,float mastervolume)
        {            
            this.SoundNames = names;
            sound_effect = new SoundEffect[soundPaths.Length];
            Instances = new SoundEffectInstance[16];
            Listener = new AudioListener();
           
            for (int i = 0; i < soundPaths.Length; i++)
            {                
                sound_effect[i] = SoundEffect.FromStream(new FileStream(gamedat.GameDirectory + soundPaths[i], FileMode.Open));
                
            }

            Listener = new AudioListener();
            emitter = new AudioEmitter();
            SoundEffect.MasterVolume = 1;
        }

        //Only one sound can be played at a time, but each sound will have an unlimited amount of "overlaps" possible
        //a machine gun would have overlapping gunshot sounds starting each  2nd  to 10th frame in a 60fps game for example

        //lets guess how many times we would start a sound effect, before the first one ends... Lets go with 16 max, that will be enough
        //This only affects a single sound effect being played over.. not others
        public void PlaySound(int soundIndex,Vector3 campos, Vector3 camdir, Vector3 shotpos, float volume)
        {

            for(int i = 0; i < 16; i++)
            {
                Instances[i] = sound_effect[soundIndex].CreateInstance();
            }

            //repeat until we find an unused listener            
            for (int i = 0; i < 16; i++)
            {
                if (Instances[i].State == SoundState.Stopped)
                {                    
                    Instances[i].Volume = volume;
                    Listener.Position = campos;
                    Listener.Forward = camdir;
                    emitter.Position = shotpos;
                    Instances[i].Apply3D(Listener, emitter);
                    Instances[i].Play();
                    break;
                }                
            }            
        }
    }    
}

