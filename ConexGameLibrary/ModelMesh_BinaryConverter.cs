﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework.Graphics;
using System.Text;
using System.IO;


namespace ConexGameLibrary
{
    public struct C3D_Header
    {
        public ushort identifier; // = 0xCCCC;
        public short offset; //header 64 is standard
        public byte day;
        public byte month;
        public short year;

        public byte hasNormals; // 00 or FF
        public byte hasUV;      // 00 or FF
        public byte hasVertexWheightsAndDeformers; // 00 or FF
        public Int32 VertexCount;

        public Int32 PointByteSize; //Per Point = 12
        public Int32 UvByteSize; //Per UV = 8
        public Int32 NormalByteCount; // 12byte
        public Int32 VertexWheightSize; // 16 byte
        public Int32 DeformersCount; //       
        public Int32 deformerNameLength; //prolly 64byte
    }

    public struct C3D_Data
    {
        public float[] points; //3 x VertexCount
        public float[] uvs;    //2 x     "
        public float[] normals;//3 x vertexCount
        public float[] vertexWeight; //4 per bone = 4x vertexCount
        // essentially, all are the same

        public float[] DeformerMatrices; //each 16 floats belong to one
        public char[][] DeformerNames;  //Maps directly to the deformers
        public char[][] ParentDeformerNames;        
    }

    public class ModelMesh_C3D_IO
    {
        int[] arr;
        byte[] data;
        FileStream stream;
        BinaryWriter writer;
        //Write the animatedModel mesh to C3D format
        public static int Write_C3D_File(String path, AnimatedModelMesh mesh)
        {

            C3D_Header c3d_header = new C3D_Header();
            C3D_Data c3d_data = new C3D_Data();

            c3d_header.identifier = 0xCCCC;
            c3d_header.offset = 64;
            c3d_header.day = (byte)DateTime.Now.Day;
            c3d_header.month = (byte)DateTime.Now.Month;
            c3d_header.year = (short)DateTime.Now.Year;

            c3d_header.hasNormals = 0xFF; // 00 or FF
            c3d_header.hasUV = 0xFF; //       // 00 or FF
            c3d_header.hasVertexWheightsAndDeformers = 0xFF; // 
            c3d_header.VertexCount = mesh.Vertices.Length;

            c3d_header.PointByteSize = 12; //Per Point = 12
            c3d_header.UvByteSize = 8; //Per UV = 8
            c3d_header.NormalByteCount = 12; // 12byte
            c3d_header.VertexWheightSize = 16; // 16 byte
            c3d_header.DeformersCount = mesh.Skeleton.Deformers.Count; //       
            c3d_header.deformerNameLength = 64; //prolly 64byte
            

            c3d_data.points = new float[c3d_header.PointByteSize * c3d_header.VertexCount];
            c3d_data.uvs = new float[c3d_header.UvByteSize * c3d_header.VertexCount];
            c3d_data.normals = new float[c3d_header.NormalByteCount * c3d_header.VertexCount];
            c3d_data.vertexWeight = new float[c3d_header.VertexWheightSize * c3d_header.VertexCount];
            c3d_data.DeformerMatrices = new float[c3d_header.DeformersCount * 64]; //16floats * 4byte * count
            c3d_data.DeformerNames = new char[c3d_header.DeformersCount][];  
            c3d_data.ParentDeformerNames = new char[c3d_header.DeformersCount][];        


            

            byte[] data;
            float[] farr = new float[100];
            for (int i = 0; i < farr.Length; i++)
            {
                farr[i] = (i * i);
            }

            //FileStream stream = new FileStream(path, FileMode.CreateNew);
            //BinaryWriter writer;
            //writer = new BinaryWriter(stream);

            data = new byte[farr.Length * 4];
            for (int i_dat = 0, i_farr = 0; i_farr < farr.Length; i_dat+= 4, i_farr++)
            {
                byte[] currentFloat = BitConverter.GetBytes(farr[i_farr]);

                data[i_dat + 0] = currentFloat[0];
                data[i_dat + 1] = currentFloat[1];
                data[i_dat + 2] = currentFloat[2];
                data[i_dat + 3] = currentFloat[3];                
            }

            //THIS step is important mmkay
            byte[] data2 = data;

            float[] farr2 = new float[100];
            
            for (int i_dat = 0, i_farr = 0; i_farr < farr.Length; i_dat += 4, i_farr++)
            {
                byte[] currentFloat; 
                currentFloat = new byte[] { data2[i_dat + 0], data2[i_dat + 1], data2[i_dat + 2], data2[i_dat + 3] };

                farr2[i_farr] = BitConverter.ToSingle(currentFloat, 0);
            }                      

            return 1;
        }
        
    }
}
