using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace ConexGameLibrary
{
    public class Buttons : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch spritebatch;
        Texture2D ButtonTexture;
        String gameTexturePath, textureName;
        Rectangle rectangle;
        MouseState mouse;       

        public Buttons(Game game,
            ref SpriteBatch spritebatch,
            String gameTexturePath,
            String textureName,
            Rectangle rectangle,
            ref MouseState mouse)
            : base(game)
        {
            this.spritebatch = spritebatch;
            this.textureName = textureName;
            this.rectangle = rectangle;
            this.gameTexturePath = gameTexturePath;
            this.mouse = mouse;
        }

        public bool buttonPressed()
        {
            if (rectangle.Contains(mouse.X, mouse.Y))
            {
                return true;
            }
            else
                return false;
            
        }

        public override void Initialize()
        {
            base.Initialize();
        }


        public override void Draw(GameTime gameTime)
        {
           
            base.Draw(gameTime);
        }
    }
}
