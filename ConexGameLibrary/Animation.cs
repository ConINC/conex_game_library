﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConexGameLibrary;
using Microsoft.Xna.Framework;
using System.IO;
using Microsoft.Xna.Framework.Input;
using System.Globalization;

namespace ConexGameLibrary
{
    //Use this type 
    public struct Ret_MatrixFromAnimation_Type
    {
        public string deformerName;
        public Matrix matrix;
    }
    
    public static class Animation_Functions
    {        
        //Interpolate the values;   duration[0] is begin  duration[1] is end
        /// <summary>
        /// Interpolates values over a curve
        /// </summary>
        /// <param name="keys">the Keys from FBX</param>
        /// <param name="duration">begin and end frame</param>
        /// <param name="interpolateMethod"> Linear, Cosine, Spline</param>
        /// <returns></returns>
        public static float[] interpolateValues(List<string> keys, long[] duration, int givenCount, int interpolation_mode, float speed_mul)
        {
            if (speed_mul == 0.1f)
                Console.WriteLine("");
            float[] altFloatList;
            
            //In the case of no keys being passed, use a list of 1's
            if (keys == null)
            {
                altFloatList = new float[givenCount];
                for(int i = 0; i < givenCount; i++)
                    altFloatList[i] = 1;
                return altFloatList;
            }

            long FrameLength = ((long)(double)(duration[1] * (double)speed_mul)) - (duration[0]);
            long perFrameTime = 1924423250;
            long frameCount = (long)( (double)duration[1] * ((double)speed_mul) / (double)perFrameTime);
            float[] valuesPerKey;
            UInt32[] framesPerKey;
            
            //Count the keys
            int CountKeys = 0;            

            for (int i = 0; i < keys.Count; i++)
            {
                if (keys[i].Contains('n')) CountKeys++; 
            }

            //We ignore the first key, since the real first key starts at an offset of one frame in timestep
            valuesPerKey = new float[CountKeys];
            framesPerKey = new UInt32[CountKeys];
                        

            int countKeyOffset = keys.Count / CountKeys;

            //Ignore the first key, since its always 0, the real first one is at 1924423250
            for (int i = 0, j = 0; i < keys.Count; i += countKeyOffset, j++)
            {
                valuesPerKey[j] = Convert.ToSingle(keys[i+1], CultureInfo.InvariantCulture.NumberFormat);

                //Divide by 1924423250, since one frame is 1924423250 long in FBX
                uint val = (UInt32)(Convert.ToInt64(keys[i], CultureInfo.InvariantCulture.NumberFormat) / 1924423250);

                framesPerKey[j] = (uint)(  ((double)j == 0) ? 0 : val * (double)speed_mul);
             }

            //Create required return lists
            float[] valueList = new float[frameCount];
            UInt32[] frameList = new UInt32[frameCount];

            //Now we pass the Segments to a specific Interpolation function as
            //p1 + u * (p2- p1)
            for (int i = 0; i < valuesPerKey.Length - 1; i++ )
            {
                uint p1 = framesPerKey[i];
                uint p2 = framesPerKey[i + 1];

                //Now loop throught the key segment from  p1 to p2
                for (uint j = p1; j < p2 && p2 <= frameCount; j++)
                {
                    // Scale is always the factor of (p2-p1) + p1  in each segment, this will be used to interpolate now
                    float scale = (float)(j - p1) / (float)(p2 - p1);

                    float val1 = valuesPerKey[i];
                    float val2 = valuesPerKey[i + 1];

                    if(interpolation_mode == 0)
                        valueList[j] = Interpolation.Linear_Interpolate((float)scale, val1, val2);
                    else if (interpolation_mode ==1)
                        valueList[j] = Interpolation.Cosine_Interpolate((float)scale, val1, val2);             
                }
            }                        
            return valueList;
        }
        
        public static AnimatedModelMesh LoadAnimatedMeshFromFBX(GeneralGameData gamedat, String relativePath)
        {
            AnimatedModelMesh animatedMesh = new AnimatedModelMesh();
            animatedMesh.Bindposes = new List<BindPose>();

            Vector3[] Points;
            int[] vertexIndex = new int[0];

            Vector3[] Normals;

            Vector2[] UVCoords;
            int[] uvIndex;


            Vector3 pos_origin = Vector3.Zero;
             
            FBX_File_Struct file = FBX_Loader.Create_FBX_File_Struct(gamedat.GameDirectory + "//" + relativePath, 0);

            #region Get vertexIndices and Points

            FBX_ModelMesh Fbx_modelmesh = new FBX_ModelMesh();
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                if (file.FBX_Objects[i] is FBX_ModelMesh)
                {
                    Fbx_modelmesh = (FBX_ModelMesh)file.FBX_Objects[i];
                    for (int zz = 0; zz < Fbx_modelmesh.properties.Count; zz++)
                    {
                        if (Fbx_modelmesh.properties[zz].Description.Contains("Lcl Translation"))
                        {
                            pos_origin = new Vector3((float)Fbx_modelmesh.properties[zz].value[0], (float)Fbx_modelmesh.properties[zz].value[1], (float)Fbx_modelmesh.properties[zz].value[2]);

                        }
                    }
                }
            }

            animatedMesh.VertexIndex = new int[Fbx_modelmesh.VertexIndices.Count];
            Points = new Vector3[Fbx_modelmesh.Vertices.Count / 3];

            int d = 0; int v = 0;
            for (; d < Fbx_modelmesh.Vertices.Count; )
            {
                Points[v] = new Vector3(Fbx_modelmesh.Vertices[d], Fbx_modelmesh.Vertices[d + 1], Fbx_modelmesh.Vertices[d + 2]);
                Points[v] += pos_origin;

                v++;
                d += 3;
            }

            for (int i = 0; i < Fbx_modelmesh.VertexIndices.Count; i++)
            {
                animatedMesh.VertexIndex[i] = Fbx_modelmesh.VertexIndices[i];
                vertexIndex = animatedMesh.VertexIndex;
            }
            #endregion

            #region Get Normal Vectors

            FBX_LayerElementNormal normalLayer;
            normalLayer = new FBX_LayerElementNormal();
            for (int i = 0; i < Fbx_modelmesh.LayerContainer.Layers.Count; i++)
            {
                if (Fbx_modelmesh.LayerContainer.Layers[i] is FBX_LayerElementNormal)
                {
                    normalLayer = (FBX_LayerElementNormal)Fbx_modelmesh.LayerContainer.Layers[i];
                }
            }

            Normals = new Vector3[normalLayer.Normals.Count / 3];
            d = 0; v = 0;
            for (; d < normalLayer.Normals.Count; )
            {
                Normals[v] = new Vector3(normalLayer.Normals[d], normalLayer.Normals[d + 1], normalLayer.Normals[d + 2]);

                v++;
                d += 3;
            }


            #endregion

            #region Get UV coords

            FBX_LayerElementUV uvLayer = new FBX_LayerElementUV();
            for (int i = 0; i < Fbx_modelmesh.LayerContainer.Layers.Count; i++)
            {
                if (Fbx_modelmesh.LayerContainer.Layers[i] is FBX_LayerElementUV)
                {
                    uvLayer = (FBX_LayerElementUV)Fbx_modelmesh.LayerContainer.Layers[i];
                }
            }


            uvIndex = new int[uvLayer.UVIndex.Count];
            v = 0;
            for (; v < uvLayer.UVIndex.Count; )
            {
                uvIndex[v] = uvLayer.UVIndex[v];
                v++;
            }

            UVCoords = new Vector2[uvLayer.UV.Count / 2];
            int u = 0;
            for (d = 0; d < uvLayer.UV.Count; )
            {
                UVCoords[u] = new Vector2(uvLayer.UV[d], uvLayer.UV[d + 1]);
                d += 2;
                u++;
            }
            #endregion

            animatedMesh.Skeleton = new DeformerSkeleton();
            animatedMesh.Skeleton.Deformers = new List<Deformer>();

            #region Load DeformerClusters
            List<FXB_DeformerCluster> FBX_DeformerClusters = new List<FXB_DeformerCluster>();
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                if (file.FBX_Objects[i] is FXB_DeformerCluster)
                {
                    FBX_DeformerClusters.Add((FXB_DeformerCluster)file.FBX_Objects[i]);
                }
            }
            #endregion

            //Get Deformer Clusters and Basic Pose
            Deformer deformer;
            FXB_DeformerCluster cluster = new FXB_DeformerCluster();
            BindPose pose;
            List<FBX_ModelLimbNode> limbnodes = new List<FBX_ModelLimbNode>();

            #region FBX_Objects_Getter no.1
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                try
                {
                    if (file.FBX_Objects[i] is FXB_DeformerCluster)
                    {
                        deformer = new Deformer();
                        cluster = (FXB_DeformerCluster)file.FBX_Objects[i];
                        deformer.VertexIndices = new int[cluster.Indexes.Count];
                        deformer.VertexWeights = new float[cluster.Weights.Count];

                        for (int counter = 0; counter < cluster.Indexes.Count; counter++)
                        {
                            deformer.VertexIndices[counter] = cluster.Indexes[counter];
                        }

                        for (int counter = 0; counter < cluster.Weights.Count; counter++)
                        {
                            deformer.VertexWeights[counter] = cluster.Weights[counter];
                        }

                        deformer.DeformerName = cluster.Name.Substring(8);


                        deformer.TransformLink = VectorMath.MatrixRound(new Matrix(
                              cluster.TransformLink[0], cluster.TransformLink[1], cluster.TransformLink[2], cluster.TransformLink[3]
                            , cluster.TransformLink[4], cluster.TransformLink[5], cluster.TransformLink[6], cluster.TransformLink[7]
                            , cluster.TransformLink[8], cluster.TransformLink[9], cluster.TransformLink[10], cluster.TransformLink[11]
                            , cluster.TransformLink[12], cluster.TransformLink[13], cluster.TransformLink[14], cluster.TransformLink[15]
                            ), 5);
                        /*
                        deformer.Transform = VectorMath.MatrixRound(new Matrix(
                              cluster.Transform[0], cluster.Transform[1], cluster.Transform[2], cluster.Transform[3]
                            , cluster.Transform[4], cluster.Transform[5], cluster.Transform[6], cluster.Transform[7]
                            , cluster.Transform[8], cluster.Transform[9], cluster.Transform[10], cluster.Transform[11]
                            , cluster.Transform[12], cluster.Transform[13], cluster.Transform[14], cluster.Transform[15]
                            ),5);
                       */

                        animatedMesh.Skeleton.Deformers.Add(deformer);
                    }

                    if (file.FBX_Objects[i] is FBX_PoseBindPose)
                    {
                        FBX_PoseBindPose poseFBX = (FBX_PoseBindPose)file.FBX_Objects[i];
                        {
                            for (int hh = 0; hh < poseFBX.PoseNodes.Count; hh++)
                            {
                                pose = new BindPose();
                                pose.JointName = poseFBX.PoseNodes[hh].Node.Substring(7);
                                pose.Matrix = new Matrix(
                                    poseFBX.PoseNodes[hh].matrix[0], poseFBX.PoseNodes[hh].matrix[1], poseFBX.PoseNodes[hh].matrix[2], poseFBX.PoseNodes[hh].matrix[3],
                                    poseFBX.PoseNodes[hh].matrix[4], poseFBX.PoseNodes[hh].matrix[5], poseFBX.PoseNodes[hh].matrix[6], poseFBX.PoseNodes[hh].matrix[7],
                                    poseFBX.PoseNodes[hh].matrix[8], poseFBX.PoseNodes[hh].matrix[9], poseFBX.PoseNodes[hh].matrix[10], poseFBX.PoseNodes[hh].matrix[11],
                                    poseFBX.PoseNodes[hh].matrix[12], poseFBX.PoseNodes[hh].matrix[13], poseFBX.PoseNodes[hh].matrix[14], poseFBX.PoseNodes[hh].matrix[15]);

                                animatedMesh.Bindposes.Add(pose);
                            }
                        }
                    }
                }
                catch
                {

                }

            }
            #endregion

            //left this out.
            #region do stuff
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                if (file.FBX_Objects[i] is FBX_ModelLimbNode)
                {
                    FBX_ModelLimbNode LimbNode = (FBX_ModelLimbNode)file.FBX_Objects[i];
                    {
                        for (int g = 0; g < LimbNode.properties.Count; g++)
                        {
                            for (int f = 0; f < animatedMesh.Skeleton.Deformers.Count; f++)
                            {
                                if (animatedMesh.Skeleton.Deformers[f].DeformerName == LimbNode.Name)
                                {
                                    /*
                                    if (LimbNode.properties[g].Description.Contains("Lcl Translation"))
                                    {
                                        animatedMesh.Skeleton.Deformers[f].LCL_Translation = new Vector3((float)LimbNode.properties[g].value[0], (float)LimbNode.properties[g].value[1], (float)LimbNode.properties[g].value[2]);
                                    }
                                    if (LimbNode.properties[g].Description.Contains("Lcl Rotation"))
                                    {
                                        animatedMesh.Skeleton.Deformers[f].LCL_Rotation = new Vector3((float)LimbNode.properties[g].value[0] / 57.295f, (float)LimbNode.properties[g].value[1] / 57.295f, (float)LimbNode.properties[g].value[2] / 57.295f);
                                    }
                                    if (LimbNode.properties[g].Description.Contains("Lcl Scaling"))
                                    {
                                        animatedMesh.Skeleton.Deformers[f].LCL_Scaling = new Vector3((float)LimbNode.properties[g].value[0], (float)LimbNode.properties[g].value[1], (float)LimbNode.properties[g].value[2]);
                                    }
                                    if (LimbNode.properties[g].Description.Contains("PreRotation"))
                                    {
                                        animatedMesh.Skeleton.Deformers[f].PreRotation = new Vector3((float)LimbNode.properties[g].value[0] / 57.295f, (float)LimbNode.properties[g].value[1] / 57.295f, (float)LimbNode.properties[g].value[2] / 57.295f);
                                    }
                                    */
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            //Get Textures
            FBX_Texture fbx_tex;
            #region textures
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                if (file.FBX_Objects[i] is FBX_Texture)
                {
                    fbx_tex = new FBX_Texture();
                    fbx_tex = (FBX_Texture)file.FBX_Objects[i];

                    animatedMesh.textureNames.Add(fbx_tex.FileName);
                    animatedMesh.relTextureNames.Add(fbx_tex.RelativeFileName);

                }
            }
            #endregion

            //Do indexing
            #region Just Index throught the Joints to give them an Index to identify

            for (int j = 0; j < animatedMesh.Skeleton.Deformers.Count; j++)
            {
                animatedMesh.Skeleton.Deformers[j].Deformer_Index = j;
                animatedMesh.Skeleton.Deformers[j].ParentDeformer_Index = -1;
            }

            #endregion

            //Also not use this for now
            #region GetD Child joint names for deformers and bindposes
            for (int i = 0; i < file.FBX_Connection.ConnectionProperties.Count; i++)
            {
                for (int j = 0; j < animatedMesh.Bindposes.Count; j++)
                {
                    if (String.Equals(animatedMesh.Bindposes[j].JointName, file.FBX_Connection.ConnectionProperties[i].ParentObjectName))
                    {
                        animatedMesh.Bindposes[j].JointChildName = file.FBX_Connection.ConnectionProperties[i].ChildObjectName;
                    }
                }
            }
            #endregion

            #region Clear connection stuff
            for (int i = 0; i < file.FBX_Connection.ConnectionProperties.Count; i++)
            {
                if (file.FBX_Connection.ConnectionProperties[i].ParentObjectName.Contains("Cluster"))
                {
                    file.FBX_Connection.ConnectionProperties.RemoveAt(i);
                    i--;
                }
                else if (file.FBX_Connection.ConnectionProperties[i].ChildObjectName.Contains("Cluster"))
                {
                    file.FBX_Connection.ConnectionProperties.RemoveAt(i);
                    i--;
                }


            }
            #endregion

            #region Set Hierarchy by Name
            for (int i = 0; i < file.FBX_Connection.ConnectionProperties.Count; i++)
            {
                for (int j = 0; j < animatedMesh.Skeleton.Deformers.Count; j++)
                {
                    if (String.Equals(animatedMesh.Skeleton.Deformers[j].DeformerName,
                        file.FBX_Connection.ConnectionProperties[i].ChildObjectName))
                    {
                        animatedMesh.Skeleton.Deformers[j].ParentDeformerName = file.FBX_Connection.ConnectionProperties[i].ParentObjectName;
                    }
                }
            }

            #endregion

            #region Assign Names/Indices of Deformers to their Parents and Childs, to identify hierarchy
            for (int k = 0; k < animatedMesh.Skeleton.Deformers.Count; k++)
            {
                for (int z = 0; z < animatedMesh.Skeleton.Deformers.Count; z++)
                {
                    animatedMesh.Skeleton.Deformers[z].Depth = -1;
                    if (String.Equals(animatedMesh.Skeleton.Deformers[z].DeformerName, animatedMesh.Skeleton.Deformers[k].ParentDeformerName))
                    {
                        animatedMesh.Skeleton.Deformers[k].ParentDeformerName = animatedMesh.Skeleton.Deformers[z].DeformerName;
                        animatedMesh.Skeleton.Deformers[k].ParentDeformer_Index = animatedMesh.Skeleton.Deformers[z].Deformer_Index;
                    }
                }
            }
            #endregion


            #region Important Step: Give each Deformer a Depth To transform from child up to parent. Just Wtf

            for (int i = 0; i < animatedMesh.Skeleton.Deformers.Count; i++)
            {
                int storedIndex = i;
                int depth = 0;

                do
                {
                    if (depth >= animatedMesh.Skeleton.Deformers[storedIndex].Depth)
                    {
                        animatedMesh.Skeleton.Deformers[storedIndex].Depth = depth;
                    }

                    storedIndex = animatedMesh.Skeleton.Deformers[storedIndex].ParentDeformer_Index;

                    if (storedIndex < 0)
                        break;

                    depth++;
                } while (storedIndex >= 0);
            }
            #endregion

            #region Now reverse the depth, so the biggest number is the deepest child

            int maxdepth = 0;

            //Get greatest value first
            for (int i = 0; i < animatedMesh.Skeleton.Deformers.Count; i++)
            {
                if (animatedMesh.Skeleton.Deformers[i].Depth > maxdepth)
                    maxdepth = animatedMesh.Skeleton.Deformers[i].Depth;
            }

            //Now reverse them
            for (int i = 0; i < animatedMesh.Skeleton.Deformers.Count; i++)
            {
                animatedMesh.Skeleton.Deformers[i].Depth = maxdepth - animatedMesh.Skeleton.Deformers[i].Depth;
            }

            #endregion

            #region Now List each Joint Info

            Console.WriteLine(
                "\n\n");
            for (int i = 0; i < animatedMesh.Skeleton.Deformers.Count; i++)
            {
                Console.WriteLine("Joint {0} has Depth {1} ", i, animatedMesh.Skeleton.Deformers[i].Depth);
            }

            #endregion

            #region Do Mesh data here

            animatedMesh.PointList = new Vector3[Points.Length];

            //Leave this empty, CPu skinned mesh will use different meshformat or so
            animatedMesh.PointList_Dynamic = new Vector3[Points.Length];

            for (int i = 0; i < Points.Length; i++)
            {
                animatedMesh.PointList_Dynamic[i] = Points[i];
                animatedMesh.PointList[i] = Points[i];
            }

            animatedMesh.VertexIndex = vertexIndex;
            animatedMesh.Normals = Normals;
            animatedMesh.UvIndex = uvIndex;
            animatedMesh.UvCoords = UVCoords;
            animatedMesh.Vertices = new CustomVertex[uvIndex.Length];

            #endregion

            #region create vertex LIst;
            for (int i = 0; i < animatedMesh.VertexIndex.Length; i++)
            {
                animatedMesh.Vertices[i] = new CustomVertex(animatedMesh.PointList[animatedMesh.VertexIndex[i]], animatedMesh.UvCoords[animatedMesh.UvIndex[i]], animatedMesh.Normals[animatedMesh.VertexIndex[i]], Vector3.Zero, Vector3.Zero);
            }

            for (int i = 0; i < animatedMesh.VertexIndex.Length; i++)
            {
                animatedMesh.Vertices[i] = new CustomVertex(animatedMesh.PointList[animatedMesh.VertexIndex[i]], animatedMesh.UvCoords[animatedMesh.UvIndex[i]], animatedMesh.Normals[animatedMesh.VertexIndex[i]], Vector3.Zero, Vector3.Zero);
            }
            #endregion

            #region Initialise Skeleton data
            animatedMesh.Skeleton.Bone_Transforms = new Matrix[animatedMesh.Skeleton.Deformers.Count];
            //animatedMesh.BoneIndices = new Vector4[animatedMesh.PointList.Length];
            //animatedMesh.BoneWeights = new Vector4[animatedMesh.PointList.Length];
            animatedMesh.Skeleton.InverseBindPose = new Matrix[animatedMesh.Skeleton.Deformers.Count];
            animatedMesh.Skeleton.maxDepth = maxdepth;            
            #endregion

            for (int i = 0; i < animatedMesh.Skeleton.Deformers.Count; i++)
            {
                animatedMesh.Skeleton.InverseBindPose[i] = Matrix.Invert(animatedMesh.Skeleton.Deformers[i].TransformLink);
            }

            return animatedMesh;
        }
        
        public static List<Frame> LoadAnimationFromFBX(GeneralGameData gamedat, String RelativeAnimationPath, int interpol_mode, float speed)
        {
            Console.WriteLine("Loading Animation from FBX");
            FBX_File_Struct file = FBX_Loader.Create_FBX_File_Struct(gamedat.GameDirectory + RelativeAnimationPath, 0);

            #region getframecount, since we rely on it, the first translation key, which is x, must be a valid list
            int frameCount = interpolateValues(file.FBX_Takes.Takes[0].JointChannels[0].TransformChannels[0].TransformMainChannel[0].SubTransformationChannel[0].Keys, file.FBX_Takes.Takes[0].LocalTime, 0, interpol_mode, speed).Length;
            if (frameCount < 1)
            {
                throw new InvalidDataException("The First Animation keys : Channel [x] has too few frames");

            }
            #endregion
            Frame[] Frames = new Frame[frameCount];

            #region Create Jointdata
            for (int i = 0; i < Frames.Length; i++)
            {
                Frames[i] = new Frame();
                Frames[i].JointData = new List<JointData>();
                for (int j = 0; j < file.FBX_Takes.Takes[0].JointChannels.Count; j++)
                {
                    Frames[i].JointData.Add(new JointData());
                }
            }
            #endregion

            SamplerJoint[] sampleJoints = new SamplerJoint[file.FBX_Takes.Takes[0].JointChannels.Count];
            for (int i = 0; i < sampleJoints.Length; i++)
            {
                sampleJoints[i] = new SamplerJoint(frameCount);
                sampleJoints[i].name = file.FBX_Takes.Takes[0].JointChannels[i].JointName;

                sampleJoints[i].transx = interpolateValues(file.FBX_Takes.Takes[0].JointChannels[i].TransformChannels[0].TransformMainChannel[0].SubTransformationChannel[0].Keys, file.FBX_Takes.Takes[0].LocalTime, frameCount, interpol_mode, speed);
                sampleJoints[i].transy = interpolateValues(file.FBX_Takes.Takes[0].JointChannels[i].TransformChannels[0].TransformMainChannel[0].SubTransformationChannel[1].Keys, file.FBX_Takes.Takes[0].LocalTime, frameCount, interpol_mode, speed);
                sampleJoints[i].transz = interpolateValues(file.FBX_Takes.Takes[0].JointChannels[i].TransformChannels[0].TransformMainChannel[0].SubTransformationChannel[2].Keys, file.FBX_Takes.Takes[0].LocalTime, frameCount, interpol_mode, speed);

                sampleJoints[i].rotx = interpolateValues(file.FBX_Takes.Takes[0].JointChannels[i].TransformChannels[0].TransformMainChannel[1].SubTransformationChannel[0].Keys, file.FBX_Takes.Takes[0].LocalTime, frameCount, interpol_mode, speed);
                sampleJoints[i].roty = interpolateValues(file.FBX_Takes.Takes[0].JointChannels[i].TransformChannels[0].TransformMainChannel[1].SubTransformationChannel[1].Keys, file.FBX_Takes.Takes[0].LocalTime, frameCount, interpol_mode, speed);
                sampleJoints[i].rotz = interpolateValues(file.FBX_Takes.Takes[0].JointChannels[i].TransformChannels[0].TransformMainChannel[1].SubTransformationChannel[2].Keys, file.FBX_Takes.Takes[0].LocalTime, frameCount, interpol_mode, speed);

                sampleJoints[i].scalex = interpolateValues(file.FBX_Takes.Takes[0].JointChannels[i].TransformChannels[0].TransformMainChannel[2].SubTransformationChannel[0].Keys, file.FBX_Takes.Takes[0].LocalTime, frameCount, interpol_mode, speed);
                sampleJoints[i].scaley = interpolateValues(file.FBX_Takes.Takes[0].JointChannels[i].TransformChannels[0].TransformMainChannel[2].SubTransformationChannel[1].Keys, file.FBX_Takes.Takes[0].LocalTime, frameCount, interpol_mode, speed);
                sampleJoints[i].scalez = interpolateValues(file.FBX_Takes.Takes[0].JointChannels[i].TransformChannels[0].TransformMainChannel[2].SubTransformationChannel[2].Keys, file.FBX_Takes.Takes[0].LocalTime, frameCount, interpol_mode, speed);
            }
            

            //This contains the JointData

            for (int k = 0; k < frameCount; k++)
            {
                for (int i = 0; i < file.FBX_Takes.Takes[0].JointChannels.Count; i++)
                {
                    Frames[k].JointData[i].JointName = sampleJoints[i].name;

                    Frames[k].JointData[i].Translation.X = sampleJoints[i].transx[k];
                    Frames[k].JointData[i].Translation.Y = sampleJoints[i].transy[k];
                    Frames[k].JointData[i].Translation.Z = sampleJoints[i].transz[k];

                    Frames[k].JointData[i].Rotation.X = MathHelper.ToRadians(sampleJoints[i].rotx[k]);
                    Frames[k].JointData[i].Rotation.Y = MathHelper.ToRadians(sampleJoints[i].roty[k]);
                    Frames[k].JointData[i].Rotation.Z = MathHelper.ToRadians(sampleJoints[i].rotz[k]);

                    Frames[k].JointData[i].Scale.X = sampleJoints[i].scalex[k];
                    Frames[k].JointData[i].Scale.Y = sampleJoints[i].scaley[k];
                    Frames[k].JointData[i].Scale.Z = sampleJoints[i].scalez[k];
                }
            }

            List<Frame> frames_list = new List<Frame>();
            for (int i = 0; i < frameCount; i++)
            {
                frames_list.Add(Frames[i]);
            }

            return frames_list;
        }

        public static AnimationContainer LoadAnimationsFromFile(GeneralGameData gamedat, string AnimationTextFile, int interpol_method)
        {
            AnimationContainer container = new AnimationContainer();

            List<Frame> AnimFrames;
            String[] Lines = File.ReadAllLines(gamedat.GameDirectory + "//" + AnimationTextFile, ASCIIEncoding.ASCII);
            for (int i = 0; i < Lines.Length; i++)
            {
                if (Lines[i].StartsWith("\"\""))
                    continue;

                if (Lines[i].StartsWith("CollectionName: "))
                {
                    container.ContainerName = Lines[i].Substring(16);
                    continue;
                }
                if (Lines[i].StartsWith("AnimationName: ") && Lines[i+1].StartsWith("PlaybackSpeed: "))
                {
                    float frameMultiplier = Convert.ToSingle(Lines[i + 1].Substring(15), CultureInfo.InvariantCulture.NumberFormat);

                    AnimFrames = LoadAnimationFromFBX(gamedat, "//" + Lines[i + 2], interpol_method, frameMultiplier);
                    Animation animation = new Animation(Lines[i].Substring(15), AnimFrames);
                    animation.FrameCount = AnimFrames.Count;
                    
                    container.AnimationCollection.Add(animation);
                    i+= 2;
                }
                else if(Lines[i].StartsWith("AnimationName: "))
                {
                    AnimFrames = LoadAnimationFromFBX(gamedat, "//" + Lines[i + 1], interpol_method, 1);
                    Animation animation = new Animation(Lines[i].Substring(15), AnimFrames);
                    animation.FrameCount = AnimFrames.Count;
                    container.AnimationCollection.Add(animation);
                    i++;
                }
            }

            return container;
        }
                
        //Use this for constant playback
        public static List<Ret_MatrixFromAnimation_Type> PlayAnimation_AnimColl(
            AnimationContainer animCont,
            String animationName,
            AnimatedModelMesh AnimatedMesh,
            AnimationController controller,
            Matrix preMatrix,
            String jointName1, Matrix addMatrix1,
            String jointName2, Matrix addMatrix2,
            String jointName3, Matrix addMatrix3,
            List<String> RequestedDeformers)
        {
            List<Ret_MatrixFromAnimation_Type> types;
            types = new List<Ret_MatrixFromAnimation_Type>();

            if (animationName == "Noone")
                return types;

            if (RequestedDeformers != null && RequestedDeformers.Count > 0)
            {
                types = new List<Ret_MatrixFromAnimation_Type>();
            }

            //Animation Count, like the "Take" in the Animation Collection
            if (animCont != null)
            {
                for (int animCount = 0; animCount < animCont.AnimationCollection.Count; animCount++)
                {
                    if (String.Equals(animCont.AnimationCollection[animCount].Name, animationName))
                    {
                        if (controller.AnimationTimer < 1)
                            break;

                        controller.AnimationLength = animCont.AnimationCollection[animCount].AnimationFrames.Count;

                        if (!(controller.AnimationTimer < controller.AnimationLength))
                        {
                            controller.AnimationTimer = 1;
                            controller.PlayAnimationOnce = false;
                        }

                        if (controller.AnimationTimer < animCont.AnimationCollection[animCount].AnimationFrames.Count)
                        {
                            #region load data from animation
                            for (int i = 0; i < animCont.AnimationCollection[animCount].AnimationFrames[controller.AnimationTimer].JointData.Count; i++)
                            {
                                for (int j = 0; j < AnimatedMesh.Skeleton.Deformers.Count; j++)
                                {
                                    if (String.Equals(AnimatedMesh.Skeleton.Deformers[j].DeformerName,
                                        animCont.AnimationCollection[animCount].AnimationFrames[controller.AnimationTimer].JointData[i].JointName))
                                    {
                                        Matrix currentMatrixAddition = Matrix.Identity;
                                        if (string.Equals(AnimatedMesh.Skeleton.Deformers[j].DeformerName, jointName1))
                                        {
                                            currentMatrixAddition = addMatrix1;
                                        }
                                        if (string.Equals(AnimatedMesh.Skeleton.Deformers[j].DeformerName, jointName2))
                                        {
                                            currentMatrixAddition = addMatrix2;
                                        }
                                        if (string.Equals(AnimatedMesh.Skeleton.Deformers[j].DeformerName, jointName3))
                                        {
                                            currentMatrixAddition = addMatrix3;
                                        }

                                        Vector3 rot; Vector3 trans; Vector3 scale;
                                        rot = animCont.AnimationCollection[animCount].AnimationFrames[controller.AnimationTimer].JointData[i].Rotation;
                                        trans = animCont.AnimationCollection[animCount].AnimationFrames[controller.AnimationTimer].JointData[i].Translation;
                                        scale = animCont.AnimationCollection[animCount].AnimationFrames[controller.AnimationTimer].JointData[i].Scale;

                                        rot.X *= -1;
                                        rot.Y *= -1;
                                        rot.Z *= -1;

                                        Vector3 rotatedForward = VectorMath.Vector3_Rotate(Vector3.Forward, rot);
                                        Vector3 rotatedUp = VectorMath.Vector3_Rotate(Vector3.Up, rot);

                                        AnimatedMesh.Skeleton.Deformers[j].TransformAnimated = Matrix.CreateWorld(trans, rotatedForward, rotatedUp) * currentMatrixAddition;
                                    }
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            controller.AnimationTimer = 0;
                            return types;
                        }                        

                        #region The Transform with new animated data in hierarchy

                        for (int i = 0; i < AnimatedMesh.Skeleton.Bone_Transforms.Length; i++)
                        {
                            AnimatedMesh.Skeleton.Bone_Transforms[i] =
                                AnimatedMesh.Skeleton.InverseBindPose[i] *
                                AnimatedMesh.Skeleton.Deformers[i].TransformAnimated;
                        }

                        #endregion

                        #region Calculate parent transmform for each joint by climbin with dynamic index
                        for (int i = AnimatedMesh.Skeleton.maxDepth; i >= 0; i--)
                        {

                            int depthChild = i;
                            //Go throught childs with depth i only;
                            for (int x = 0; x < AnimatedMesh.Skeleton.Deformers.Count; x++)
                            {
                                int climbIndex = AnimatedMesh.Skeleton.Deformers[x].ParentDeformer_Index;
                                while (depthChild == AnimatedMesh.Skeleton.Deformers[x].Depth && climbIndex != -1)
                                {                                    
                                    AnimatedMesh.Skeleton.Bone_Transforms[x] *= AnimatedMesh.Skeleton.Deformers[climbIndex].TransformAnimated;
                                    climbIndex = AnimatedMesh.Skeleton.Deformers[climbIndex].ParentDeformer_Index;
                                    //Console.WriteLine("Joint {0} with parent {1}", x, climbIndex);                                                                        
                                }
                            }
                        }
                        #endregion

                        for (int i = 0; i < AnimatedMesh.Skeleton.Bone_Transforms.Length; i++)
                        {
                            AnimatedMesh.Skeleton.Bone_Transforms[i] *= preMatrix;
                            
                        }

                        //Turn off later
                        #region Use CPU skinning for now.
                        if (true)
                        {
                            int requesterIndex = 0;
                            for (int k = 0; k < AnimatedMesh.Skeleton.Deformers.Count; k++)
                            {
                                //Lets try to filter the requested matrices here                               
                                if (RequestedDeformers != null && requesterIndex < RequestedDeformers.Count && AnimatedMesh.Skeleton.Deformers[k].DeformerName.Equals(RequestedDeformers[requesterIndex]))
                                {
                                    Ret_MatrixFromAnimation_Type t = new Ret_MatrixFromAnimation_Type();
                                    t.deformerName = RequestedDeformers[requesterIndex];
                                    t.matrix = AnimatedMesh.Skeleton.Bone_Transforms[k] * AnimatedMesh.Skeleton.InverseBindPose[k];
                                    requesterIndex++;
                                    types.Add(t);
                                }
                                for (int z = 0; z < AnimatedMesh.Skeleton.Deformers[k].VertexIndices.Length; z++ /*z < AnimatedMesh.staticVertex.Length; z++*/)
                                {
                                    AnimatedMesh.PointList_Dynamic[AnimatedMesh.Skeleton.Deformers[k].VertexIndices[z]] =
                                        Vector3.Transform(AnimatedMesh.PointList[AnimatedMesh.Skeleton.Deformers[k].VertexIndices[z]],
                                        AnimatedMesh.Skeleton.Bone_Transforms[k]);
                                }
                            }
                            for (int i = 0; i < AnimatedMesh.VertexIndex.Length; i++)
                            {
                                AnimatedMesh.Vertices[i] = new CustomVertex(AnimatedMesh.PointList_Dynamic[AnimatedMesh.VertexIndex[i]], AnimatedMesh.Vertices[i].TextureCoordinate, AnimatedMesh.Vertices[i].Normal, AnimatedMesh.Vertices[i].BiNormal, AnimatedMesh.Vertices[i].Tangent);
                            }
                        }
                        #endregion
                    }
                }
            }
            return types;
        }
    }

    public struct PlayOnceAnimation_Data
    {
        public bool PlayingOnce;

    }

    /// <summary>
    /// An animation Controller for each NPC, Player, or whatever
    /// </summary>
    public class AnimationController
    {
        public String ControllerOwnerName;
        public UInt64 ControllerOwnerHash;
        public AnimationContainer AnimationContainer;
        public int AnimationTimer = 0;
        public bool AnimationRunning, AnimationEnded;
        public int AnimationLength;
        public String idleAnimName;

        public bool PlayAnimationOnce;
        public string currentAnimationName;

        public int IdleAnimationTimer = 0;
        public string[] animationNames; //Contains all loaded animation names
        
        public AnimationController(AnimationContainer cont, string idleName, string ownername)
        {
            ControllerOwnerName = ownername;
            AnimationTimer = 0;
            AnimationEnded = true;
            AnimationRunning = false;
            PlayAnimationOnce = false;
            currentAnimationName = "Noone";
            this.AnimationContainer = cont;
            idleAnimName = idleName;
        }

        
        public static void setAnimationPlayback_Once(  AnimationController animcontroller, string animationName)
        {
            animcontroller.AnimationTimer = 0;
            animcontroller.PlayAnimationOnce = true;
            animcontroller.currentAnimationName = animationName;

        }
    }    

    public class Animation
    {
        public String Name;
        public int FrameCount;
        public List<Frame> AnimationFrames;
        
        public Animation(String name, List<Frame> frames)
        {
            this.Name = name;
            this.AnimationFrames = frames;
        }        
    }

    public class AnimationContainer
    {
        public String ContainerName;
        public List<Animation> AnimationCollection;

        public AnimationContainer()
        {
            AnimationCollection = new List<Animation>();
        }
    }

    public class SamplerJoint
    {
        public string name;
        public float[] rotx, roty, rotz;
        public float[] transx, transy, transz;
        public float[] scalex, scaley, scalez;

        public SamplerJoint(int frameCount)
        {
            rotx = new float[frameCount];
            roty = new float[frameCount];
            rotz = new float[frameCount];

            transx = new float[frameCount];
            transy = new float[frameCount];
            transz = new float[frameCount];

            scalex = new float[frameCount];
            scaley = new float[frameCount];
            scalez = new float[frameCount];
        }
    }
}