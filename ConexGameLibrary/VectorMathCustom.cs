﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ConexGameLibrary
{       
    /// <summary>
    /// This class contains vector3 related math functions, including a function to create a plane from a triangle
    /// </summary>
    public static class VectorMath //Describes math for vector operations
    {
        public static void PrintMatrix(Matrix mat1)
        {
            Console.Write("{0} {1} {2} {3}\n", Math.Round(mat1.M11, 4), Math.Round(mat1.M12,4), Math.Round(mat1.M13,4), Math.Round(mat1.M14,4));
            Console.Write("{0} {1} {2} {3}\n", Math.Round(mat1.M21, 4), Math.Round(mat1.M22,4), Math.Round(mat1.M23,4), Math.Round(mat1.M24,4));
            Console.Write("{0} {1} {2} {3}\n", Math.Round(mat1.M31, 4), Math.Round(mat1.M32,4), Math.Round(mat1.M33,4), Math.Round(mat1.M34,4));
            Console.Write("{0} {1} {2} {3}\n", Math.Round(mat1.M41, 4), Math.Round(mat1.M42,4), Math.Round(mat1.M43,4), Math.Round(mat1.M44,4));
            Console.WriteLine("\n");
        }

        public static Single getRayFactor(Vector3 start, Vector3 end)
        {
            Single factor = VectorMath.get_length(end - start);

            return factor;
        }

        //Rotation in X,Y,Z Achse als Radianten
        public static Vector3 MatrixTransform_transpose(Vector3 vector, Matrix matrix)
        {
            Vector3 newVector = Vector3.Zero;

            newVector.X = vector.X * matrix.M11 + vector.Y * matrix.M12 + vector.Z * matrix.M13;
            newVector.Y = vector.X * matrix.M21 + vector.Y * matrix.M22 + vector.Z * matrix.M23;
            newVector.Z = vector.X * matrix.M31 + vector.Y * matrix.M32 + vector.Z * matrix.M33;
            //newVector += matrix.Translation;
            return newVector;
        }
        
        public static Vector3 MatrixTransform(Vector3 vector, Matrix matrix)
        {
            Vector3 newVector = Vector3.Zero;             

            // Point ( x, y, z )   =    x y z  = x
            //                          x y z  = y
            //                          x y z  = z

            newVector.X = vector.X * matrix.M11 + vector.Y * matrix.M12 + vector.Z * matrix.M13;

            newVector.Y = vector.X * matrix.M21 + vector.Y * matrix.M22 + vector.Z * matrix.M23;

            newVector.Z = vector.X * matrix.M31 + vector.Y * matrix.M32 + vector.Z * matrix.M33;

            newVector += matrix.Translation;
            return newVector;
        }

        public static Matrix MatrixRound(Matrix m,int digits)
        {
            Matrix m_new = new Matrix();

            m_new.M11 = (float)Math.Round(m.M11,digits); 
            m_new.M12 = (float)Math.Round(m.M12,digits);
            m_new.M13 = (float)Math.Round(m.M13,digits);
            m_new.M14 = (float)Math.Round(m.M14,digits);

            m_new.M21 = (float)Math.Round(m.M21,digits);
            m_new.M22 = (float)Math.Round(m.M22,digits);
            m_new.M23 = (float)Math.Round(m.M23,digits); 
            m_new.M24 = (float)Math.Round(m.M24,digits);

            m_new.M31 = (float)Math.Round(m.M31,digits);
            m_new.M32 = (float)Math.Round(m.M32,digits); 
            m_new.M33 = (float)Math.Round(m.M33,digits);
            m_new.M34 = (float)Math.Round(m.M34,digits);

            m_new.M41 = (float)Math.Round(m.M41, digits);
            m_new.M42 = (float)Math.Round(m.M42, digits);
            m_new.M43 = (float)Math.Round(m.M43, digits);
            m_new.M44 = (float)Math.Round(m.M44, digits);

            return m_new;
            
        }

        public static Vector3 Vector3_toRadians(Vector3 v)
        {
            Vector3 vnew = new Vector3(MathHelper.ToRadians(v.X), MathHelper.ToRadians(v.Y), MathHelper.ToRadians(v.Z));
            return vnew;
        }

        public static Vector3 Vector3_Rotate(Vector3 vector, Vector3 angles)
        {
            Vector3 vec = Vector3.Zero;
            /*
            vec.X = 
                Cos(angles.Y) * Cos(angles.Z) *vector.X +
                (Cos(angles.X) * Sin(angles.Z) + Sin(angles.X) * Sin(angles.Y) * Cos(angles.Z)) * vector.Y +
                (Sin(angles.X) * Sin(angles.Z) - Cos(angles.X) * Sin(angles.Y) * Cos(angles.Z)) * vector.Z;

            vec.X =
                -Cos(angles.Y) * Sin(angles.Z) * vector.X +
                (Cos(angles.X) * Cos(angles.Z) - Sin(angles.X)*Sin(angles.Y)*Sin(angles.Z))*vector.Y +
                (Sin(angles.X) * Cos(angles.Z) + Cos(angles.X) * Sin(angles.Y) * Sin(angles.Z)) * vector.Z;

            vec.Z =
                Sin(angles.Y) * vector.X -
                (Sin(angles.X) * Cos(angles.Y) * vector.Y) +
               Cos(angles.X) * Cos(angles.Y) * vector.Z;

            */
            vec = Vector3_RotateX(vector, angles.X);
            vec = Vector3_RotateY(vec, angles.Y);
            vec = Vector3_RotateZ(vec, angles.Z);
            
            return vec;
        }

        public static Vector3 Vector3_RotateX(Vector3 vector, float radians_x)
        {
            Vector3 vec_ret;
            vec_ret.X = vector.X;
            vec_ret.Y = vector.Y * Cos(radians_x) + vector.Z * Sin(radians_x);
            vec_ret.Z = -vector.Y * Sin(radians_x) + vector.Z * Cos(radians_x);

            return vec_ret;
        }

        public static Vector3 Vector3_RotateY(Vector3 vector, float a)
        {
            Vector3 vec_ret;
            vec_ret.X = vector.X * Cos(a) - vector.Z * Sin(a);
            vec_ret.Y = vector.Y;
            vec_ret.Z = vector.X * Sin(a) + vector.Z * Cos(a); 

            return vec_ret;
        }

        public static Vector3 Vector3_RotateZ(Vector3 vector, float radians_z)
        {
            Vector3 vec_ret;
            vec_ret.X = vector.X * Cos(radians_z) + vector.Y * Sin(radians_z);
            vec_ret.Y = -vector.X * Sin(radians_z) + vector.Y * Cos(radians_z);
            vec_ret.Z = vector.Z;

            return vec_ret;
        }

        public static Vector2 Vector2_Rotate(Vector2 vector, float rotation)
        {
            Vector2 vec_ret;
            vec_ret.X = vector.X * Cos(rotation) + vector.Y * Sin(rotation);
            vec_ret.Y = -vector.X * Sin(rotation) + vector.Y * Cos(rotation);

            return vec_ret;
        }

        public static float Sin(float rads)
        {
            return (float)Math.Sin((double)rads);
        }

        public static float Cos(float rads)
        {
            double radD =  Convert.ToDouble(rads);
            double cosD = Math.Cos(radD);
            float cosF = Convert.ToSingle(cosD);
            return cosF;
        }

        public static Vector3 Vector3_Round(Vector3 vec, int digits)
        {
            vec.X = (float)Math.Round((double)vec.X, digits);
            vec.Y = (float)Math.Round((double)vec.Y, digits);
            vec.Z = (float)Math.Round((double)vec.Z, digits);
            return vec;
        }

        public static Vector3 getCrossProduct(Vector3 vec1, Vector3 vec2)
        {
            Vector3 cross;
            cross.X = ((vec1.Y * vec2.Z) - (vec1.Z * vec2.Y));
            cross.Y = ((vec1.Z * vec2.X) - (vec1.X * vec2.Z));
            cross.Z = ((vec1.X * vec2.Y) - (vec1.Y * vec2.X));

            return cross;
        }

        //Is probaly outdated now, was used in wall collision detection, but now not used.
        /// <summary>
        /// Outdated!!! gets the clamped angle 0 = 90degree  1= 180 or 0 degree
        /// </summary>
        /// <param name="cross"></param>
        /// <param name="camDir"></param>
        /// <returns></returns>
        public static Single returnClampedAngleDegree(Vector3 cross, Vector3 camDir)
        {
            return (Single)(
                    Math.Acos(
                        (double)
                            ((VectorMath.getScalarProduct(cross, camDir) / (VectorMath.getAbsoluteVector(cross) * VectorMath.getAbsoluteVector(camDir)))
                    )
                )
            );
        }

        public static float getScalarProduct(Vector3 vec1, Vector3 vec2)
        {
            float scalar;
            scalar = (vec1.X * vec2.X) + (vec1.Y * vec2.Y) + (vec1.Z * vec2.Z);
            return scalar;
        }

        public static float roundf(float value, int digits)
        {
            double newval = Math.Round((double)value, digits);
            return (float)newval;
        }

        /// <summary>
        /// Returns scalar / |a| * |b|
        /// </summary>
        /// <param name="vec1"></param>
        /// <param name="vec2"></param>
        /// <returns></returns>
        public static float getScalarProductBeforeCosinus(Vector3 vec1, Vector3 vec2)
        {
            float scalar;
            scalar = ((vec1.X * vec2.X) + (vec1.Y * vec2.Y) + (vec1.Z * vec2.Z)) / (get_length(vec1) * get_length(vec2));
            return scalar;
        }

        public static float getAngleBetweenVector(Vector3 vec1, Vector3 vec2)
        {
            float scalar;
            scalar = ((vec1.X * vec2.X) + (vec1.Y * vec2.Y) + (vec1.Z * vec2.Z)) / (get_length(vec1) * get_length(vec2));
            scalar = (float)Math.Acos((double)(scalar));
            return scalar;
        }

        public static float getAtan2OfVector(Vector3 vec)
        {
            return (float)Math.Atan2(vec.X, vec.Z);
        }

        public static float getAbsoluteVector(Vector3 vec3)
        {
            //Returns the absoltue of an vector!  by formula:   a = squareRoot(a²+b²+c²) 
            float absVec3 = (float)Math.Sqrt(Math.Pow(vec3.X, 2) + Math.Pow(vec3.Y, 2) + Math.Pow(vec3.Z, 2));
            return absVec3;
        }

        public static float get_length(Vector2 vec2)
        {
            //Returns the absoltue of an vector!  by formula:   a = squareRoot(a²+b²+c²) 
            float absVec2 = (float)Math.Sqrt(Math.Pow(vec2.X, 2) + Math.Pow(vec2.Y, 2));
            return absVec2;
        }

        /// <summary>
        /// Redirection to the get Absolute vector method!
        /// </summary>
        /// <param name="vec3"></param>
        /// <returns></returns>
        public static float get_length(Vector3 vec3)
        {
            return getAbsoluteVector(vec3);
        }

        public static double getAbsoluteVector(Vector2 vec2)
        {
            //Returns the absoltue of an vector!  by formula:   a = squareRoot(a²+b²+c²) 
            double absVec2 = Math.Sqrt(Math.Pow(vec2.X, 2) + Math.Pow(vec2.Y, 2));
            return absVec2;
        }

        public static Vector3 ReturnStep(Vector3 start, Vector3 end, long currentFrame, long maxFrame)
        {
            Vector3 difference = Vector3.Subtract(end, start);
            Single stepMultiplicator = currentFrame / (Single)maxFrame;
            Vector3 Step = Vector3.Multiply(difference, stepMultiplicator);
            var newStep = Vector3.Add(start, Step);

            return newStep;
        }

        /// <summary>
        /// Use this for Center, get the radius with the other function, use both !
        /// </summary>
        /// <param name="tri"></param>
        /// <returns></returns>
        public static Vector3 getCenterOfTriangle(Triangle tri)
        {
            float x = (tri.p1.X + tri.p2.X + tri.p3.X) / 3;
            float y = (tri.p1.Y + tri.p2.Y + tri.p3.Y) / 3;
            float z = (tri.p1.Z + tri.p2.Z + tri.p3.Z) / 3;
            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Overloaded function 
        /// </summary>
        /// <param name="tri"></param>
        /// <returns></returns>
        public static Vector3 getCenterOfTriangle(Vector3 a, Vector3 b, Vector3 c)
        {
            float x = (a.X + b.X + c.X) / 3;
            float y = (a.Y + b.Y + c.Y) / 3;
            float z = (a.Z + b.Z + c.Z) / 3;
            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Is used with the other function (getCenterOfTriangle)
        /// </summary>
        /// <param name="tri"></param>
        /// <returns></returns>
        public static Single getMaxRadiusAroundTriangle(Triangle tri)
        {
            float x = (tri.p1.X + tri.p2.X + tri.p3.X) / 3;
            float y = (tri.p1.Y + tri.p2.Y + tri.p3.Y) / 3;
            float z = (tri.p1.Z + tri.p2.Z + tri.p3.Z) / 3;
            var center = new Vector3(x, y, z);

            float[] distances = new float[3];
            distances[0] = Vector3.Distance(center, tri.p1);
            distances[1] = Vector3.Distance(center, tri.p2);
            distances[2] = Vector3.Distance(center, tri.p3);

            return distances.Max();
        }

        public static Single getMaxRadiusAroundTriangle(Vector3 a, Vector3 b, Vector3 c)
        {
            float x = (a.X + b.X + c.X) / 3;
            float y = (a.Y + b.Y + c.Y) / 3;
            float z = (a.Z + b.Z + c.Z) / 3;
            var center = new Vector3(x, y, z);

            float[] distances = new float[3];
            distances[0] = Vector3.Distance(center, a);
            distances[1] = Vector3.Distance(center, b);
            distances[2] = Vector3.Distance(center, c);

            return distances.Max();
        }

        public static Vector3 InterpolateLinear(Vector3 start, Vector3 end, float factor)
        {
            return Vector3.Add(start, Vector3.Multiply(Vector3.Subtract(end, start), factor));
        }

        public static float getDistancePointRay(Vector3 RayPos, Vector3 RayDirection, Vector3 Point)
        {
            float top = VectorMath.get_length(VectorMath.getCrossProduct(RayDirection,Vector3.Subtract(Point, RayPos)));
            float bottom = VectorMath.get_length(RayDirection);

            return top / bottom;
        }


        public static Vector3 getClosestPointOnLineSegment(Vector3 A, Vector3 B, Vector3 Point)
        {
            Vector3 AB = B - A;
            //Project C on the Ab segment, to get position on  the ray
            float t = VectorMath.getScalarProduct(Point - A, AB) / VectorMath.getScalarProduct(AB, AB);

            if (t < 0.0f) t = 0.0f;
            if (t > 1.0f) t = 1.0f;

            //Compute projected pos from the t interval on ray
            Vector3 d = A + t * AB;

            return d;
        }

    }

}
