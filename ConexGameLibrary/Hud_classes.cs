﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using ConexGameLibrary;

namespace ConexGameLibrary
{    
    public class HUD_Data
    {
        public Vector2 HealthPos; public String HealthHud;
        public Vector2 ArmorPos; public string ArmorHud;

        public Vector2 Magazines_RemainPos; public string MagsHud;
        public Vector2 BulletsLoadedPos; public string BulletsHud;

        public List<WindowPolygon> windowpolygons;

        public HUD_Data(Vector2 hpos,Vector2 apos, Vector2 mags, Vector2 bullets)
        {
            this.HealthPos = hpos;
            this.ArmorPos = apos;
            this.BulletsLoadedPos = bullets;
            this.Magazines_RemainPos = mags;

            windowpolygons = new List<WindowPolygon>();
        }
    }

    //each one
    public class Hud_Settings_Game
    {
        //One for each player
        public List<HUD_Data> hudposition;

        public Hud_Settings_Game()
        {       
            hudposition = new List<HUD_Data>();
        }
    }
}
