using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using System.Globalization;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Xna;


namespace ConexGameLibrary
{

    public class CustomModelLoader
    {
        String[] lines;
        Int32 lineCount = 0;

        public Vector3[] pointList;
        Int32 pointCount = 1;

        Vector2[] uvList;
        Int32 uvCount = 1;

        Vector3[] vertexNormal;
        Int32 vertexNormalCount = 1;

        CustomVertex[] vertexList;

        StreamReader strReader;
        FileInfo modelStream;
        Int32 tempVertexCount = 0;
        int trianglecount;
        CustomVertex[] newVertexList;


        public CustomModelLoader()
        {
        }

        public CustomVertex[] LoadModel(string modelPathP)
        {
            modelStream = new FileInfo(modelPathP);
            strReader = new StreamReader(modelStream.FullName);

            lineCount = getLinesCount();
            lines = getLines();
            Console.WriteLine("The file contains {0} \n ", lineCount);
            LoadPointData();
            Console.WriteLine("\n{0} points were created \n", pointCount - 1);
            LoadUVdata();
            Console.WriteLine("\n{0} uv points were created \n", uvCount - 1);
            LoadVNdata();
            Console.WriteLine("\n{0} VN points were created \n", vertexNormalCount - 1);
            LoadFaces();
            Console.WriteLine("\n{0} vertexCreated \n", tempVertexCount);

            modelStream = null;

            
            CustomVertex[] customVertex = new CustomVertex[Convert.ToUInt32(vertexList.GetLength(0)) - 3];

            uint counter = 0;
            foreach(CustomVertex cst in customVertex)
            {
                customVertex[counter] = vertexList[counter];
                counter++;
            }

            resetData();           

            return customVertex;
        }

        public CustomVertex[] LoadModelFromLines(string[] linesParams)
        {

            lineCount = linesParams.GetLength(0);
            lines = linesParams;
            Console.WriteLine("The file contains {0} \n ", lineCount);
            LoadPointData();
            Console.WriteLine("\n{0} points were created \n", pointCount - 1);
            LoadUVdata();
            Console.WriteLine("\n{0} uv points were created \n", uvCount - 1);
            LoadVNdata();
            Console.WriteLine("\n{0} VN points were created \n", vertexNormalCount - 1);
            LoadFaces();
            Console.WriteLine("\n{0} vertexCreated \n", tempVertexCount);

            modelStream = null;


            CustomVertex[] customVertex = new CustomVertex[Convert.ToUInt32(vertexList.GetLength(0)) - 3];

            uint counter = 0;
            foreach (CustomVertex cst in customVertex)
            {
                customVertex[counter] = vertexList[counter];
                counter++;
            }

            resetData();



            return customVertex;
        }

        #region Will be called within LoadModel

        private int getLinesCount()
        {
            return lineCount = File.ReadAllLines(modelStream.FullName).Count();
        }
        private string[] getLines()
        {
            return File.ReadAllLines(modelStream.FullName);
        }

        private void LoadPointData()
        {
            Int32 cntPoint = 0;
            foreach (string line in lines)
            {
                if (line.StartsWith("v "))
                {
                    pointCount++;
                }
            }
            pointList = new Vector3[pointCount];

            foreach (string line in lines)
            {
                if (line.StartsWith("v "))
                {
                    cntPoint++;
                    pointList[cntPoint] = createPoint(line);
                    Console.WriteLine(pointList[cntPoint]);

                }
            }
        }
        private void LoadUVdata()
        {
            Int32 cntUV = 0;
            foreach (string line in lines)
            {
                if (line.StartsWith("vt "))
                {
                    uvCount++;
                }
            }
            uvList = new Vector2[uvCount];
            foreach (string line in lines)
            {
                if (line.StartsWith("vt "))
                {
                    cntUV++;
                    uvList[cntUV] = createUV(line);
                    Console.WriteLine(uvList[cntUV]);
                }
            }
        }
        private void LoadVNdata()
        {

            Int32 cntVN = 0;
            foreach (string line in lines)
            {
                if (line.StartsWith("vn "))
                {
                    vertexNormalCount++;
                }
                vertexNormal = new Vector3[vertexNormalCount];
                if (line.StartsWith("vn "))
                {
                    cntVN++;
                    vertexNormal[cntVN] = createVN(line);
                    Console.WriteLine(vertexNormal[cntVN]);
                }
            }
        }
        private void LoadFaces()
        {
            Int32 cntF = 1;
            foreach (string line in lines)
            {
                if (line.StartsWith("f "))
                {
                    cntF++;
                }
            }
            vertexList = new CustomVertex[(cntF * 3)];

            foreach (string line in lines)
            {
                if (line.StartsWith("f "))
                {
                    string lineFormat;
                    //Make sure it begins after "f " and not before
                    lineFormat = line.Substring(2);

                    string[] faces = new string[3];
                    //Split Line of "f 1/1/1 2/2/2 3/3/3" into array of single faces 1/1/1     
                    faces = lineFormat.Split(' ');
                    //Now i have an array of strings, defining face data "1/1/1" for example
                    //Loop throught each triangle data
                    foreach (string face in faces)
                    {
                        string[] vertexSingleData = new String[3];
                        Int32[] vertexData = new Int32[3];
                        //Split each vertexInformation into its indices for vector3,
                        //uv coordinate and vectornormal
                        vertexSingleData = (face.Split('/'));
                        //convert dat
                        for (Int32 i = 0; i < 3; i++)
                        {
                            vertexData[i] = Convert.ToInt32(vertexSingleData[i]);
                        }
                        addVertex(vertexData);
                        
                        //I got a single vertex now                        
                    }
                }
            }
        }

        private void addVertex(Int32[] vertexData)
        {
            {
                trianglecount++;
                if (trianglecount > 3)
                {
                    Console.WriteLine("\n");
                    trianglecount = 0;
                }
            }
            Int32 v, uv, vn;
            v = vertexData[0];
            uv = vertexData[1];
            vn = vertexData[2];

            CustomVertex customvertex = new CustomVertex(pointList[v], uvList[uv],vertexNormal[vn]);
            vertexList[tempVertexCount] = customvertex;
            Console.WriteLine("vec3: {0}, uv: {1}", pointList[vertexData[0]], uvList[vertexData[1]]);

            tempVertexCount++;
        }

        private Vector3 createPoint(string line)
        {
            string newLine;
            Vector3 vec3;
            newLine = line.Substring(2);
            Single[] floatlistV = new Single[3];
            string[] stringFloatList = new string[3];

            stringFloatList = newLine.Split(' ');
            {
                floatlistV[0] = float.Parse(stringFloatList[0], CultureInfo.InvariantCulture.NumberFormat);
                floatlistV[1] = float.Parse(stringFloatList[1], CultureInfo.InvariantCulture.NumberFormat);
                floatlistV[2] = float.Parse(stringFloatList[2], CultureInfo.InvariantCulture.NumberFormat);
            }
            {
                vec3.X = floatlistV[0];
                vec3.Y = floatlistV[1];
                vec3.Z = floatlistV[2];
            }
            return vec3;
        }
        private Vector2 createUV(string line)
        {
            string newLine;
            Vector2 vec2; ;
            //has to start at 3, since "vt " is the begin
            //If not 3, it will start reading the empty space before the actual
            //data.. so the array will be sized 3 not 2 and it will not work
            newLine = line.Substring(3);

            Single[] floatlistVT = new Single[2];
            string[] stringFloatListUV = new string[2];

            stringFloatListUV = newLine.Split(' ');
            {
                floatlistVT[0] = Convert.ToSingle(stringFloatListUV[0], CultureInfo.InvariantCulture.NumberFormat);
                floatlistVT[1] = Convert.ToSingle(stringFloatListUV[1], CultureInfo.InvariantCulture.NumberFormat);
            }
            {
                vec2.X = floatlistVT[0];
                vec2.Y = floatlistVT[1];
            }

            return vec2;
        }
        private Vector3 createVN(string line)
        {
            string newLine;
            Vector3 vec3;

            newLine = line.Substring(3);

            Single[] floatlistVN = new Single[3];
            string[] stringFloatListVN = new string[3];

            stringFloatListVN = newLine.Split(' ');
            {
                floatlistVN[0] = float.Parse(stringFloatListVN[0], CultureInfo.InvariantCulture.NumberFormat);
                floatlistVN[1] = float.Parse(stringFloatListVN[1], CultureInfo.InvariantCulture.NumberFormat);
                floatlistVN[2] = float.Parse(stringFloatListVN[2], CultureInfo.InvariantCulture.NumberFormat);
            }
            {
                vec3.X = floatlistVN[0];
                vec3.Y = floatlistVN[1];
                vec3.Z = floatlistVN[2];
            }
            return vec3;
        }

        private void resetData()
        {
            modelStream = null;
            strReader = null;
            pointCount = 1;
            pointList = null;
            uvCount = 1;
            uvList = null;
            vertexNormal = null;
            vertexNormalCount = 1;

            tempVertexCount = 0;
            trianglecount = 0;
            lineCount = 0;
            lines = null;
            tempVertexCount = 0;
        }
        #endregion
    }

    public class vlmOpenSave
    {
        public VertexListModel vlm;

        public VertexListModel open(string path)
        {
            try
            {
                BinaryFormatter binaryformat = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read);

                vlm = (VertexListModel)binaryformat.Deserialize(stream);

                Console.WriteLine("vlm model succesfully loaded");

                stream.Close();
                stream = null;
                return vlm;
            }
            catch (Exception a)
            {
                System.Console.WriteLine(a);
                return null;
            }
        }

        public void save(string path)
        {
            try
            {
                BinaryFormatter binaryformatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                binaryformatter.Serialize(stream, vlm);
                Console.WriteLine("MOdel succesfully saved");
                vlm = null;
                stream.Close();
                stream = null;

            }
            catch (Exception a)
            {
                System.Console.WriteLine(a);
            }
        }

        public void editTexPath(string newPath)
        {
            vlm.texturePath = newPath;
            Console.WriteLine("Texture changed to: {0}", newPath);

        }
    }

    [SerializableAttribute]
    public struct CustomVertex : IVertexType
    {
        Vector3 vertexPosition;
        Vector3 vertexNormal;
        Vector2 vertexTextureCoordinate;        

        public Vector3 Position
        {
            get { return vertexPosition; }
            set { vertexPosition = value; }
        }
        public Vector3 Normal
        {
            get { return vertexNormal; }
            set { vertexNormal = value; }
        }
        public Vector2 TextureCoordinate
        {
            get { return vertexTextureCoordinate; }
            set { vertexTextureCoordinate = value; }
        }

             public readonly static VertexDeclaration vertexDeclaration = new VertexDeclaration(

            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(3 * sizeof(Single), VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
            new VertexElement(6*sizeof(Single) , VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0)        
            );
          
                
        public CustomVertex(Vector3 pos, Vector2 textureCoordinate, Vector3 normal)
        {
            vertexPosition = pos;
            vertexTextureCoordinate = textureCoordinate;
            vertexNormal = normal;            
        }
        
        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return vertexDeclaration; }
        }
    }

    public class CustomVertexAnimation : IVertexType
    {
        Vector3 vertexPosition;
        Vector3 vertexNormal;
        Vector2 vertexTextureCoordinate;

        //Experimental, might work this way, else string!
        int[] joint_indices = new int[5];
        float[] joint_weights = new float[5];

        public Vector3 Position
        {
            get { return vertexPosition; }
            set { vertexPosition = value; }
        }
        public Vector3 Normal
        {
            get { return vertexNormal; }
            set { vertexNormal = value; }
        }
        public Vector2 TextureCoordinate
        {
            get { return vertexTextureCoordinate; }
            set { vertexTextureCoordinate = value; }
        }

        public int[] JointIndices
        {
            get { return joint_indices; }
            set { joint_indices = value; }
        }

        public float[] JointAffections
        {
            get { return joint_weights; }
            set { joint_weights = value; }
        }

        public readonly static VertexDeclaration vertexDeclaration = new VertexDeclaration(

       new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
       new VertexElement(3 * sizeof(Single), VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
       new VertexElement(6 * sizeof(Single), VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
       new VertexElement(11 * sizeof(Single), VertexElementFormat.Vector3, VertexElementUsage.BlendIndices, 0),
       new VertexElement(16 * sizeof(Single), VertexElementFormat.Vector3, VertexElementUsage.BlendWeight, 0)
       );


        public CustomVertexAnimation(Vector3 pos, Vector2 textureCoordinate, Vector3 normal)
        {
            vertexPosition = pos;
            vertexTextureCoordinate = textureCoordinate;
            vertexNormal = normal;

        }

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return vertexDeclaration; }
        }

    }

    [SerializableAttribute]
    public class VertexListModel
    {
        public VertexListModel(CustomVertex[] customVertices, String texturePath)
        {
            this.customVertex = customVertices;
            this.texturePath = texturePath;
        }

        public VertexListModel(CustomVertex[] customVertices, String[] texturesParam)
        {
            this.customVertex = customVertices;            
        }

        public VertexListModel()
        { }

        public CustomVertex[] customVertex;
        public string texturePath;
        
    }
       

    public class CustomBinarySerialiser
    {
        public CustomBinarySerialiser()
        {
        }

        public bool SerialiseModel(ConexGameLibrary.VertexListModel modelObject, String pathAndname)
        {
            var fileStream = new FileStream(pathAndname, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            try
            {
                binaryFormatter.Serialize(fileStream, modelObject);
                fileStream.Close();
                fileStream = null;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Creating model failed. Error: {0}", e);
                fileStream.Close();
                fileStream = null;
                return false;
            }

        }

        public VertexListModel DeserializeModel(String path)
        {
            VertexListModel vertexListModel_obj;

            var fileStream = new FileStream(path, FileMode.Open);
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            vertexListModel_obj = (VertexListModel)binaryFormatter.Deserialize(fileStream);
            fileStream.Close();
            fileStream = null;
            binaryFormatter = null;
            return vertexListModel_obj;
        }
    }
}
