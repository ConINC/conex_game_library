﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ConexGameLibrary
{
    public class Camera
    {
        //View Contains Position and Lookat        
        private Microsoft.Xna.Framework.Matrix View;
        private Microsoft.Xna.Framework.Matrix Projection;

        //Only Edit those structs to change the cameras state
        //the View matrix will be updated
        private Microsoft.Xna.Framework.Vector3 Position;
        private Microsoft.Xna.Framework.Vector3 LookAt; //Dont change Lookat Directly
        private Microsoft.Xna.Framework.Vector3 Rotation;


        //Static stays the same, The result is calculated as sTATIC + ADAPT
        //Adapt value is sloping towards Set, for smooth transfer 
        public Microsoft.Xna.Framework.Vector3 CameraUpVector_Static = new Microsoft.Xna.Framework.Vector3(0, 1, 0); //Dont change this
        public Microsoft.Xna.Framework.Vector3 CameraUpVector_Set = Vector3.Zero;        
        public Microsoft.Xna.Framework.Vector3 CameraUpVector_Adapt = Vector3.Zero;
        public Microsoft.Xna.Framework.Vector3 CameraUpVector_Final = Vector3.Zero;


        public Microsoft.Xna.Framework.Vector3 CameraRelativePosition_Adapt = new Microsoft.Xna.Framework.Vector3(0, 0, 0);
        public Microsoft.Xna.Framework.Vector3 CameraRelativePosition_Set = new Microsoft.Xna.Framework.Vector3(0, 0, 0);

        public Microsoft.Xna.Framework.Vector3 CameraLookForwardDirection;


        /*-----------------------------------------------------------------------*/
        //Constructor and Initialise
        public Camera()
        {
            this.LookAt = Microsoft.Xna.Framework.Vector3.Forward;
        }

        public void initialiseCamera(
            Microsoft.Xna.Framework.Vector3 position,
            Microsoft.Xna.Framework.Matrix proj,
            Microsoft.Xna.Framework.Vector3 rotation)
        {
            this.Position = position;
            this.Projection = proj;
            this.Rotation = rotation;
            this.LookAt = Microsoft.Xna.Framework.Vector3.Forward;
        }
        /*-----------------------------------------------------------------------*/


        /*-----------------------------------------------------------------------*/
        //Rotation Get/Set
        public void Set_Rotation(Microsoft.Xna.Framework.Vector3 rotation)
        {
            if (rotation.X > 1.3f)
                rotation.X = 1.3f;
            if (rotation.X < -1.3f)
                rotation.X = -1.3f;
            this.Rotation = rotation;
        }
        public Microsoft.Xna.Framework.Vector3 Get_Rotation()
        {
            return Rotation;
        }
        /*-----------------------------------------------------------------------*/
        
        /*-----------------------------------------------------------------------*/
        //Position Get/Set
        public void Set_Position(Microsoft.Xna.Framework.Vector3 newPosition)
        {
            this.Position = newPosition;
        }
        public Microsoft.Xna.Framework.Vector3 Get_Positon()
        {
            return Position;
        }
        /*-----------------------------------------------------------------------*/
        
        public Vector3 Get_LookAt()
        {
            return LookAt;
        }

        public Microsoft.Xna.Framework.Matrix Get_ViewMatrix()
        {
            return View;
        }

        public void Set_ViewMatrix(Matrix viewNew)
        {
            this.View = viewNew;
        }

        public void Set_Projection(Microsoft.Xna.Framework.Matrix Proj)
        {
            this.Projection = Proj;
        }

        public Microsoft.Xna.Framework.Matrix Get_Projection()
        {
            return Projection;
        }

        public void UpdateAdaptValues(float powerShift, float powerRotate)
        {
            if (CameraRelativePosition_Set != Vector3.Zero)
            {
                Vector3 diff = (CameraRelativePosition_Set - CameraRelativePosition_Adapt) * powerShift;
                if (VectorMath.get_length(diff) > 0.002f)
                {
                    CameraRelativePosition_Adapt += diff;
                }
            }
            if (CameraUpVector_Set != Vector3.Zero)
            {
                Vector3 diff = (CameraUpVector_Set - CameraUpVector_Adapt) * powerRotate;
                if (VectorMath.get_length(diff) > 0.002f)
                {
                    CameraUpVector_Adapt += diff;
                }
            }
        }
        
        /*----------------------------------------------------------------------*/
        /// <summary>
        /// This function Takes the LookAt Vector, ROTATES it first, then adds it onto the position
        /// and creates the ViewMatrix using Position and Rotation, however, the Lookat 
        /// vector NEVER changes, the manipulated "lookat" is given right to the viewMatrix, 
        /// LookAt is only a directional vector for rotation.
        /// </summary>
        public void updateViewMatrix()
        {
            UpdateAdaptValues(0.12f, 0.10f);
            
            //newCamUp = Vector3.Transform(Microsoft.Xna.Framework.Matrix.CreateRotationX(            
            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(
                Position,
                Microsoft.Xna.Framework.Vector3.Add(Position,VectorMath.Vector3_Rotate(LookAt,Rotation)),
                CameraUpVector_Final
                );
        }
        
        public void updateCameraLookTowards()
        {             

            CameraLookForwardDirection = VectorMath.Vector3_Rotate(LookAt, Rotation);
        }

    }
}
