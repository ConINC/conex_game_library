﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ConexGameLibrary;

namespace ConexGameLibrary
{
    #region new Formats

    public struct _Deformer
    {
        public string Name;
        public string Parent;

        public int index;
        public int parent_index;

        public int depth;
        public Matrix matrix;
    }

    public struct _Material
    {
        float bumpFactor;
        float specularFactor;

        int colorIndex;
        int normalIndex;
        int specIndex;
    }

    public struct _Model
    {
        public _Deformer[] deformers;
        public Vertex_GpuSkin[] vertices;

        public string col, norm, spec;

        public _Material material;
    }

    public struct Vertex_GpuSkin : IVertexType
    {
        public Vector3 vertexPosition;
        public Vector3 vertexNormal;
        public Vector2 vertexTextureCoordinate;
        public Vector4 vertexWeight;
        public Vector4 vertexWeightIndex;


        public Vector3 Position
        {
            get { return vertexPosition; }
            set { vertexPosition = value; }
        }
        public Vector3 Normal
        {
            get { return vertexNormal; }
            set { vertexNormal = value; }
        }
        public Vector2 TextureCoordinate
        {
            get { return vertexTextureCoordinate; }
            set { vertexTextureCoordinate = value; }
        }
        public Vector4 VertexWeight
        {
            get { return vertexWeight; }
            set { vertexWeight = value; }
        }
        public Vector4 VertexWeightIndex
        {
            get { return vertexWeightIndex; }
            set { vertexWeightIndex = value; }
        }

        public readonly static VertexDeclaration vertexDeclaration = new VertexDeclaration(
           new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
           new VertexElement(3 * sizeof(Single), VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
           new VertexElement(6 * sizeof(Single), VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
           new VertexElement(8 * sizeof(Single), VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 0),
           new VertexElement(12 * sizeof(Single), VertexElementFormat.Vector4, VertexElementUsage.BlendIndices, 0)
        );

        public Vertex_GpuSkin(Vector3 pos, Vector2 textureCoordinate, Vector3 normal, Vector4 vertWeight, Vector4 vertIndex)
        {
            vertexPosition = pos;
            vertexTextureCoordinate = textureCoordinate;
            vertexNormal = normal;
            vertexWeight = vertWeight;
            vertexWeightIndex = vertIndex;
        }

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return vertexDeclaration; }
        }
    }

    #endregion

    public static class _Model_loader
    {

        public static _Model _Model_Load(string gamedir, string relpath)
        {
            _Model model = new _Model();
            model.material = new _Material();

            List<int>[] vertex_Weightindex_List; 
            List<Single>[] vertex_WeightWeight_List; 


            AnimatedModelMesh animatedMesh = new AnimatedModelMesh();
            animatedMesh.Bindposes = new List<BindPose>();
                      

            Vector3[] Points;
            int[] vertexIndex = new int[0];

            Vector3[] Normals;

            Vector2[] UVCoords;
            int[] uvIndex;


            Vector3 pos_origin = Vector3.Zero;

            FBX_File_Struct file = FBX_Loader.Create_FBX_File_Struct(gamedir + "//" + relpath, 0);

            #region Get vertexIndices and Points

            FBX_ModelMesh Fbx_modelmesh = new FBX_ModelMesh();
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                if (file.FBX_Objects[i] is FBX_ModelMesh)
                {
                    Fbx_modelmesh = (FBX_ModelMesh)file.FBX_Objects[i];
                    for (int zz = 0; zz < Fbx_modelmesh.properties.Count; zz++)
                    {
                        if (Fbx_modelmesh.properties[zz].Description.Contains("Lcl Translation"))
                        {
                            pos_origin = new Vector3((float)Fbx_modelmesh.properties[zz].value[0], (float)Fbx_modelmesh.properties[zz].value[1], (float)Fbx_modelmesh.properties[zz].value[2]);

                        }
                    }
                }
            }

            animatedMesh.VertexIndex = new int[Fbx_modelmesh.VertexIndices.Count];
            Points = new Vector3[Fbx_modelmesh.Vertices.Count / 3];

            int d = 0; int v = 0;
            for (; d < Fbx_modelmesh.Vertices.Count; )
            {
                Points[v] = new Vector3(Fbx_modelmesh.Vertices[d], Fbx_modelmesh.Vertices[d + 1], Fbx_modelmesh.Vertices[d + 2]);
                Points[v] += pos_origin;

                v++;
                d += 3;
            }

            for (int i = 0; i < Fbx_modelmesh.VertexIndices.Count; i++)
            {
                animatedMesh.VertexIndex[i] = Fbx_modelmesh.VertexIndices[i];
                //vertexIndex = animatedMesh.VertexIndex;
            }
            #endregion

            #region Get Normal Vectors

            FBX_LayerElementNormal normalLayer;
            normalLayer = new FBX_LayerElementNormal();
            for (int i = 0; i < Fbx_modelmesh.LayerContainer.Layers.Count; i++)
            {
                if (Fbx_modelmesh.LayerContainer.Layers[i] is FBX_LayerElementNormal)
                {
                    normalLayer = (FBX_LayerElementNormal)Fbx_modelmesh.LayerContainer.Layers[i];
                }
            }

            Normals = new Vector3[normalLayer.Normals.Count / 3];
            d = 0; v = 0;
            for (; d < normalLayer.Normals.Count; )
            {
                Normals[v] = new Vector3(normalLayer.Normals[d], normalLayer.Normals[d + 1], normalLayer.Normals[d + 2]);

                v++;
                d += 3;
            }


            #endregion

            #region Get UV coords

            FBX_LayerElementUV uvLayer = new FBX_LayerElementUV();
            for (int i = 0; i < Fbx_modelmesh.LayerContainer.Layers.Count; i++)
            {
                if (Fbx_modelmesh.LayerContainer.Layers[i] is FBX_LayerElementUV)
                {
                    uvLayer = (FBX_LayerElementUV)Fbx_modelmesh.LayerContainer.Layers[i];
                }
            }


            uvIndex = new int[uvLayer.UVIndex.Count];
            v = 0;
            for (; v < uvLayer.UVIndex.Count; )
            {
                uvIndex[v] = uvLayer.UVIndex[v];
                v++;
            }

            UVCoords = new Vector2[uvLayer.UV.Count / 2];
            int u = 0;
            for (d = 0; d < uvLayer.UV.Count; )
            {
                UVCoords[u] = new Vector2(uvLayer.UV[d], uvLayer.UV[d + 1]);
                d += 2;
                u++;
            }
            #endregion

            animatedMesh.Skeleton = new DeformerSkeleton();
            animatedMesh.Skeleton.Deformers = new List<Deformer>();

            #region Load DeformerClusters
            List<FXB_DeformerCluster> FBX_DeformerClusters = new List<FXB_DeformerCluster>();
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                if (file.FBX_Objects[i] is FXB_DeformerCluster)
                {
                    FBX_DeformerClusters.Add((FXB_DeformerCluster)file.FBX_Objects[i]);
                }
            }
            #endregion

            //Get Deformer Clusters and Basic Pose
            Deformer deformer;
            FXB_DeformerCluster cluster = new FXB_DeformerCluster();
            BindPose pose;
            List<FBX_ModelLimbNode> limbnodes = new List<FBX_ModelLimbNode>();

            #region FBX_Objects_Getter no.1
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                try
                {
                    if (file.FBX_Objects[i] is FXB_DeformerCluster)
                    {
                        deformer = new Deformer();
                        cluster = (FXB_DeformerCluster)file.FBX_Objects[i];
                        deformer.VertexIndices = new int[cluster.Indexes.Count];
                        deformer.VertexWeights = new float[cluster.Weights.Count];

                        for (int counter = 0; counter < cluster.Indexes.Count; counter++)
                        {
                            deformer.VertexIndices[counter] = cluster.Indexes[counter];
                        }

                        for (int counter = 0; counter < cluster.Weights.Count; counter++)
                        {
                            deformer.VertexWeights[counter] = cluster.Weights[counter];
                        }

                        deformer.DeformerName = cluster.Name.Substring(8);


                        deformer.TransformLink = VectorMath.MatrixRound(new Matrix(
                              cluster.TransformLink[0], cluster.TransformLink[1], cluster.TransformLink[2], cluster.TransformLink[3]
                            , cluster.TransformLink[4], cluster.TransformLink[5], cluster.TransformLink[6], cluster.TransformLink[7]
                            , cluster.TransformLink[8], cluster.TransformLink[9], cluster.TransformLink[10], cluster.TransformLink[11]
                            , cluster.TransformLink[12], cluster.TransformLink[13], cluster.TransformLink[14], cluster.TransformLink[15]
                            ), 5);
                        
                        animatedMesh.Skeleton.Deformers.Add(deformer);
                    }

                    if (file.FBX_Objects[i] is FBX_PoseBindPose)
                    {
                        FBX_PoseBindPose poseFBX = (FBX_PoseBindPose)file.FBX_Objects[i];
                        {
                            for (int hh = 0; hh < poseFBX.PoseNodes.Count; hh++)
                            {
                                pose = new BindPose();
                                pose.JointName = poseFBX.PoseNodes[hh].Node.Substring(7);
                                pose.Matrix = new Matrix(
                                    poseFBX.PoseNodes[hh].matrix[0], poseFBX.PoseNodes[hh].matrix[1], poseFBX.PoseNodes[hh].matrix[2], poseFBX.PoseNodes[hh].matrix[3],
                                    poseFBX.PoseNodes[hh].matrix[4], poseFBX.PoseNodes[hh].matrix[5], poseFBX.PoseNodes[hh].matrix[6], poseFBX.PoseNodes[hh].matrix[7],
                                    poseFBX.PoseNodes[hh].matrix[8], poseFBX.PoseNodes[hh].matrix[9], poseFBX.PoseNodes[hh].matrix[10], poseFBX.PoseNodes[hh].matrix[11],
                                    poseFBX.PoseNodes[hh].matrix[12], poseFBX.PoseNodes[hh].matrix[13], poseFBX.PoseNodes[hh].matrix[14], poseFBX.PoseNodes[hh].matrix[15]);

                                animatedMesh.Bindposes.Add(pose);
                            }
                        }
                    }
                }
                catch
                {

                }

            }
            #endregion

            //left this out.
            #region do stuff
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                if (file.FBX_Objects[i] is FBX_ModelLimbNode)
                {
                    FBX_ModelLimbNode LimbNode = (FBX_ModelLimbNode)file.FBX_Objects[i];
                    {
                        for (int g = 0; g < LimbNode.properties.Count; g++)
                        {
                            for (int f = 0; f < animatedMesh.Skeleton.Deformers.Count; f++)
                            {
                                if (animatedMesh.Skeleton.Deformers[f].DeformerName == LimbNode.Name)
                                {
                                    
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            //Get Textures
            FBX_Texture fbx_tex;
            #region textures
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                if (file.FBX_Objects[i] is FBX_Texture)
                {
                    fbx_tex = new FBX_Texture();
                    fbx_tex = (FBX_Texture)file.FBX_Objects[i];

                    animatedMesh.textureNames.Add(fbx_tex.FileName);
                    animatedMesh.relTextureNames.Add(fbx_tex.RelativeFileName);

                }
            }
            #endregion

            //Do indexing
            #region Just Index throught the Joints to give them an Index to identify

            for (int j = 0; j < animatedMesh.Skeleton.Deformers.Count; j++)
            {
                animatedMesh.Skeleton.Deformers[j].Deformer_Index = j;
                animatedMesh.Skeleton.Deformers[j].ParentDeformer_Index = -1;
            }

            #endregion

            //Also not use this for now
            #region GetD Child joint names for deformers and bindposes
            for (int i = 0; i < file.FBX_Connection.ConnectionProperties.Count; i++)
            {
                for (int j = 0; j < animatedMesh.Bindposes.Count; j++)
                {
                    if (String.Equals(animatedMesh.Bindposes[j].JointName, file.FBX_Connection.ConnectionProperties[i].ParentObjectName))
                    {
                        animatedMesh.Bindposes[j].JointChildName = file.FBX_Connection.ConnectionProperties[i].ChildObjectName;
                    }
                }
            }
            #endregion

            #region Clear connection stuff
            for (int i = 0; i < file.FBX_Connection.ConnectionProperties.Count; i++)
            {
                if (file.FBX_Connection.ConnectionProperties[i].ParentObjectName.Contains("Cluster"))
                {
                    file.FBX_Connection.ConnectionProperties.RemoveAt(i);
                    i--;
                }
                else if (file.FBX_Connection.ConnectionProperties[i].ChildObjectName.Contains("Cluster"))
                {
                    file.FBX_Connection.ConnectionProperties.RemoveAt(i);
                    i--;
                }


            }
            #endregion

            #region Set Hierarchy by Name
            for (int i = 0; i < file.FBX_Connection.ConnectionProperties.Count; i++)
            {
                for (int j = 0; j < animatedMesh.Skeleton.Deformers.Count; j++)
                {
                    if (String.Equals(animatedMesh.Skeleton.Deformers[j].DeformerName,
                        file.FBX_Connection.ConnectionProperties[i].ChildObjectName))
                    {
                        animatedMesh.Skeleton.Deformers[j].ParentDeformerName = file.FBX_Connection.ConnectionProperties[i].ParentObjectName;
                    }
                }
            }

            #endregion

            #region Assign Names/Indices of Deformers to their Parents and Childs, to identify hierarchy
            for (int k = 0; k < animatedMesh.Skeleton.Deformers.Count; k++)
            {
                for (int z = 0; z < animatedMesh.Skeleton.Deformers.Count; z++)
                {
                    animatedMesh.Skeleton.Deformers[z].Depth = -1;
                    if (String.Equals(animatedMesh.Skeleton.Deformers[z].DeformerName, animatedMesh.Skeleton.Deformers[k].ParentDeformerName))
                    {
                        animatedMesh.Skeleton.Deformers[k].ParentDeformerName = animatedMesh.Skeleton.Deformers[z].DeformerName;
                        animatedMesh.Skeleton.Deformers[k].ParentDeformer_Index = animatedMesh.Skeleton.Deformers[z].Deformer_Index;
                    }
                }
            }
            #endregion


            #region Important Step: Give each Deformer a Depth To transform from child up to parent. Just Wtf

            for (int i = 0; i < animatedMesh.Skeleton.Deformers.Count; i++)
            {
                int storedIndex = i;
                int depth = 0;

                do
                {
                    if (depth >= animatedMesh.Skeleton.Deformers[storedIndex].Depth)
                    {
                        animatedMesh.Skeleton.Deformers[storedIndex].Depth = depth;
                    }

                    storedIndex = animatedMesh.Skeleton.Deformers[storedIndex].ParentDeformer_Index;

                    if (storedIndex < 0)
                        break;

                    depth++;
                } while (storedIndex >= 0);
            }
            #endregion

            #region Now reverse the depth, so the biggest number is the deepest child

            int maxdepth = 0;

            //Get greatest value first
            for (int i = 0; i < animatedMesh.Skeleton.Deformers.Count; i++)
            {
                if (animatedMesh.Skeleton.Deformers[i].Depth > maxdepth)
                    maxdepth = animatedMesh.Skeleton.Deformers[i].Depth;
            }

            //Now reverse them
            for (int i = 0; i < animatedMesh.Skeleton.Deformers.Count; i++)
            {
                animatedMesh.Skeleton.Deformers[i].Depth = maxdepth - animatedMesh.Skeleton.Deformers[i].Depth;
            }

            #endregion

            #region Now List each Joint Info

            Console.WriteLine(
                "\n\n");
            for (int i = 0; i < animatedMesh.Skeleton.Deformers.Count; i++)
            {
                Console.WriteLine("Joint {0} has Depth {1} ", i, animatedMesh.Skeleton.Deformers[i].Depth);
            }

            #endregion

            #region Do Mesh data here

            animatedMesh.PointList = new Vector3[Points.Length];

            //Leave this empty, CPu skinned mesh will use different meshformat or so
            animatedMesh.PointList_Dynamic = new Vector3[Points.Length];

            for (int i = 0; i < Points.Length; i++)
            {
                animatedMesh.PointList_Dynamic[i] = Points[i];
                animatedMesh.PointList[i] = Points[i];
            }

            animatedMesh.VertexIndex = Fbx_modelmesh.VertexIndices.ToArray();
            animatedMesh.Normals = Normals;
            animatedMesh.UvIndex = uvIndex;
            animatedMesh.UvCoords = UVCoords;
            animatedMesh.Vertices = new CustomVertex[uvIndex.Length];

            #endregion

            #region create vertex LIst;
            for (int i = 0; i < animatedMesh.VertexIndex.Length; i++)
            {
                animatedMesh.Vertices[i] = new CustomVertex(animatedMesh.PointList[animatedMesh.VertexIndex[i]], animatedMesh.UvCoords[animatedMesh.UvIndex[i]], animatedMesh.Normals[animatedMesh.VertexIndex[i]], Vector3.Zero, Vector3.Zero);
            }

            for (int i = 0; i < animatedMesh.VertexIndex.Length; i++)
            {
                animatedMesh.Vertices[i] = new CustomVertex(animatedMesh.PointList[animatedMesh.VertexIndex[i]], animatedMesh.UvCoords[animatedMesh.UvIndex[i]], animatedMesh.Normals[animatedMesh.VertexIndex[i]], Vector3.Zero, Vector3.Zero);
            }
            #endregion

            #region Initialise Skeleton data
            animatedMesh.Skeleton.Bone_Transforms = new Matrix[animatedMesh.Skeleton.Deformers.Count];
            //animatedMesh.BoneIndices = new Vector4[animatedMesh.PointList.Length];
            //animatedMesh.BoneWeights = new Vector4[animatedMesh.PointList.Length];
            animatedMesh.Skeleton.InverseBindPose = new Matrix[animatedMesh.Skeleton.Deformers.Count];
            animatedMesh.Skeleton.maxDepth = maxdepth;
            #endregion
            
            //Now assign it to the actual _Model , instead of using AnimatedMOdelmesh


            model.vertices = new Vertex_GpuSkin[animatedMesh.Vertices.Length];
            for (int i = 0; i < animatedMesh.Vertices.Length; i++)
            {
                int index = animatedMesh.VertexIndex[i];

                if (index >= animatedMesh.PointList.Length)
                    index = 0;

                model.vertices[i].Position = animatedMesh.PointList[index];
                model.vertices[i].Normal = animatedMesh.Normals[i];
                model.vertices[i].TextureCoordinate = animatedMesh.UvCoords[animatedMesh.UvIndex[i]];
                //model.vertices[i]. = animatedMesh.Vertices[i].Position;
            }

            model.col = animatedMesh.relTextureNames[0];

            #region norm bump isnt a must
            try
            {
                model.norm = animatedMesh.relTextureNames[1];
            }
            catch { }
            try
            {
                model.spec = animatedMesh.relTextureNames[2];
            }
            catch { }
            #endregion

            model.deformers = new _Deformer[animatedMesh.Skeleton.Deformers.Count];

            for(int i = 0; i < animatedMesh.Skeleton.Deformers.Count; i++)
            {
                model.deformers[i].index = animatedMesh.Skeleton.Deformers[i].Deformer_Index;
                model.deformers[i].parent_index = animatedMesh.Skeleton.Deformers[i].ParentDeformer_Index;
                model.deformers[i].Parent = animatedMesh.Skeleton.Deformers[i].ParentDeformerName;
                model.deformers[i].Name = animatedMesh.Skeleton.Deformers[i].DeformerName;                
            }


            vertex_Weightindex_List = new List<int>[animatedMesh.VertexIndex.Length];
            vertex_WeightWeight_List = new List<float>[animatedMesh.VertexIndex.Length];
            for(int i = 0; i < animatedMesh.VertexIndex.Length; i++)
            {
                vertex_Weightindex_List[i] = new List<int>();
                vertex_WeightWeight_List[i] = new List<float>();

            }
            

            for (int i = 0; i < animatedMesh.Skeleton.Deformers.Count; i++)
            {
                for (int j = 0; j < animatedMesh.Skeleton.Deformers[i].VertexIndices.Length; j++)
                {
                    int currentIndex = animatedMesh.Skeleton.Deformers[i].VertexIndices[j];
                    float currentweight = animatedMesh.Skeleton.Deformers[i].VertexWeights[j];
                    //Now put the index into the points, not the deformers anymore

                    vertex_Weightindex_List[currentIndex].Add(currentIndex);
                    vertex_WeightWeight_List[currentIndex].Add(currentweight);
                }
            }

            for (int i = 0; i < model.vertices.Length; i++)
            {
                for(int j = 0; j < vertex_Weightindex_List[i].Count; j++)
                {
                    if(j == 0)
                        model.vertices[i].vertexWeightIndex.X = vertex_Weightindex_List[i][0];
                    if(j == 1)
                        model.vertices[i].vertexWeightIndex.Y = vertex_Weightindex_List[i][1];
                    if(j == 2)
                        model.vertices[i].vertexWeightIndex.Z = vertex_Weightindex_List[i][2];
                    if(j == 3)
                        model.vertices[i].vertexWeightIndex.W = vertex_Weightindex_List[i][3];

                    if(j == 0)
                        model.vertices[i].vertexWeight.X = vertex_WeightWeight_List[i][0];
                    if(j == 1)
                        model.vertices[i].vertexWeight.Y = vertex_WeightWeight_List[i][1];
                    if(j == 2)
                        model.vertices[i].vertexWeight.Z = vertex_WeightWeight_List[i][2];
                    if(j == 3)
                        model.vertices[i].vertexWeight.W = vertex_WeightWeight_List[i][3];
                }
            }

            return model;
        }
    }



    #region Materials and Shaders
    public interface IMaterial
    {
        
    }

    public class Phong : IMaterial
    {
        public String DiffuseMap = String.Empty;
        public float[] EmissiveColor = new float[]{1,1,1};
        public float EmissiveFactor = 1;
        public float[] AmbientColor = new float[] { 1, 1, 1 };
        public float AmbientFactor = 1;
        public float[] DiffuseColor = new float[] { 1, 1, 1 };
        public float DiffuseFactor = 1;

        public float[] TransparentColor = new float[] { 0, 0, 0 };
        public float TransparencyFactor = 0;
        
        public String BumpMap = String.Empty;
        public float[] Bump = new float[]{0.5f,0.5f,0.5f};
        
        public String SpecularMap = String.Empty;
        public float[] SpecularColor = new float[] { 0, 0, 0 };
        public float SpecularFactor = 0;
        public float ShininessExponent = 0;
        public float[] ReflectionColor = new float[] { 1, 1, 1 };
        public float ReflectionFactor = 0;
        public float[] Emissive = new float[] { 1, 1, 1 };
        public float[] Ambient = new float[] { 1, 1, 1 };
        public float[] Diffuse = new float[] { 1, 1, 1 };
        public float[] Specular = new float[] { 1, 1, 1 };
        public float Shininess = 0;
        public float Opacity = 1;
        public float Reflectivity = 0;

        public Phong(){}
    }

    
    public class Material
    {
        public string shaderName;
        public string texturePath;
        public string bumppath;
        public string specularpath;
        Vector3own kd;
        Vector3own ka;
        Vector3own tf;
        Single ni;
        Vector3own ks;
        Single ns;
        public Single bumpfactor;

        public Material(string shaderName_param, string texPath)
        {
            this.shaderName = shaderName_param;
            this.texturePath = texPath;
            this.specularpath = string.Empty;
            this.bumppath = string.Empty;

        }
        public Material(string shaderName_param, string texPath, string b_path, string specpath)
        {
            this.shaderName = shaderName_param;
            this.texturePath = texPath;
            this.bumppath = b_path;
            this.specularpath = specpath;
        }
        public Material()
        {

        }


    }
    #endregion

    #region Model only Meshes

    public class Deformer
    {
        //Use Indices instead of names.. Better performance
        public String DeformerName, ParentDeformerName;
        public int Deformer_Index, ParentDeformer_Index;
        //public Vector3 LCL_Rotation, LCL_Translation, PreRotation, LCL_Scaling;   Same for this stuff
        public Matrix TransformLink, TransformAnimated;                              //Not use transform or Bindpose Yet

        public int Depth; //0 is most parent, -> max is most child

        public int[] VertexIndices; //Dont use them for GPU Skinning, the mesh contains these lists..
        public float[] VertexWeights; //Dont use for GPU Skinning
    }

    public class BindPose
    {
        public string JointParentName;
        public string JointChildName;
        public string JointName;
        public Matrix Matrix;
    }
    
    public class DeformerSkeleton
    {
        //Hierarchically transformed of course.
        public Matrix[] Bone_Transforms;
        public Matrix[] InverseBindPose; //inverse of transformlink from each joint,not hierarchical

        public int maxDepth;

        public List<Deformer> Deformers;
    }

    public class JointData
    {
        public String JointName;
        public Vector3 Translation;
        public Vector3 Rotation;
        public Vector3 Scale;
        public Matrix matrix;
    }

    public class Frame
    {
        public List<JointData> JointData;

        public Frame()
        {
            JointData = new List<JointData>();
        }
    }

    public class AnimatedModelMesh : IMesh
    {
        public Vector3 base_rotation_optional;
        public List<string> textureNames = new List<string>();
        public List<string> relTextureNames = new List<string>();
        public List<BindPose> Bindposes;

        public DeformerSkeleton Skeleton;

        public Texture2D DiffuseTexture;
        public Texture2D NormalTexture;
        public Texture2D SpecularTexture;

        //---------------------Mesh data-----------------------//
        public Vector3[] PointList;
        public Vector3[] PointList_Dynamic;
        public int[] VertexIndex;

        public Vector2[] UvCoords;
        public int[] UvIndex;

        public Vector3[] Normals;
        //-----------------------------------------------------//

        //Array = VertexCount, caries a list of connected deformers
        //For Hitbox collision
        
        public List<String>[] perVertex_DeformerRelation; 
        //--------------------------------------------------//

        public CustomVertex[] Vertices; //Dont transform these on gpu skinning        

    }

    public class StaticModelMesh
    {
        public List<string> textureNames = new List<string>();
        public List<string> reltextureNames = new List<string>();

        public Texture2D DiffuseTexture;
        public Texture2D NormalTexture;
        public Texture2D SpecularTexture;

        //---------------------Mesh data-----------------------//
        public Vector3[] PointList;
        public int[] VertexIndex;

        public Vector2[] UvCoords;
        public int[] UvIndex;

        public Vector3[] Normals;
        //-----------------------------------------------------//

        public CustomVertex[] Vertices; //Dont transform these on gpu skinning
    }    
    #endregion
        
    public struct CustomVertex : IVertexType
    {
        public Vector3 vertexPosition;
        public Vector3 vertexNormal;
        public Vector2 vertexTextureCoordinate;
        public Vector3 vertexTangent;
        public Vector3 vertexBinormal;        

        public Vector3 Position
        {
            get { return vertexPosition; }
        }
        public Vector3 Normal
        {
            get { return vertexNormal; }
        }
        public Vector2 TextureCoordinate
        {
            get { return vertexTextureCoordinate; }
        }
        public Vector3 BiNormal
        {
            get { return vertexBinormal; }
        }
        public Vector3 Tangent
        {
            get { return vertexTangent; }
        }

        public readonly static VertexDeclaration vertexDeclaration = new VertexDeclaration(

       new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
       new VertexElement(3 * sizeof(Single), VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
       new VertexElement(6 * sizeof(Single), VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
       new VertexElement(8 * sizeof(Single), VertexElementFormat.Vector3, VertexElementUsage.Binormal, 0),
       new VertexElement(11 * sizeof(Single), VertexElementFormat.Vector3, VertexElementUsage.Tangent, 0)
       );

        public CustomVertex(Vector3 pos, Vector2 textureCoordinate, Vector3 normal, Vector3 binorm, Vector3 tang)
        {
            vertexPosition = pos;
            vertexTextureCoordinate = textureCoordinate;
            vertexNormal = normal;
            vertexTangent = tang;
            vertexBinormal = binorm;

        }

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return vertexDeclaration; }
        }
    }
            
    #region CustomVertex Only Positon and UV!
    public struct EffectVertex : IVertexType
    {
        public Vector3 vertexPosition;
        public Vector2 vertexTextureCoordinate;


        public Vector3 Position
        {
            get 
            { 
                return vertexPosition; 
            }
        }
       
        public Vector2 TextureCoordinate
        {
            get { return vertexTextureCoordinate; }
        }
        
        public readonly static VertexDeclaration vertexDeclaration = new VertexDeclaration(
           new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),       
           new VertexElement(3 * sizeof(Single), VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0)   
       );


        public EffectVertex(Vector3 pos, Vector2 textureCoordinate)
        {
            vertexPosition = pos;
            vertexTextureCoordinate = textureCoordinate;
        }

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return vertexDeclaration; }
        }
    }

    #endregion

    #region Map related Meshes

    public class BlockAsString
    {
        public List<String> BlockLines;

        public BlockAsString()
        {
            BlockLines = new List<String>();
        }
    }
    
    /// <summary>
    /// This is used in Map extraction in the first step, it simply puts 1 mesh with a texture and shader inside!
    /// </summary>
 
    public class MeshGroup : IMesh
    {

        public bool transparencyFlag = false;
        public CustomVertex[] vertices;
        public Material material;
        public Texture2D TextureDiffuse, TextureBump, TextureSpecular;
        public Phong Phong;
        public string GroupName;
        public Vector3 Position;
        public float MaxRadius;

        public MeshGroup()
        {
            material = new Material();
            Phong = new Phong();
        }

        public void CreateBuffers(GraphicsDevice dev, int vertexCount)
        {            
        }
    }
    
    /// <summary>
    /// This is a, in second step, the mesh is cut into wallpolygons( 90 degree ) and floor polygons, which are any other!
    /// This is needed because the engine will treat both mesh(types) with a different calculation.
    /// </summary>
    /// 

    public class Block
    {
        #region constructors
        public Block(ModelBlockFloor floor, ModelBlockWall wall)
        {
            this.floorModel = floor;
            this.wallModel = wall;
        }
        public Block(string group, ModelBlockFloor floor, ModelBlockWall wall)
        {
            this.group = group;
            this.floorModel = floor;
            this.wallModel = wall;
        }
        public Block() { }
        #endregion;
        public String group;

        public ModelBlockFloor floorModel;
        public ModelBlockWall wallModel;
    }

    /// <summary>
    /// Wall polygons go here, even after seperation, they will keep their material, so they can be rendered with their correct texture
    /// </summary>

    public class ModelBlockWall : IMesh
    {
        public Vector3 CenterPosition;
        public float RadiusFromCenter;
        public CustomVertex[] vertices;
        public Vector3[] triangleCenters; //one Center for each 3 triangles
        public float[] maxTriangleRadius;
        public Material material;
        public string group;
    }

    /// <summary>
    /// Floor polygons go here, they keep their materials aswell!!
    /// </summary>

    public class ModelBlockFloor : IMesh
    {
        public Vector3 CenterPosition;
        public float RadiusFromCenter;
        public CustomVertex[] vertices;
        public Vector3[] triangleCenters; //one Center for each 3 triangles
        public float[] maxTriangleRadius;
        public Material material;
        public string group;
    }
    

    class Vector3own
    {
        public Single xCoord
        {
            get { return xCoord; }
            set { xCoord = value; }
        }

        public Single yCoord
        {
            get { return yCoord; }
            set { yCoord = value; }
        }

        public Single zCoord
        {
            get { return zCoord; }
            set { zCoord = value; }
        }

        public Vector3own(Single X, Single Y, Single Z)
        {
            xCoord = X; yCoord = Y; zCoord = Z;
        }

        public Vector3own()
        {

        }
    }


    class Vector2own
    {
        public Single xCoord
        {
            get { return xCoord; }
            set { xCoord = value; }
        }

        public Single yCoord
        {
            get { return yCoord; }
            set { yCoord = value; }
        }

        public Vector2own(Single X, Single Y)
        {
            xCoord = X; yCoord = Y;
        }

        public Vector2own()
        {

        }


    }

    public interface IMesh
    {
    }

    public class Point_PhysicObject
    {
        public Vector3 Position;
        public Single Mass;
        public Vector3 Impulse;
        public Vector3 Speed;

        public Point_PhysicObject(Vector3 pos, float mass, Vector3 speed)
        {
            this.Speed = speed;
            this.Mass = mass;
            this.Position = pos;
        }

        public Vector3 getImpulse()
        {
            return Mass * Speed;
        }

        public float getKineticEnergy()
        {
            return (VectorMath.getScalarProduct(getImpulse(), Speed) / 2);
        }


    }

    public class PhysicsModel : IMesh
    {
        public Vector3 Pulse;
        public Single Mass;
        public string GroupName;
        public MeshGroup visualMesh;
        public MeshGroup physicsMesh;
        public Vector3 Rotation;
        public Vector3 Position;
        public Matrix MeshMatrix;
        private Vector3 LookAt;
        private Single[] FallCounter;
        private Single[] CornerHeights;
                
        public PhysicsModel()
        {
            LookAt = Vector3.Forward;
            Pulse = Vector3.Zero;
            Mass = 1;
            Rotation = Vector3.Zero;
            Position = Vector3.Zero;            
            UpdateMatrix();
        }        

        public void UpdateMatrix()
        {
            MeshMatrix = Matrix.CreateWorld(Position, VectorMath.Vector3_Rotate(LookAt, Rotation), VectorMath.Vector3_Rotate(Vector3.Up, Rotation)); 
        }
    }
    #endregion
}
