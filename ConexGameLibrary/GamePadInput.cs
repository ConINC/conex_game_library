﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.DirectInput;

namespace ConexGameLibrary
{
    //Class for Joystick Input

    /// <summary>
    /// This will allow Joystick Input for max 4 Gamepads!
    /// </summary>
    public struct Button_Info
    {
        public int RawOffset;
        public float Value;
    }

    public class JoystickData
    {
        public Guid currentGuid;
        public int playerNumber;

        #region This Area is for The MappedKeys

        public Button_Info 
            /*Game control*/ Start, Select,
            /*Dpad*/ DpadUp, DpadRight, DpadDown, DpadLeft,
            /*Buttons Front*/ ButtonTriangle, ButtonCircle, ButtonCross, ButtonSquare,
            /*Stick left*/ StickLeftX, StickLeftY, StickLeftPress,
            /*Stick Right*/ StickRightX, StickRightY, StickRightPress,
            /*UpperButtons*/ ButtonLeft1, ButtonLeft2, ButtonRight1, ButtonRight2;

        #endregion

        public JoystickData()
        {     
            currentGuid = Guid.Empty;
            //Function Buttons
            Start = new Button_Info(); Select = new Button_Info();
            Start.Value = 0; Select.Value = 0;

            //Dpad
            DpadUp = new Button_Info(); DpadRight = new Button_Info();
            DpadUp.Value = -1; DpadRight.Value = -1;

            DpadDown = new Button_Info(); DpadLeft = new Button_Info();
            DpadDown.Value = -1; DpadLeft.Value = -1;

            //Buttons
            ButtonTriangle = new Button_Info(); ButtonCircle = new Button_Info();
            ButtonCross = new Button_Info(); ButtonSquare = new Button_Info();
            ButtonTriangle.Value = -1; ButtonCircle.Value = -1;
            ButtonCross.Value = -1; ButtonSquare.Value = -1;

            //Left Stick
            StickLeftX = new Button_Info(); StickLeftY = new Button_Info(); StickLeftPress = new Button_Info();
            StickLeftX.Value = ushort.MaxValue/2; StickLeftY.Value = ushort.MaxValue/2; StickLeftPress.Value = -1;

            //Right Stick
            StickRightX = new Button_Info(); StickRightY = new Button_Info(); StickRightPress = new Button_Info();
            StickRightX.Value = ushort.MaxValue / 2; StickRightY.Value = ushort.MaxValue/2; StickRightPress.Value = -1;

            //Shoulder buttons
            ButtonLeft1 = new Button_Info(); ButtonLeft2 = new Button_Info();
            ButtonLeft1.Value = -1; ButtonLeft2.Value = -1;

            ButtonRight1 = new Button_Info(); ButtonRight1 = new Button_Info();
            ButtonRight2.Value = -1; ButtonRight2.Value = -1;

        }

        public JoystickUpdate[] perJoystick_update;
    }

    /// <summary>
    /// This Class will only handle the initialisaiton and data-recieving
    /// WILL be implemented as Singleton!.. Obvious
    /// </summary>
    public class Joystick_Input
    {
        public static JoystickData[] joystickData;

        private static Joystick_Input JoystickInputInstance;

        private int playerCount = 0;
        //Make Joysticks
        private Joystick[] joysticks;

        private Guid[] joystickGuids;
        public List<Guid> guids = new List<Guid>();

        private DirectInput[] directInputs;

        private DeviceInstance[] deviceInstancesArray;

        public static Joystick_Input Current
        {
            get
            {
                if (JoystickInputInstance == null)
                {
                    JoystickInputInstance = new Joystick_Input();
                }
                return JoystickInputInstance;
            }
        }

        //Konstruktor
        public Joystick_Input()
        {
            //Tu nichts, da es über Initialise läuft
        }

        public void initialiseJoysticks()
        {
            #region List initialisation
            //Create List objects
            joysticks = new Joystick[4];
            joystickData = new JoystickData[4];
            joystickGuids = new Guid[4];
            directInputs = new DirectInput[4];
            deviceInstancesArray = new DeviceInstance[4];

            joystickData[0] = new JoystickData();
            joystickData[1] = new JoystickData();
            joystickData[2] = new JoystickData();
            joystickData[3] = new JoystickData();
            #endregion

            //-----------------------------------------------
            //Fill lists first
            directInputs[0] = new DirectInput();
            directInputs[1] = new DirectInput();
            directInputs[2] = new DirectInput();
            directInputs[3] = new DirectInput();

            joystickGuids[0] = Guid.Empty;
            joystickGuids[1] = Guid.Empty;
            joystickGuids[2] = Guid.Empty;
            joystickGuids[3] = Guid.Empty;

            deviceInstancesArray[0] = new DeviceInstance();
            deviceInstancesArray[1] = new DeviceInstance();
            deviceInstancesArray[2] = new DeviceInstance();
            deviceInstancesArray[3] = new DeviceInstance();
            //-----------------------------------------------

            //Now iterate throught devices to get the joysticks
            for (int innerCounter = 0; innerCounter < 4; innerCounter++)
            {
                try
                {
                    deviceInstancesArray[innerCounter] = directInputs[innerCounter].GetDevices(DeviceType.Gamepad, DeviceEnumerationFlags.AllDevices)[innerCounter];
                    Guid tempGuid = deviceInstancesArray[innerCounter].InstanceGuid;
                    joystickGuids[innerCounter] = tempGuid;
                    guids.Add(tempGuid);
                    Console.WriteLine("Gamepad {0} found with Guid {1}", innerCounter, joystickGuids[innerCounter].ToString());
                    continue;
                }
                catch { }

                try
                {
                    deviceInstancesArray[innerCounter] = directInputs[innerCounter].GetDevices(DeviceType.Device, DeviceEnumerationFlags.AllDevices)[innerCounter];
                    Guid tempGuid = deviceInstancesArray[innerCounter].InstanceGuid;
                    joystickGuids[innerCounter] = tempGuid;
                    guids.Add(tempGuid);
                    Console.WriteLine("Device {0} found with Guid {1}", innerCounter, joystickGuids[innerCounter].ToString());
                    continue;
                }
                catch { }

                try
                {
                    deviceInstancesArray[innerCounter] = directInputs[innerCounter].GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices)[innerCounter];
                    Guid tempGuid = deviceInstancesArray[innerCounter].InstanceGuid;
                    joystickGuids[innerCounter] = tempGuid;
                    guids.Add(tempGuid);
                    Console.WriteLine("Joystick {0} found with Guid {1}", innerCounter, joystickGuids[innerCounter].ToString());
                    continue;
                }
                catch { }
            }

            Console.WriteLine(guids.Count);
            playerCount = guids.Count;

            // Instantiate the joystick
            for (int i = 0; i < guids.Count; i++)
                joysticks[i] = new Joystick(directInputs[i], guids[i]);

            // Query all suported ForceFeedback effects            
            IList<EffectInfo>[] allEffectsArray = new IList<EffectInfo>[guids.Count];

            for (int i = 0; i < guids.Count; i++)
            {
                if (joysticks[i] != null)
                {
                    allEffectsArray[i] = joysticks[i].GetEffects();
                    joysticks[i].Properties.BufferSize = 128;
                    joysticks[i].Acquire();
                }
            }
        }

        //Incase of unplugging or so, do this step to reacquire 
        public void releaseGamepads()
        {
            for (int i = 0; i < joysticks.Length; i++)
            {
                joysticks[i].Dispose();
                joysticks[i] = null;
                joysticks = null;
            }
        }

        //return the public 
        public Joystick[] Joystick_List
        {
            get { return joysticks; }
        }

        public int PlayerCount
        {
            get { return playerCount; }
        }

        public JoystickData[] JoystickData
        {
            get { return joystickData; }
            set { JoystickData = joystickData; }
        }

        public JoystickData[] Get_JoystickData()
        {
            for (int i = 0; i < PlayerCount; i++)
            {
                joystickData[i].playerNumber = i;

                JoystickData[i].perJoystick_update = Joystick_List[i].GetBufferedData();
                JoystickData[i].playerNumber = i;
                joystickData[i].currentGuid = guids[i];

                for (int j = 0; j < JoystickData[i].perJoystick_update.Length; j++)
                {
                    if (joystickData[i].perJoystick_update[j].RawOffset == 55)
                    {
                        joystickData[i].ButtonRight2.RawOffset = 55;
                        joystickData[i].ButtonRight2.Value = joystickData[i].perJoystick_update[j].Value;
                    }

                    if (joystickData[i].perJoystick_update[j].RawOffset == 4)
                    {
                        joystickData[i].StickLeftY.RawOffset = 4;
                        joystickData[i].StickLeftY.Value = joystickData[i].perJoystick_update[j].Value;
                    }

                    if (joystickData[i].perJoystick_update[j].RawOffset == 0)
                    {
                        joystickData[i].StickLeftX.RawOffset = 0;
                        joystickData[i].StickLeftX.Value = joystickData[i].perJoystick_update[j].Value;
                    }

                    if (joystickData[i].perJoystick_update[j].RawOffset == 24)
                    {
                        joystickData[i].StickRightY.RawOffset = 24;
                        joystickData[i].StickRightY.Value = joystickData[i].perJoystick_update[j].Value;
                    }

                    if (joystickData[i].perJoystick_update[j].RawOffset == 20)
                    {
                        joystickData[i].StickRightX.RawOffset = 20;
                        joystickData[i].StickRightX.Value = joystickData[i].perJoystick_update[j].Value;
                    }

                    if (joystickData[i].perJoystick_update[j].RawOffset == 57)
                    {
                        joystickData[i].Start.RawOffset = 57;
                        joystickData[i].Start.Value = joystickData[i].perJoystick_update[j].Value;
                    }

                    if (joystickData[i].perJoystick_update[j].RawOffset == 48)
                    {
                        joystickData[i].ButtonCross.RawOffset = 48;
                        joystickData[i].ButtonCross.Value = joystickData[i].perJoystick_update[j].Value;
                    }

                    if (joystickData[i].perJoystick_update[j].RawOffset == 52)
                    {
                        joystickData[i].ButtonLeft1.RawOffset = 52;
                        joystickData[i].ButtonLeft1.Value = joystickData[i].perJoystick_update[j].Value;
                    }

                    if (joystickData[i].perJoystick_update[j].RawOffset == 53)
                    {
                        joystickData[i].ButtonLeft2.RawOffset = 53;
                        joystickData[i].ButtonLeft2.Value = joystickData[i].perJoystick_update[j].Value;
                    }


                    if (joystickData[i].perJoystick_update[j].RawOffset == 55)
                    {
                        joystickData[i].ButtonRight2.RawOffset = 55;
                        joystickData[i].ButtonRight2.Value = joystickData[i].perJoystick_update[j].Value;
                    }

                    if (joystickData[i].perJoystick_update[j].RawOffset == 54)
                    {
                        joystickData[i].ButtonRight1.RawOffset = 54;
                        joystickData[i].ButtonRight1.Value = joystickData[i].perJoystick_update[j].Value;
                    }
                    
                }
            }

            return JoystickData;
        }

        //This function is quite big, it will be used to map the gamepads buttons
        //To the right action, independant from the gamepad
        public void Joystick_MapData()
        {
            //The ButtonMaps will be loaded via text
        }
    }
}
