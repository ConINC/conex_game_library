﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using ConexGameLibrary;

namespace ConexGameLibrary
{
    public static class ModelLoader
    {
        public static StaticModelMesh LoadMeshFromFBX(GeneralGameData gamedat, String relativePath)
        {
            StaticModelMesh mesh = new StaticModelMesh();
            
            Vector3[] Points;
            int[] vertexIndex = new int[0];

            Vector3[] Normals;

            Vector2[] UVCoords;
            int[] uvIndex;
            
            Vector3 pos_origin = Vector3.Zero;

            FBX_File_Struct file = FBX_Loader.Create_FBX_File_Struct(gamedat.GameDirectory + "//" + relativePath, 0);

            #region Get vertexIndices and Points

            FBX_ModelMesh Fbx_modelmesh = new FBX_ModelMesh();
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                if (file.FBX_Objects[i] is FBX_ModelMesh)
                {
                    Fbx_modelmesh = (FBX_ModelMesh)file.FBX_Objects[i];
                    for (int zz = 0; zz < Fbx_modelmesh.properties.Count; zz++)
                    {
                        if (Fbx_modelmesh.properties[zz].Description.Contains("Lcl Translation"))
                        {
                            pos_origin = new Vector3((float)Fbx_modelmesh.properties[zz].value[0], (float)Fbx_modelmesh.properties[zz].value[1], (float)Fbx_modelmesh.properties[zz].value[2]);

                        }
                    }
                }
            }

            mesh.VertexIndex = new int[Fbx_modelmesh.VertexIndices.Count];
            Points = new Vector3[Fbx_modelmesh.Vertices.Count / 3];

            int d = 0; int v = 0;
            for (; d < Fbx_modelmesh.Vertices.Count; )
            {
                Points[v] = new Vector3(Fbx_modelmesh.Vertices[d], Fbx_modelmesh.Vertices[d + 1], Fbx_modelmesh.Vertices[d + 2]);
                Points[v] += pos_origin;

                v++;
                d += 3;
            }

            for (int i = 0; i < Fbx_modelmesh.VertexIndices.Count; i++)
            {
                mesh.VertexIndex[i] = Fbx_modelmesh.VertexIndices[i];
                vertexIndex = mesh.VertexIndex;

            }
            #endregion

            #region Get Normal Vectors

            FBX_LayerElementNormal normalLayer;
            normalLayer = new FBX_LayerElementNormal();
            for (int i = 0; i < Fbx_modelmesh.LayerContainer.Layers.Count; i++)
            {
                if (Fbx_modelmesh.LayerContainer.Layers[i] is FBX_LayerElementNormal)
                {
                    normalLayer = (FBX_LayerElementNormal)Fbx_modelmesh.LayerContainer.Layers[i];
                }
            }

            Normals = new Vector3[normalLayer.Normals.Count / 3];
            d = 0; v = 0;
            for (; d < normalLayer.Normals.Count; )
            {
                Normals[v] = new Vector3(normalLayer.Normals[d], normalLayer.Normals[d + 1], normalLayer.Normals[d + 2]);

                v++;
                d += 3;
            }


            #endregion

            #region Get UV coords

            FBX_LayerElementUV uvLayer = new FBX_LayerElementUV();
            for (int i = 0; i < Fbx_modelmesh.LayerContainer.Layers.Count; i++)
            {
                if (Fbx_modelmesh.LayerContainer.Layers[i] is FBX_LayerElementUV)
                {
                    uvLayer = (FBX_LayerElementUV)Fbx_modelmesh.LayerContainer.Layers[i];
                }
            }


            uvIndex = new int[uvLayer.UVIndex.Count];
            v = 0;
            for (; v < uvLayer.UVIndex.Count; )
            {
                uvIndex[v] = uvLayer.UVIndex[v];
                v++;
            }

            UVCoords = new Vector2[uvLayer.UV.Count / 2];
            int u = 0;
            for (d = 0; d < uvLayer.UV.Count; )
            {
                UVCoords[u] = new Vector2(uvLayer.UV[d], uvLayer.UV[d + 1]);
                d += 2;
                u++;
            }
            #endregion
            
            //Get Textures
            FBX_Texture fbx_tex;
            #region textures
            for (int i = 0; i < file.FBX_Objects.Count; i++)
            {
                if (file.FBX_Objects[i] is FBX_Texture)
                {
                    fbx_tex = new FBX_Texture();
                    fbx_tex = (FBX_Texture)file.FBX_Objects[i];

                    mesh.textureNames.Add(gamedat.GameDirectory + "//" + fbx_tex.RelativeFileName);
                    mesh.reltextureNames.Add(fbx_tex.RelativeFileName);

                }
            }

            #endregion

            #region Do Mesh data here
            mesh.PointList = new Vector3[Points.Length];

            for (int i = 0; i < Points.Length; i++)
            {
                mesh.PointList[i] = Points[i];
            }

            mesh.VertexIndex = vertexIndex;
            mesh.Normals = Normals;
            mesh.UvIndex = uvIndex;
            mesh.UvCoords = UVCoords;
            mesh.Vertices = new CustomVertex[uvIndex.Length];
            #endregion

            #region create vertex LIst;
            for (int i = 0; i < mesh.VertexIndex.Length; i++)
            {
                mesh.Vertices[i] = new CustomVertex(mesh.PointList[mesh.VertexIndex[i]], mesh.UvCoords[mesh.UvIndex[i]], mesh.Normals[mesh.VertexIndex[i]], Vector3.Zero, Vector3.Zero);
            }
            #endregion

            return mesh;
        }
    }
}
