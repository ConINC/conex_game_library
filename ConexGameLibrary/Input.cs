using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using SharpDX.DirectInput;
using SharpDX;

namespace ConexGameLibrary
{
    /*
    class InputJoystick
    {
        /// <summary>
        /// Class to handle Gamepad Input so far
        /// </summary>        

        public bool initJoystick()
        {
            var directInput = new DirectInput();
            List<Guid> joystickGuids = new List<Guid>();
            

            //This loops throught gamepad devices and adds them to the list
            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
            {
                var joystickGuid = deviceInstance.InstanceGuid;
                joystickGuids.Add(joystickGuid);
            }

            if (joystickGuids.Count > 0)
            {
                foreach (Guid guid in joystickGuids)
                {
                    if (guid == Guid.Empty)
                    {
                        //Returns false if no Joystick can be found
                        Console.WriteLine("No Joystick could be found");
                        return false;
                    }
                }
            }

            else
            {
                // instance the joystick
                Joystick[] joysticks = new Joystick[joystickGuids.Count -1];

                byte counter = 0;
                foreach (Joystick joystick in joysticks)
                {
                    joysticks[counter] = new Joystick(directInput,joystickGuids[counter]);
                    counter++;
                }

                //create effectInfo List
                var allEffects = new List<EffectInfo>();

                foreach (Joystick joystick in joysticks)
                {
                    allEffects.Add(new EffectInfo());
                }

                //Query all forcefeedback effects
                byte effectCount = 0;
                foreach(Guid guid in joystickGuids)
                {
                    allEffects[effectCount] = joysticks[effectCount].GetEffectInfo(joystickGuids[effectCount]);
                    effectCount++;
                }



                foreach (var effectinfo in allEffects)
                {
                    Console.WriteLine("Effect available {0}", effectinfo.Name);
                }

                
                foreach(var effectinfo in allEffects)
                    Console.WriteLine("effect available {0}", effectinfo.Name);
                foreach (var effectinfo in allEffects)
                    Console.WriteLine("effects availbable {0}", effectinfo.Name);
                

                //set buffer size to use buffered data
                foreach (Joystick joystick in joysticks)
                {
                    joystick.Properties.BufferSize = 128;
                    joystick.Acquire();
                }

                //poll events from the joystick

                while (true)
                {
                    foreach (Joystick joystick in joysticks)
                    {
                        joystick.Poll();

                        var datas = joystick.GetBufferedData();

                        foreach (var state in datas)
                            Console.WriteLine("Key/Button pressed: {0} from Joystick {1}  ", state, joystick.Information);
                    }
                }
            }
            return true;
        }

    }
    */
    
}
