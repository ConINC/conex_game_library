﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConexGameLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ConexGameLibrary
{
    public class MM_ScreenPerspectives
    {
        public Matrix[] perspectives;
        public int[,] persp_size;

        public MM_ScreenPerspectives(int viewport_x, int viewport_y)
        {
            perspectives = new Matrix[4];
        }
    }

    public struct ScreenSettings
    {
        public RasterizerState[] rasterizers;
        public int EnableInput_screen;

        public bool SingleScreen;
        public int focusScreenIndex;
        public String[] identifierText;
        // 0 = top, 1 = persp, 2 = side, 3 = front
        public bool[] wireframe_screens;      
    }
    
    public class MM_Viewports
    {
        public Viewport[] viewports;
        public Viewport FullViewPort;
        
        public MM_Viewports(int width, int height, int fullw,int fullh)
        {
            this.FullViewPort = new Viewport(0,0,fullw, fullh);
            viewports = new Viewport[4];
            viewports[0] = new Viewport(new Rectangle(0, 0, width, height)); //Top usually
            viewports[1] = new Viewport(new Rectangle(width, 0, width, height)); //Persp usually
            viewports[2] = new Viewport(new Rectangle(0, height, width, height)); // Left 
            viewports[3] = new Viewport(new Rectangle(width, height, width, height)); // Front
        }

    }

    public class MM_Views
    {
        public Matrix persp_view, top_view,side_view, front_view;
    }

    public class MM_Camera
    {
        public Matrix View;
        public Matrix Perspective;
        public Vector3 Position;

        public Vector3 LookAt_Static, LookAt_Sideway_static;
        public Vector3 UpVectorStatic = Vector3.Up;
        public Vector3 LookDirection, LookAtSide;
        public Vector3 upDir;
        public Vector3 Rotation;        

        KeyboardState ks;
        
        public MM_Camera(Matrix perps, Vector3 Position, Vector3 rotation, Vector3 LookAtBegin, Vector3 sidewaystatic, Vector3 upDirection)
        {
            upDir = upDirection;
            LookAt_Sideway_static = sidewaystatic;

            this.LookAt_Static = LookAtBegin;
            this.Perspective = perps;
            this.Position = Position;
            this.Rotation = rotation;
            

            UpdateCamera();
        }

        public void HandleInput(int width, int height)
        {
            ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Up))
            {
                Rotation.X += 0.05f;
            }
            if (ks.IsKeyDown(Keys.Down))
            {
                Rotation.X -= 0.05f;
            }
            if (ks.IsKeyDown(Keys.Left))
            {
                Rotation.Y += 0.05f;
            }
            if (ks.IsKeyDown(Keys.Right))
            {
                Rotation.Y -= 0.05f;
            }


            #region keyboard Input
            

            if (ks.IsKeyDown(Keys.W))
            {
                this.Position += LookDirection;
            }

            if (ks.IsKeyDown(Keys.S))
            {
                this.Position -= LookDirection;
            }
            
            if (ks.IsKeyDown(Keys.D))
            {
                this.Position += LookAtSide;
            }

            if (ks.IsKeyDown(Keys.A))
            {
                this.Position -= LookAtSide;
            }           
            #endregion
        }

        public void UpdateCamera()
        {
            Matrix transformMatrix = Matrix.CreateRotationX(Rotation.X) * Matrix.CreateRotationY(Rotation.Y) * Matrix.CreateRotationZ(Rotation.Z);

            LookDirection = Vector3.Transform(LookAt_Static, transformMatrix);
            LookAtSide = Vector3.Transform(LookAt_Sideway_static, transformMatrix);
            LookDirection = Vector3.Normalize(LookDirection);
            LookAtSide = Vector3.Normalize(LookAtSide);

            View = Matrix.CreateLookAt(Position, Position + LookDirection, upDir);
        }
    }
        
    public interface Primitive
    { 
    }

    public class Mapmaker_Data
    {
        public MM_ScreenPerspectives mm_persps;
        public MM_Viewports mm_viewps;
        public MM_Camera[] mm_cams;

        public SpriteBatch spritebatch;
        public SpriteFont standardFont;
        
        public Effect basicShader;
        public Effect VertexColorShader;

        public MM_Views viewContainer;
        public ScreenSettings screensettings;
        public float Ortographic_speed;

        public int rendertarget_width, rendertarget_height;

        public Mapmaker_Data()
        {
            
        }
    }

    public class MapMaker_Content_Graphics
    {
        public Effect basicShader_flat, basicShader_color; //For Lines

        public LineGrid[] Grids; //Well need 4
    }

    public class LineGrid
    {
        public int RenderToScreen;
        //private Line[] LineGrid1,LineGrid2;
        public VertexPositionColor[] lines1, lines2;
        private Color GridColor;
        private float GridSize; //Widht and height, quadratic
        private int GridDivisions;
        private bool[] xyz_up;
        float Widht = 0.1f;
        //Grid starts at origin
        public LineGrid(int screen_index, float gridsize, int griddivs, Color gridcolor, bool[] xyz_p, float width)
        {
            Widht = width;
            RenderToScreen = screen_index;
            GridColor = gridcolor;
            GridSize = gridsize;
            if (griddivs < 1)
                GridDivisions = 1;
            else
                GridDivisions =  griddivs;
        
            xyz_up = xyz_p;

            CreateGridNow(gridcolor, Widht);
        }

        private void CreateGridNow(Color col, float width)
        {
            //LineGrid1 = new Line[GridDivisions + 1];
            //LineGrid2 = new Line[GridDivisions + 1];
            lines1 = new VertexPositionColor[(GridDivisions + 1)*6];
            lines2 = new VertexPositionColor[(GridDivisions + 1)*6];


            if (xyz_up[1])
            {
                float currentXpos = 0;
                float currentYpos = 0;

                float LineEveryUnits = GridSize / GridDivisions;
                float HalfSize = GridSize / 2;
                
                float steps = GridDivisions;
                float counter = 0;
                //X dir
                for (int i = 0; i <= steps*6; i+= 6)
                {
                    currentXpos = (0 - HalfSize) + LineEveryUnits * (float)i / 6;

                    Console.WriteLine(currentXpos);
                    Vector3 newPoint1 = new Vector3(currentXpos, 0, -HalfSize);
                    Vector3 newPoint2 = new Vector3(currentXpos + width , 0, -HalfSize);
                    Vector3 newPoint3 = new Vector3(currentXpos + width, 0, +HalfSize);

                    Vector3 newPoint4 = new Vector3(currentXpos, 0, -HalfSize);
                    Vector3 newPoint5 = new Vector3(currentXpos , 0, +HalfSize);
                    Vector3 newPoint6 = new Vector3(currentXpos + width, 0, +HalfSize);

                    lines1[i + 0] = new VertexPositionColor(newPoint1, col);
                    lines1[i + 1] = new VertexPositionColor(newPoint2, col);
                    lines1[i + 2] = new VertexPositionColor(newPoint3, col);

                    lines1[i + 3] = new VertexPositionColor(newPoint4, col);
                    lines1[i + 4] = new VertexPositionColor(newPoint5, col);
                    lines1[i + 5] = new VertexPositionColor(newPoint6, col);

                    //LineGrid1[i] = new Line(new Vector3(currentXpos, 0, -HalfSize), new Vector3(currentXpos, 0, +HalfSize), col);
                }

                 //Y dir
                for (int i = 0; i <= steps * 6; i += 6)
                {
                    currentYpos = (0 - HalfSize) + LineEveryUnits * (float)i / 6;

                   
                    Vector3 newPoint1 = new Vector3(-HalfSize, 0, currentYpos);
                    Vector3 newPoint2 = new Vector3(HalfSize, 0, currentYpos);
                    Vector3 newPoint3 = new Vector3(HalfSize, 0, currentYpos + width);

                    Vector3 newPoint4 = new Vector3(-HalfSize, 0, currentYpos + width);
                    Vector3 newPoint5 = new Vector3(-HalfSize, 0, currentYpos);
                    Vector3 newPoint6 = new Vector3(HalfSize, 0, currentYpos + width);

                    lines2[i + 0] = new VertexPositionColor(newPoint1, col);
                    lines2[i + 1] = new VertexPositionColor(newPoint2, col);
                    lines2[i + 2] = new VertexPositionColor(newPoint3, col);

                    lines2[i + 3] = new VertexPositionColor(newPoint4, col);
                    lines2[i + 4] = new VertexPositionColor(newPoint5, col);
                    lines2[i + 5] = new VertexPositionColor(newPoint6, col);

                    //LineGrid1[i] = new Line(new Vector3(currentXpos, 0, -HalfSize), new Vector3(currentXpos, 0, +HalfSize), col);
                }
            }        
        }
    }

    public class Line
    {
        public VertexPositionColor[] LineVertices; //each line = 3 verts
        public Vector3 p1, p2;

        public Line(Vector3 p1p,Vector3 p2p, Color col)
        {
            p1 = p1p;
            p2 = p2p;

            createLine(col);
        }

        public void createLine(Color color_new)
        {
            LineVertices = new VertexPositionColor[3];
            LineVertices[0] = new VertexPositionColor(p1, color_new);
            LineVertices[1] = new VertexPositionColor(p1, color_new);
            LineVertices[2] = new VertexPositionColor(p2, color_new);
        }

        public void ChangeColor(Color color_new)
        {
            LineVertices[0] = new VertexPositionColor(p1, color_new);
            LineVertices[1] = new VertexPositionColor(p1, color_new);
            LineVertices[2] = new VertexPositionColor(p2, color_new);
        }
    }    
}
