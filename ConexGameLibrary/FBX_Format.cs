﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;

namespace ConexGameLibrary
{
    /*----------------------------------------*/
    public struct FBX_HEADER
    {
        public short FBXHeaderVersion;
        public short FBXVersion;
        
        //CreationTimeStamp comes now
        public short Version;
        public short Year;
        public byte Day;
        public byte Month;
        public byte Hour;
        public byte Minute;
        public byte Second;
        public short Millisecond;

        public String Creator;
    }

    public class FBX_ObjectType
    {
        public String Type;
        public short Count;

        
    }

    public struct FBX_Definitions
    {
        public short Version;
        public long Count;

        public List<FBX_ObjectType> objectTypes;
    }

    public struct FBX_PropertyStruct
    {
        public String Description;
        public String Datatype;
        public String DescriptionAddition; //Usually empty
        //Can be vector, float, int, bool...etc
        public List<Object> value; //Type will be set when loading
    }

    public interface IFBXObject
    {
        
    }
    /*----------------------------------------*/

    /*----------------------------------------*/
    //Layer Stuff
    public class FBX_LayerContainer //LayerContainer list
    {
        short Version;
        public List<FBX_Layer> Layers;
        public List<FBX_LayerElement_Subnodes> SubLayers;
    }
    public class FBX_Layer
    {
        public short Version;
        public string Name;
        public String MappingInformationType;
        public String ReferenceInformationType;
    }
    public class FBX_LayerElement_Subnodes :    FBX_Layer
    {
        public string Type;
        public int TypeIndex;
    }
    public class FBX_LayerElementNormal : FBX_Layer
    {        
        public List<float> Normals; // Vector3.
    }
    public class FBX_LayerElementUV : FBX_Layer
    {
        public List<float> UV; // Vector2.
        public List<int> UVIndex;
    }
    public class FBX_LayerElementSmoothing : FBX_Layer
    {        
        public List<float> Smoothing; // Vector3. Idk..
    }
    public class FBX_LayerElementMaterial : FBX_Layer
    {       
        public short Materials;
    }    
    public class FBX_LayerElementTexture : FBX_Layer
    {
        public string Blendmode;
        public int TextureAlpha;
        public List<int> TextureID;
    }
    public class FBX_LayerElementSpecularTextures : FBX_Layer
    {
        public string Blendmode;
        public int TextureAlpha;
        public List<int> TextureID;
    }
    public class FBX_LayerElementBumpTextures : FBX_Layer
    {
        public string Blendmode;
        public int TextureAlpha;
        public List<int> TextureID;
    }
    /*----------------------------------------*/

    /*----------------------------------------*/
    //PoseBindPose Object
    public struct FBX_PoseNode
    {
        public String Node;
        public float[] matrix;
    }

    /*----------------------------------------*/
    public class FBX_ModelMesh : IFBXObject
    {
        public String Type, Name, SubType;
        public short Version;
        public List<FBX_PropertyStruct> properties;
        public byte Multilayer;
        public byte MultiTake;
        public char Shading;
        public sbyte Culling; //1 normal, -1 backface , 0 noone
        public List<float> Vertices;
        public List<int> VertexIndices;
        public List<int> EdgeIndices;
        public short GeometryVersion;

        //This will contain a List of Different Layers in one Container
        //each Layer is a LayerContainer
        //(Layer Itself is the base type so i only need one type)
        public FBX_LayerContainer LayerContainer;
    }

    public class FBX_ModelLimbNode : IFBXObject
    {
        public String Type, Name, SubType;
        public short Version;
        public List<FBX_PropertyStruct> properties;
        public byte Multiplayer;
        public byte MultiTake;
        public char Shading;
        public sbyte Culling; //1 normal, -1 backface, 0 noone
        public String TypeFlags;
    }

    public class FBX_PoseBindPose : IFBXObject
    {
        public String Type, Name, SubType;
        public short Version;
        public short NumberPoseNodes;
        List<FBX_PropertyStruct> Properties;
        public List<FBX_PoseNode> PoseNodes;        
    }    

    public class FBX_Material : IFBXObject
    {
        public string Name, Type, Subtype;
        public short Version;
        public String ShadingModel;
        public short MultiLayer;
        public List<FBX_PropertyStruct> properties;
    }

    public class FBX_DeformerSkin : IFBXObject
    {
        public short Version;
        public short MultiLayer;
        public String Type, Name, Subtype;
        List<FBX_PropertyStruct> Proprties;
        float LinkDeformAccuray;
    }
    
    public class FXB_DeformerCluster : IFBXObject
    {
        public short Version;
        public byte MultiLayer;
        public String Type, Subtype, Name;
        public List<FBX_PropertyStruct> Properties;
        public FBX_UserData UserData;
        public List<int> Indexes;
        public List<float> Weights;
        public float[] Transform = new float[16];
        public float[] TransformLink = new float[16];
    }

    public class FBX_Video : IFBXObject
    {       
        public String Type, Subtype, Name;
        public List<FBX_PropertyStruct> Properties;
        public int UseMipMap;
        public String Media;
        public String Filename;
        public String RelativeFileName;
        public String TextureName;
        
    }

    public class FBX_Texture : IFBXObject
    {
        public short Version;
        public String Type, Subtype, Name;
        public List<FBX_PropertyStruct> Properties;
        public int UseMipMap;
        public String TextureName;
        public String Media;
        public String FileName;
        public String RelativeFileName;
        public float[] ModelUVTranslation;
        public float[] ModelUVScaling;
        public String Texture_Alpha_Source;
        public float[] Cropping;

    }
    /*-------------------------------------------------*/
        
    public class FBX_GlobalSettings : IFBXObject
    {
        public short Version;
        public List<FBX_PropertyStruct> Properties;
    }

    public struct FBX_UserData
    {
        object dat1, dat2;
    }
    
    /*-------------------------------------------------*/
    public struct FBX_RelationProperty
    {
        public String Type;
        public String ObjectName;
        public String ObjectType;
    }

    public struct FBX_ObjectRelations
    {
        public List<FBX_RelationProperty> RelationProperties;
    }
    /*-------------------------------------------------*/
    
    /*-------------------------------------------------*/
    public struct FBX_ConnectionProperty
    {
        public String Type; //Often OO
        public String ChildObjectName;
        public String ParentObjectName;
    }

    public struct FBX_Connection
    {
        public List<FBX_ConnectionProperty> ConnectionProperties;
    }
    /*-------------------------------------------------*/
    
    /*-------------------------------------------------*/
    //This is the lowest channel struct
    public struct FBX_Take_SubChannelNode
    {
        public char channel;
        public int Default;
        public int KeyVer;
        public int KeyCount;
        public float[] Color;
        public int LayerType;
        public List<String> Keys;
    }

    //This is one of the transformation types
    //translate, rotate, scale
    public struct FBX_Take_TransformationChannel
    {
        //Translate, rot, scale
        public Char Type;
        public FBX_Take_SubChannelNode[] SubTransformationChannel; //3 of them per type
    }

    //Contains the Transform channels
    public struct FBX_ChannelTransform
    {
        public String ChannelName;
        public FBX_Take_TransformationChannel[] TransformMainChannel; //3 per transform type
    }

    public struct FBX_ChannelVisibillity
    {
        public int Default;
        public int KeyVer;
        public int KeyCount;
        public List<object> Key;
        public float[] Color;
        public int LayerType;
        public List<Object> Keys;
    }

    public struct FBX_ChannelLiw
    {
        public int Default;
        public float[] Color;
        public int keyVer;
        public int KeyCount;
    }

    public struct FBX_ChannelPerJoint
    {
        public String JointName;
        public String Type; //Models Animation mostly
        public Single Version;
        public List<FBX_ChannelTransform> TransformChannels;

        //Not sure if neccesairy 
        public List<FBX_ChannelVisibillity> ChannelVisibillity; 
        public List<FBX_ChannelLiw> ChannelLiw; 
    }
    /*-------------------------------------------------*/

    /*----------------------------------------*/
    public struct FBX_Take
    {
        public int TakeCount;
        public String FileName;
        public long[] LocalTime; //size of 2 Longs
        public long[] ReferenceTime; //same

        public List<FBX_ChannelPerJoint> JointChannels; //in Take

    }

    public struct FBX_TakeContainer
    {
        public long CurrentTake;
        public List<FBX_Take> Takes;
    }
    /*----------------------------------------*/
    
    /*----------------------------------------*/
    //Version 5 structs//
    public struct FBX_AmbientRenderSettings
    {
        public short Version;
        public float[] AmbientLightColor;
    }

    public struct FBX_FogOptions
    {
        public byte FlogEnable;
        public byte FogMode;
        public float FogDensity;
        public float FogStart;
        public float FogEnd;
        public float[] FogColor;
    }

    public struct FBX_Settings
    {
        public int FrameRate;
        public int TimeFormat;
        public int SnapOnFrame;
        public int ReferenceTimeIndex;
        public long TimeLineStartTime;
        public long TimeLineStopTime;
    }

    public struct FBX_RenderSetting
    {
        public String DefaultCamera;
        public int DefaultViewingMode;
    }

    public struct FBX_Version5
    {
        public FBX_AmbientRenderSettings AmbientSettings;
        public FBX_FogOptions FogOption;
        public FBX_Settings Settings;
        public FBX_RenderSetting RenderSettings;
    }
    /*----------------------------------------*/
    
    /*----------------------------------------*/
    //FBX File Structure
    public struct FBX_File_Struct
    {
        public FBX_HEADER FbxHeader;
        public FBX_Definitions Definitions;

        /* Contains FbxMesh, LimbNode, Deformer..Etc */
        /*Globalsetting is also in that List! */
        public List<IFBXObject> FBX_Objects;
        public FBX_ObjectRelations FBX_Relations;
        public FBX_Connection FBX_Connection;

        public FBX_TakeContainer FBX_Takes;
        public FBX_Version5 FBX_Version5;


    }
    /*----------------------------------------*/
    
    public class FBX_Loader
    {
        public static FBX_File_Struct Create_FBX_File_Struct(String path, int ignore)
        {
            FBX_File_Struct file = new FBX_File_Struct();

            //Temp 
            file.FBX_Takes = new FBX_TakeContainer();
            file.FBX_Takes.Takes = new List<FBX_Take>();

            List<string> Lines = Get_FbxFileLines(path, ignore);
            file.FbxHeader = FBX_CreateHeader(Lines);
            file.Definitions = FBX_CreateDefinitions(Lines);
            file.FBX_Relations = FBX_CreateObjectRelations(Lines);
            file.FBX_Connection = FBX_CreateConnection(Lines);
            file.FBX_Version5 = FBX_CreateVersion5(Lines);
            file.FBX_Objects = FBX_CreateObjectList(Lines);
            file.FBX_Takes.Takes.Add(FBX_CreateTake(Lines));
            
            //Important note, Layer Elements are multiple times in the file
            return file;
        }

        /// <summary>
        /// 1 = Ignore Comments, 2=Ignore Empty Lines, 3=Ignore Both.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="IgnoreSetting"></param>
        /// <returns></returns>
        public static List<String> Get_FbxFileLines(String path, int IgnoreSetting)
        {
            List<String> Lines = new List<string>();
            String[] linesArray;
            
            linesArray = File.ReadAllLines(path);
            
            

            if (IgnoreSetting == 1)
            {
                for (int i = 0; i < linesArray.Length; i++)
                {
                    if (!linesArray[i].StartsWith(";"))
                        Lines.Add(linesArray[i]);
                }
            }

            if (IgnoreSetting == 2)
            {
                for (int i = 0; i < linesArray.Length; i++)
                {
                    if (!linesArray[i].Equals(string.Empty))
                        Lines.Add(linesArray[i]);
                }
            }

            if (IgnoreSetting == 3)
            {
                for (int i = 0; i < linesArray.Length; i++)
                {
                    if (!linesArray[i].Equals(string.Empty) && !linesArray[i].StartsWith(";"))
                        Lines.Add(linesArray[i]);
                }
            }

            if (IgnoreSetting == 0)
                Lines.AddRange(linesArray);

            linesArray = null;

            return Lines;
        }
        public static float[] getFloatArrayFromString(String Line)
        {
            string[] splits = Line.Split(',');
            float[] f = new float[splits.Length];
            for (int i = 0; i < splits.Length; i++)
            {
                f[i] = Convert.ToSingle(splits[i], CultureInfo.InvariantCulture.NumberFormat);
            }
            return f;
        }

        public static FBX_HEADER FBX_CreateHeader(List<String> Lines)
        {            
            FBX_HEADER Header = new FBX_HEADER();

            String[] HeaderLines;

            int opensDetected = 0;
            int closesDetected = 0;

            int InnerScope = 0;
            int startHeader = 0;
            int endHeader = 0;

            #region Get the Header Length
            int i = 0;
            do
            {
                if (Lines[i].Contains("FBXHeaderExtension"))
                    startHeader = i;

                if (Lines[i].Contains("{"))
                    opensDetected++;

                if (Lines[i].Contains("}"))
                    closesDetected++;

                if (InnerScope == 0 && closesDetected == 3 && 3 == opensDetected)
                {
                    endHeader = i + 1;
                    break;
                }
                i++;
            } while (true);

            #endregion

            HeaderLines = new String[endHeader - startHeader];

            for (int count = startHeader; count < endHeader; count++)
            {
                HeaderLines[count - startHeader] = Lines[count];
            }

            for (i = 0; i < HeaderLines.Length; i++)
            {
                if (Lines[i].Contains("FBXHeaderVersion: "))
                {
                    Header.FBXHeaderVersion = Convert.ToInt16(Lines[i].Substring(Lines[i].IndexOf("FBXHeaderVersion: ") + 18));
                }
                if (Lines[i].Contains("FBXVersion: "))
                {
                    Header.FBXVersion = Convert.ToInt16(Lines[i].Substring(Lines[i].IndexOf("FBXVersion: ") + 12));
                }

                if (Lines[i].Contains("CreationTimeStamp:"))
                    InnerScope++;

                if (Lines[i].Contains("Version: ") && InnerScope == 1)
                {
                    Header.Version = Convert.ToInt16(Lines[i].Substring(Lines[i].IndexOf("Version: ") + 9));
                }

                if (Lines[i].Contains("Year: ") && InnerScope == 1)
                {
                    Header.Year = Convert.ToInt16(Lines[i].Substring(Lines[i].IndexOf("Year: ") + 6));
                }

                if (Lines[i].Contains("Month: ") && InnerScope == 1)
                {
                    Header.Month = Convert.ToByte(Lines[i].Substring(Lines[i].IndexOf("Month: ") + 7));
                }
                if (Lines[i].Contains("Day: ") && InnerScope == 1)
                {
                    Header.Day = Convert.ToByte(Lines[i].Substring(Lines[i].IndexOf("Day: ") + 5));
                }

                if (Lines[i].Contains("Hour: ") && InnerScope == 1)
                {
                    Header.Hour = Convert.ToByte(Lines[i].Substring(Lines[i].IndexOf("Hour: ") + 6));
                }
                if (Lines[i].Contains("Minute: ") && InnerScope == 1)
                {
                    Header.Minute = Convert.ToByte(Lines[i].Substring(Lines[i].IndexOf("Minute: ") + 8));
                }

                if (Lines[i].Contains("Second: ") && InnerScope == 1)
                {
                    Header.Second = Convert.ToByte(Lines[i].Substring(Lines[i].IndexOf("Second: ") + 8));
                }

                if (Lines[i].Contains("Millisecond: ") && InnerScope == 1)
                {
                    Header.Millisecond = Convert.ToInt16(Lines[i].Substring(Lines[i].IndexOf("Millisecond: ") + 13));
                }
            }
            return Header;
        }
        
        public static FBX_ObjectType Get_FbxObjectType(String[] Lines)
        {
            FBX_ObjectType type = new FBX_ObjectType();

            if (Lines[0].Contains("ObjectType:"))
            {
                if (Lines[0].Contains("Model"))
                {
                    type.Type = "Model";
                    type.Count = Convert.ToInt16(Lines[1].Substring(Lines[1].IndexOf("Count: ") + 7));
                }

                if (Lines[0].Contains("Geometry"))
                {
                    type.Type = "Geometry";
                    type.Count = Convert.ToInt16(Lines[1].Substring(Lines[1].IndexOf("Count: ") + 7));
                }


                if (Lines[0].Contains("Material"))
                {
                    type.Type = String.Copy("Material");
                    type.Count = Convert.ToInt16(Lines[1].Substring(Lines[1].IndexOf("Count: ") + 7));
                }


                if (Lines[0].Contains("Texture"))
                {
                    type.Type = "Texture";
                    type.Count = Convert.ToInt16(Lines[1].Substring(Lines[1].IndexOf("Count: ") + 7));
                }

                if (Lines[0].Contains("Video"))
                {
                    type.Type = "Video";
                    type.Count = Convert.ToInt16(Lines[1].Substring(Lines[1].IndexOf("Count: ") + 7));
                }

                if (Lines[0].Contains("Deformer"))
                {
                    type.Type = "Deformer";
                    type.Count = Convert.ToInt16(Lines[1].Substring(Lines[1].IndexOf("Count: ") + 7));
                }

                if (Lines[0].Contains("Pose"))
                {
                    type.Type = "Pose";
                    type.Count = Convert.ToInt16(Lines[1].Substring(Lines[1].IndexOf("Count: ") + 7));
                }

                if (Lines[0].Contains("GlobalSettings"))
                {
                    type.Type = "GlobalSettings";
                    type.Count = Convert.ToInt16(Lines[1].Substring(Lines[1].IndexOf("Count: ") + 7));
                }
            }
            return type;
        }
        public static FBX_Definitions FBX_CreateDefinitions(List<String> Lines)
        {
            FBX_Definitions definitions = new FBX_Definitions();
            definitions.objectTypes = new List<FBX_ObjectType>();
            String[] DefinitionLines;

            bool startReading = false;
            int opensDetected = 0;
            int closesDetected = 0;

            int InnerScope = 0;
            int startDef = 0;
            int endDef = 0;
            int DefLength = 0;
            int i = 0;

            #region Detect Definition and Read first 2 lines
            do
            {
                if (Lines[i].Contains("Definitions: "))
                {
                    startDef = i;
                    startReading = true;
                }

                if (Lines[i].Contains("{") && startReading)
                {
                    opensDetected++; InnerScope++;
                }
                if (Lines[i].Contains("}") && startReading)
                {
                    closesDetected++; InnerScope--;
                }
                if (InnerScope == 0 && closesDetected == opensDetected && startReading)
                {
                    endDef = i;
                    break;
                }
                i++;
            } while (true);
            DefLength = endDef - startDef + 1;

            DefinitionLines = new String[DefLength];

            for (int count = startDef; count <= endDef; count++)
            {
                DefinitionLines[count - startDef] = Lines[count];
            }

            for (i = 0; i < DefinitionLines.Length; i++)
            {
                if (DefinitionLines[i].Contains("Version: "))
                    definitions.Version = Convert.ToInt16(DefinitionLines[i].Substring(DefinitionLines[i].IndexOf("Version: ") + 9));

                if (DefinitionLines[i].Contains("Version: ") && DefinitionLines[i + 1].Contains("Count: "))
                    definitions.Count = Convert.ToInt16(DefinitionLines[i + 1].Substring(DefinitionLines[i + 1].IndexOf("Count: ") + 7));
            }
            #endregion


            for (i = 0; i < DefinitionLines.Length; i++)
            {
                if (DefinitionLines[i].Contains("ObjectType"))
                    definitions.objectTypes.Add(Get_FbxObjectType(new String[] { DefinitionLines[i], DefinitionLines[i + 1] }));
            }

            return definitions;
        }

        public static FBX_RelationProperty Get_RelationProperty(String Line)
        {
            FBX_RelationProperty property = new FBX_RelationProperty();
            Line = Line.TrimStart(' ');
            Line = Line.TrimEnd(new char[] { ' ', '{' });
            String[] splits = Line.Split(' ');
            splits[0] = splits[0].Trim(':'); splits[1] = splits[1].Trim(new char[] { '\"', ',' }); splits[2] = splits[2].Trim('\"');

            if (splits[1].Contains(splits[0]))
                splits[1] = splits[1].Remove(splits[1].IndexOf(splits[0]), splits[0].Length);
            splits[1] = splits[1].Trim(':');

            property.ObjectName = splits[1]; property.ObjectType = splits[2]; property.Type = splits[0];

            return property;
        }
        public static FBX_ObjectRelations FBX_CreateObjectRelations(List<String> Lines)
        {
            FBX_ObjectRelations objectRelations = new FBX_ObjectRelations();
            objectRelations.RelationProperties = new List<FBX_RelationProperty>();

            String[] RelationLines;

            bool startReading = false;
            int opensDetected = 0;
            int closesDetected = 0;

            int InnerScope = 0;
            int start = 0;
            int end = 0;
            int Length = 0;
            int i = 0;

            #region Detect Definition and Read first 2 lines
            do
            {
                if (Lines[i].Contains("Relations: "))
                {
                    start = i;
                    startReading = true;
                }

                if (Lines[i].Contains("{") && startReading)
                {
                    opensDetected++; InnerScope++;
                }
                if (Lines[i].Contains("}") && startReading)
                {
                    closesDetected++; InnerScope--;
                }
                if (InnerScope == 0 && closesDetected == opensDetected && startReading)
                {
                    end = i;
                    break;
                }
                i++;
            } while (true);

            Length = end - start + 1;

            #endregion

            RelationLines = new String[Length];

            for (int count = start; count <= end; count++)
            {
                RelationLines[count - start] = Lines[count];
            }

            
            for (i = 0; i < RelationLines.Length; i++)
            {
                if (RelationLines[i].Length > 10 && !RelationLines[i].Contains("Relations: "))
                    objectRelations.RelationProperties.Add(Get_RelationProperty(RelationLines[i]));
            }            
            return objectRelations;            
        }

        public static FBX_ConnectionProperty Get_ConnectionProperty(String Line)
        {
            FBX_ConnectionProperty connection = new FBX_ConnectionProperty();
            Line = Line.TrimStart(' ');
            Line = Line.TrimEnd(new char[] { ' ', '{' });
            String[] splits = Line.Split(' ');

            connection.Type = splits[1].Trim(new char[] { '\"', ',' });
            splits[2] = splits[2].Trim(new char[] { '\"', ',' });
            splits[3] = splits[3].Trim(new char[] { '\"', ',' });
            
            connection.ChildObjectName = splits[2].Substring(splits[2].IndexOf(':') + 2);
            connection.ParentObjectName = splits[3].Substring(splits[3].IndexOf(':') + 2);

            return connection;
        }
        public static FBX_Connection FBX_CreateConnection(List<String> Lines)
        {
            FBX_Connection ObjectConnection = new FBX_Connection();
            ObjectConnection.ConnectionProperties = new List<FBX_ConnectionProperty>();

            String[] ConnectionLines;

            bool startReading = false;
            int opensDetected = 0;
            int closesDetected = 0;

            int InnerScope = 0;
            int start = 0;
            int end = 0;
            int Length = 0;
            int i = 0;

            #region Detect Definition and Read first 2 lines
            do
            {
                if (Lines[i].Contains("Connections: "))
                {
                    start = i;
                    startReading = true;
                }

                if (Lines[i].Contains("{") && startReading)
                {
                    opensDetected++; InnerScope++;
                }
                if (Lines[i].Contains("}") && startReading)
                {
                    closesDetected++; InnerScope--;
                }
                if (InnerScope == 0 && closesDetected == opensDetected && startReading)
                {
                    end = i;
                    break;
                }
                i++;
            } while (true);

            Length = end - start + 1;

            #endregion

            ConnectionLines = new String[Length];

            for (int count = start; count <= end; count++)
            {
                ConnectionLines[count - start] = Lines[count];
            }
            
            for (i = 0; i < ConnectionLines.Length; i++)
            {
                if (ConnectionLines[i].Length > 6 && !ConnectionLines[i].Contains("Connections: "))
                    ObjectConnection.ConnectionProperties.Add(Get_ConnectionProperty(ConnectionLines[i]));
            }
            return ObjectConnection;
        }

        public static FBX_Version5 FBX_CreateVersion5(List<String> Lines)
        {
            FBX_Version5 version5 = new FBX_Version5();
            String[] Version5Lines;

            bool startReading = false;
            int opensDetected = 0;
            int closesDetected = 0;

            int InnerScope = 0;
            int start = 0;
            int end = 0;
            int Length = 0;
            int i = 0;

            #region Detect Definition and Read first 2 lines
            do
            {
                if (Lines[i].Contains("Version5: "))
                {
                    start = i;
                    startReading = true;
                }

                if (Lines[i].Contains("{") && startReading)
                {
                    opensDetected++; InnerScope++;
                }
                if (Lines[i].Contains("}") && startReading)
                {
                    closesDetected++; InnerScope--;
                }
                if (InnerScope == 0 && closesDetected == opensDetected && startReading)
                {
                    end = i;
                    break;
                }
                i++;
            } while (true);

            Length = end - start + 1;

            #endregion

            Version5Lines = new String[Length];

            for (int count = start; count <= end; count++)
            {
                Version5Lines[count - start] = Lines[count];
            }

            //Copy data Directly
            for (i = 0; i < Version5Lines.Length; i++)
            {
                #region AmbientRenderSettings
                if (Version5Lines[i].Contains("Version: ") && !Version5Lines[i].Contains("Version5: "))
                    version5.AmbientSettings.Version = Convert.ToInt16(Version5Lines[i].TrimStart(' ').Substring(9));

                if (Version5Lines[i].Contains("AmbientLightColor: "))
                {
                    version5.AmbientSettings.AmbientLightColor =  getFloatArrayFromString(Version5Lines[i].TrimStart(' ').Substring(19));
                }
                #endregion

                #region Region Fog
                if (Version5Lines[i].Contains("FlogEnable:"))
                    version5.FogOption.FlogEnable = Convert.ToByte(Version5Lines[i].TrimStart(' ').Substring(12));

                if (Version5Lines[i].Contains("FogMode: "))
                    version5.FogOption.FogMode = Convert.ToByte(Version5Lines[i].TrimStart(' ').Substring(9));

                if (Version5Lines[i].Contains("FogDensity: "))
                    version5.FogOption.FogDensity = Convert.ToSingle(Version5Lines[i].TrimStart(' ').Substring(12), CultureInfo.InvariantCulture.NumberFormat);



                if (Version5Lines[i].Contains("FogStart: "))
                    version5.FogOption.FogStart = Convert.ToSingle(Version5Lines[i].TrimStart(' ').Substring(10), CultureInfo.InvariantCulture.NumberFormat);

                if (Version5Lines[i].Contains("FogEnd: "))
                    version5.FogOption.FogEnd = Convert.ToSingle(Version5Lines[i].TrimStart(' ').Substring(8), CultureInfo.InvariantCulture.NumberFormat);
                                

                if (Version5Lines[i].Contains("FogColor: "))
                {
                    version5.FogOption.FogColor = getFloatArrayFromString(Version5Lines[i].TrimStart(' ').Substring(10));
                }
                #endregion
                
                #region Region Settings
                if (Version5Lines[i].Contains("FrameRate: "))
                {
                    version5.Settings.FrameRate = Convert.ToInt32(Version5Lines[i].TrimStart(' ').Substring(11).Trim('\"'));
                }

                if (Version5Lines[i].Contains("TimeFormat: "))
                {
                    version5.Settings.TimeFormat = Convert.ToInt32(Version5Lines[i].TrimStart(' ').Substring(12).Trim('\"'));
                }


                if (Version5Lines[i].Contains("SnapOnFrames: "))
                {
                    version5.Settings.SnapOnFrame = Convert.ToInt32(Version5Lines[i].TrimStart(' ').Substring(14).Trim('\"'));
                }

                if (Version5Lines[i].Contains("ReferenceTimeIndex: "))
                {
                    version5.Settings.ReferenceTimeIndex = Convert.ToInt32(Version5Lines[i].TrimStart(' ').Substring(20).Trim('\"'));
                }

                if (Version5Lines[i].Contains("TimeLineStartTime: "))
                {
                    version5.Settings.TimeLineStartTime = Convert.ToInt64(Version5Lines[i].TrimStart(' ').Substring(19).Trim('\"'));
                }

                if (Version5Lines[i].Contains("TimeLineStopTime: "))
                {
                    version5.Settings.TimeLineStopTime = Convert.ToInt64(Version5Lines[i].TrimStart(' ').Substring(18).Trim('\"'));
                }
                #endregion
                
                #region RenderSettings
                if (Version5Lines[i].Contains("DefaultCamera: "))
                {
                    version5.RenderSettings.DefaultCamera= Version5Lines[i].TrimStart(' ').Substring(15).Trim('\"');
                }

                if (Version5Lines[i].Contains("DefaultViewingMode: "))
                {
                    version5.RenderSettings.DefaultViewingMode = Convert.ToInt32(Version5Lines[i].TrimStart(' ').Substring(20).Trim('\"'));
                }
                #endregion
            }
            return version5;
        }
        public static FBX_PropertyStruct FBX_CreatePropertyField(string line)
        {
            FBX_PropertyStruct propStruct = new FBX_PropertyStruct();
            propStruct.value = new List<object>();
            string[] split;
            line = line.Trim(' ').Substring(10);
            split = line.Split(',');
            propStruct.Description = split[0].Trim('\"');
            propStruct.Datatype = split[1].Trim(new char[] { '\"', ' ' });
            propStruct.DescriptionAddition = split[2].Trim(new char[] { '\"', ' ' });
            if (line.Contains("PreRotation"))
                Console.WriteLine("");
            for (int i = 3; i < split.Length; i++)
            {
                try
                {
                    propStruct.value.Add(Convert.ToSingle(split[i].Trim(new char[] { '\"', ' ' }), CultureInfo.InvariantCulture.NumberFormat));
                }
                catch
                {
                    propStruct.value.Add(split[3].Trim(new char[] { '\"', ' ' }));
                }
            }
            return propStruct;
        }

        //To read the multi-line datas from string
        public static List<float> FBX_GetLayerElementDataFloat(List<string> lines, int substring)
        {
            if (lines.Count < 1)
                return null;
            List<float> dataFloat = new List<float>();

            if (lines[0].Contains("Vertices: "))
            {
                dataFloat = new List<float>();
                string lineSingle = String.Empty;

                lines[0] = lines[0].Trim(' ').Substring(substring);

                for(int i = 0; i < lines.Count; i++)
                {
                    lineSingle += lines[i].Trim(' ');         
                }
                string[] splitLine = lineSingle.Split(',');

                for (int i = 0; i < splitLine.Length; i++)
                {
                    dataFloat.Add(Convert.ToSingle(splitLine[i], CultureInfo.InvariantCulture.NumberFormat));
                }

                float last = dataFloat[dataFloat.Count - 1];
                int c = lineSingle.Length;
                return dataFloat;
            }
            if (lines[0].Contains("Weights: "))
            {
                dataFloat = new List<float>();
                string lineSingle = String.Empty;

                lines[0] = lines[0].Trim(' ').Substring(substring);

                for (int i = 0; i < lines.Count; i++)
                {
                    lineSingle += lines[i].Trim(' ');
                }
                string[] splitLine = lineSingle.Split(',');

                for (int i = 0; i < splitLine.Length; i++)
                {
                    dataFloat.Add(Convert.ToSingle(splitLine[i], CultureInfo.InvariantCulture.NumberFormat));
                }

                float last = dataFloat[dataFloat.Count - 1];
                int c = lineSingle.Length;
                return dataFloat;
            }

            if (lines[0].Contains("Normals: "))
            {
                dataFloat = new List<float>();
                string lineSingle = String.Empty;

                lines[0] = lines[0].Trim(' ').Substring(substring);

                for (int i = 0; i < lines.Count; i++)
                {
                    lineSingle += lines[i].Trim(' ');
                }
                string[] splitLine = lineSingle.Split(',');

                for (int i = 0; i < splitLine.Length; i++)
                {
                    dataFloat.Add(Convert.ToSingle(splitLine[i], CultureInfo.InvariantCulture.NumberFormat));
                }

                float last = dataFloat[dataFloat.Count - 1];
                int c = lineSingle.Length;
                return dataFloat;
            }

            if (lines[0].Trim(' ').StartsWith("UV: ") && !lines[0].Trim(' ').StartsWith("UVIndex: "))
            {
                dataFloat = new List<float>();
                string lineSingle = String.Empty;

                lines[0] = lines[0].Trim(' ').Substring(substring);

                for (int i = 0; i < lines.Count; i++)
                {
                    lineSingle += lines[i].Trim(' ');
                }
                string[] splitLine = lineSingle.Split(',');

                for (int i = 0; i < splitLine.Length; i++)
                {
                    dataFloat.Add(Convert.ToSingle(splitLine[i], CultureInfo.InvariantCulture.NumberFormat));
                }

                float last = dataFloat[dataFloat.Count - 1];
                int c = lineSingle.Length;
                return dataFloat;
            }

            if (lines[0].Trim(' ').StartsWith("Smoothing: "))
            {
                dataFloat = new List<float>(); 
                string lineSingle = String.Empty;

                lines[0] = lines[0].Trim(' ').Substring(substring);

                for (int i = 0; i < lines.Count; i++)
                {
                    lineSingle += lines[i].Trim(' ');
                }
                string[] splitLine = lineSingle.Split(',');

                for (int i = 0; i < splitLine.Length; i++)
                {
                    dataFloat.Add(Convert.ToSingle(splitLine[i], CultureInfo.InvariantCulture.NumberFormat));
                }

                float last = dataFloat[dataFloat.Count - 1];
                int c = lineSingle.Length;
                return dataFloat;
            }          
            return null;
        }
        public static List<int> FBX_GetLayerElementDataInt(List<string> lines,int substring)
        {
            List<int> dataInt = new List<int>();

            if (lines[0].Contains("PolygonVertexIndex: "))
            {
                dataInt = new List<int>();
                string lineSingle = String.Empty;

                lines[0] = lines[0].Trim(' ').Substring(substring);

                for (int i = 0; i < lines.Count; i++)
                {
                    lineSingle += lines[i].Trim(' ');
                }
                string[] splitLine = lineSingle.Split(',');

                for (int i = 0; i < splitLine.Length; i++)
                {
                    int value = Convert.ToInt32(splitLine[i], CultureInfo.InvariantCulture.NumberFormat);
                    if (value < 0) 
                        value = (value * -1) - 1;
                    dataInt.Add(value);
                }

                float last = dataInt[dataInt.Count - 1];
                int c = lineSingle.Length;
                return dataInt;
            }
            if (lines[0].Contains("Edges: "))
            {
                dataInt = new List<int>();
                string lineSingle = String.Empty;

                lines[0] = lines[0].Trim(' ').Substring(substring);

                for (int i = 0; i < lines.Count; i++)
                {
                    lineSingle += lines[i].Trim(' ');
                }
                string[] splitLine = lineSingle.Split(',');

                for (int i = 0; i < splitLine.Length; i++)
                {
                    int value = Convert.ToInt32(splitLine[i], CultureInfo.InvariantCulture.NumberFormat);
                    if (value < 0)
                        value = (value * -1) - 1;
                    dataInt.Add(value);
                }

                float last = dataInt[dataInt.Count - 1];
                int c = lineSingle.Length;
                return dataInt;
            }
            if (lines[0].Contains("UVIndex: "))
            {
                dataInt = new List<int>();
                string lineSingle = String.Empty;

                lines[0] = lines[0].Trim(' ').Substring(substring);

                for (int i = 0; i < lines.Count; i++)
                {
                    lineSingle += lines[i].Trim(' ');
                }
                string[] splitLine = lineSingle.Split(',');

                for (int i = 0; i < splitLine.Length; i++)
                {
                    int value = Convert.ToInt32(splitLine[i], CultureInfo.InvariantCulture.NumberFormat);
                    if (value < 0)
                        value = (value * -1) - 1;
                    dataInt.Add(value);
                }

                float last = dataInt[dataInt.Count - 1];
                int c = lineSingle.Length;
                return dataInt;
            }

            if (lines[0].Contains("Indexes: "))
            {
                dataInt = new List<int>();
                string lineSingle = String.Empty;

                lines[0] = lines[0].Trim(' ').Substring(substring);

                for (int i = 0; i < lines.Count; i++)
                {
                    lineSingle += lines[i].Trim(' ');
                }
                string[] splitLine = lineSingle.Split(',');

                for (int i = 0; i < splitLine.Length; i++)
                {
                    int value = Convert.ToInt32(splitLine[i], CultureInfo.InvariantCulture.NumberFormat);
                    if (value < 0)
                        value = (value * -1) - 1;
                    dataInt.Add(value);
                }

                float last = dataInt[dataInt.Count - 1];
                int c = lineSingle.Length;
                return dataInt;
            }
            if (lines[0].Contains("TextureId: "))
            {
                dataInt = new List<int>();
                string lineSingle = String.Empty;

                lines[0] = lines[0].Trim(' ').Substring(substring);

                for (int i = 0; i < lines.Count; i++)
                {
                    lineSingle += lines[i].Trim(' ');
                }
                string[] splitLine = lineSingle.Split(',');

                for (int i = 0; i < splitLine.Length; i++)
                {
                    int value = Convert.ToInt32(splitLine[i], CultureInfo.InvariantCulture.NumberFormat);
                    if (value < 0)
                        value = (value * -1) - 1;
                    dataInt.Add(value);
                }

                float last = dataInt[dataInt.Count - 1];
                int c = lineSingle.Length;
                return dataInt;
            }
            return null;
        }
        //----------------------------------------

        public static FBX_LayerContainer FBX_GrabLayerElementsFromObjectNode_ForModelObject(List<String> Lines)
        {
            FBX_LayerContainer container = new FBX_LayerContainer();

            List<String> LinesLayerData = new List<String>();
            bool StartReadingLayerData = false;
            List<FBX_Layer> FBX_LAYERS = new List<FBX_Layer>();
            //Trigger this by first occurence of word Layer
            bool writeLayerElementNormal= false, 
                writeLayerElementUV = false, 
                WriteLayerElementSmoothing = false, 
                writeLayerElementMaterial = false,
                writeLayerElementTexture = false,
                writeLayerElementNormalTexture = false,
                writeLayerElementSpecularTexture = false;

            for (int i = 0; i < Lines.Count; i++)
            {
                Lines[i] = Lines[i].Trim(' ');
                if (Lines[i].StartsWith("Layer"))
                    StartReadingLayerData = true;

                if (StartReadingLayerData)
                {
                    LinesLayerData.Add(Lines[i]);
                }
            }

            FBX_LayerElementNormal layerNormal = new FBX_LayerElementNormal();
            FBX_LayerElementUV layerUV = new FBX_LayerElementUV();
            FBX_LayerElementMaterial layerMaterial = new FBX_LayerElementMaterial();
            FBX_LayerElementSmoothing layerSmoothing = new FBX_LayerElementSmoothing();

            FBX_LayerElementTexture layerTexture = new FBX_LayerElementTexture();
            FBX_LayerElementTexture layerSpecularTexture = new FBX_LayerElementTexture();
            FBX_LayerElementTexture layerBumpTexture = new FBX_LayerElementTexture();

            bool StartReadingSubLayers = false;
            FBX_LayerElement_Subnodes sublayer = new FBX_LayerElement_Subnodes();
            container.SubLayers = new List<FBX_LayerElement_Subnodes>();

            for (int i = 0; i < LinesLayerData.Count; i++)
            {
                int additionalCounter = 0;

                #region Create LayerElementNormals
                
                if (LinesLayerData[i].Contains("LayerElementNormal:") && LinesLayerData[i].Contains("{"))
                {
                    writeLayerElementNormal = true;
                }
                if (LinesLayerData[i].Contains("Version:") && writeLayerElementNormal)
                {
                    layerNormal.Version = Convert.ToInt16(LinesLayerData[i].Substring(9));
                }
                if (LinesLayerData[i].Contains("Name: ") && writeLayerElementNormal)
                {
                    layerNormal.Name = LinesLayerData[i].Substring(6).Trim('\"');
                }

                if (LinesLayerData[i].Contains("MappingInformationType: ") && writeLayerElementNormal)
                {
                    layerNormal.MappingInformationType = LinesLayerData[i].Trim(' ').Substring(24).Trim('\"');
                }

                if (LinesLayerData[i].Contains("ReferenceInformationType: ") && writeLayerElementNormal)
                {
                    layerNormal.ReferenceInformationType = LinesLayerData[i].Trim(' ').Substring(26).Trim('\"');
                }

                if (LinesLayerData[i].Contains("Normals: ") && writeLayerElementNormal)
                {                    
                    List<String> normalLines = new List<string>();

                    for (int f = i; !LinesLayerData[f].Contains("}"); f++)
                    {
                        additionalCounter++;
                        normalLines.Add(LinesLayerData[f]);
                        
                    }
                    i += additionalCounter;

                    layerNormal.Normals = FBX_GetLayerElementDataFloat(normalLines, 9); 
                    
                }
                additionalCounter = 0;
                if (LinesLayerData[i].Contains("}") && writeLayerElementNormal)
                {
                    writeLayerElementNormal = false;
                    FBX_LAYERS.Add(layerNormal);
                }
                #endregion
                
                #region Create LayerElementUV

                if (LinesLayerData[i].Contains("LayerElementUV: ") && LinesLayerData[i].Contains("{"))
                {
                    writeLayerElementUV = true;
                }
                if (LinesLayerData[i].Contains("Version:") && writeLayerElementUV)
                {
                    layerUV.Version = Convert.ToInt16(LinesLayerData[i].Substring(9));
                }
                if (LinesLayerData[i].Contains("Name: ") && writeLayerElementUV)
                {
                    layerUV.Name = LinesLayerData[i].Substring(6).Trim('\"');
                }

                if (LinesLayerData[i].Contains("MappingInformationType: ") && writeLayerElementUV)
                {
                    layerUV.MappingInformationType = LinesLayerData[i].Trim(' ').Substring(24).Trim('\"');
                }

                if (LinesLayerData[i].Contains("ReferenceInformationType: ") && writeLayerElementUV)
                {
                    layerUV.ReferenceInformationType = LinesLayerData[i].Trim(' ').Substring(26).Trim('\"');
                }

                if (LinesLayerData[i].Contains("UV: ") && writeLayerElementUV && !LinesLayerData[i].Contains("LayerElementUV:"))
                {
                    List<String> UVLines = new List<string>();

                    for (int f = i; !LinesLayerData[f].Contains("UVIndex: "); f++)
                    {
                        additionalCounter++;
                        UVLines.Add(LinesLayerData[f]);

                    }
                    i += additionalCounter;

                    layerUV.UV = FBX_GetLayerElementDataFloat(UVLines, 4);                   
                }

                additionalCounter = 0;
                if (LinesLayerData[i].Contains("UVIndex: ") && writeLayerElementUV)
                {
                    writeLayerElementUV = true;
                    List<String> UVIndexLines = new List<string>(); 

                    for (int f = i; !LinesLayerData[f].Contains("}"); f++)
                    {
                        additionalCounter++;
                        UVIndexLines.Add(LinesLayerData[f]);

                    }
                    i += additionalCounter;

                    layerUV.UVIndex = FBX_GetLayerElementDataInt(UVIndexLines, 9);  
                }

                if (LinesLayerData[i].Contains("}") && writeLayerElementUV)
                {
                    writeLayerElementUV = false;
                    FBX_LAYERS.Add(layerUV);
                }
                #endregion

                #region Create LayerElementSmoothing
                               
                if (LinesLayerData[i].Contains("LayerElementSmoothing: "))
                {
                    WriteLayerElementSmoothing = true;
                }
                if (LinesLayerData[i].Contains("Version:") && WriteLayerElementSmoothing)
                {
                    layerSmoothing.Version = Convert.ToInt16(LinesLayerData[i].Substring(9));
                }
                if (LinesLayerData[i].Contains("Name: ") && WriteLayerElementSmoothing)
                {
                    layerSmoothing.Name = LinesLayerData[i].Substring(6).Trim('\"');
                }

                if (LinesLayerData[i].Contains("MappingInformationType: ") && WriteLayerElementSmoothing)
                {
                    layerSmoothing.MappingInformationType = LinesLayerData[i].Trim(' ').Substring(24).Trim('\"');
                }

                if (LinesLayerData[i].Contains("ReferenceInformationType: ") && WriteLayerElementSmoothing)
                {
                    layerSmoothing.ReferenceInformationType = LinesLayerData[i].Trim(' ').Substring(26).Trim('\"');
                }

                if (LinesLayerData[i].Contains("Smoothing: ") && WriteLayerElementSmoothing && !LinesLayerData[i].Contains("LayerElementSmoothing: "))
                {
                    List<String> SmoothingLines = new List<string>();

                    for (int f = i; !LinesLayerData[f].Contains("}"); f++)
                    {
                        additionalCounter++;
                        SmoothingLines.Add(LinesLayerData[f]);

                    }
                    i += additionalCounter;

                    layerSmoothing.Smoothing = FBX_GetLayerElementDataFloat(SmoothingLines, 11);  
                }

                if (LinesLayerData[i].Contains("}") && WriteLayerElementSmoothing)
                {
                    WriteLayerElementSmoothing = false;
                    FBX_LAYERS.Add(layerSmoothing);
                }
                #endregion

                #region Create LayerElementMaterial

                if (LinesLayerData[i].Contains("LayerElementMaterial: ") && LinesLayerData[i].Contains("{"))
                {
                    writeLayerElementMaterial = true;
                }
                if (LinesLayerData[i].Contains("Version:") && writeLayerElementMaterial)
                {
                    layerMaterial.Version = Convert.ToInt16(LinesLayerData[i].Substring(9));
                }
                if (LinesLayerData[i].Contains("Name: ") && writeLayerElementMaterial)
                {
                    layerMaterial.Name = LinesLayerData[i].Substring(6).Trim('\"');
                }

                if (LinesLayerData[i].Contains("MappingInformationType: ") && writeLayerElementMaterial)
                {
                    layerMaterial.MappingInformationType = LinesLayerData[i].Trim(' ').Substring(24).Trim('\"');
                }

                if (LinesLayerData[i].Contains("ReferenceInformationType: ") && writeLayerElementMaterial)
                {
                    layerMaterial.ReferenceInformationType = LinesLayerData[i].Trim(' ').Substring(26).Trim('\"');
                }

                if (LinesLayerData[i].Contains("Materials: ") && writeLayerElementMaterial && !LinesLayerData[i].Contains("LayerElementSmoothing: "))
                {
                    //Console.WriteLine(LinesLayerData[23390]);
                    layerMaterial.Materials = Convert.ToInt16(LinesLayerData[i].Substring(11));
                }

                if (LinesLayerData[i].Contains("}") && writeLayerElementMaterial)
                {
                    writeLayerElementMaterial = false;
                    FBX_LAYERS.Add(layerMaterial);
                }

                #endregion

                #region Create LayerElementTexture

                if (LinesLayerData[i].Contains("LayerElementTexture: ") && LinesLayerData[i].Contains("{"))
                {
                    writeLayerElementTexture = true;
                }
                if (LinesLayerData[i].Contains("Version:") && writeLayerElementTexture)
                {
                    layerTexture.Version = Convert.ToInt16(LinesLayerData[i].Substring(9));
                }
                if (LinesLayerData[i].Contains("Name: ") && writeLayerElementTexture)
                {
                    layerTexture.Name = LinesLayerData[i].Substring(6).Trim('\"');
                }

                if (LinesLayerData[i].Contains("MappingInformationType: ") && writeLayerElementTexture)
                {
                    layerTexture.MappingInformationType = LinesLayerData[i].Trim(' ').Substring(24).Trim('\"');
                }

                if (LinesLayerData[i].Contains("ReferenceInformationType: ") && writeLayerElementTexture)
                {
                    layerTexture.ReferenceInformationType = LinesLayerData[i].Trim(' ').Substring(26).Trim('\"');
                }

                if (LinesLayerData[i].Contains("BlendMode: ") && writeLayerElementTexture && !LinesLayerData[i].Contains("LayerElementTexture: "))
                {
                    layerTexture.Blendmode = LinesLayerData[i].Substring(11).Trim('\"');
                }
                                
                if (LinesLayerData[i].Contains("TextureAlpha: ") && writeLayerElementTexture && !LinesLayerData[i].Contains("LayerElementTexture: "))
                {
                    layerTexture.TextureAlpha = Convert.ToInt16(LinesLayerData[i].Substring(14).Trim('\"'));
                }
                
                if (LinesLayerData[i].Contains("TextureId: ") && writeLayerElementTexture && !LinesLayerData[i].Contains("LayerElementTexture: "))
                {
                    List<String> TextureIDLines = new List<string>();

                    for (int f = i; !LinesLayerData[f].Contains("}"); f++)
                    {
                        additionalCounter++;
                        TextureIDLines.Add(LinesLayerData[f]);
                    }
                    i += additionalCounter;

                    layerTexture.TextureID = FBX_GetLayerElementDataInt(TextureIDLines, 11);
                }

                if (LinesLayerData[i].Contains("}") && writeLayerElementTexture)
                {
                    writeLayerElementTexture = false;
                    FBX_LAYERS.Add(layerTexture);
                }
                #endregion

                #region Create LayerElementSpecularTexture

                if (LinesLayerData[i].Contains("LayerElementSpecularTextures: ") && LinesLayerData[i].Contains("{"))
                {
                    writeLayerElementSpecularTexture = true;
                }
                if (LinesLayerData[i].Contains("Version:") && writeLayerElementSpecularTexture)
                {
                    layerSpecularTexture.Version = Convert.ToInt16(LinesLayerData[i].Substring(9));
                }
                if (LinesLayerData[i].Contains("Name: ") && writeLayerElementSpecularTexture)
                {
                    layerSpecularTexture.Name = LinesLayerData[i].Substring(6).Trim('\"');
                }

                if (LinesLayerData[i].Contains("MappingInformationType: ") && writeLayerElementSpecularTexture)
                {
                    layerSpecularTexture.MappingInformationType = LinesLayerData[i].Trim(' ').Substring(24).Trim('\"');
                }

                if (LinesLayerData[i].Contains("ReferenceInformationType: ") && writeLayerElementSpecularTexture)
                {
                    layerSpecularTexture.ReferenceInformationType = LinesLayerData[i].Trim(' ').Substring(26).Trim('\"');
                }

                if (LinesLayerData[i].Contains("BlendMode: ") && writeLayerElementSpecularTexture && !LinesLayerData[i].Contains("LayerElementTexture: "))
                {
                    layerSpecularTexture.Blendmode = LinesLayerData[i].Substring(11).Trim('\"');
                }

                if (LinesLayerData[i].Contains("TextureAlpha: ") && writeLayerElementSpecularTexture && !LinesLayerData[i].Contains("LayerElementTexture: "))
                {
                    layerSpecularTexture.TextureAlpha = Convert.ToInt16(LinesLayerData[i].Substring(14).Trim('\"'));
                }


                if (LinesLayerData[i].Contains("TextureId: ") && writeLayerElementSpecularTexture && !LinesLayerData[i].Contains("LayerElementTexture: "))
                {
                    List<String> TextureSpecularIDLines = new List<string>();

                    for (int f = i; !LinesLayerData[f].Contains("}"); f++)
                    {
                        additionalCounter++;
                        TextureSpecularIDLines.Add(LinesLayerData[f]);
                    }
                    i += additionalCounter;

                    layerSpecularTexture.TextureID = FBX_GetLayerElementDataInt(TextureSpecularIDLines, 11);
                }

                if (LinesLayerData[i].Contains("}") && writeLayerElementSpecularTexture)
                {
                    writeLayerElementSpecularTexture = false;
                    FBX_LAYERS.Add(layerSpecularTexture);
                }
                #endregion

                #region Create LayerElementBumpTexture

                if (LinesLayerData[i].Contains("LayerElementBumpTextures: ") && LinesLayerData[i].Contains("{"))
                {
                    writeLayerElementNormalTexture = true;
                }
                if (LinesLayerData[i].Contains("Version:") && writeLayerElementNormalTexture)
                {
                    layerBumpTexture.Version = Convert.ToInt16(LinesLayerData[i].Substring(9));
                }
                if (LinesLayerData[i].Contains("Name: ") && writeLayerElementNormalTexture)
                {
                    layerBumpTexture.Name = LinesLayerData[i].Substring(6).Trim('\"');
                }

                if (LinesLayerData[i].Contains("MappingInformationType: ") && writeLayerElementNormalTexture)
                {
                    layerBumpTexture.MappingInformationType = LinesLayerData[i].Trim(' ').Substring(24).Trim('\"');
                }

                if (LinesLayerData[i].Contains("ReferenceInformationType: ") && writeLayerElementNormalTexture)
                {
                    layerBumpTexture.ReferenceInformationType = LinesLayerData[i].Trim(' ').Substring(26).Trim('\"');
                }

                if (LinesLayerData[i].Contains("BlendMode: ") && writeLayerElementNormalTexture && !LinesLayerData[i].Contains("LayerElementTexture: "))
                {
                    layerBumpTexture.Blendmode = LinesLayerData[i].Substring(11).Trim('\"');
                }

                if (LinesLayerData[i].Contains("TextureAlpha: ") && writeLayerElementNormalTexture && !LinesLayerData[i].Contains("LayerElementTexture: "))
                {
                    layerBumpTexture.TextureAlpha = Convert.ToInt16(LinesLayerData[i].Substring(14).Trim('\"'));
                }


                if (LinesLayerData[i].Contains("TextureId: ") && writeLayerElementNormalTexture && !LinesLayerData[i].Contains("LayerElementTexture: "))
                {
                    List<String> TextureBumpIDLines = new List<string>();

                    for (int f = i; !LinesLayerData[f].Contains("}"); f++)
                    {
                        additionalCounter++;
                        TextureBumpIDLines.Add(LinesLayerData[f]);
                    }
                    i += additionalCounter;

                    layerBumpTexture.TextureID = FBX_GetLayerElementDataInt(TextureBumpIDLines, 11);
                }

                if (LinesLayerData[i].Contains("}") && writeLayerElementNormalTexture)
                {
                    writeLayerElementNormalTexture = false;
                    FBX_LAYERS.Add(layerBumpTexture);
                }
                #endregion

                if (LinesLayerData[i].StartsWith("LayerElement:  {"))
                {
                    sublayer = new FBX_LayerElement_Subnodes();
                    sublayer.Type = LinesLayerData[i + 1].Substring(6).Trim('\"');
                    sublayer.TypeIndex = Convert.ToInt32(LinesLayerData[i + 2].Substring(12));
                    container.SubLayers.Add(sublayer);
                }
            }
            container.Layers = FBX_LAYERS;

            return container;
        }

        public static FBX_ModelMesh FBX_CreateModelMesh(List<String> Lines)
        {
            FBX_ModelMesh mesh = new FBX_ModelMesh();
            String[] split = Lines[0].TrimStart(' ').TrimEnd(new char[] { '\"', ' ', '{' }).Split(' ');
            mesh.Type = split[0].Trim(':');
            mesh.Name = split[1].Trim(new char[] { '\"', ',' }).Substring(split[0].Length + 2);
            mesh.SubType = split[2].Trim('\"');
            split = null;
            mesh.properties = new List<FBX_PropertyStruct>();
            mesh.Vertices = new List<float>();
            mesh.VertexIndices = new List<int>();

            bool LayerElementClosed = true;
            bool triggerOnce = true;
            mesh.LayerContainer = new FBX_LayerContainer();

            bool FBX_GrabLayerElementsFromObjectNode_ForModelObject_Once = true;
            #region Read All data except LayerNodes

            for (int i = 0; i < Lines.Count; i++)
            {
                if (FBX_GrabLayerElementsFromObjectNode_ForModelObject_Once)
                {
                    mesh.LayerContainer = FBX_GrabLayerElementsFromObjectNode_ForModelObject(Lines);
                    FBX_GrabLayerElementsFromObjectNode_ForModelObject_Once = false;
                }

                if (Lines[i].Contains("::") && Lines[i + 1].Contains("Version: ") && triggerOnce && LayerElementClosed)
                {
                    mesh.Version = Convert.ToInt16(Lines[i + 1].Trim(new char[] { ' ', ':', '\t' }).Substring(9));
                    triggerOnce = false;
                }

                if (Lines[i].Contains("Property: ") && LayerElementClosed)
                {
                    //mesh.properties.Add(FBX_CreatePropertyField(Lines[i]));
                }
                if (Lines[i].Contains("MultiLayer: ") && LayerElementClosed)
                {
                    mesh.Multilayer = Convert.ToByte(Lines[i].Trim(new char[] { ' ', ':' }).Substring(10).Trim(new char[] { ' ', ':', '\t' }));
                }
                if (Lines[i].Contains("MultiTake: ") && LayerElementClosed)
                {
                    mesh.MultiTake = Convert.ToByte(Lines[i].Trim(new char[] { ' ', ':' }).Substring(9).Trim(new char[] { ' ', ':', '\t' }));
                }
                if (Lines[i].Contains("Shading: ") && LayerElementClosed)
                {
                    mesh.Shading = Convert.ToChar(Lines[i].Trim(new char[] { ' ', ':' }).Substring(7).Trim(new char[] { ' ', ':', '\t' }));
                }
                if (Lines[i].Contains("Culling: ") && LayerElementClosed)
                {
                    if (Lines[i].Contains("CullingOff"))
                        mesh.Culling = 0;

                    else if (Lines[i].Contains("CullingOn"))
                        mesh.Culling = 1;
                    else
                        mesh.Culling = -1;

                }
                int additionalCounter = 0;
                List<String> verticeLines = new List<string>();

                if (Lines[i].Contains("Vertices: ") && LayerElementClosed)
                {                    
                    for (int d = i; !Lines[d].Contains("PolygonVertexIndex") ; d++)
                    {
                        additionalCounter++;
                        verticeLines.Add(Lines[d]);
                    }
                    i += additionalCounter;
                    mesh.Vertices = FBX_GetLayerElementDataFloat(verticeLines,10);
                }

                additionalCounter = 0;
                List<String> vertexIndicesLines = new List<string>();

                if (Lines[i].Contains("PolygonVertexIndex: ") && LayerElementClosed)
                {
                    for (int d = i; !Lines[d].Contains("Edges:"); d++)
                    {
                        additionalCounter++;
                        vertexIndicesLines.Add(Lines[d]);
                    }
                    
                    i += additionalCounter;
                    mesh.VertexIndices = FBX_GetLayerElementDataInt(vertexIndicesLines, 20);
                }
                
                additionalCounter = 0;
                List<String> EdgesLines = new List<string>();
                if (Lines[i].Contains("Edges: ") && LayerElementClosed)
                {
                    for (int d = i; !Lines[d].Contains("GeometryVersion:"); d++)
                    {
                        additionalCounter++;
                        EdgesLines.Add(Lines[d]);
                    }
                    i += additionalCounter;
                    mesh.EdgeIndices = FBX_GetLayerElementDataInt(EdgesLines,7);
                }

                if (Lines[i].Contains("GeometryVersion: ") && LayerElementClosed)
                {
                    mesh.GeometryVersion = Convert.ToInt16(Lines[i].Trim(new char[] { ' ' }).Substring(17).Trim(' '));
                }
                #endregion
           
                       
            }

            return mesh;
        }

        public static FBX_ModelLimbNode FBX_CreateModelLimbNode(List<String> Lines)
        {
            FBX_ModelLimbNode limbnode = new FBX_ModelLimbNode();
            limbnode.properties = new List<FBX_PropertyStruct>();
            

            for (int i = 0; i < Lines.Count; i++)
            {
                if(Lines[i].Contains("Property: "))
                    limbnode.properties.Add(FBX_CreatePropertyField(Lines[i]));
           
                Lines[i] = Lines[i].Trim(new char[] { ' ','\"', '}'});
            }

            String[] split = Lines[0].TrimStart(' ').TrimEnd(new char[] { '\"', ' ', '{' }).Split(' ');
            limbnode.Type = split[0].Trim(':');
            limbnode.Name = split[1].Trim(new char[] { '\"', ',' }).Substring(split[0].Length).Trim(':');
            limbnode.SubType = split[2].Trim('\"');

            for (int i = 0; i < Lines.Count; i++)
            {
                if (Lines[i].Contains("Version: "))
                {
                    limbnode.Version = Convert.ToInt16(Lines[i].Substring(9));
                }
                if (Lines[i].Contains("MultiLayer: "))
                {
                    limbnode.Multiplayer = Convert.ToByte(Lines[i].Substring(12));
                }
                if (Lines[i].Contains("MultiTake: "))
                {
                    limbnode.MultiTake = Convert.ToByte(Lines[i].Substring(11));
                }
                if (Lines[i].Contains("Shading: "))
                {
                    limbnode.Shading = Convert.ToChar(Lines[i].Substring(9));
                }

                if (Lines[i].Contains("Culling: "))
                {
                    string culling = Lines[i].Substring(9).Trim('\"');
                    if(culling.Contains("Off"))
                        limbnode.Culling = 0;
                    else if(culling.Contains("On"))
                        limbnode.Culling = 1;
                    else
                        limbnode.Culling = -1;
                }
                if (Lines[i].Contains("TypeFlags: "))
                {
                    limbnode.TypeFlags = Lines[i].Substring(11).Trim('\"');
                }
            }
            Console.WriteLine("Limbnode created");           
            return limbnode;
        }

        public static FBX_PoseBindPose FBX_CreateModelBindPose(List<String> Lines)
        {
            FBX_PoseBindPose bindpose = new FBX_PoseBindPose();
            bindpose.PoseNodes = new List<FBX_PoseNode>();
  
            String[] split = Lines[0].TrimStart(new char[] { '\"', ' ', '{' }).Split(' ');
            bindpose.Type = split[0].Trim(':');
            bindpose.Name = split[1].Trim(new char[] { '\"', ',' }).Substring(split[0].Length).Trim(':');
            bindpose.SubType = split[2].Trim('\"');

            FBX_PoseNode node = new FBX_PoseNode();
            for (int i = 0; i < Lines.Count; i++)
            {
                String matrixString = string.Empty;
                
                if (Lines[i].Contains("Matrix: "))
                {

                    for (int d = i; !Lines[d].Contains("}"); d++)
                    {
                        matrixString += Lines[d];
                        i++;
                    }

                    string[] matrixLines = matrixString.Trim(' ').Substring(8).Split(',');
                    float[] matrix = new float[matrixLines.Length];

                    for (int b = 0; b < matrix.Length; b++)
                        matrix[b] = Convert.ToSingle(matrixLines[b], CultureInfo.InvariantCulture.NumberFormat);

                    node.matrix = matrix;
                    bindpose.PoseNodes.Add(node);
                }

                String name;
                if (Lines[i].Contains("PoseNode: ") && Lines[i + 1].Contains("Node: "))
                {
                    name = Lines[i + 1].Trim(new char[] { '\t', ' ' }).Substring(6).Trim('\"');
                    node.Node = name;
                }

                if (Lines[i].Contains("Version: "))
                {
                    bindpose.Version = Convert.ToInt16(Lines[i].Trim(' ').Substring(9));
                }
                if (Lines[i].Contains("Type: "))
                {
                    bindpose.SubType = Lines[i].Trim(' ').Substring(6).Trim('\"');
                }
                if (Lines[i].Contains("NbPoseNodes: "))
                {
                    bindpose.NumberPoseNodes = Convert.ToInt16(Lines[i].Trim(new char[] {'\"', ' ' }).Substring(13));
                }
                
            }
            Console.WriteLine("BindPose created");
            return bindpose;
        }

        public static FBX_Material FBX_CreateMaterial(List<String> Lines)
        {
            FBX_Material material = new FBX_Material();
            material.properties = new List<FBX_PropertyStruct>();
            
            String[] split = Lines[0].TrimStart(new char[] { '\"', ' ', '{' }).Split(' ');
            material.Type = split[0].Trim(':');
            material.Name = split[1].Trim(new char[] { '\"', ',' }).Substring(split[0].Length).Trim(':');

            for (int i = 0; i < Lines.Count; i++)
            {
                Lines[i] = Lines[i].Trim(new char[] { ' ', '\"', '}' });

                if(Lines[i].Contains("Property: "))
                     material.properties.Add(FBX_CreatePropertyField(Lines[i]));
                
                if (Lines[i].Contains("Version: "))
                {
                    material.Version = Convert.ToInt16(Lines[i].Trim(' ').Substring(9));
                }

                if (Lines[i].Contains("ShadingModel: "))
                {
                    material.ShadingModel = Lines[i].Substring(14).Trim('\"');
                }

                if (Lines[i].Contains("MultiLayer: "))
                {
                    material.MultiLayer = Convert.ToInt16(Lines[i].Substring(12));
                }
            }

            Console.WriteLine("Material created");
            return material;
        }

        public static FBX_DeformerSkin FBX_CreateDeformerSkin(List<String> Lines)
        {
            FBX_DeformerSkin skin = new FBX_DeformerSkin();

            String[] split = Lines[0].TrimStart(new char[] { '\"', ' ', '{' }).Split(' ');
            skin.Type = split[0].Trim(':');
            skin.Name = split[1].Trim(new char[] { '\"', ',' }).Substring(split[0].Length).Trim(':');
            skin.Subtype = split[2].Trim(new char[] { '\"', ',' });

            for (int i = 0; i < Lines.Count; i++)
            {
                Lines[i] = Lines[i].Trim(new char[] { ' ', '\"', '}' });

                
                if (Lines[i].Contains("Version: "))
                {
                    skin.Version = Convert.ToInt16(Lines[i].Trim(' ').Substring(9));
                }

                if (Lines[i].Contains("MultiLayer: "))
                {
                    skin.MultiLayer = Convert.ToInt16(Lines[i].Substring(12));
                }
            }

            Console.WriteLine("Skin created");
            return skin;
        }

        public static FXB_DeformerCluster FBX_CreateDeformerCluster(List<String> Lines)
        {
            FXB_DeformerCluster cluster = new FXB_DeformerCluster();

            String[] split = Lines[0].TrimStart(new char[] { '\"', ' ', '{' }).Split(' ');
            cluster.Type = split[0].Trim(':');
            cluster.Name = split[1].Trim(new char[] { '\"', ',' }).Substring(split[0].Length+3).Trim(':');
            cluster.Subtype = split[2].Trim(new char[] { '\"', ',' });
            cluster.Properties = new List<FBX_PropertyStruct>();
            int additionalCounter = 0;

            for (int i = 0; i < Lines.Count; i++)
            {
                Lines[i] = Lines[i].Trim(new char[] { ' ', '\"', '}' });
                
                if (Lines[i].Contains("Version: "))
                {
                    cluster.Version = Convert.ToInt16(Lines[i].Trim(' ').Substring(9));
                }
                if (Lines[i].Contains("MultiLayer: "))
                {
                    cluster.MultiLayer = Convert.ToByte(Lines[i].Substring(12));
                }

                if (Lines[i].Contains("Indexes: "))
                {
                    List<String> IndexesLines = new List<string>();

                    for (int f = i; !Lines[f].Contains("Weights: "); f++)
                    {
                        additionalCounter++;
                        IndexesLines.Add(Lines[f]);

                    }
                    i += additionalCounter;

                    cluster.Indexes = FBX_GetLayerElementDataInt(IndexesLines, 9); 
                }

                additionalCounter = 0;
                if (Lines[i].Contains("Weights: "))
                {
                    List<String> weightLines = new List<string>();

                    for (int f = i; !Lines[f].Contains("Transform: "); f++)
                    {
                        additionalCounter++;
                        weightLines.Add(Lines[f]);

                    }
                    i += additionalCounter;

                    cluster.Weights = FBX_GetLayerElementDataFloat(weightLines, 9); 
                }

                additionalCounter = 0;
                if (Lines[i].Contains("Transform: "))
                {
                    String TransformString = string.Empty;

                    for (int f = i; !Lines[f].Contains("TransformLink: "); f++)
                    {
                        additionalCounter++;
                        TransformString += Lines[f];

                    }
                    i += additionalCounter;

                    string[] transformMatrix = TransformString.Trim(' ').Substring(11).Split(',');
                    cluster.Transform = new float[transformMatrix.Length];
                    for (int d = 0; d < transformMatrix.Length; d++)
                    {
                        cluster.Transform[d] = Convert.ToSingle(transformMatrix[d], CultureInfo.InvariantCulture.NumberFormat);
                    }
                }


                additionalCounter = 0;
                if (Lines[i].Contains("TransformLink: "))
                {
                    String TransformLinkString = string.Empty;

                    for (int f = i; !Lines[f].Contains("}"); f++)
                    {
                        additionalCounter++;
                        TransformLinkString += Lines[f];

                    }
                    i += additionalCounter;

                    string[] transformLinkMatrix = TransformLinkString.Trim(' ').Substring(15).Split(',');
                    cluster.TransformLink = new float[transformLinkMatrix.Length];
                    for (int d = 0; d < transformLinkMatrix.Length; d++)
                    {
                        cluster.TransformLink[d] = Convert.ToSingle(transformLinkMatrix[d], CultureInfo.InvariantCulture.NumberFormat);
                    }
                }
            }

            Console.WriteLine("Cluster created");
            return cluster;
        }

        public static FBX_Video FBX_CreateVideo(List<String> Lines)
        {
            FBX_Video video = new FBX_Video();

            String[] split = Lines[0].TrimStart(new char[] { '\"', ' ', '{' }).Split(' ');
            video.Type = split[0].Trim(':');
            video.Name = split[1].Trim(new char[] { '\"', ',' }).Substring(split[0].Length).Trim(':');
            video.Subtype = split[2].Trim(new char[] { '\"', ',' });

            for (int i = 0; i < Lines.Count; i++)
            {
                Lines[i] = Lines[i].Trim(new char[] { ' ', '\"', '}', '\t' });

                if (Lines[i].Contains("Filename: "))
                {
                    video.Filename = Lines[i].Trim(new char[] { ' ', '\t' }).Substring(18).Trim('\"');
                }

                if (Lines[i].Contains("RelativeFilename: "))
                {
                    video.RelativeFileName = Lines[i].Trim(new char[] { ' ', '\t'} ).Substring(18).Trim('\"');
                }

                if (Lines[i].Contains("UseMipMap: "))
                {
                    video.UseMipMap = Convert.ToInt32(Lines[i].Substring(11));
                }
            }

            Console.WriteLine("Video created");
            return video;
        }

        public static FBX_Texture FBX_CreateTexture(List<String> Lines)
        {
            FBX_Texture texture = new FBX_Texture();
            texture.Properties = new List<FBX_PropertyStruct>();

            String[] split = Lines[0].TrimStart(new char[] { '\"', ' ', '{' }).Split(' ');
            texture.Type = split[0].Trim(':');
            texture.Name = split[1].Trim(new char[] { '\"', ',' }).Substring(split[0].Length).Trim(':');
            texture.Subtype = split[2].Trim(new char[] { '\"', ',' });

            for (int i = 0; i < Lines.Count; i++)
            {
                Lines[i] = Lines[i].Trim(new char[] { ' ', '\"', '}' });

                if (Lines[i].Contains("Version: "))
                {
                    texture.Version = Convert.ToInt16(Lines[i].Trim(' ').Substring(9));
                }
                if (Lines[i].Contains("TextureName: "))
                {
                    texture.TextureName = Lines[i].Trim(' ').Substring(13).Trim('\"');
                }
                if (Lines[i].Contains("Media: "))
                {
                    texture.Media = Lines[i].Trim(' ').Substring(7).Trim('\"');
                }

                if (Lines[i].Contains("FileName: "))
                {
                    texture.FileName = Lines[i].Trim(' ').Substring(10).Trim('\"');
                }
                if (Lines[i].Contains("RelativeFilename: "))
                {
                    texture.RelativeFileName = Lines[i].Trim(' ').Substring(18).Trim('\"');
                }
                if (Lines[i].Contains("ModelUVTranslation: "))
                {
                    float[] uv;
                    string[] splits;
                    splits = Lines[i].Substring(20).Split(',');
                    uv = new float[splits.Length];
                    for (int d = 0; d < splits.Length; d++)
                    {
                        uv[d] = Convert.ToInt32(splits[d], CultureInfo.InvariantCulture.NumberFormat);
                        texture.ModelUVTranslation = uv;
                    }
                }

                if (Lines[i].Contains("ModelUVScaling: "))
                {
                    float[] uv;
                    string[] splits;
                    splits = Lines[i].Substring(16).Split(',');
                    uv = new float[splits.Length];
                    for (int d = 0; d < splits.Length; d++)
                    {
                        uv[d] = Convert.ToInt32(splits[d], CultureInfo.InvariantCulture.NumberFormat);
                        texture.ModelUVScaling = uv;
                    }
                }

                if (Lines[i].Contains("Texture_Alpha_Source: "))
                {
                    texture.Texture_Alpha_Source = Lines[i].Trim(' ').Substring(22).Trim('\"');
                }



                if (Lines[i].Contains("Cropping: "))
                {
                    float[] crop;
                    string[] splits;
                    splits = Lines[i].Substring(10).Split(',');
                    crop = new float[splits.Length];
                    for (int d = 0; d < splits.Length; d++)
                    {
                        crop[d] = Convert.ToInt32(splits[d], CultureInfo.InvariantCulture.NumberFormat);
                        texture.Cropping = crop;
                    }
                }
            }

            Console.WriteLine("Texture created");
            return texture;
        }

        //This needs editing
        public static List<String> FBX_getKeyLine(List<string> line, int substring)
        {
            List<String> objects = new List<String>();
            string[] splits;
            string CombinedLines = string.Empty;

            if (line[0].Contains("Key: "))
            {
                for (int i = 0; i < line.Count; i++)
                {
                    CombinedLines += line[i];
                }

                splits = CombinedLines.Trim(' ').Substring(substring).Split(',');

                for (int i = 0; i < splits.Length; i++)
                {
                    objects.Add((splits[i]));
                }
            }
            return objects;
        }

        public static FBX_ChannelPerJoint FBX_CreateChannelPerJoint(List<String> Lines)
        {
            FBX_ChannelPerJoint jointchannel = new FBX_ChannelPerJoint();
            String[] splits = Lines[0].Trim(new char[] { ' ', '\"', '{' }).Split(' ');
            splits[0] = splits[0].Trim(':');
            splits[1] = splits[1].Trim(new char[] { ' ', '\"' }).Substring(splits[0].Length+2);
            jointchannel.Type = splits[0]; jointchannel.JointName = splits[1];
            jointchannel.JointName = splits[1];
            jointchannel.TransformChannels = new List<FBX_ChannelTransform>();

            FBX_ChannelTransform transformchannel = new FBX_ChannelTransform();
            FBX_Take_TransformationChannel[] fbx_take_transformationChannels = new FBX_Take_TransformationChannel[3];            

            for (int i = 0; i < 3; i++)
            {
                fbx_take_transformationChannels[i] = new FBX_Take_TransformationChannel();
                fbx_take_transformationChannels[i].SubTransformationChannel = new FBX_Take_SubChannelNode[3];
                for (int j = 0; j < 3; j++)
                {
                    fbx_take_transformationChannels[i].SubTransformationChannel[j] = new FBX_Take_SubChannelNode();
                }
            }
            
            bool readT = false, readR = false, readS = false;
            for (int i = 0; i < Lines.Count; i++)
            {
                if (Lines[i].Contains("Channel: \"T\""))
                {
                    readT = true; readR = false; readS = false;
                }
                if (Lines[i].Contains("Channel: \"R\""))
                {
                    readT = false; readR = true; readS = false;
                }
                if (Lines[i].Contains("Channel: \"S\""))
                {
                    readT = false; readR = false; readS = true;
                }

                //Read the Translation Transform type
                #region Read Translate
                if (readT)
                {    
                    //IMPORTANT!!! Change the assigned type in else if parts 
                    fbx_take_transformationChannels[0].Type = 'T';
                    if (Lines[i].Contains("Channel: \"X\"") && Lines[i+2].Contains("Color"))
                    {
                        fbx_take_transformationChannels[0].SubTransformationChannel[0].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        //fbx_take_transformationChannels[0].SubTransformationChannel[0].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        string[] colorsplit = Lines[i+2].Trim(' ').Substring(7).Split(',');
                        float[] color = new float[3];
                        color[0] = Convert.ToSingle(colorsplit[0], CultureInfo.InvariantCulture.NumberFormat);
                        color[1] = Convert.ToSingle(colorsplit[1], CultureInfo.InvariantCulture.NumberFormat);
                        color[2] = Convert.ToSingle(colorsplit[2], CultureInfo.InvariantCulture.NumberFormat);
                        fbx_take_transformationChannels[0].SubTransformationChannel[0].Color = color;
                    }
                    else if (Lines[i].Contains("Channel: \"X\"") && Lines[i + 2].Contains("KeyVer") && Lines[i + 3].Contains("KeyCount"))
                    {
                       
                        fbx_take_transformationChannels[0].SubTransformationChannel[0].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        //fbx_take_transformationChannels[0].SubTransformationChannel[0].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        fbx_take_transformationChannels[0].SubTransformationChannel[0].KeyVer = Convert.ToInt32(Lines[i + 2].Trim(' ').Substring(8));
                        fbx_take_transformationChannels[0].SubTransformationChannel[0].KeyVer = Convert.ToInt32(Lines[i + 3].Trim(' ').Substring(10));

                        List<String> keys = new List<String>();
                        if (Lines[i + 4].Contains("Key: "))
                        {
                            int additionalCounter = 0;
                            List<String> KeyLines = new List<string>();

                            for (int f = i + 4; !Lines[f].Contains("Color: "); f++)
                            {
                                additionalCounter++;
                                KeyLines.Add(Lines[f]);

                            }
                            i += additionalCounter;

                            keys = FBX_getKeyLine(KeyLines, 5);
                        }
                                                
                        fbx_take_transformationChannels[0].SubTransformationChannel[0].Keys = keys;                        
                    }
                    

                    if (Lines[i].Contains("Channel: \"Y\"") && Lines[i + 2].Contains("Color"))
                    {
                        fbx_take_transformationChannels[0].SubTransformationChannel[1].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        //fbx_take_transformationChannels[0].SubTransformationChannel[1].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        string[] colorsplit = Lines[i + 2].Trim(' ').Substring(7).Split(',');
                        float[] color = new float[3];
                        color[0] = Convert.ToSingle(colorsplit[0], CultureInfo.InvariantCulture.NumberFormat);
                        color[1] = Convert.ToSingle(colorsplit[1], CultureInfo.InvariantCulture.NumberFormat);
                        color[2] = Convert.ToSingle(colorsplit[2], CultureInfo.InvariantCulture.NumberFormat);
                        fbx_take_transformationChannels[0].SubTransformationChannel[1].Color = color;
                    }
                    else if (Lines[i].Contains("Channel: \"Y\"") && Lines[i + 2].Contains("KeyVer") && Lines[i + 3].Contains("KeyCount"))
                    {
                        fbx_take_transformationChannels[0].SubTransformationChannel[1].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        //fbx_take_transformationChannels[0].SubTransformationChannel[1].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        fbx_take_transformationChannels[0].SubTransformationChannel[1].KeyVer = Convert.ToInt32(Lines[i + 2].Trim(' ').Substring(8));

                        List<String> keys = new List<String>();
                        if (Lines[i + 4].Contains("Key: "))
                        {
                            int additionalCounter = 0;
                            List<String> KeyLines = new List<string>();

                            for (int f = i + 4; !Lines[f].Contains("Color: "); f++)
                            {
                                additionalCounter++;
                                KeyLines.Add(Lines[f]);

                            }
                            i += additionalCounter;

                            keys = FBX_getKeyLine(KeyLines, 5);
                        }

                        fbx_take_transformationChannels[0].SubTransformationChannel[1].Keys = keys;
                                               
                    }


                    if (Lines[i].Contains("Channel: \"Z\"") && Lines[i + 2].Contains("Color"))
                    {
                        fbx_take_transformationChannels[0].SubTransformationChannel[2].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        string[] colorsplit = Lines[i + 2].Trim(' ').Substring(7).Split(',');
                        float[] color = new float[3];
                        color[0] = Convert.ToSingle(colorsplit[0], CultureInfo.InvariantCulture.NumberFormat);
                        color[1] = Convert.ToSingle(colorsplit[1], CultureInfo.InvariantCulture.NumberFormat);
                        color[2] = Convert.ToSingle(colorsplit[2], CultureInfo.InvariantCulture.NumberFormat);
                        fbx_take_transformationChannels[0].SubTransformationChannel[2].Color = color;
                    }
                    else if (Lines[i].Contains("Channel: \"Z\"") && Lines[i + 2].Contains("KeyVer") && Lines[i + 3].Contains("KeyCount"))
                    {
                        fbx_take_transformationChannels[0].SubTransformationChannel[2].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        
                        fbx_take_transformationChannels[0].SubTransformationChannel[2].KeyVer = Convert.ToInt32(Lines[i + 2].Trim(' ').Substring(8));

                        List<String> keys = new List<String>();
                        if (Lines[i + 4].Contains("Key: "))
                        {
                            int additionalCounter = 0;
                            List<String> KeyLines = new List<string>();

                            for (int f = i + 4; !Lines[f].Contains("Color: "); f++)
                            {
                                additionalCounter++;
                                KeyLines.Add(Lines[f]);

                            }
                            i += additionalCounter;

                            keys = FBX_getKeyLine(KeyLines, 5);
                        }

                        fbx_take_transformationChannels[0].SubTransformationChannel[2].Keys = keys;
                        
                    }
                }
                #endregion

                //Read the Rotation Transform type
                #region Read Rotate
                if (readR)
                {                   
                    fbx_take_transformationChannels[1].Type = 'R';
                    if (Lines[i].Contains("Channel: \"X\"") && Lines[i + 2].Contains("Color"))
                    {
                        fbx_take_transformationChannels[1].SubTransformationChannel[0].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        fbx_take_transformationChannels[1].SubTransformationChannel[0].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        string[] colorsplit = Lines[i + 2].Trim(' ').Substring(7).Split(',');
                        float[] color = new float[3];
                        color[0] = Convert.ToSingle(colorsplit[0], CultureInfo.InvariantCulture.NumberFormat);
                        color[1] = Convert.ToSingle(colorsplit[1], CultureInfo.InvariantCulture.NumberFormat);
                        color[2] = Convert.ToSingle(colorsplit[2], CultureInfo.InvariantCulture.NumberFormat);
                        fbx_take_transformationChannels[1].SubTransformationChannel[0].Color = color;
                    }
                    else if (Lines[i].Contains("Channel: \"X\"") && Lines[i + 2].Contains("KeyVer") && Lines[i + 3].Contains("KeyCount"))
                    {
                        fbx_take_transformationChannels[1].SubTransformationChannel[0].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));

                        fbx_take_transformationChannels[1].SubTransformationChannel[0].KeyVer = Convert.ToInt32(Lines[i + 2].Trim(' ').Substring(8));

                        List<String> keys = new List<String>();
                        if (Lines[i + 4].Contains("Key: "))
                        {
                            int additionalCounter = 0;
                            List<String> KeyLines = new List<string>();

                            for (int f = i + 4; !Lines[f].Contains("Color: "); f++)
                            {
                                additionalCounter++;
                                KeyLines.Add(Lines[f]);

                            }
                            i += additionalCounter;

                            keys = FBX_getKeyLine(KeyLines, 5);
                        }

                        fbx_take_transformationChannels[1].SubTransformationChannel[0].Keys = keys;

                    }


                    if (Lines[i].Contains("Channel: \"Y\"") && Lines[i + 2].Contains("Color"))
                    {
                        fbx_take_transformationChannels[1].SubTransformationChannel[1].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        fbx_take_transformationChannels[1].SubTransformationChannel[1].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        string[] colorsplit = Lines[i + 2].Trim(' ').Substring(7).Split(',');
                        float[] color = new float[3];
                        color[0] = Convert.ToSingle(colorsplit[0], CultureInfo.InvariantCulture.NumberFormat);
                        color[1] = Convert.ToSingle(colorsplit[1], CultureInfo.InvariantCulture.NumberFormat);
                        color[2] = Convert.ToSingle(colorsplit[2], CultureInfo.InvariantCulture.NumberFormat);
                        fbx_take_transformationChannels[1].SubTransformationChannel[1].Color = color;
                    }
                    else if (Lines[i].Contains("Channel: \"Y\"") && Lines[i + 2].Contains("KeyVer") && Lines[i + 3].Contains("KeyCount"))
                    {
                        fbx_take_transformationChannels[1].SubTransformationChannel[1].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));

                        fbx_take_transformationChannels[1].SubTransformationChannel[1].KeyVer = Convert.ToInt32(Lines[i + 2].Trim(' ').Substring(8));

                        List<String> keys = new List<String>();
                        if (Lines[i + 4].Contains("Key: "))
                        {
                            int additionalCounter = 0;
                            List<String> KeyLines = new List<string>();

                            for (int f = i + 4; !Lines[f].Contains("Color: "); f++)
                            {
                                additionalCounter++;
                                KeyLines.Add(Lines[f]);

                            }
                            i += additionalCounter;

                            keys = FBX_getKeyLine(KeyLines, 5);
                        }

                        fbx_take_transformationChannels[1].SubTransformationChannel[1].Keys = keys;

                       
                    }

                    if (Lines[i].Contains("Channel: \"Z\"") && Lines[i + 2].Contains("Color"))
                    {
                        fbx_take_transformationChannels[1].SubTransformationChannel[2].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        fbx_take_transformationChannels[1].SubTransformationChannel[2].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        string[] colorsplit = Lines[i + 2].Trim(' ').Substring(7).Split(',');
                        float[] color = new float[3];
                        color[0] = Convert.ToSingle(colorsplit[0], CultureInfo.InvariantCulture.NumberFormat);
                        color[1] = Convert.ToSingle(colorsplit[1], CultureInfo.InvariantCulture.NumberFormat);
                        color[2] = Convert.ToSingle(colorsplit[2], CultureInfo.InvariantCulture.NumberFormat);
                        fbx_take_transformationChannels[1].SubTransformationChannel[2].Color = color;
                    }
                    else if (Lines[i].Contains("Channel: \"Z\"") && Lines[i + 2].Contains("KeyVer") && Lines[i + 3].Contains("KeyCount"))
                    {
                        fbx_take_transformationChannels[1].SubTransformationChannel[2].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));

                        fbx_take_transformationChannels[1].SubTransformationChannel[2].KeyVer = Convert.ToInt32(Lines[i + 2].Trim(' ').Substring(8));
                        fbx_take_transformationChannels[1].SubTransformationChannel[2].KeyVer = Convert.ToInt32(Lines[i + 3].Trim(' ').Substring(10));

                        List<String> keys = new List<String>();
                        if (Lines[i + 4].Contains("Key: "))
                        {
                            int additionalCounter = 0;
                            List<String> KeyLines = new List<string>();

                            for (int f = i + 4; !Lines[f].Contains("Color: "); f++)
                            {
                                additionalCounter++;
                                KeyLines.Add(Lines[f]);

                            }
                            i += additionalCounter;

                            keys = FBX_getKeyLine(KeyLines, 5);
                        }
                        fbx_take_transformationChannels[1].SubTransformationChannel[2].Keys = keys;

                    }

                }
                #endregion

                //Read the Scale Transform type
                #region Read Scale
                if (readS)
                {

                    fbx_take_transformationChannels[2].Type = 'S';
                    if (Lines[i].Contains("Channel: \"X\"") && Lines[i + 2].Contains("Color"))
                    {
                        fbx_take_transformationChannels[2].SubTransformationChannel[0].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        fbx_take_transformationChannels[2].SubTransformationChannel[0].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        string[] colorsplit = Lines[i + 2].Trim(' ').Substring(7).Split(',');
                        float[] color = new float[3];
                        color[0] = Convert.ToSingle(colorsplit[0], CultureInfo.InvariantCulture.NumberFormat);
                        color[1] = Convert.ToSingle(colorsplit[1], CultureInfo.InvariantCulture.NumberFormat);
                        color[2] = Convert.ToSingle(colorsplit[2], CultureInfo.InvariantCulture.NumberFormat);
                        fbx_take_transformationChannels[2].SubTransformationChannel[0].Color = color;

                    }
                    else if (Lines[i].Contains("Channel: \"Z\"") && Lines[i + 2].Contains("KeyVer") && Lines[i + 3].Contains("KeyCount"))
                    {
                        fbx_take_transformationChannels[2].SubTransformationChannel[0].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        fbx_take_transformationChannels[2].SubTransformationChannel[0].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        fbx_take_transformationChannels[2].SubTransformationChannel[0].KeyVer = Convert.ToInt32(Lines[i + 2].Trim(' ').Substring(8));

                        List<String> keys = new List<String>();
                        if (Lines[i + 4].Contains("Key: "))
                        {
                            int additionalCounter = 0;
                            List<String> KeyLines = new List<string>();

                            for (int f = i + 4; !Lines[f].Contains("Color: "); f++)
                            {
                                additionalCounter++;
                                KeyLines.Add(Lines[f]);

                            }
                            i += additionalCounter;

                            keys = FBX_getKeyLine(KeyLines, 5);
                        }
                        fbx_take_transformationChannels[2].SubTransformationChannel[0].Keys = keys;
                                                
                    }


                    if (Lines[i].Contains("Channel: \"Y\"") && Lines[i + 2].Contains("Color"))
                    {
                        fbx_take_transformationChannels[2].SubTransformationChannel[1].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        fbx_take_transformationChannels[2].SubTransformationChannel[1].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        string[] colorsplit = Lines[i + 2].Trim(' ').Substring(7).Split(',');
                        float[] color = new float[3];
                        color[0] = Convert.ToSingle(colorsplit[0], CultureInfo.InvariantCulture.NumberFormat);
                        color[1] = Convert.ToSingle(colorsplit[1], CultureInfo.InvariantCulture.NumberFormat);
                        color[2] = Convert.ToSingle(colorsplit[2], CultureInfo.InvariantCulture.NumberFormat);
                        fbx_take_transformationChannels[2].SubTransformationChannel[1].Color = color;
                    }
                    else if (Lines[i].Contains("Channel: \"Z\"") && Lines[i + 2].Contains("KeyVer") && Lines[i + 3].Contains("KeyCount"))
                    {
                        fbx_take_transformationChannels[2].SubTransformationChannel[1].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        fbx_take_transformationChannels[2].SubTransformationChannel[1].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        fbx_take_transformationChannels[2].SubTransformationChannel[1].KeyVer = Convert.ToInt32(Lines[i + 2].Trim(' ').Substring(8));
                        fbx_take_transformationChannels[2].SubTransformationChannel[1].KeyVer = Convert.ToInt32(Lines[i + 3].Trim(' ').Substring(10));

                        List<String> keys = new List<String>();
                        if (Lines[i + 4].Contains("Key: "))
                        {
                            int additionalCounter = 0;
                            List<String> KeyLines = new List<string>();

                            for (int f = i + 4; !Lines[f].Contains("Color: "); f++)
                            {
                                additionalCounter++;
                                KeyLines.Add(Lines[f]);

                            }
                            i += additionalCounter;

                            keys = FBX_getKeyLine(KeyLines, 5);
                        }
                        fbx_take_transformationChannels[2].SubTransformationChannel[1].Keys = keys;
                                               
                    }


                    if (Lines[i].Contains("Channel: \"Z\"") && Lines[i + 2].Contains("Color"))
                    {
                        fbx_take_transformationChannels[2].SubTransformationChannel[2].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        fbx_take_transformationChannels[2].SubTransformationChannel[2].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        string[] colorsplit = Lines[i + 2].Trim(' ').Substring(7).Split(',');
                        float[] color = new float[3];
                        color[0] = Convert.ToSingle(colorsplit[0], CultureInfo.InvariantCulture.NumberFormat);
                        color[1] = Convert.ToSingle(colorsplit[1], CultureInfo.InvariantCulture.NumberFormat);
                        color[2] = Convert.ToSingle(colorsplit[2], CultureInfo.InvariantCulture.NumberFormat);
                        fbx_take_transformationChannels[2].SubTransformationChannel[2].Color = color;
                    }
                    else if (Lines[i].Contains("Channel: \"Z\"") && Lines[i + 2].Contains("KeyVer") && Lines[i + 3].Contains("KeyCount"))
                    {
                        fbx_take_transformationChannels[2].SubTransformationChannel[2].channel = Convert.ToChar(Lines[i].Trim(' ').Substring(9).Trim('{').Trim('\"').Trim(' ').Trim('\"'));
                        fbx_take_transformationChannels[2].SubTransformationChannel[2].Default = Convert.ToInt32(Lines[i + 1].Trim(' ').Substring(9));
                        fbx_take_transformationChannels[2].SubTransformationChannel[2].KeyVer = Convert.ToInt32(Lines[i + 2].Trim(' ').Substring(8));
                        fbx_take_transformationChannels[2].SubTransformationChannel[2].KeyVer = Convert.ToInt32(Lines[i + 3].Trim(' ').Substring(10));


                        List<String> keys = new List<String>();
                        if (Lines[i + 4].Contains("Key: "))
                        {
                            int additionalCounter = 0;
                            List<String> KeyLines = new List<string>();

                            for (int f = i + 4; !Lines[f].Contains("Color: "); f++)
                            {
                                additionalCounter++;
                                KeyLines.Add(Lines[f]);

                            }
                            i += additionalCounter;

                            keys = FBX_getKeyLine(KeyLines, 5);
                        }
                        fbx_take_transformationChannels[2].SubTransformationChannel[2].Keys = keys;
                                                
                    }
                }
                #endregion
            }
            transformchannel.TransformMainChannel = fbx_take_transformationChannels;
            jointchannel.TransformChannels.Add(transformchannel);
            return jointchannel;
        }
        
        public static List<IFBXObject> FBX_CreateObjectList(List<String> Lines)
        {
            List<IFBXObject> ObjectsList = new List<IFBXObject>();
            List<String> ObjectLines;

            bool startReading = false;
            int opensDetected = 0;
            int closesDetected = 0;

            int InnerScope = 0;
            int start = 0;
            int end = 0;
            int Length = 0;
            int i = 0;

            #region Detect Definition and Read first 2 lines
            do
            {
                if (Lines[i].Contains("Objects: "))
                {
                    start = i;
                    startReading = true;
                }

                if (Lines[i].Contains("{") && startReading)
                {
                    opensDetected++; InnerScope++;
                }
                if (Lines[i].Contains("}") && startReading)
                {
                    closesDetected++; InnerScope--;
                }
                if (InnerScope == 0 && closesDetected == opensDetected && startReading)
                {
                    end = i;
                    break;
                }
                i++;
            } while (true);

            Length = end - start + 1;

            #endregion

            ObjectLines = new List<string>();

            for (int count = start; count <= end; count++)
            {                
                ObjectLines.Add(Lines[count]);
            }
            ObjectLines.RemoveAt(0); ObjectLines.RemoveAt(ObjectLines.Count - 1);

            List<int> BeginOfNodes, EndOfNodes, NodeLengths;
            BeginOfNodes = new List<int>(); EndOfNodes =new List<int>(); NodeLengths = new List<int>();

            bool nodeClosed = true;
            InnerScope = 0;
            opensDetected = 0;
            closesDetected = 0;

            #region Detect Nodes
            for (i = 0; i <ObjectLines.Count; i++)
            {

                if (ObjectLines[i].Contains("{") && nodeClosed)
                {
                    BeginOfNodes.Add(i);
                    nodeClosed = false;
                    opensDetected++;
                    continue;
                }

                if (ObjectLines[i].Contains("{"))
                {
                    opensDetected++;
                }

                if (ObjectLines[i].Contains("}"))
                {
                    closesDetected++; 
                }

                if(ObjectLines[i].Contains("}") && (nodeClosed == false) && (opensDetected == closesDetected) )
                {
                    nodeClosed = true;
                    EndOfNodes.Add(i);


                    closesDetected = 0;
                    opensDetected = 0;
                 
                    continue;
                }
            }
            #endregion

            List<string> NodeLines = new List<string>();
            for(i = 0; i < BeginOfNodes.Count; i++)
            {
                for(int j = BeginOfNodes[i]; j <= EndOfNodes[i]; j++)
                {
                    NodeLines.Add(ObjectLines[j]);
                }

                if (NodeLines[0].Contains("Mesh") && NodeLines[0].Contains("Model"))
                    ObjectsList.Add(FBX_CreateModelMesh(NodeLines));

                if (NodeLines[0].Contains("Model") && NodeLines[0].Contains("LimbNode"))
                    ObjectsList.Add(FBX_CreateModelLimbNode(NodeLines));

                if (NodeLines[0].Contains("Pose") && NodeLines[0].Contains("BindPose"))
                    ObjectsList.Add(FBX_CreateModelBindPose(NodeLines));

                if(NodeLines[0].Contains("Material"))
                    ObjectsList.Add(FBX_CreateMaterial(NodeLines));

                if (NodeLines[0].Contains("Deformer") && NodeLines[0].Contains("Skin"))
                    ObjectsList.Add(FBX_CreateDeformerSkin(NodeLines));

                if (NodeLines[0].Contains("Deformer") && NodeLines[0].Contains("Cluster"))
                    ObjectsList.Add(FBX_CreateDeformerCluster(NodeLines));

                if (NodeLines[0].Contains("Video") && NodeLines[0].Contains("Clip"))
                    ObjectsList.Add(FBX_CreateVideo(NodeLines));
                
                if (NodeLines[0].Contains("Texture") && NodeLines[0].Contains("TextureVideoClip"))
                    ObjectsList.Add(FBX_CreateTexture(NodeLines));
                
                NodeLines.Clear();
            }

            return ObjectsList;
        }

        public static FBX_Take FBX_CreateTake(List<String> Lines)
        {
            
            FBX_Take take = new FBX_Take();
            take.JointChannels = new List<FBX_ChannelPerJoint>();
            List<String> TakeLines = new List<string>(),TakeLinesNew = new List<string>();
            int InnerScope = 0, opensDetected = 0, closesDetected = 0, start, end;
            bool startReading = false;

            #region Go Into the Takes  Node
            for (int i = 0; i < Lines.Count; i++)
            {
                if ((Lines[i].StartsWith("Takes: ") && Lines[i].Contains("{")))
                {
                    start = i;
                    opensDetected++;
                    InnerScope++;
                    startReading = true;
                }
                if (Lines[i].Contains("}"))
                {
                    InnerScope--;
                    closesDetected++;

                }
                if (closesDetected == opensDetected && InnerScope == 0)
                {
                    end = i;
                    startReading = false;

                }
                if (startReading)
                    TakeLines.Add(Lines[i].Trim(' '));
            }                                   
            TakeLines.RemoveAt(0); TakeLines.RemoveAt(TakeLines.Count - 1);
             #endregion

            #region Get Data from the first Take -> will be the only mostly so no further
            for (int i = 0; i < TakeLines.Count; i++)
            {
                if (TakeLines[i].StartsWith("LocalTime: "))
                {
                    string[] split = TakeLines[i].Substring(11).Split(',');
                    take.LocalTime = new long[2];
                    take.LocalTime[0] = Convert.ToInt64(split[0], CultureInfo.InvariantCulture.NumberFormat);
                    take.LocalTime[1] = Convert.ToInt64(split[1], CultureInfo.InvariantCulture.NumberFormat);
                }
                if (TakeLines[i].StartsWith("ReferenceTime: "))
                {
                    string[] split = TakeLines[i].Substring(15).Split(',');
                    take.ReferenceTime = new long[2];
                    take.ReferenceTime[0] = Convert.ToInt64(split[0], CultureInfo.InvariantCulture.NumberFormat);
                    take.ReferenceTime[1] = Convert.ToInt64(split[1], CultureInfo.InvariantCulture.NumberFormat);
                }
                if (TakeLines[i].StartsWith("FileName: "))
                {
                    take.FileName = TakeLines[i].Substring(10);
                }
            }
            #endregion

            #region Now go into the first Take node and remove the opening and ending brackets from take 001
            List<int> JointBegin = new List<int>(), JointEnd = new List<int>();
            opensDetected = 0; closesDetected = 0; InnerScope = 0; startReading = false;

            for (int i = 0; i < TakeLines.Count; i++)
            {
                if ((TakeLines[i].StartsWith("Take: ") && TakeLines[i].Contains("{")))
                {
                    start = i;
                    opensDetected++;
                    InnerScope++;
                    startReading = true;
                    continue;
                }
                if (TakeLines[i].Contains("{"))
                {
                    InnerScope++;
                    opensDetected++;
                }
                if (TakeLines[i].Contains("}"))
                {
                    InnerScope--;
                    closesDetected++;
                }
                if (closesDetected == opensDetected && InnerScope == 0)
                {
                    end = i;
                    startReading = false;
                }
                if (startReading)
                    TakeLinesNew.Add(TakeLines[i].Trim(' '));
            }           
            #endregion
            TakeLines.Clear(); //Remove the TakeLines List, we refil it again now

            //Now we have only each Joints transformation channel left in the list ;)
            #region Now inside the first take node, we will get the start and end of Each joint channel, detects the Both Model take nodes
            JointBegin = new List<int>();
            JointEnd = new List<int>();
            opensDetected = 0; closesDetected = 0; InnerScope = 0; startReading = false;
            
            for (int i = 0; i < TakeLinesNew.Count; i++)
            {
                if ((TakeLinesNew[i].StartsWith("Model: ") && TakeLinesNew[i].Contains("{")))
                {
                    JointBegin.Add( i);
                    opensDetected++;
                    InnerScope++;
                    startReading = true;
                    continue;
                }
                if (TakeLinesNew[i].Contains("{") && startReading)
                {
                    InnerScope++;
                    opensDetected++;
                }
                if (TakeLinesNew[i].Contains("}") && startReading)
                {
                    InnerScope--;
                    closesDetected++;
                }
                if (closesDetected == opensDetected && InnerScope == 0 && startReading)
                {
                    JointEnd.Add(i);
                    startReading = false;                    
                }
                
            }
            #endregion
            List<String> CurrentLines = new List<string>();
            FBX_ChannelPerJoint channelcurrent;
            for (int i = 0; i < JointBegin.Count; i++)
            {
                channelcurrent = new FBX_ChannelPerJoint();
                for (int j = JointBegin[i]; j <= JointEnd[i]; j++)
                {
                    CurrentLines.Add(TakeLinesNew[j]);
                }                
                channelcurrent = FBX_CreateChannelPerJoint(CurrentLines);
                CurrentLines.Clear();
                take.JointChannels.Add(channelcurrent);
            }

            return take;
        }

    }
}



