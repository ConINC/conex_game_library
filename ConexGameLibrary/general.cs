﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace ConexGameLibrary
{
    // Very evil class!
    public static class ObjectCopier
    {
        public static T Clone<T>(T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("Only serializable objects allowed", "source");
            }

            if (source == null)
            {
                return default(T); //returns null
            }

            BinaryFormatter formatter = new BinaryFormatter();
            var s = new MemoryStream();
            using (s) //correctly use of idisposable objects
            {
                formatter.Serialize(s, source);
                s.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(s);
            }
            
        }
    }
}
