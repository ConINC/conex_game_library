﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Text;
using ConexGameLibrary;
using System.IO;
using System.Drawing;

namespace ConexGameLibrary
{

    public class GameSegment
    {
        public SpriteBatch spritebatch;
        public List<SpriteFont> fonts;

        public Single[] PlayerHeights;
        public Level Level;
        public List<Effect> Effects;
        public List<Player> Players;
        public List<Camera> Cameras;
        public Viewport[] viewports;
        public List<Matrix> Perspectives;

        public List<PhysicsModel> PhysicsModels;
        public List<Vehicle> Vehicles;
        public Texture2D normal_basic, specular_basic;

        public List<Mirror> Mirrors;
        public List<Shadow> Shadows;
        public Texture2D FlashLightTexture;

        public EffectSprite_Container spriteContainer;
        public List<ParticlePool> ParticlePoolCollection;
        public Projectile_Pool projectilePool;
        
        public List<Sound_Container> Sound_Collection; //TESTING
        public List<Sound_Container> GeneralSound_Effects;

        public List<AnimationContainer> Animation_Collections;
        public AmmoTypes ammotypes = new AmmoTypes();
        public NPC_Group npc_group;

        public NPC_Path_Collection Npc_Path_collection;

        public PhysicsSpere_Container sphere_container;

        public List<I_GameItems> itemsInMap;

        public Hud_Settings_Game hudsettings;

        public Joystick_Input joystickinput = new Joystick_Input();

        public GameSegment()
        {
            this.GeneralSound_Effects = new List<Sound_Container>();
            hudsettings = new Hud_Settings_Game();
            fonts = new List<SpriteFont>();
            itemsInMap = new List<I_GameItems>();
            sphere_container = new PhysicsSpere_Container();
            Npc_Path_collection = new NPC_Path_Collection();
            spriteContainer = new EffectSprite_Container(100);

            Shadows = new List<Shadow>();
            Mirrors = new List<Mirror>();
            Level = new Level();
            Effects = new List<Effect>();
            
            Players = new List<Player>();
            npc_group = new NPC_Group();

            Cameras = new List<Camera>();
            this.viewports = new Viewport[4];
            this.Perspectives = new List<Matrix>();
            PlayerHeights = new Single[4];
            PlayerHeights[0] = 0; PlayerHeights[1] = 0; PlayerHeights[2] = 0; PlayerHeights[3] = 0;

            PhysicsModels = new List<PhysicsModel>();
            Vehicles = new List<Vehicle>();

            this.ParticlePoolCollection = new List<ParticlePool>();
            this.projectilePool = new Projectile_Pool();

            this.Animation_Collections = new List<AnimationContainer>();
            this.Sound_Collection = new List<Sound_Container>();

        }        
    }


}
