﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConexGameLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ConexGameLibrary
{
    class Lighting
    {
    }

    public class Mirror
    {
        public float Reflectivity;
        public float Distance;
        public Vector4 MirrorColor;
        public RenderTarget2D RenderTarget;
        public Vector3 Position;
        public Vector3 Rotation;
        public Vector3 LookAt = Vector3.Forward;
        public Vector3 UpVector = Vector3.Up;
        public Matrix ViewMatrix;
        public Matrix ProjectionMirror;
        public Texture2D RenderedReflection;

        public Mirror(Vector3 pos, Vector3 rot, float aspectRatio,float FieldOFview)
        {
            ViewMatrix = Matrix.CreateLookAt(pos, Vector3.Zero, UpVector);
            ProjectionMirror = Matrix.CreatePerspectiveFieldOfView(FieldOFview, aspectRatio, 1, 100100);
        }        

        public Matrix Get_Mirror_ViewMatrix()
        {
            return ViewMatrix;
        }

        public Matrix Get_Mirror_Projection()
        {
            return ProjectionMirror;
        }
    }

    public class Shadow
    {
        public RenderTarget2D RenderTarget;
        public Vector3 Position;
        public Vector3 Rotation;
        public Vector3 LookAtDirection = Vector3.Forward;
        public Vector3 UpVector = Vector3.Up;
        public Matrix ViewMatrix;
        public Matrix ProjectionShadow;

        public Shadow(Vector3 pos, Vector3 Target, Vector3 upvec, float aspectRatio, float FieldOFview)
        {
            this.Position = pos;
            this.UpVector = upvec;
            ViewMatrix = Matrix.CreateLookAt(pos,Target , upvec);
            ProjectionShadow = Matrix.CreatePerspectiveFieldOfView(FieldOFview, aspectRatio, 1, 50600);
            this.LookAtDirection = Target - pos;
            
            this.Position = pos;
        }

        public Matrix Get_Shadow_ViewMatrix()
        {
            return ViewMatrix;
        }

        public Matrix Get_Shadow_Projection()
        {
            return ProjectionShadow;
        }

        public void UpdateViewMatrix(Vector3 playerpos)
        {
            ViewMatrix = Matrix.CreateLookAt(Position, playerpos, UpVector);
        }
    }
}
