﻿using System;
using System.Collections.Generic;
using ConexGameLibrary;
using Microsoft.Xna.Framework;

namespace ConexGameLibrary
{
     
    //A class Holding Ammotypes
    public static class AmmoTypes
    {
        public static Ammo_Type_Data type_22 = new Ammo_Type_Data(1.8f, 250, "22lr");
        public static Ammo_Type_Data type_9mm = new Ammo_Type_Data(6.0f, 400, "9mm");
        public static Ammo_Type_Data type_380 = new Ammo_Type_Data(6.2f, 300, "380auto");
        public static Ammo_Type_Data type_57mm = new Ammo_Type_Data(2, 800, "5.7mm");
        public static Ammo_Type_Data type_46mm = new Ammo_Type_Data(2.7f, 700, "4.6mm");
        public static Ammo_Type_Data type_38 = new Ammo_Type_Data(10, 300, "38special");
        public static Ammo_Type_Data type_357m = new Ammo_Type_Data(10, 500, "357magnum");
        public static Ammo_Type_Data type_44m = new Ammo_Type_Data(15, 400, "44magnum");
        public static Ammo_Type_Data type_45acp = new Ammo_Type_Data(12, 250, "45acp");                     
    }
    public class Ammo_Type_Data
    {
        public String AmmoType;
        public float Gramms;
        public float Speed;

        public Ammo_Type_Data(float gramms, float speed, string Type)
        {
            this.Gramms = gramms;
            this.Speed = speed;
            this.AmmoType = Type;
        }

    }

    //Gun related Classes
    public class Magazine
    {
        public int BulletCount;
        public String AmmoType;

        public Magazine(int bullets, string type)
        {
            BulletCount = bullets;
            AmmoType = type;
        }
    }

    public interface I_Firearm
    {        
        void Shoot();
        void Chamber();
        void SetFiringMOde();       

        void Update(Vector3 Position, Vector3 Rotation);
        void Reload(  List<Magazine> mags);
    }

    public interface I_Meelee
    {
        void Shoot();
    }

    public class Firearm_User
    {
        public String currentGunName; //If Noone, uses Hands

        //Use the blow for all guns, change properties with each gun
        public Vector3 Position; //Relative to hand
        public Vector3 Rotation; //Relative to Hand
        public Vector3 newAimRot;

        public Vector3 forwardVector = Vector3.Forward;
        public Matrix currentMatrix;
        public Vector3 AimDirection;

        public Vector3 CasingExhaustPosition;
        public Vector3 CastingExhaustDirection;

        public int ShootAnimationLength;
        public int ReloadTimer, ShootDelayTimer;
        public int ReloadAnimationLength;
        public int ReloadTime, ShootTime;

        public float SineCounter, CosineCounter; //For AimError
        public float AimError;
        public float AimErrorSpeed; //Speed of functionCounter  for Sine/Cosine

        public float AnimationPlaybackSpeed;
        public float AimErrorRunning, AimErrorWalking, AimErrorSneaking, AimErrorStanding;
        public string[] FiringModes; //0=safe, 1=semi, 2=Auto

        public int BulletsPerShot;
        public int BulletsPerMagazine;
        public int Bullets_In_Current_Magazine;
        public int BulletInChamber = 0;
        public Ammo_Type_Data AmmoType;

        //Only use the animationController for these, the mesh is used from a global list!
        public AnimationController worldGunModel_animController;
        public AnimationController viewGunModel_animController;

        public Firearm_User()
        {
            this.currentGunName = "Hands";
            ReloadTimer = 0;
            ShootDelayTimer = 0;
            AimError = 0;
            AimErrorSpeed = 1;
            SineCounter = 0;
            CosineCounter = 0;
            ShootAnimationLength = 0;
            
            AimErrorRunning = 2;
            AimErrorSneaking = 0.5f;
            AimErrorWalking = 1.0f;
            AimError = AimErrorWalking;

            //this.BulletsPerShot = bulletsPerShot;
            //this.BulletsPerMagazine = bulletsPerMag;
            //this.Bullets_In_Current_Magazine = startAmmo;
            //this.AmmoType = ammotype;
            
        }

        //Dont forget Semi/Auto Timer, currently its just working for Auto
        public void Update(Vector3 Position, Vector3 Rotation)
        {
            this.Position = Position;
            this.Rotation = Rotation;

            if (ReloadTimer > 0)
                ReloadTimer--;

            if (ShootDelayTimer > 0)
                ShootDelayTimer--;

            if (SineCounter >= MathHelper.TwoPi)
                SineCounter = 0;
            else
                SineCounter += AimErrorSpeed;

            if (CosineCounter >= MathHelper.TwoPi)
                CosineCounter = 0;
            else
                CosineCounter += AimErrorSpeed;

            float newXrot = Rotation.X + (float)Math.Sin(SineCounter) * AimError * 0.25f;
            float newYrot = Rotation.Y + (float)Math.Cos(CosineCounter) * AimError * 0.25f;
            newAimRot.X = newXrot;
            newAimRot.Y = -newYrot + MathHelper.Pi;
            newAimRot.Z = 0;

            currentMatrix = Matrix.CreateRotationY(newAimRot.Y) * Matrix.CreateTranslation(Position);

            AimDirection = Vector3.Transform(forwardVector, currentMatrix);
        }

        public void Reload(  List<Magazine> mags)
        {
            if (mags.Count > 0 && String.Equals(mags[mags.Count - 1].AmmoType, AmmoType.AmmoType) && ReloadTimer == 0 && ShootDelayTimer == 0)
            {
                ReloadTimer = ReloadTime;

                mags.RemoveAt(mags.Count - 1);
                Bullets_In_Current_Magazine = mags[mags.Count - 1].BulletCount;
            }
        }

        public void Chamber()
        {
            if (Bullets_In_Current_Magazine > 0 && ReloadTimer == 0)
            {
                ReloadTimer = ReloadTime;

                BulletInChamber = 1;
                Bullets_In_Current_Magazine--;
            }
        }

        public void Shoot()
        {
            if (BulletInChamber == 1 && ShootDelayTimer == 0)
            {
                //Fire, play Animation, adjust AimError and else
                BulletInChamber = 0;
                ShootDelayTimer = ShootTime;

                if (Bullets_In_Current_Magazine > 0)
                {
                    Bullets_In_Current_Magazine--;
                    BulletInChamber = 1;
                }
            }
        }

        public void SetFiringMOde()
        {
            

        }

        public void CopyGunProperties(Generic_Firearm firearm)
        {
            currentGunName = firearm.gunName;
            FiringModes = firearm.FiringModes;
            CasingExhaustPosition = firearm.CasingExhaustPosition;
            CastingExhaustDirection = firearm.CastingExhaustDirection;
            ShootAnimationLength = firearm.ShootAnimationLength;
            ReloadTime = firearm.ReloadTime;
            ShootTime = firearm.ShootTime;
            AimError = firearm.AimError;
            BulletsPerMagazine = firearm.BulletsPerMagazine;
            BulletsPerShot = firearm.BulletsPerShot;
            AmmoType = firearm.AmmoType;       
        }
    }

    public class Pistol : I_Firearm
    {
        public Vector3 Position; //Relative to hand
        public Vector3 Rotation; //Relative to Hand

        public Vector3 newAimRot;
        public Vector3 forwardVector = Vector3.Forward;
        public Matrix currentMatrix;
        public Vector3 AimDirection;

        public String Name;
        
        public Vector3 CasingExhaustPosition;
        public Vector3 CastingExhaustDirection;

        public int ShootAnimationLength;
        public int ReloadTimer, ShootDelayTimer;
        public int ReloadAnimationLength;
        public int ReloadTime, ShootTime;

        public float SineCounter, CosineCounter; //For AimError
        public float AimError;
        public float AimErrorSpeed; //Speed of functionCounter  for Sine/Cosine

        public float AnimationPlaybackSpeed;
        public float AimErrorRunning, AimErrorWalking, AimErrorSneaking, AimErrorStanding;
        public int FiringMode = 0; //0=safe, 1=semi, 2=Auto

        public int BulletsPerShot;
        public int BulletsPerMagazine;
        public int Bullets_In_Current_Magazine;

        public int BulletInChamber = 0;

        public Ammo_Type_Data AmmoType;

        //
        public AnimatedModelMesh worldGunModel;

        public AnimatedModelMesh viewGunModel;
        

        public Pistol(int bulletsPerShot, int bulletsPerMag, int startAmmo, Ammo_Type_Data ammotype)
        {

            ReloadTimer = 0;
            ShootDelayTimer = 0;
            AimError = 0;
            AimErrorSpeed = 1;
            SineCounter = 0;
            CosineCounter = 0;
            ShootAnimationLength = 0;
            
            AimErrorRunning = 2;
            AimErrorSneaking = 0.5f;
            AimErrorWalking = 1.0f;
            AimError = AimErrorWalking;

            this.BulletsPerShot = bulletsPerShot;
            this.BulletsPerMagazine = bulletsPerMag;
            this.Bullets_In_Current_Magazine = startAmmo;
            this.AmmoType = ammotype;
            
        }

        //Dont forget Semi/Auto Timer, currently its just working for Auto
        public void Update(Vector3 Position, Vector3 Rotation)
        {
            this.Position = Position;
            this.Rotation = Rotation;

            if (ReloadTimer > 0)
                ReloadTimer--;

            if (ShootDelayTimer > 0)
                ShootDelayTimer--;

            if (SineCounter >= MathHelper.TwoPi) 
                SineCounter = 0;
            else
                SineCounter += AimErrorSpeed;

            if (CosineCounter >= MathHelper.TwoPi)
                CosineCounter = 0;
            else
                CosineCounter += AimErrorSpeed;

            float newXrot = Rotation.X + (float)Math.Sin(SineCounter) * AimError * 0.25f;
            float newYrot = Rotation.Y + (float)Math.Cos(CosineCounter) * AimError * 0.25f;
            newAimRot.X = newXrot;
            newAimRot.Y = -newYrot + MathHelper.Pi;
            newAimRot.Z = 0;

            currentMatrix = Matrix.CreateRotationY(newAimRot.Y) * Matrix.CreateTranslation(Position);

            AimDirection = Vector3.Transform(forwardVector, currentMatrix);
        }

        public void Reload(  List<Magazine> mags)
        {
            if (mags.Count > 0 && String.Equals(mags[mags.Count-1].AmmoType, AmmoType.AmmoType) && ReloadTimer == 0 && ShootDelayTimer == 0)
            {
                ReloadTimer = ReloadTime;

                mags.RemoveAt(mags.Count-1);
                Bullets_In_Current_Magazine = mags[mags.Count-1].BulletCount;
            }
        }

        public void Chamber()
        {
            if (Bullets_In_Current_Magazine > 0 && ReloadTimer == 0)
            {
                ReloadTimer = ReloadTime / 2;
                
                BulletInChamber = 1;
                Bullets_In_Current_Magazine--;
            }
        }

        public void Shoot()
        {
            if (BulletInChamber == 1 && ShootDelayTimer == 0)
            {
                //Fire, play Animation, adjust AimError and else
                BulletInChamber = 0;
                ShootDelayTimer = ShootTime;

                if (Bullets_In_Current_Magazine > 0)
                {
                    Bullets_In_Current_Magazine--;
                    BulletInChamber = 1;
                }
            }
        }

        public void SetFiringMOde()
        {
            if (FiringMode < 3)
                FiringMode++;
            else
                FiringMode = 0;
 
        }
    }
}
