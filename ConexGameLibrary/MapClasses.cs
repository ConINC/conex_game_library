﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.IO;

namespace ConexGameLibrary
{
    public interface IEntities
    {
    }

    public interface Triggers
    {
    }

    public class SkyBox
    {
        public MeshGroup Mesh;
        public Phong Shader;
    }

    public class MapProperties
    {
        public float Gravity;
        public short Weather;
        public short time;        
    }

    public class Light
    {
        public string name;
        public int lighttype;
        public RenderTarget2D top, bottom, left, front, right, back;
        public RenderTarget2D lightmap;
        public int res_x, res_y;
        public float lightIntensity;
        public byte falloff; //0 linear, 1 quadratic, 2 expon?
        public float distance; //falloff will be 0 there
        public Vector4 lightColor; //normalize for shader
        public Vector3 Position;
        public Vector3 Direction; //Only for directed
        
        public Matrix vmTop, vmBottom, vmLeft, vmFront, vmRight, vmBack;
        public Matrix viewDirected;
        public Matrix projection;

        //For directed
        public Light(string name, int lighttype, float intensity, byte foff, float dis, Vector4 lcolor, Vector3 pos, Vector3 dir, int w, int h, float fov)
        {
            this.Direction = dir;
            this.Position = pos;
            this.lightColor = lcolor;
            this.distance = dis;
            this.falloff = foff;
            this.lightIntensity = intensity;
            this.res_x = w;
            this.res_y = h;
            this.projection = createProjection(fov, w, h);
            this.name = name;
            this.lighttype = lighttype;

            Vector3 camUpVec;
            if (this.Direction == Vector3.Up || this.Direction == Vector3.Down)
            {
                camUpVec = Vector3.Forward;
            }
            else
                camUpVec = Vector3.Up;           
        }
                
        //Create a fieldof view matrix
        public Matrix createProjection(float fov, int wp, int hp)
        {
            return Matrix.CreatePerspectiveFieldOfView(fov, (float)wp / (float)hp, 0.2f, 1500);
        }
    }

    //--------------------------------------------------------------------//
    public class Level
    {
        public List<Block> Npc_Walk_floor; //FOr npcs to stay in place
        public List<Block> MapBlocks; //For collision
        public List<MeshGroup> MapMeshes; //Obsolete soon

        public Obj_Mesh[] visualMap; //new datatype, will replace ^
        public Obj_Material[] materials; 

        public List<SkyBox> MapSkyboxes;
        public List<IEntities> MapEntities;
        public List<Triggers> MapTriggers;
        public MapProperties MapProperties;
        public List<MeshGroup> LevelMeshesStatic;
        public Light[] mapLights;

        public bool draw_vb = false, draw_normal = true;

        public MapLoaderNew maploader;

        public Level()
        {
            maploader = new MapLoaderNew();
            MapBlocks = new List<Block>();
            MapMeshes = new List<MeshGroup>();
            Npc_Walk_floor = new List<Block>();
            MapSkyboxes = new List<SkyBox>();
            MapTriggers = new List<Triggers>();
            MapProperties = new MapProperties();
            LevelMeshesStatic = new List<MeshGroup>();

        }        
                
        //--------------------------------------------------------------------//        
    }
    
}
