﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.IO;
using Microsoft.Xna.Framework.Graphics;

namespace ConexGameLibrary
{
    public class HudElement : DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        Texture2D texture;
        Int32 hudPositionX, hudPositionY, hudSizeX,hudSizeY;
        string Path,basePath;
        FileStream filestream;
        Rectangle rectangle;        

        public HudElement(Game game,string basePath, ref SpriteBatch spriteBatch_p, Int32 hudPositionX, Int32 hudPositionY, Int32 hudSizeX, Int32 hudSizeY, string texturePath)
            : base(game)
        {
            this.basePath = basePath;
            this.Path = texturePath;
            this.spriteBatch = spriteBatch_p;
            Console.WriteLine("Spritebatch parameter initialised");
            this.hudPositionX = hudPositionX;
            this.hudPositionY = hudPositionY;
            this.hudSizeX = hudSizeX;
            this.hudSizeY = hudSizeY;
        }

        public override void Initialize()
        {
            rectangle = new Rectangle(hudPositionX, hudPositionY, hudSizeX, hudSizeY);
            filestream = new FileStream(basePath + Path, FileMode.Open);            
            texture = Texture2D.FromStream(Game.GraphicsDevice, filestream);
            //stream has to be closed
            filestream.Close();
            filestream = null;

            base.Initialize();
        }

        public override void Draw(GameTime gameTime)
        {
            if (spriteBatch != null)
            {
                spriteBatch.Begin();
                spriteBatch.Draw(texture, rectangle, Color.White);
                spriteBatch.End();
            }
            base.Draw(gameTime);

        }
    }
}
