﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ConexGameLibrary
{
    //Remove the r once the original is gone!
    public class SphereVolume_r
    {
        public Vector3 Position;
        public Single Radius;

        public SphereVolume_r(Vector3 position, Single radius)
        {
            this.Radius = radius;
            this.Position = position;
        }
    }

    public class Triangle_r
    {
        public Triangle_r()
        {
        }
        public Vector3 p1;
        public Vector3 p2;
        public Vector3 p3;
    }    

    public struct Plane_normal_r
    {
        public Single distanceOrig;        
        public Vector3 rVector;
        public Vector3 nVector;
        public Vector3 randomPoint;
    
        public Plane_normal_r(Single distanceOrig, Vector3 nVector, Vector3 rVector, Vector3 randomPointonPlane)
        {
            this.distanceOrig = distanceOrig; //Simply one point on the plane
            this.nVector = nVector; //The cross product of 2 directional vectors in the plane (p3-p2) X (p3-p1) or d1 X d2
            this.rVector = rVector; //This is the distance of 2 points on the plane
            this.randomPoint = randomPointonPlane;
        }
    }
        
}
