using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;
using System.Collections;
using System.Globalization;

namespace ConexGameLibrary
{
    public class Sphere
    {
        public Sphere(Vector3 position, float radius)
        {
            this.radius = radius;
            this.position = position;
        }

        float radius { get; set; }
        Vector3 position { get; set; }

    }

    public class ModelEntity
    {
        public string EntityName;
        public String Modelname;
        public Vector3 Position;
        public Vector3 Rotation;
        public UInt16 modelEntityCount;
    }

    public class Item
    {
        public string ItemName
        {
            get;
            set;
        } 
        public VertexListModel ItemModel
        {
            get;
            set;
        } 
        public Vector3 position
        {
            get;
            set;
        } 
        public Vector3 rotation
        {
            get;
            set;
        } 
        
        //Only use for sphere sphere intersect

        public Item(VertexListModel ItemModel, Vector3 pos, Vector3 rot, Sphere sphere, string name)
        {
            this.ItemModel = ItemModel;
            this.position = pos;
            this.rotation = rot;
            this.ItemName = name;
        }
    }

    public class StaticShape
    {
        public VertexListModel staticpolygon 
        {            
            get;
            set;
        }
        public Material[] materials;

        public String tag //No use for it yet
        {
            get;
            set;
        } 

        public StaticShape(VertexListModel polygons, Material[] materials, string tag)
        {
            this.staticpolygon = polygons;
            this.tag = tag;
            this.materials = materials;
        }
    }

    public class Terrain
    {
        public VertexListModel mesh;
        public Material[] materials;
        public Terrain(VertexListModel mesh, Material[] materialsP)
        {
            this.mesh = mesh;
            this.materials = materialsP;
        }
    }

    public class Wall
    {
        public VertexListModel mesh;
        public Material[] materials;
        public Wall(VertexListModel mesh, Material[] materialsP)
        {
            this.mesh = mesh;
            this.materials = materialsP;
        }
    }

    public class MapFile
    {
        public List<ModelEntity> modelEntities;
        public List<Item> items;
        public List<StaticShape> staticShapes;
        

        public MapFile()
        {
            modelEntities = new List<ModelEntity>();
        }
        public void AddModel(String name, Vector3 position, Vector3 rotation, string Entityname)
        {
            ModelEntity modelEntity = new ModelEntity();
            modelEntity.Modelname = name;
            modelEntity.Position = position;
            modelEntity.Rotation = rotation;
            modelEntity.EntityName = Entityname;

            modelEntity.modelEntityCount = Convert.ToUInt16(modelEntities.Count);
            modelEntities.Add(modelEntity);
            modelEntity = null;
        }
        public void getFullListOfModelEntitys()
        {
            foreach (ModelEntity modelEnt in modelEntities)
            {
                Console.WriteLine(modelEnt.Modelname +" : " + modelEnt.Position + " : " + (modelEnt.modelEntityCount -1) + " : " + modelEnt.EntityName);
            }            
        }
        public void removeLatest()
        {
            UInt16 index = Convert.ToUInt16(modelEntities.Count -1);
            modelEntities.RemoveAt(index);
        }
        public string getLatestEntity()
        {
            string latestEntity;
            latestEntity = "Modelname: " + modelEntities[modelEntities.Count - 1].Modelname + " : Entity:  " + modelEntities[modelEntities.Count - 1].EntityName;
            return latestEntity;            
        }
        public void removeEntityByName(string byName)
        {
            UInt16 counterList;
            counterList = 0;

            foreach (ModelEntity mdlEnt in modelEntities)
            {
                if (mdlEnt.EntityName == byName)
                {
                    Console.WriteLine("Model entity {0}, with index {1}, modelName {2}",

                        modelEntities[counterList].modelEntityCount,
                        modelEntities[counterList].Modelname);

                    modelEntities.RemoveAt(counterList);
                    //RemoveAt automaticly sorts the list and makes sure there is no emtpy element inside it
                    //Must manualy reassign the entityCount inside the list, by using a counter """IMPORTANT"""
                }
            }
        }
    }
    
    public class Block
    {
        public String[] linesInBlock;

        public Block(int size)
        {
            linesInBlock = new string[size];
        }
        
    }

    public class Material
    {
        public string shaderName;
        public string texture;

        public Material()
        {        
        }        
    }
    
    public class MaterialBlocks
    {
        public string[] lines;
    }

    public class Map
    {        
        public List<Terrain> terrains;
        public List<Wall> walls;

        public Map()
        {
            walls = new List<Wall>();
            terrains = new List<Terrain>();
        }
    }    

    public class MapLoader
    {
        private List<Block> blocks;
        private List<Material> materials = new List<Material>();
        private List<MaterialBlocks> materialBlocks = new List<MaterialBlocks>();
        private string[] Lines;        
        string mapName;

        public MapLoader(string mapNameParam)
        {
            this.mapName = mapNameParam;
        }

        #region This whole stuff is used for the Map extraction

        private string[] readLines(string path)
        {
            return File.ReadAllLines(path);
        }
        //Needed Fields for the Process, these are kept alive until the entire map is done extracting        

        //Should be called once every model
        private int getModelLineCount()
        {
            byte gInModel = 0;
            int modelLineCount = 0;
            //Count the modellines first
            foreach (string line in Lines)
            {
                if (line.StartsWith("g "))
                {
                    gInModel++;
                    modelLineCount++;
                }

                else
                {
                    modelLineCount++;
                }

                if (gInModel <= 2)
                {

                }
                else
                {
                    break;
                }
            }

            return modelLineCount - 1;

        }

        //This function creates a block from the first Model
        //The first model's lines are written into a Block, the block
        //is then added to a generic List of Blocks. Has to work with resizeOriginArray()
        //to ensure that each time th function runs, the next Model is shifted to a block!
        private void cutOffBlock()
        {
            Block block = new Block(getModelLineCount());

            int counter = 0;
            foreach (string line in block.linesInBlock)
            {
                block.linesInBlock[counter] = Lines[counter];

                //Console.WriteLine(block.linesInBlock[counter]);

                counter++;
            }

            blocks.Add(block);
        }

        //This function creates a new array of lines, without the first model being part of it
        //So the entire process can restart
        private void resizeOriginArray()
        {
            int counter = getModelLineCount();
            int counter2 = 0;
            string[] array = new string[Lines.GetLength(0) - getModelLineCount()];

            while (counter < Lines.GetLength(0))
            {
                array[counter2] = Lines[counter];

                counter++; counter2++;
            }
            Lines = array;

            // THIS FUNCTION MUST RUN 1 MORE LINE to add the last face of the last model!
        }

        //This will count the double G's in the List of models, returns Model Amount
        private int getAmountOfModels()
        {
            int ModelsAmount = 0;

            int gInModel = 0;

            foreach (string line in Lines)
            {
                if (line.StartsWith("g "))
                {
                    gInModel++;
                }

                if (gInModel == 2)
                {
                    ModelsAmount++;
                    gInModel = 0;
                }
            }

            return ModelsAmount;

        }

        #endregion

        //Start the entire process of map extraction
        public Block[] extractMap(string path)
        {
            mapName = path;
            path = path + ".obj";

            //THIS FUNCTION IS CORRUPTED BLOCK COUNTER DOESNT WORK CORRECTLY!!!!!!!!!!!!

            blocks = new List<Block>();
            Block[] blocksArr;
            //Will get the Lines
            Lines = readLines(path);

            int repeat = getAmountOfModels();
            int counter = 1;
            while (counter <= repeat)
            {
                cutOffBlock();
                resizeOriginArray();
                counter++;
            }

            //COPY The fucking dynamic list to an array
            //to return it...
            int blockCounter = 0;
            blocksArr = new Block[blocks.Count];

            foreach (Block block in blocks)
            {
                blocksArr[blockCounter] = block;
                blockCounter++;
            }
            return blocksArr;

        }

        public Map MakeMapFile(string path)
        {
            Map map = new Map();
            Block[] blocksInFunc;
            String[] matlines;
            MaterialBlocks materialBlockInMakeMapFile;
            Material[] materials;

            //For the 3D Objects
            VertexListModel[] models;

            //The Model converter
            

            //First get the Blocks for each model
            blocksInFunc = extractMap(path);
            
            //Now get the Materials from .mtl file
            matlines = getMaterialFileLines(path);
            materialBlockInMakeMapFile = filterShaderInfo(matlines);
            materials = seperateMaterialBlock(materialBlockInMakeMapFile);

            //create all models from the .obj file           

            models = new VertexListModel[blocksInFunc.Length];


            int counterMakeModelFile = 0;
            while (counterMakeModelFile < blocksInFunc.Length)
            {
                models[counterMakeModelFile] = new VertexListModel();
                counterMakeModelFile++;
            }

            //THIS FUNCTION IS A TRICK TO GET AROUND THE List-index problem  with faces
            List<string> LinesInfrontCheat = new List<string>();
            

            foreach (Block block in blocksInFunc) // HERE I just cheat by adding all lists back into each block
            {
                foreach (string line in block.linesInBlock)
                {
                    if (line.StartsWith("v "))
                    {
                        LinesInfrontCheat.Add(line);
                    }
                    if (line.StartsWith("vt "))
                    {
                        LinesInfrontCheat.Add(line);
                    }
                    if (line.StartsWith("vn "))
                    {
                        LinesInfrontCheat.Add(line);
                    }
                }
            }

            
            var stop = false;
            int counterModel = 0;
            foreach(VertexListModel model in models)
            {
                if (stop == true)
                {
                    
                }
                
                CustomModelLoader cst = new CustomModelLoader();


                //This joins the first list, then the second ontop "IMPORTANT ORDER!
                string[] newLinesInBlock = new string[LinesInfrontCheat.Count + blocks[counterModel].linesInBlock.Length];
                int counterFirstList = 0;
                while(counterFirstList < LinesInfrontCheat.Count)
                {
                    newLinesInBlock[counterFirstList] = LinesInfrontCheat[counterFirstList];

                    counterFirstList++;
                }

                //Adds the Block ontop!
               
                int counterInBlock = 0;
                while(counterFirstList < newLinesInBlock.Length)
                {
                    newLinesInBlock[counterFirstList] = blocks[counterModel].linesInBlock[counterInBlock];

                    counterInBlock++;
                    counterFirstList++;
                }

                model.customVertex = cst.LoadModelFromLines(newLinesInBlock);
                counterModel++;
                cst = null;
                                
                stop = true;
            }

            //Now create StaticShapes and Terrainobjects by reading the blocks
            
            int counter = 0;
            //LOOP FOR EACH BLOCK!
            while(counter < blocksInFunc.Length)
            {
                //Per Block or Per Model!
                if ("objects_" == checkGroup(blocksInFunc[counter]))
                {
                    //The Materials In each block
                    string[] matsPerBlock = checkMaterials(blocksInFunc[counter]);
                    Material[] materialForABlock = new Material[matsPerBlock.Length];
                   
                    //Get the Materials in the Block/model

                    int innerMatCounter = 0; //Compare first shader in Model with all Shaders available to assign

                    #region Get MaterialArray "FOR A SINGLE BLOCK";
                    while (innerMatCounter < matsPerBlock.Length)
                    {

                        //Compare the name of shaders Per Block with the shaderName from Material list, if fits -> assign it
                         //Loop throught all materials to check if that ONE material of that ONE block is part of it, if so, add it!

                        int assignMaterialCounter = 0;
                        while (assignMaterialCounter < materials.Length)
                        {
                            if (matsPerBlock[innerMatCounter] == materials[assignMaterialCounter].shaderName)
                            {
                                //Is looping throught the material List and staying at firs material from Block -> looks which material in Block is the same as in Material List
                                //And assigns it
                                materialForABlock[innerMatCounter] = materials[assignMaterialCounter];
                            }                             
                            assignMaterialCounter++; //Since 5 materials available, check if first shader of Block is equal any of the, assign, loops materials amount 
                        }

                        innerMatCounter++; //Repeat the loop to check if the Next Shader from Block is the same from the material list
                    }
                    #endregion

                    map.terrains.Add(new Terrain(models[counter], materialForABlock));

                }

                counter++;
            }

            return map;
        }

        //Will check if the Block or Model contains one of the group names
        private string checkGroup(Block block)
        {
            string contains = string.Empty;
            foreach (String line in block.linesInBlock)
            {
                if (line.Contains("objects_"))
                {
                    contains = "objects_";
                }                
            }
            Console.WriteLine("model added to group {0}", contains);
            return contains;
        }
        /*
        //This needs to have a follower function that opens a stream to the MATERIAL File
        //The mtl file is not included in the map yet
        //THIS FUNCTION: gets the MATERIAL NAME, not texture name
        //The texture name has to be extracted from the matching materialName!!!
        //Like: material PhongE1SG -> in map.mtl  phongE1SG has a line called map_kd
        //This is the texture_name!!!!!
        */
        /*
        //repeat this function for each block
        //Compare with mats in the mtl file later
        */
        public string[] checkMaterials(Block block)
        {
            string[] materials;

            int matArrCounter = 0;
            foreach (String line in block.linesInBlock)
            {
                if (line.StartsWith("usemtl "))
                {
                    matArrCounter++;
                }
            }
            materials = new string[matArrCounter];


            int matCounter = 0;
            foreach (String line in block.linesInBlock)
            {
                if (line.StartsWith("usemtl "))
                {
                    //the materials will only contain the shader Name now
                    materials[matCounter] = line.Substring(7);
                    matCounter++;
                }
            }
            return materials;
        }
        //The function above copys the lines with the assigned shaders 
        //INSIDE the model file -> go into mtl for texture!

        //FINISHED! :-D
        #region FUNCTION for MaterilFile extraction!!!! 
        /// <summary>
        /// This function reads the .mtl file, filters out the shader and the texture name belonging to it
        /// As a result, it returns an array of Material classes, each containing a shaderName and textureName
        /// each Block from the map file, or each "Model" in the map can contain several shaders
        /// Another function called  CheckMaterials() Will return a string array, with the names of the used shader by that
        /// model -> When rendering the map/Model, each 3D Objekt in the Map is a class, consisting of the vertexlistmodel
        /// and a list of Materials... Prepare this function for drawing by creating an Effect class for each shader in the 3D-Objekt-class
        /// The Effects texture can be loaded using a stream, the textureName is inside the Material!!!! This might be doubled, since
        /// the raw VertexlistModel class can already carry several texture names, use the method of choice, but for the map files
        /// I would stick to the Materials classes first... and leave the texture array in the vertexlistmodel untouched!, both way work however
        /// 
        /// </summary>
        /// <param name="mapName"></param>
        /// <returns></returns>
        public string[] getMaterialFileLines(string mapName)
        {
            return File.ReadAllLines(mapName + ".mtl");
        }

        //This function will extract the the texture name/path for now
        //it will acces the .mtl file, and look for the shaders listed 
        //in the .obj file, if it finds the matching shader -> copy texture name
        //and return all texture names in an string[]

        //The material file is not referenced in the obj file as it normally would be
        //the function simply looks for an mtl with the same name as the map

        //Filters out newmtl_ line  and map_kd line and creates string array from it 
        public MaterialBlocks filterShaderInfo(string[] linesWithMats)
        {
            MaterialBlocks matBlock = new MaterialBlocks();
            

            string[] newMatLine = new string[linesWithMats.Length / 4];
            int counter = 0;

            foreach (string line in linesWithMats)
            {
                if (line.StartsWith("newmtl ") )
                {
                    newMatLine[counter] = line;
                    counter++;
                }
                else if (line.StartsWith("map_Kd "))
                {
                    newMatLine[counter] = line;
                    counter++;
                }
            }
            matBlock.lines = newMatLine;
            //throw new Exception();
            return matBlock;
        }
        //Works perfect -tested

        //Cut the matblocks into Material[] with each Material having 2 lines
        public Material[] seperateMaterialBlock(MaterialBlocks matBlock)
        {
            Material[] materials = new Material[matBlock.lines.Length / 2];

            int counter = 0;

            int lineCounter1 = 0; //Loops in 2-steps , 0 2 4 6 8 "for shader"
            int lineCounter2 = 1; // loops in 2 steps , 1 3 5 7 9 "for texture"

            while (counter < materials.Length)
            {
                materials[counter] = new Material(); //Create the class before using!

                materials[counter].shaderName = matBlock.lines[lineCounter1].Substring(7);
                
                materials[counter].texture = matBlock.lines[lineCounter2].Substring(7);


                lineCounter1++; lineCounter1++;
                lineCounter2++; lineCounter2++;
                counter++; //Counts the material classes, nothing else                
                
            }
            
            return materials;
            
        }
        //return all Lines from the material file
        #endregion
    }
    
    public class MapNew
    {        
    }
}


