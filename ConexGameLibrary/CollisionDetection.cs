﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace ConexGameLibrary
{
    public class RayTriangle_CollisionData
    {
        //r0 + u*r1 
        public bool doesIntersect;
        public Vector3 IntersectPoint;
        public float uValue;
        public float distance;
        public List<String> collider_data;
        //positive if intersection is infront RayPosition
        //negative if intersection is "behind" rayPosition
        public RayTriangle_CollisionData()
        {
            collider_data = new List<string>();
            doesIntersect = false;
            IntersectPoint = Vector3.Zero;
            uValue = 0;
            distance = -1;
        }
        
    }        

    //Static functions to calculate COllision Detections in this class
    public class CollisionDetection
    {
        /*---------------------------------------------------------------------------------------*/
        //Triangle Center
        public static Vector3 getCenterOfTriangle(Triangle tri)
        {
            float x = (tri.p1.X + tri.p2.X + tri.p3.X) / 3;
            float y = (tri.p1.Y + tri.p2.Y + tri.p3.Y) / 3;
            float z = (tri.p1.Z + tri.p2.Z + tri.p3.Z) / 3;
            return new Vector3(x, y, z);
        }
        public static Vector3 getCenterOfTriangle(Vector3 a, Vector3 b, Vector3 c)
        {
            float x = (a.X + b.X + c.X) / 3;
            float y = (a.Y + b.Y + c.Y) / 3;
            float z = (a.Z + b.Z + c.Z) / 3;
            return new Vector3(x, y, z);
        }
        /*---------------------------------------------------------------------------------------*/
        
        /*---------------------------------------------------------------------------------------*/
        /*radius around triangle */
        public static Single getMaxRadiusAroundTriangle(Triangle tri)
        {
            float x = (tri.p1.X + tri.p2.X + tri.p3.X) / 3;
            float y = (tri.p1.Y + tri.p2.Y + tri.p3.Y) / 3;
            float z = (tri.p1.Z + tri.p2.Z + tri.p3.Z) / 3;
            var center = new Vector3(x, y, z);

            float[] distances = new float[3];
            distances[0] = Vector3.Distance(center, tri.p1);
            distances[1] = Vector3.Distance(center, tri.p2);
            distances[2] = Vector3.Distance(center, tri.p3);

            return distances.Max();

        }
        public static Single getMaxRadiusAroundTriangle(Vector3 a, Vector3 b, Vector3 c)
        {
            var center = getCenterOfTriangle(a, b, c);

            float[] distances = new float[3];
            distances[0] = Vector3.Distance(center, a);
            distances[1] = Vector3.Distance(center, b);
            distances[2] = Vector3.Distance(center, c);

            return distances.Max();

        }
        /*---------------------------------------------------------------------------------------*/
        
        /*---------------------------------------------------------------------------------------*/
        /* Intersection point triangle ray */
        public static bool checkTriangleIntersect(Vector3 A, Vector3 B, Vector3 C, Ray ray)
        {
            //Getting the difference or distance
            Vector3 AB, AC;
            AB = Vector3.Subtract(B, A);
            AC = Vector3.Subtract(C, A);

            //For simplicity, I create extra vectors for the ray, instead of using it directly
            Vector3 rayOrigin, rayDirection;
            rayOrigin = ray.Position;
            rayDirection = ray.Direction;
            //now getting the crossproduct from ray direction and a distance from 2 points
            //Cross product : An orthogonal to the level that 
            //vector a and b create "pen standing on paper example"
            Vector3 CrossProduct = VectorMath.getCrossProduct(rayDirection, AC);
            //Now scalar of the crossProduct and the first distance AB
            float ScalarProduct = VectorMath.getScalarProduct(CrossProduct, AB);
            //divide 1 by scalar product
            float f = 1 / ScalarProduct;
            Vector3 s = Vector3.Subtract(rayOrigin, A);
            float u = f * (VectorMath.getScalarProduct(s, CrossProduct));
            Vector3 Q = VectorMath.getCrossProduct(s, AB);
            float v = f * VectorMath.getScalarProduct(rayDirection, Q);
            //if u < 0 or u > 1 -> No collision
            if (u < 0 || u > 1)
            {
                return false;
            }
            //if v < 0 or (u+v) > 1 -> No collision
            else if (v < 0 || (u + v) > 1)
            {
                //No collision still
                return false;
            }
            else
            {
                //Have colision, leave here
                return true;
            }
        }
        public static Vector3 intersectionPointTriangleRay(Triangle tri, Ray ray)
        {
            Vector3 a = Vector3.Subtract(tri.p3, tri.p1);
            Vector3 b = Vector3.Subtract(tri.p2, tri.p1);

            Vector3 nVector = VectorMath.getCrossProduct(a, b);

            float d = VectorMath.getScalarProduct(nVector, tri.p1);


            float tempScalarProduct = VectorMath.getScalarProduct(nVector, ray.Direction);

            float t = -((-d + VectorMath.getScalarProduct(nVector, ray.Position)) / tempScalarProduct);

            Vector3 qPoint = ray.Position + ray.Direction * t;

            return qPoint;
        }
        public static Vector3 intersectionPointTriangleRay(Vector3 a, Vector3 b, Vector3 c, Ray ray)
        {
            Vector3 CA = Vector3.Subtract(c, a);
            Vector3 BA = Vector3.Subtract(b, a);

            Vector3 nVector = VectorMath.getCrossProduct(CA, BA);

            float d = VectorMath.getScalarProduct(nVector, a);
            
            float tempScalarProduct = VectorMath.getScalarProduct(nVector, ray.Direction);

            float t = -((-d + VectorMath.getScalarProduct(nVector, ray.Position)) / tempScalarProduct);

            Vector3 qPoint = ray.Position + ray.Direction * t;

            return qPoint;
        }
        public static float intersectionPointTriangleRay_returnFactor(Vector3 a, Vector3 b, Vector3 c, Ray ray)
        {
            Vector3 A = Vector3.Subtract(c, a);
            Vector3 B = Vector3.Subtract(b, a);

            Vector3 nVector = VectorMath.getCrossProduct(A, B);

            float d = VectorMath.getScalarProduct(nVector, a);

            float tempScalarProduct = VectorMath.getScalarProduct(nVector, ray.Direction);

            float t = -((-d + VectorMath.getScalarProduct(nVector, ray.Position)) / tempScalarProduct);

            Vector3 qPoint = ray.Position + ray.Direction * t;

            return t;
        }
        public static RayTriangle_CollisionData checkTriangleIntersect_returnFull(Vector3 A, Vector3 B, Vector3 C, Ray ray)
        {
            RayTriangle_CollisionData data;
            //Getting the difference or distance
            Vector3 AB, AC;
            AB = Vector3.Subtract(B, A);
            AC = Vector3.Subtract(C, A);

            //For simplicity, I create extra vectors for the ray, instead of using it directly
            Vector3 rayOrigin, rayDirection;
            rayOrigin = ray.Position;
            rayDirection = ray.Direction;
            //now getting the crossproduct from ray direction and a distance from 2 points
            //Cross product : An orthogonal to the level that 
            //vector a and b create "pen standing on paper example"
            Vector3 CrossProduct = VectorMath.getCrossProduct(rayDirection, AC);
            //Now scalar of the crossProduct and the first distance AB
            float ScalarProduct = VectorMath.getScalarProduct(CrossProduct, AB);
            //divide 1 by scalar product
            float f = 1 / ScalarProduct;
            Vector3 s = Vector3.Subtract(rayOrigin, A);
            float u = f * (VectorMath.getScalarProduct(s, CrossProduct));
            Vector3 Q = VectorMath.getCrossProduct(s, AB);
            float v = f * VectorMath.getScalarProduct(rayDirection, Q);
            //if u < 0 or u > 1 -> No collision
            if (u < 0 || u > 1)
            {
                data = new RayTriangle_CollisionData();
                data.doesIntersect = false;
                data.uValue = 0;
                data.IntersectPoint = Vector3.Zero;
                data.distance = float.MaxValue;
                return data;
            }
            //if v < 0 or (u+v) > 1 -> No collision
            else if (v < 0 || (u + v) > 1)
            {
                //No collision still
                data = new RayTriangle_CollisionData();
                data.doesIntersect = false;
                data.uValue = 0;
                data.IntersectPoint = Vector3.Zero;
                data.distance = float.MaxValue;
                return data;
            }
            else
            {
                //Ray: rs =  r1 + µ* a   //Plane: n*(rs - r0) = 0; //put into eachother->solve to µ
                data = new RayTriangle_CollisionData();

                Vector3 n = VectorMath.getCrossProduct(AB, AC); //Cross of plane
                Vector3 sub = Vector3.Subtract(A, ray.Position); // r0- r1
                float bottomScalar = VectorMath.getScalarProduct(n, ray.Direction);
                float topScalar = VectorMath.getScalarProduct(sub, n);
                float Lamba = topScalar / bottomScalar;
                data.IntersectPoint = ray.Position + Lamba * ray.Direction;
                data.distance = VectorMath.get_length(Vector3.Subtract(data.IntersectPoint, ray.Position));
                data.uValue = Lamba;
                data.doesIntersect = true;
                return data;
            }
        }
        /*---------------------------------------------------------------------------------------*/
        
        /*---------------------------------------------------------------------------------------*/
        public static float planeRayAngle(Vector3 down_direction, Vector3 a, Vector3 b, Vector3 c)
        {
            Vector3 normal = VectorMath.getCrossProduct(Vector3.Subtract(a, b), Vector3.Subtract(a, c));

            return VectorMath.getScalarProductBeforeCosinus(normal, down_direction);
        }
        /*---------------------------------------------------------------------------------------*/

        /*---------------------------------------------------------------------------------------*/
        public static float getObjectHeight(  List<Block> blocks,   Vector3 Position, float orig_height)
        {

            float height = 0;
            foreach (Block block in blocks)
            {
                for (int i = 0; i < block.floorModel.vertices.Length; )
                {
                    int cnt1, cnt2, cnt3;
                    cnt1 = i; cnt2 = i + 1; cnt3 = i + 2;
                    var tri = new Triangle();

                    tri.p1 = block.floorModel.vertices[cnt1].Position;
                    tri.p2 = block.floorModel.vertices[cnt2].Position;
                    tri.p3 = block.floorModel.vertices[cnt3].Position;

                    Vector3 point = intersectionPointTriangleRay(tri, new Ray(Position, Vector3.Down));

                    if (CollisionDetection.checkTriangleIntersect(tri.p1, tri.p2, tri.p3, new Ray(Position, Vector3.Down)))
                    {
                        if ((point.Y >= Position.Y - 1) && !(point.Y > Position.Y + 1))
                        {
                            height = point.Y;
                        }
                    }

                    i += 3;
                }
            }
            return height;
        }
        /*---------------------------------------------------------------------------------------*/

        /*---------------------------------------------------------------------------------------*/
        public static bool isInsideRadius(Vector3 position, SphereVolume sphere)
        {
            if (VectorMath.getAbsoluteVector(Vector3.Subtract(position, sphere.Position)) < sphere.Radius)
            {
                return true;
            }
            else
                return false;
        }
        /*---------------------------------------------------------------------------------------*/

        /*---------------------------------------------------------------------------------------*/
        // UNFINISHED CODE
        //Will be mainly used to calculate Sphere Convex Hull Collision
        public static Vector3 HandleGameEntityCollision(Vector3 requestedMovement, I_GameEntity gameEntity,   GameSegment segment, float floorRadiusSubtractor, float fallSpeedMultiplier)
        {
            SphereConvexHull sphere;
            if (gameEntity.getConvexHull() is SphereConvexHull)
            {
                sphere = gameEntity.getConvexHull() as SphereConvexHull;
            }
            else
                return requestedMovement;
            
            GeometryCollisionData_Simple data;

            #region Calculate Pusback wall
            for (int i = 0; i < segment.Level.MapBlocks.Count; i++)
            {               
                //do a quick sphere/sphere check upfront before going deeper
                float playerBlockDistance = VectorMath.get_length(segment.Level.MapBlocks[i].wallModel.CenterPosition - gameEntity.get_Position());

                //Very dangerous, gameEntity.getConvexHull might not return a sphere, but a capsule or triangle, unless i created a good hierarchy, this will do.
                float checkDis = segment.Level.MapBlocks[i].wallModel.RadiusFromCenter + (gameEntity.getConvexHull() as SphereConvexHull).Radius;

                if (playerBlockDistance > checkDis)
                    continue;

                data = TriangleList_To_Sphere_Intersection(segment.Level.MapBlocks[i].wallModel.vertices, segment.Level.MapBlocks[i].wallModel.triangleCenters, segment.Level.MapBlocks[i].wallModel.maxTriangleRadius, sphere);

                if (!data.doesIntersect)
                    continue;

                //Now handle the pushing back
                if(data.Distance <= sphere.Radius - 0.05f)
                {
                    float dis = data.Distance;

                    if (gameEntity is Player)
                    {
                        ((Player)gameEntity).DistanceFromFloor = dis;

                        Vector3 hullpos = ((Player)gameEntity).SphereConvexHull.Position;

                        Vector3 Dir_objectToPlayer = Vector3.Normalize(hullpos - data.PositionOfIntersect);
                        float pushbackfactor = sphere.Radius - dis;

                        ((Player)gameEntity).Position_Player += pushbackfactor * Dir_objectToPlayer;
                    }

                    if (gameEntity is NPC_Follower)
                    {
                        Vector3 hullpos = ((NPC_Follower)gameEntity).sphereConvexHull.Position;

                        Vector3 Dir_objectToPlayer = Vector3.Normalize(hullpos - data.PositionOfIntersect);
                        float pushbackfactor = sphere.Radius - dis;

                        ((NPC_Follower)gameEntity).Position += pushbackfactor * Dir_objectToPlayer;
                    }
                }
            }
            #endregion

            #region Calculate pusback floor
            for (int i = 0; i < segment.Level.MapBlocks.Count; i++)
            {
                //do a quick sphere/sphere check upfront before going deeper
                float playerBlockDistance = VectorMath.get_length(segment.Level.MapBlocks[i].floorModel.CenterPosition - gameEntity.get_Position());

                //Very dangerous, gameEntity.getConvexHull might not return a sphere, but a capsule or triangle, unless i created a good hierarchy, this will do.
                float checkDis = segment.Level.MapBlocks[i].floorModel.RadiusFromCenter + (gameEntity.getConvexHull() as SphereConvexHull).Radius;

                data = TriangleList_To_Sphere_Intersection(segment.Level.MapBlocks[i].floorModel.vertices, segment.Level.MapBlocks[i].floorModel.triangleCenters, segment.Level.MapBlocks[i].floorModel.maxTriangleRadius, sphere);
             
                if (!data.doesIntersect)
                {
                    if (gameEntity is Player)
                    {
                        ((Player)gameEntity).AdditionalMovement.Y -= 0.005f * fallSpeedMultiplier;
                        
                        float currentfall = ((Player)gameEntity).AdditionalMovement.Y;

                    }


                    if (gameEntity is NPC_Follower)
                    {
                        ((NPC_Follower)gameEntity).AdditionalMovement.Y -= 0.005f * fallSpeedMultiplier;

                        float currentfall = ((NPC_Follower)gameEntity).AdditionalMovement.Y;

                    }
                }
                //Now handle the pushing back for the floor                                
                else if (data.Distance <= sphere.Radius - floorRadiusSubtractor)
                {
                    if (gameEntity is Player)
                    {
                        float dis = data.Distance;
                         

                        Vector3 hullpos = ((Player)gameEntity).SphereConvexHull.Position;

                        Vector3 Dir_objectToPlayer = Vector3.Normalize(hullpos - data.PositionOfIntersect);
                        float pushbackfactor = (sphere.Radius - floorRadiusSubtractor) - dis;


                        ((Player)gameEntity).AdditionalMovement.Y = 0.0f;
                        ((Player)gameEntity).Position_Player += pushbackfactor * Dir_objectToPlayer;
                    }

                    if (gameEntity is NPC_Follower)
                    {
                        float dis = data.Distance;


                        Vector3 hullpos = ((NPC_Follower)gameEntity).sphereConvexHull.Position;

                        Vector3 Dir_objectToPlayer = Vector3.Normalize(hullpos - data.PositionOfIntersect);
                        float pushbackfactor = (sphere.Radius - floorRadiusSubtractor) - dis;

                        ((NPC_Follower)gameEntity).AdditionalMovement.Y = 0.0f;
                        ((NPC_Follower)gameEntity).Position += pushbackfactor * Dir_objectToPlayer;
                    }
                }

                //If its not close to anything, let it fall
                else if (data.Distance >= sphere.Radius - 0)
                {
                    //Now we have to pre-calculate the distance he will fall, to prevent skipping the floor
                    float maxDis_to_fall = data.Distance;

                    if (gameEntity is Player)
                    {
                        ((Player)gameEntity).AdditionalMovement.Y -= 0.005f * fallSpeedMultiplier;
                        ((Player)gameEntity).onFloor = false;
                        float currentfall = ((Player)gameEntity).AdditionalMovement.Y;

                    }


                    if (gameEntity is NPC_Follower)
                    {
                        ((NPC_Follower)gameEntity).AdditionalMovement.Y -= 0.005f * fallSpeedMultiplier;

                        float currentfall = ((NPC_Follower)gameEntity).AdditionalMovement.Y;

                    }
                    //How far will he fall with current fallspeed?

                }
                else 
                {
                    
                }
            
            }

            #endregion

            return requestedMovement;
        }
        /*----------------------------------------------------------------------------------------------------------------*/
        
        /*---------------------------------------------------------------------------------------*/
        // UNFINISHED CODE
        //Will be mainly used to calculate Sphere Convex Hull Collision
        public static void HandleGameEntityCollision(I_GameEntity gameEntity,   GameSegment segment, float floorRadiusSubtractor, float fallSpeedMultiplier)
        {
            SphereConvexHull sphere;
            if (gameEntity.getConvexHull() is SphereConvexHull)
            {
                sphere = gameEntity.getConvexHull() as SphereConvexHull;
            }
            else
                return;

            GeometryCollisionData_Simple data;

            #region Calculate Pusback wall
            for (int i = 0; i < segment.Level.MapBlocks.Count; i++)
            {
                //do a quick sphere/sphere check upfront before going deeper
                float playerBlockDistance = VectorMath.get_length(segment.Level.MapBlocks[i].wallModel.CenterPosition - gameEntity.get_Position());

                //Very dangerous, gameEntity.getConvexHull might not return a sphere, but a capsule or triangle, unless i created a good hierarchy, this will do.
                float checkDis = segment.Level.MapBlocks[i].wallModel.RadiusFromCenter + (gameEntity.getConvexHull() as SphereConvexHull).Radius;

                if (playerBlockDistance > checkDis)
                    continue;


                data = TriangleList_To_Sphere_Intersection(segment.Level.MapBlocks[i].wallModel.vertices, segment.Level.MapBlocks[i].wallModel.triangleCenters, segment.Level.MapBlocks[i].wallModel.maxTriangleRadius, sphere);

                if (!data.doesIntersect)
                    continue;

                //Now handle the pushing back
                if (data.Distance <= sphere.Radius - 0.05f)
                {
                    float dis = data.Distance;

                    if (gameEntity is Player)
                    {
                        ((Player)gameEntity).DistanceFromFloor = dis;

                        Vector3 hullpos = ((Player)gameEntity).SphereConvexHull.Position;

                        Vector3 Dir_objectToPlayer = Vector3.Normalize(hullpos - data.PositionOfIntersect);
                        float pushbackfactor = sphere.Radius - dis;

                        ((Player)gameEntity).Position_Player += pushbackfactor * Dir_objectToPlayer;
                    }

                    if (gameEntity is NPC_Follower)
                    {
                        Vector3 hullpos = ((NPC_Follower)gameEntity).sphereConvexHull.Position;

                        Vector3 Dir_objectToPlayer = Vector3.Normalize(hullpos - data.PositionOfIntersect);
                        float pushbackfactor = sphere.Radius - dis;

                        ((NPC_Follower)gameEntity).Position += pushbackfactor * Dir_objectToPlayer;
                    }

                }
            }
            #endregion

            #region Calculate pusback floor
            for (int i = 0; i < segment.Level.MapBlocks.Count; i++)
            {
                //do a quick sphere/sphere check upfront before going deeper
                float playerBlockDistance = VectorMath.get_length(segment.Level.MapBlocks[i].floorModel.CenterPosition - gameEntity.get_Position());

                //Very dangerous, gameEntity.getConvexHull might not return a sphere, but a capsule or triangle, unless i created a good hierarchy, this will do.
                float checkDis = segment.Level.MapBlocks[i].floorModel.RadiusFromCenter + (gameEntity.getConvexHull() as SphereConvexHull).Radius;

                data = TriangleList_To_Sphere_Intersection(segment.Level.MapBlocks[i].floorModel.vertices, segment.Level.MapBlocks[i].floorModel.triangleCenters, segment.Level.MapBlocks[i].floorModel.maxTriangleRadius, sphere);

                if (!data.doesIntersect)
                {
                    if (gameEntity is Player)
                    {
                        ((Player)gameEntity).AdditionalMovement.Y -= 0.005f * fallSpeedMultiplier;

                        float currentfall = ((Player)gameEntity).AdditionalMovement.Y;

                    }


                    if (gameEntity is NPC_Follower)
                    {
                        ((NPC_Follower)gameEntity).AdditionalMovement.Y -= 0.005f * fallSpeedMultiplier;

                        float currentfall = ((NPC_Follower)gameEntity).AdditionalMovement.Y;

                    }
                }
                //Now handle the pushing back for the floor                                
                else if (data.Distance <= sphere.Radius - floorRadiusSubtractor)
                {
                    if (gameEntity is Player)
                    {
                        float dis = data.Distance;


                        Vector3 hullpos = ((Player)gameEntity).SphereConvexHull.Position;

                        Vector3 Dir_objectToPlayer = Vector3.Normalize(hullpos - data.PositionOfIntersect);
                        float pushbackfactor = (sphere.Radius - floorRadiusSubtractor) - dis;


                        ((Player)gameEntity).AdditionalMovement.Y = 0.0f;
                        ((Player)gameEntity).Position_Player += pushbackfactor * Dir_objectToPlayer;
                    }

                    if (gameEntity is NPC_Follower)
                    {
                        float dis = data.Distance;


                        Vector3 hullpos = ((NPC_Follower)gameEntity).sphereConvexHull.Position;

                        Vector3 Dir_objectToPlayer = Vector3.Normalize(hullpos - data.PositionOfIntersect);
                        float pushbackfactor = (sphere.Radius - floorRadiusSubtractor) - dis;

                        ((NPC_Follower)gameEntity).AdditionalMovement.Y = 0.0f;
                        ((NPC_Follower)gameEntity).Position += pushbackfactor * Dir_objectToPlayer;
                    }
                }

                //If its not close to anything, let it fall
                else if (data.Distance >= sphere.Radius - 0)
                {
                    //Now we have to pre-calculate the distance he will fall, to prevent skipping the floor
                    float maxDis_to_fall = data.Distance;

                    if (gameEntity is Player)
                    {
                        ((Player)gameEntity).AdditionalMovement.Y -= 0.005f * fallSpeedMultiplier;
                        ((Player)gameEntity).onFloor = false;
                        float currentfall = ((Player)gameEntity).AdditionalMovement.Y;

                    }


                    if (gameEntity is NPC_Follower)
                    {
                        ((NPC_Follower)gameEntity).AdditionalMovement.Y -= 0.005f * fallSpeedMultiplier;

                        float currentfall = ((NPC_Follower)gameEntity).AdditionalMovement.Y;

                    }
                    //How far will he fall with current fallspeed?

                }
                else
                {

                }

            }

            #endregion
        }
        /*----------------------------------------------------------------------------------------------------------------*/
        
        /*----------------------------------------------------------------------------------------------------------------*/
        //Check spheres against eachother, this is the simplest method used for Item pickups, trigger areas, and such
        /// <summary>
        /// If no Colision Occurs, this returns Infinite for each value
        /// </summary>
        /// <param name="sphere1"></param>
        /// <param name="sphere2"></param>
        /// <returns></returns>
        public static Vector3 SphereSphereCollision(SphereConvexHull sphere1, SphereConvexHull sphere2)
        {
            float DistanceCenters = VectorMath.get_length(sphere1.Position - sphere2.Position);
            Vector3 CollisionPoint;

            if (DistanceCenters < sphere1.Radius + sphere2.Radius)
            {
                Vector3 direction = Vector3.Normalize(sphere2.Position - sphere1.Position);
                CollisionPoint = sphere1.Position + direction * sphere1.Radius;
                //Vector3 collisionPoint = direction* 
            }
            else
                CollisionPoint = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);

            return CollisionPoint;
        }
        /*----------------------------------------------------------------------------------------------------------------*/
        
        /*----------------------------------------------------------------------------------------------------------------*/
        // calculates the intersecting point of 2 triangles, returns the first one
        /// <summary>
        /// Checks if 2 triangles intersect, can use this for convex hulls collision detection
        /// </summary>
        /// <param name="p1a"></param>
        /// <param name="p2a"></param>
        /// <param name="p3a"></param>
        /// <param name="surfaceNormal_1"></param>
        /// <param name="p1b"></param>
        /// <param name="p2b"></param>
        /// <param name="p3b"></param>
        /// <param name="surfaceNormal_2"></param>
        /// <returns></returns>
        public static GeometryCollisionData_Simple Triangle_Triangle_Intersection(
            Vector3 A1, Vector3 A2, Vector3 A3, Vector3 surfaceNormal_1,
            Vector3 B1, Vector3 B2, Vector3 B3, Vector3 surfaceNormal_2
        )
        {
            GeometryCollisionData_Simple collData = new GeometryCollisionData_Simple();
            collData.Distance = -1;
            collData.doesIntersect = false;
            collData.PositionOfIntersect = Vector3.Zero;

            #region Check if paralel, if yes, no collision;
            float dot = VectorMath.roundf(VectorMath.getScalarProductBeforeCosinus(surfaceNormal_1, surfaceNormal_2),3);

            //They are paralel, this doesnt count as collision here!
            if (dot == 0)
            {                
                //With standard values from top, no intersect
                return collData;
            }
            #endregion

            #region Check if any Edge penetrates the surface of the other
            Ray edge_A1, edge_A2, edge_A3;           

            RayTriangle_CollisionData edge_A1_data;
            RayTriangle_CollisionData edge_A2_data;
            RayTriangle_CollisionData edge_A3_data;


            /* When 2 Triangles intersect eachother, An Edge from Triangle 1 will go throught the surface of Triangle 2
             * At that point, An edge from triangle 2 Will also Intersect the surface of Triangle1, Thats why
             *  We only have to check one triangles edges against the surface of triangle2.
             * 
             */
            

            edge_A1 = new Ray(A1, A2 - A1);
            edge_A1_data = CollisionDetection.checkTriangleIntersect_returnFull(B1, B2, B3, edge_A1);
            //Try the first Edge, if no collision, find the next one and try, This method is very efficient, if we have collision, it will return right away.
            if (edge_A1_data.doesIntersect == true && edge_A1_data.uValue > 0 && edge_A1_data.uValue <= edge_A1_data.distance)
            {
                collData.doesIntersect = true;
                collData.PositionOfIntersect = edge_A1_data.IntersectPoint;
                
                //NOt sure how i will implement distance, yet
                collData.Distance = edge_A1_data.distance;
                
                return collData;
            }


            //Try Second Edge
            edge_A2 = new Ray(A2, A3 - A2);
            edge_A2_data = CollisionDetection.checkTriangleIntersect_returnFull(B1, B2, B3, edge_A2);

            if (edge_A2_data.doesIntersect == true && edge_A2_data.uValue > 0 && edge_A2_data.uValue <= edge_A2_data.distance)
            {
                collData.doesIntersect = true;
                collData.PositionOfIntersect = edge_A2_data.IntersectPoint;

                //NOt sure how i will implement distance, yet
                collData.Distance = edge_A2_data.distance;

                return collData;
            }

            //Try Third Edge
            edge_A3 = new Ray(A3, A1 - A3);
            edge_A3_data = CollisionDetection.checkTriangleIntersect_returnFull(B1, B2, B3, edge_A3);

            if (edge_A3_data.doesIntersect == true && edge_A3_data.uValue > 0 && edge_A3_data.uValue <= edge_A3_data.distance)
            {
                collData.doesIntersect = true;
                collData.PositionOfIntersect = edge_A3_data.IntersectPoint;

                //NOt sure how i will implement distance, yet
                collData.Distance = edge_A3_data.distance;

                return collData;
            }


            // NOW CHECK FOR TRIANGLE 2
            Ray edge_B1, edge_B2, edge_B3;

            RayTriangle_CollisionData edge_B1_data;
            RayTriangle_CollisionData edge_B2_data;
            RayTriangle_CollisionData edge_B3_data;

            edge_B1 = new Ray(B1, B2 - B1);
            edge_B1_data = CollisionDetection.checkTriangleIntersect_returnFull(A1, A2, A3, edge_B1);
            //Try the first Edge, if no collision, find the next one and try, This method is very efficient, if we have collision, it will return right away.
            if (edge_B1_data.doesIntersect == true && edge_B1_data.uValue > 0 && edge_B1_data.uValue <= edge_B1_data.distance)
            {
                collData.doesIntersect = true;
                collData.PositionOfIntersect = edge_B1_data.IntersectPoint;

                //NOt sure how i will implement distance, yet
                collData.Distance = edge_B1_data.distance;

                return collData;
            }


            //Try Second Edge
            edge_B2 = new Ray(B2, B3 - B2);
            edge_B2_data = CollisionDetection.checkTriangleIntersect_returnFull(A1, A2, A3, edge_B2);

            if (edge_B2_data.doesIntersect == true && edge_B2_data.uValue > 0 && edge_B2_data.uValue <= edge_B2_data.distance)
            {
                collData.doesIntersect = true;
                collData.PositionOfIntersect = edge_B2_data.IntersectPoint;

                //NOt sure how i will implement distance, yet
                collData.Distance = edge_B2_data.distance;

                return collData;
            }

            //Try Third Edge
            edge_B3 = new Ray(B3, B1 - B3);
            edge_B3_data = CollisionDetection.checkTriangleIntersect_returnFull(A1, A2, A3, edge_B3);

            if (edge_B3_data.doesIntersect == true && edge_B3_data.uValue > 0 && edge_B3_data.uValue <= edge_B3_data.distance)
            {
                collData.doesIntersect = true;
                collData.PositionOfIntersect = edge_B3_data.IntersectPoint;

                //NOt sure how i will implement distance, yet
                collData.Distance = edge_B3_data.distance;

                return collData;
            }
            #endregion

            return collData;
        }
        
        //Calculates 2 lists of triangles against eachother, aborting and returning as soon as one intersection is found
        /// <summary>
        /// Check Custom_Convex_Hulls against eachother with this, returns true once a ray is intersecting
        /// </summary>
        /// <param name="verts1"></param>
        /// <param name="verts2"></param>
        /// <returns></returns>
        public static GeometryCollisionData_Simple TriangleList_To_TriangleList_Intersection(
            CustomVertex[] verts1, 
            CustomVertex[] verts2
        )
        {
            GeometryCollisionData_Simple data;
            data.Distance = -1;
            data.doesIntersect = false;
            data.PositionOfIntersect = Vector3.Zero;

            for (int i = 0; i < verts1.Length && !data.doesIntersect; i+= 3)
            {
                for (int j = 0; j < verts2.Length && !data.doesIntersect; j += 3)
                {
                    data = Triangle_Triangle_Intersection(
                        verts1[i + 0].Position,
                        verts1[i + 1].Position,
                        verts1[i + 2].Position,
                        verts1[i].Normal,
                        verts2[j + 0].Position,
                        verts2[j + 1].Position,
                        verts2[j + 2].Position,
                        verts2[j].Normal);                                        
                }
            }

            return data;
        }
        /*----------------------------------------------------------------------------------------------------------------*/
        
        /*----------------------------------------------------------------------------------------------------------------*/
        //Check a sphere convex hull against a triangle
        /// <summary>
        /// Check triangle against sphere with this one
        /// </summary>
        /// <param name="verts1"></param>
        /// <param name="verts2"></param>
        /// <returns></returns>
        public static GeometryCollisionData_Simple Triangle_To_Sphere_Intersection(
            Vector3 A, Vector3 B, Vector3 C, Vector3 triangleNormal, SphereConvexHull sphere)
        {
            GeometryCollisionData_Simple collData = new GeometryCollisionData_Simple();
            collData.Distance = -1;
            collData.doesIntersect = false;
            collData.PositionOfIntersect = Vector3.Zero;

            RayTriangle_CollisionData data =  CollisionDetection.checkTriangleIntersect_returnFull(A,B,C,new Ray(sphere.Position, triangleNormal));
            Single currentDistance = float.MaxValue;
            Vector3 Dir1 = B - A;
            Vector3 AtoP = sphere.Position - A;
            Vector3 BtoP = sphere.Position - B;
            //If the center of the sphere lines up with the triangle inner surface, it will return true nice
            if (data.distance < sphere.Radius && collData.Distance < currentDistance && data.doesIntersect && data.uValue < 0)
            {
                collData.Distance = data.distance;
                collData.doesIntersect = true;
                collData.PositionOfIntersect = data.IntersectPoint;
                
                return collData;
            }

            float[] dists = new float[3];

            Vector3 closest_point1 = VectorMath.getClosestPointOnLineSegment(A, B, sphere.Position);
            dists[0] = VectorMath.get_length(sphere.Position - closest_point1);

            Vector3 closest_point2 = VectorMath.getClosestPointOnLineSegment(B, C, sphere.Position);
            dists[1] = VectorMath.get_length(sphere.Position - closest_point2);

            Vector3 closest_point3 = VectorMath.getClosestPointOnLineSegment(C, A, sphere.Position);
            dists[2] = VectorMath.get_length(sphere.Position - closest_point3);

            float minimal = dists[0];
            Vector3 closestPoint = closest_point1;

            if (dists[1] < minimal)
            {
                minimal = dists[1];
                closestPoint = closest_point2;
            }

            if (dists[2] < minimal)
            {
                minimal = dists[2];
                closestPoint = closest_point3;
            }

            if (collData.Distance < sphere.Radius)
            {
                collData.Distance = minimal;
                collData.doesIntersect = true;
                collData.PositionOfIntersect = closestPoint;
            }
            else
            {
                collData.Distance = -1;
                collData.doesIntersect =false;
                collData.PositionOfIntersect = Vector3.Zero;
            }
            
            
            
            return collData;
        }

        //Check a list of triangles against a sphere Convex hull
        /// <summary>
        /// Check Custom_Convex_Hulls against eachother with this, returns true once a ray is intersecting
        /// </summary>
        /// <param name="verts1"></param>
        /// <param name="verts2"></param>
        /// <returns></returns>
        public static GeometryCollisionData_Simple TriangleList_To_Sphere_Intersection(
            CustomVertex[] verts1,
            Vector3[] centers,
            float[] maxrads,
            SphereConvexHull sphere)
        {
            GeometryCollisionData_Simple data;
            data.Distance = -1;
            data.doesIntersect = false;
            data.PositionOfIntersect = Vector3.Zero;

            GeometryCollisionData_Simple newData = data;
            newData.Distance = float.MaxValue;
                        
            for (int i = 0, j = 0; i < verts1.Length; i += 3, j++)
            {
                float disPlayerTriangle = VectorMath.get_length(centers[j] - sphere.Position);
                float disCheck = sphere.Radius + maxrads[j];

                if (disPlayerTriangle > disCheck)
                    continue;

                data = Triangle_To_Sphere_Intersection(verts1[i + 0].Position,verts1[i + 1].Position,verts1[i + 2].Position,verts1[i].Normal, sphere);


                if (data.doesIntersect && data.Distance < newData.Distance)
                {
                    newData.Distance = data.Distance;
                    newData.doesIntersect = true;
                    newData.PositionOfIntersect = data.PositionOfIntersect;
                }
                
            }

            if (newData.Distance < sphere.Radius && data.doesIntersect)
            {
                //data.Distance = newData.Distance;
                data = newData;
                
            }
            

            return data;
        }
        /*----------------------------------------------------------------------------------------------------------------*/

        public static List<String> Calculate_Ray_To_Hitbox(Matrix premat, AnimatedModelMesh hitboxMesh, Ray ray)
        {            
            RayTriangle_CollisionData data = new RayTriangle_CollisionData();
            List<String> hit_list = new List<string>();

            for (int def = 0; def < hitboxMesh.Skeleton.Deformers.Count; def++)
            {
                int sub = hitboxMesh.Skeleton.Deformers[def].VertexIndices.Length % 3;
                int len = hitboxMesh.Skeleton.Deformers[def].VertexIndices.Length - sub;

                for (int index = 0; index < len; index += 3)
                {
                    int ind1, ind2, ind3;
                    ind1 = hitboxMesh.Skeleton.Deformers[def].VertexIndices[index];
                    ind2 = hitboxMesh.Skeleton.Deformers[def].VertexIndices[index + 1];
                    ind3 = hitboxMesh.Skeleton.Deformers[def].VertexIndices[index + 2];

                    Vector3 v1, v2, v3;

                    v1 = hitboxMesh.PointList_Dynamic[ind1];
                    v2 = hitboxMesh.PointList_Dynamic[ind2];
                    v3 = hitboxMesh.PointList_Dynamic[ind3];
                                        
                    data = CollisionDetection.checkTriangleIntersect_returnFull(v1, v2, v3, ray);

                    if (data.doesIntersect && data.uValue > 0)
                    {
                        //Console.WriteLine("Hit in {0}", hitboxMesh.Skeleton.Deformers[def].DeformerName);
                        hit_list.Add(hitboxMesh.Skeleton.Deformers[def].DeformerName);
                    }
                }
            }

            data.collider_data = hit_list;
            return data.collider_data;
        }
    }

    public struct SphereReturnData
    {
        public List<Vector3> SurfaceToSphereDirections;
        public List<float> Contact_distances;
        public List<Vector3> IntersectPoints;
        public List<Vector3> IntersectPoints_tri;
    }
    
    //Same as above, but use this for principle..
    public struct GeometryCollisionData_Simple
    {
        public Vector3 PositionOfIntersect;
        public bool doesIntersect;
        public Single Distance;
    }

    //Every Convex hull will implement this.
    public interface I_ConvexHull
    {
        GeometryCollisionData_Simple calculateCollision(I_ConvexHull hull);

        void update_ConvexCollider(Vector3 pos);
    }      

    //Use to pre-test npcs, players, items, vehicles, physicsobjects
    //before more complicate Capsule or ConvexCollider testing
    public class SphereConvexHull : I_ConvexHull
    {
        public Vector3 Position;
        public float Radius;

        public SphereConvexHull(Vector3 Position, float radius)
        {
            this.Position = Position;
            this.Radius = radius;
        }

        public Vector3 get_Position()
        {
            return Position;
        }

        public float get_Radius()
        {
            return Radius;
        }

        public void update_ConvexCollider(Vector3 position)
        {
            this.Position = position;
        }

        public GeometryCollisionData_Simple calculateCollision(I_ConvexHull hull)
        {
            //Mooo!
            return new GeometryCollisionData_Simple();
        }
    }

    public class CustomConvexHull :I_ConvexHull 
    {
        public Vector3 Position;
        public Vector3 Rotation;

        public CustomVertex[] vertices;

        public CustomConvexHull(Vector3 pos, Vector3 rot, CustomVertex[] verts)
        {
            this.Position = pos;
            this.Rotation = rot;
            this.vertices = verts;
        }

        public void update_ConvexCollider(Vector3 pos)
        {
            //Update k?
        }

        public GeometryCollisionData_Simple calculateCollision(I_ConvexHull hull)
        {
            GeometryCollisionData_Simple data = new GeometryCollisionData_Simple();
            
            //Call once, the collisionDeteciton function should Handle the diff types and all.


            //Mooo!
            return new GeometryCollisionData_Simple();
        }
    }
    
    /// <summary>
    /// The Capsule is at the moment, made using 2 sphere convex hulls
    /// </summary>
    public class CapsuleConvexHull : I_ConvexHull
    {
        //Those both are offsets from the carriers Position
        private Vector3 LowerPoint;
        private Vector3 UpperPoint;

        //THese ones are the updated ones
        public SphereConvexHull lowerSphere;
        public SphereConvexHull upperSphere;
        
        /// <param name="rootpos">The Parent Object Position</param>
        /// <param name="lowerPOffset">The lower sphere of the capsule relative to rootpos</param>
        /// <param name="upperPOffset">The upper Sphere relative to rootpos</param>
        public CapsuleConvexHull(Vector3 rootpos, Vector3 lowerPOffset, Vector3 upperPOffset, float radius)
        {
            lowerSphere = new SphereConvexHull(rootpos + lowerPOffset, radius);
            upperSphere = new SphereConvexHull(rootpos + upperPOffset, radius);
            this.LowerPoint = lowerPOffset;
            this.UpperPoint = upperPOffset;
        }

        public void update_ConvexCollider(Vector3 parentPos)
        {
            lowerSphere.Position = parentPos + LowerPoint;
            upperSphere.Position = parentPos + UpperPoint;
        }

        public GeometryCollisionData_Simple calculateCollision(I_ConvexHull hull)
        {
            return new GeometryCollisionData_Simple();
        }        
    }
           
}
