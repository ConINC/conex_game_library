﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ConexGameLibrary;

namespace ConexGameLibrary
{
    public class Generic_Firearm
    {
        public string gunName;
        public int worldModelIndex = -1;
        public int viewModelIndex = -1;

        public string viewModel_filename;
        public string worldModel_filename;

        public string idleAnimationName;
        public string shootAnimation, reloadAnimation, EquipAnimation, DropAnimation, chamberAnimation;
        public string shootSound_Name, reloadsound_Name, equipSound_Name, dropSound_Name, chamberSound_Name;

        public Vector3 CasingExhaustPosition;
        public Vector3 CastingExhaustDirection;

        public string[] FiringModes;

        public int ShootAnimationLength;
        public int ReloadTimer, ShootDelayTimer;
        public int ReloadAnimationLength;
        public int ReloadTime, ShootTime;
        public float AimError;
        public int BulletsPerShot;
        public int BulletsPerMagazine;
        public Ammo_Type_Data AmmoType = AmmoTypes.type_9mm;

        public void setWorldModelIndex(int index)
        {
            this.worldModelIndex = index;
        }

        public void setViewModelIndex(int index)
        {
            this.viewModelIndex = index;
        }

        //important, manually set the index of the world/view model from the lists + hash
        public void Set_GenericFirearmData(GunLoadInfo info)
        {
            this.viewModel_filename = info.viewModel_filename;
            this.worldModel_filename = info.worldModel_filename;

            this.gunName = info.gunName;
            this.idleAnimationName = info.idleAnimationName;
            this.shootAnimation = info.shootAnimation;
            this.reloadAnimation = info.reloadAnimation;
            this.EquipAnimation = info.EquipAnimation;
            this.DropAnimation = info.DropAnimation;
            this.chamberAnimation = info.chamberAnimation;
            this.shootSound_Name = info.shootSound_Name;
            this.reloadsound_Name = info.reloadsound_Name;
            this.equipSound_Name = info.equipSound_Name;
            this.dropSound_Name = info.dropSound_Name;
            this.chamberSound_Name = info.chamberSound_Name;

        }

        public void SetGunProperties(Firearm_Property prop)
        {
            this.ShootTime = prop.ShootTime;
            this.ReloadTime = prop.ReloadTime;
            this.AimError = prop.AimError;
            this.BulletsPerMagazine = prop.BulletsPerMagazine;
            this.BulletsPerShot = prop.BulletsPerShot;
            this.AmmoType = prop.AmmoType;
            this.FiringModes = prop.firingModes;
        }
    }

    //Firearm_Proprty contains the guntype data, 
    public class Player_Property //CHANGE THIS
    {        

    }
    
    public class Firearm_Property
    {
        public int ShootAnimationLength;
        public int ReloadTimer, ShootDelayTimer;
        public int ReloadAnimationLength;
        public int ReloadTime, ShootTime;
        public float AimError;
        public string[] firingModes;
        public int BulletsPerShot;
        public int BulletsPerMagazine;
        public Ammo_Type_Data AmmoType = AmmoTypes.type_9mm;
        
        public Firearm_Property(
            int shoottimep,
            int reloadtimep,
            float aimerrorp,
            int bullpermagp,
            int bullpershotp,
            Ammo_Type_Data ammotypep,
            string[] firingmodesp)
        {
            this.ShootTime = shoottimep;
            this.ReloadTime = reloadtimep;
            this.AimError = aimerrorp;
            this.BulletsPerMagazine = bullpermagp;
            this.BulletsPerShot = bullpershotp;
            this.AmmoType = ammotypep;
            this.firingModes = firingmodesp;
        }
    }              

    public class GunLoadInfo
    {
        public string gunName;
        public string viewModel_filename;
        public string worldModel_filename;
        public string idleAnimationName;
        public string shootAnimation, reloadAnimation, EquipAnimation, DropAnimation, chamberAnimation;
        public string shootSound_Name, reloadsound_Name, equipSound_Name, dropSound_Name, chamberSound_Name; //From gunSoundCollection        

        public GunLoadInfo(
            string gunname,
            string viewmodelfilename,
            string worldmodelfilename,
            string idleanimname,
            string shoot, string reload, string equip, string drop, string chamber,
            string shootsound, string reloadsound, string equipsound, string dropsound, string chambersound)
        {
            this.gunName = gunname;
            this.viewModel_filename = viewmodelfilename;
            this.worldModel_filename = worldmodelfilename;
            this.idleAnimationName = idleanimname;
            this.shootAnimation = shoot;
            this.reloadAnimation = reload;
            this.EquipAnimation = equip;
            this.DropAnimation = drop;
            this.chamberAnimation = chamber;
            this.shootSound_Name = shootsound;
            this.reloadsound_Name = reloadsound;
            this.equipSound_Name = equipsound;
            this.dropSound_Name = dropsound;
            this.chamberSound_Name = chambersound;
        }
    }

    public class PlayerLoadInfo
    {        
            public string playerName;            
            public string worldModel_filename;
            public string hitboxModel_filename;

            public string idleAnimationName_unarmed, idleAnimationName_pistol, idleAnimationName_rifle;
            public string walkAnimation, reloadAnimation, EquipAnimation, DropAnimation, chamberAnimation;

            public PlayerLoadInfo(
                string name,
                string worldmodelfilename,
                string hitboxModelfilename,
                string idleanimname_unarm,
                string idleanimname_pistol,
                string idleanimname_rifle,
                string shoot, 
                string reload,
                string equip, 
                string drop)
            {
                this.hitboxModel_filename = hitboxModelfilename;
                this.playerName = name;   
                this.worldModel_filename = worldmodelfilename;
                this.idleAnimationName_unarmed = idleanimname_unarm;
                this.idleAnimationName_pistol = idleanimname_pistol;
                this.idleAnimationName_rifle = idleanimname_rifle;
                this.reloadAnimation = reload;
                this.EquipAnimation = equip;
                this.DropAnimation = drop;            
            }         
    }

    public struct Entity_Hit_Data
    {
        public String Hit_Object;
        public Vector3 Hit_Position;
        public float Hit_Distance;
    }

    public struct Shooting_Data
    {
        public Ray shoot_ray;
        public float shot_power; //In Joule = 0.5*m*v² = E
        public short bullet_type; //0 = FMJ  1 = HPJ  2 = Ball
    }

    //Use this for objects that can be targeted
    public interface I_Targetable
    {
        Vector3 get_Position_Targetable();
    }

    public interface I_GameEntity
    {
        void setConvexHull(SphereConvexHull hull);
        I_ConvexHull getConvexHull();

        void updateConvexHull();

        Vector3 get_Position();

        void get_Shot(Shooting_Data dat,AnimatedModelMesh hitbox, Matrix m);
    }

    public class GeneralGameData
    {
        public String ActiveSegment = String.Empty; //Can be Menu, Game, or Editor

        public String GameDirectory, RelativeMapFileAndPath, SkyboxPath;
        public int PlayerCount;
        public int width, height;
        public int Framerate;
        public bool AllowUserResizing;
        public bool fullscreen;
        public bool MultiSampling;
        public float AspectRatio;
        public bool wideFormat;
        public float ViewDisMax;
        public float ViewDisMin;
        public string LightFileEnding;
        public float WalkSpeed;
        public float PlayerHeight;
        public float Gravity;
        public Vector3[] startPoints;

        public Vector4 AmbientLightAndIntensity;
        public Vector4 DiffuseLightColor;
        public float DiffuseLightIntensity;
        public Vector3 DiffuseLightDirection;
        public float SpecularReflectivity;

        public float[] function_counters;

        //This part is for Load_PlayerAnimatedModels
        public string[] idleAnimationNames;
        public string[] modelPathNames;
        public int parentedModelsCount;
        public string[] playerNames;
        //----------------------------------------//


        public GeneralGameData()
        {
        }

        //------------------------------------------//
        //Set data
        public void SetGameData(
            string GameDir,
            int playerCount,
            int width, int height,
            int fps, bool userresize,
            bool fullscreen, bool multisample,
            float aspectRatio,
            bool wideFormatParameter,
            float viewdismin,
            float viewdismax,
            string lightFileEnding,
            float walkSpeed,
            float playerheight,
            float gravity,            
            Vector3[] startPoints)
        {
            this.GameDirectory = GameDir;
            this.PlayerCount = playerCount;
            this.Framerate = fps;
            this.width = width;
            this.height = height;
            this.AllowUserResizing = userresize;
            this.fullscreen = fullscreen;
            this.MultiSampling = multisample;
            this.AspectRatio = aspectRatio;
            this.wideFormat = wideFormatParameter;
            this.ViewDisMin = viewdismin;
            this.ViewDisMax = viewdismax;
            this.LightFileEnding = lightFileEnding;
            this.WalkSpeed = walkSpeed;            
            PlayerHeight = playerheight;
            this.Gravity = gravity;
            this.startPoints = startPoints;

            function_counters = new float[4];
            function_counters[0] = 0;
            function_counters[1] = MathHelper.PiOver2;
            function_counters[2] = MathHelper.Pi;
            function_counters[3] = MathHelper.TwoPi * 4/3;

            
        }
        //------------------------------------------//
    }        

    public class Player : I_Targetable, I_GameEntity
    {
        //The Lookat is a directional vector
        private Microsoft.Xna.Framework.Matrix PlayerViewMatrix; //Base Matrix..
        private Microsoft.Xna.Framework.Vector3 LookAt_Player; //will be done interally
        private Microsoft.Xna.Framework.Vector3 Player_UpVector;

        public Microsoft.Xna.Framework.Vector3 Position_Player;
        private Microsoft.Xna.Framework.Vector3 Rotation_Player;
        private float RotationSpeed;
        public int PlayerNumber;
        public float InvertYaxis = 1; // 1 is off/ -1 is on

        public Vector3 AdditionalMovement; //For falling and flying!
        public int PlayerJumpCounter = 0;
        public int playerJumpTime = 80;
        public int playerRespawnTimer = 0, playerRespawnTime = 80 * 3;
        public bool IsJumpingOrFlying = false; //This will turn off the collision function to not "grab" the player

        public bool CanJump = false;
        public float DistanceFromFloor = 10;
        public bool onFloor = false;

        public int Health = 100;
        public bool playerIsDead = false;

        public float SpeedMultiply = 1; public int SpeedTimer = 0; public int SpeedTime = 30 * 80;
        public float JumpMultiply = 1; public int JumpTimer = 0; public int JumpTime = 30 * 80;
        public bool JetpackEnabled = true; public int JetPackTmer = 0; public int JetpackTime = 30 * 80;
        public int Armor = 0;

        //For left right Bending, used for the animation!!!
        public Vector3 LeftRightBend_Set;
        public Vector3 LeftRightBend_adapt;

        public float WalkSpeed_Running;
        public float WalkSpeed_Walking;
        public float WalkSpeed_Sneak;

        public float Walkspeed_Current;

        //use for hitbox, visual player, gun in world
        public int playerModelNumber = 0;
        public AnimationController WorldModel_AnimController;
        public AnimationController ViewGun_AnimController;
        public Generic_Firearm firearm_current;

        public String JoyStickGuid = string.Empty;

        public String currentGunName;

        public List<String> ownedGuns;
        
        public Matrix[] attachedModelMatrices;
        public int[] parentedModelsAttachedIndices;

        //Collider
        public SphereConvexHull SphereConvexHull; //Use for in-range objects        

        //used for the wall repel to see where hes going
        public Vector3 CurrentPlayMoveDirection;

        public Camera PlayerCamera;

        public Vector3 get_Position()
        {
            return this.Position_Player;
        }

        //Same function, just for I_targetable.. Will be adjusted later
        //The required position might be too low to be a target area 
        public Vector3 get_Position_Targetable()
        {
            return this.Position_Player;
        }
        
        //Calculate the damage 
        public void get_Shot(Shooting_Data dat, AnimatedModelMesh hitbox, Matrix m)
        {
            List<string> list = CollisionDetection.Calculate_Ray_To_Hitbox(m, hitbox, dat.shoot_ray);

            foreach (String l in list)
            {
                Console.WriteLine(l);
            }
        }

        public void SetCamera(Camera camera)
        {
            this.PlayerCamera = camera;
        }

        public Camera GetCamera()
        {
            return PlayerCamera;
        }

        //Initialise Rotation and Position only
        public Player()
        {
            this.LookAt_Player = Microsoft.Xna.Framework.Vector3.Forward;
            this.LeftRightBend_adapt = Vector3.Zero;
            this.LeftRightBend_Set = Vector3.Zero;
        }

        public void UpdatePlayerAdapt(float power)
        {
            Vector3 diff = (LeftRightBend_Set - LeftRightBend_adapt) * power;

            if (VectorMath.get_length(diff) > 0.001f)
            {
                LeftRightBend_adapt += diff;
            }
        }

        public float get_DistanceFromFloor()
        {
            return DistanceFromFloor;
        }

        public bool is_ObjectOnFloor()
        {
            return onFloor;
        }

        public void CreateColliders(SphereConvexHull sphcol)
        {
            this.SphereConvexHull = sphcol;
        }

        public void UpdatePlayerStuff()
        {
            UpdatePlayerAdapt(0.11f);

            if (JumpTimer > 0)
            {
                JumpTimer--;
                JumpMultiply = 2;
            }
            else
                JumpMultiply = 1;

            if (playerRespawnTimer > 0)
            {
                playerRespawnTimer--;

            }

            if (SpeedTimer > 0)
            {
                SpeedMultiply = 3;
                SpeedTimer--;
            }
            else
                SpeedMultiply = 1;

            if (JetPackTmer > 0)
            {
                JetPackTmer--;
                JetpackEnabled = true;
            }
            else
                JetpackEnabled = false;

            if (PlayerJumpCounter > 0)
            {
                CanJump = false;
                PlayerJumpCounter--;
            }
            IsJumpingOrFlying = false;

            Position_Player += AdditionalMovement;

            if (Health <= 0)
            {
                playerIsDead = true;
            }
        }


        public I_ConvexHull getConvexHull()
        {
            return SphereConvexHull;
        }

        public void setConvexHull(SphereConvexHull hull)
        {
            SphereConvexHull = hull;
        }

        public void updateConvexHull()
        {
            this.SphereConvexHull.Position = Position_Player;
            this.SphereConvexHull.Position.Y += 2.0f;
        }

        /*----------------------------------------------------------------------*/
        //Player Position Get/set
        public void Set_PlayerPosition(Microsoft.Xna.Framework.Vector3 newPos)
        {
            this.Position_Player = newPos;
        }
        public Microsoft.Xna.Framework.Vector3 Get_PlayerPosition()
        {
            return this.Position_Player;
        }
        /*----------------------------------------------------------------------*/

        /*----------------------------------------------------------------------*/
        //Player Rotation Get/set
        public void Set_PlayerRotation(Microsoft.Xna.Framework.Vector3 rotation)
        {
            if (rotation.X > 1.3f)
                rotation.X = 1.3f;
            if (rotation.X < -1.3f)
                rotation.X = -1.3f;
            Rotation_Player = rotation;
        }
        public Microsoft.Xna.Framework.Vector3 Get_PlayerRotation()
        {
            return this.Rotation_Player;
        }
        /*----------------------------------------------------------------------*/

        /*-----------------------------------------------------------------------*/
        //UpVector Get/Set
        public void Set_UpVector(Microsoft.Xna.Framework.Vector3 newUpVector)
        {
            this.Player_UpVector = newUpVector;
        }
        public Microsoft.Xna.Framework.Vector3 Get_UpVector()
        {
            return Player_UpVector;
        }
        /*-----------------------------------------------------------------------*/

        /*----------------------------------------------------------------------*/
        /// <summary>
        /// This function Takes the LookAt Vector, ROTATES it first, then adds it onto the position
        /// and creates the ViewMatrix using Position and Rotation, however, the Lookat 
        /// vector NEVER changes, the manipulated "lookat" is given right to the viewMatrix, 
        /// LookAt is only a directional vector for rotation.
        /// </summary>
        public void updateViewMatrix()
        {
            PlayerViewMatrix = Microsoft.Xna.Framework.Matrix.CreateLookAt(
                Position_Player,
                Vector3.Add(Position_Player, VectorMath.Vector3_Rotate(LookAt_Player, Rotation_Player)),
                Player_UpVector);

        }
        /*----------------------------------------------------------------------*/

        //For collision detection later
        public Microsoft.Xna.Framework.Vector3 Get_PlayerLookat()
        {
            return LookAt_Player;
        }
    }
    
    public class Vehicle
    {
        public float Speed;
        public float Horsepower;
        public float steeringPower;
        public PhysicsModel physicsModel;
        public Vector3[] WheelPositionsBuffer;
        public Vector3[] LocalWheelPositionsStatic;
        public Vector3 Rotation; //Contains general Car rotation
        public float[] FallCounters;
        public float[] Heights;
        public Vector3 CenterPosition;
        public Vector3 DriveDirection;
        public Vector3 SteeringRotation; //Will be added everytime its steered and drives

        public Vehicle(float speed, float hp, float steerin, Vector3 Position , Vector3 Rotate)
        {
            CenterPosition = Position;
            WheelPositionsBuffer = new Vector3[4];
            LocalWheelPositionsStatic = new Vector3[4];
            this.Speed = speed;
            this.Horsepower = hp;
            this.steeringPower = steerin;
            this.FallCounters =  new float[4];
            this.Heights = new float[4];
            this.Rotation = Rotate;
        }

        public Vehicle(float speed, float hp, float steerin, Vector3[] wheelpos, Vector3 Rotate)
        {
            
            WheelPositionsBuffer = new Vector3[4];
            LocalWheelPositionsStatic = new Vector3[4];
            this.Speed = speed;
            this.Horsepower = hp;
            this.steeringPower = steerin;
            this.LocalWheelPositionsStatic = wheelpos;
            this.FallCounters = new float[4];
            this.Heights = new float[4];
            this.Rotation = Rotate;
        }

        public void CalculateWheelPositions(Vector3[] pos)
        {
            this.LocalWheelPositionsStatic = pos;
        }
    }
}
