﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConexGameLibrary;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ConexGameLibrary
{
    public static class Interpolation
    {
        public static float Linear_Interpolate(float u, float p1, float p2)
        {
            return p1 + (p2 - p1) * u;
        }

        public static float Cosine_Interpolate(float u, float p1, float p2)
        {   
            //Calculate a slope of with sine

            //Creates a sine of max Interval 2, with wavelength two Pi
            float factor = u * MathHelper.Pi - MathHelper.Pi/2;

            float sinVal = (VectorMath.Sin(factor) + 1) / 2;

            float final = p1 + (p2 - p1) * sinVal;


            return final;
        }

        public static float Cubic_Interpolate(float u, float p0, float p1, float p2, float p3)
        {
            //Use the first quarter of cosine for smooth transition
            float a0, a1, a2, a3, u2;

            u2 = u * u;

            a0 = p3 - p2 - p0 + p1;
            a1 = p0 - p1 - a0;
            a2 = p2 - p0;
            a3 = p1;       

            return (a0 * u2 * u) + (a1*u2) + (a2*u) + a3;
        }
    }
}
