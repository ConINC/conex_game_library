﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ConexGameLibrary;

namespace ConexGameLibrary
{
    public class Zombie_basic
    {
        public Matrix rotmatrix;
        public Vector3 position;
        public float rotation;
        public VertexListModel vlm;
        public Texture2D texture2d;
        CustomBinarySerialiser cst;    

        public Zombie_basic(Vector3 spawnPos, string basepath, string modelname)
        {
            cst = new CustomBinarySerialiser();
            rotmatrix = new Matrix();
            this.position = spawnPos;
            rotation = 0;
            position = Vector3.Zero;
            vlm = cst.DeserializeModel(basepath + modelname);
            
        }

        public void updateNPC()
        {            
            
        }
    }
}
