using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ConexGameLibrary
{    

    //EVERYTHING IS SOON TO BE OBSOLETE
    public class SphereVolume
    {
        public Vector3 Position;
        public Single Radius;

        public SphereVolume(Vector3 position, Single radius)
        {   
            this.Radius = radius;
            this.Position = position;
        }
    }

    public class Triangle
    {
        public Triangle()
        {
        }        
        public Vector3 p1;        
        public Vector3 p2;
        public Vector3 p3;
        public Vector3 surfaceNormal;
    }    
  
}
