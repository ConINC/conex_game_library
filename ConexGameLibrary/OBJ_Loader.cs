﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using System.Globalization;

namespace ConexGameLibrary
{
    public class Obj_Material
    {
        public String ShaderName;
        public int shaderindex;
        public float tfx,tfy,tfz; //transparency
        public float bumpfactor;
        public String colormap_path;
        public String bumpmap_path;
        public String specularmap_path;
        public int textureHash;
        public Texture2D color, bump, specular;
    }
    
    public class Face
    {
        public String AppliedShader;
        public int shaderindex;
        public int indexbuffer;
        public List<CustomVertex> vertexlist;
        public VertexBuffer vertexbuffer;
        public Vector3 offset;
        public float radius;

        public Face()
        {
            vertexlist = new List<CustomVertex>();
        }        
    }

    public class Obj_Mesh
    {
        public Face[] faces;
        public String groupName;

        public Vector3 offset;
        public float mesh_MaxRadius;
        

        public Obj_Mesh(int faceCount)
        {
            faces = new Face[faceCount];
        }
    }

    public class OBJ_GROUP_asString
    {
        public List<String> groupLines;

        public OBJ_GROUP_asString()
        {
            groupLines = new List<string>();
        }
    }

    
    public class ObjGroupsAndMaterialsPack
    {
        public Obj_Mesh[] groups;
        public Obj_Material[] obj_mats;
    }

    public class OBJ_Loader
    {
        //We take a single group as string-array, a face is created from triangles with the same applied texture
        private static Face[] CreateObjectFaces(List<String> lines, List<Vector3> points, List<Vector2> uvs, List<Vector3> vns)
        {
            NumberFormatInfo nfi = CultureInfo.InvariantCulture.NumberFormat;
            List<String>[] facesAsString;
            List<String> appliedShadersToFaces;
            Face[] faces;

            int facesCount = 0;

            for (int i = 0; i < lines.Count; i++)
            {
                if (lines[i].StartsWith("usemtl"))
                    facesCount++;
            }
            facesAsString = new List<String>[facesCount];
            appliedShadersToFaces = new List<String>();
            faces = new Face[facesCount];

            int currentFace = 0;
            for(int i = 0; i < lines.Count; i++)
            {
                //create face with these data
                if (facesAsString[currentFace] == null)
                {
                    facesAsString[currentFace] = new List<String>();                    
                }
                //copy the raw face lines, not with mtl
                if (!lines[i].StartsWith("usemtl"))
                {
                    facesAsString[currentFace].Add(lines[i]);
                    
                }
                if(lines[i].StartsWith("usemtl"))
                {
                    appliedShadersToFaces.Add(lines[i].Substring(7));
                }
                //since the faces come below the usemtl line, we use it to find the introduction of the next facelist
                if (!lines[i].StartsWith("usemtl") && i + 1 < lines.Count && lines[i + 1].StartsWith("usemtl"))
                {
                    currentFace++;
                }     
            }

            //Copy it to a temp list which creates an array with the amount of usemtls, which are treated as a face
            //Then we dynamicle extend the amount of triangles within a face, by seeing how many vertex are given
            //before the next introduction of a new face (usemtl)
            for (int i = 0; i < facesAsString.Length; i++)
            {
                faces[i] = new Face();
                for (int j = 0; j < facesAsString[i].Count; j++)
                {
                    faces[i].AppliedShader = appliedShadersToFaces[i];

                    string[] tripplet = facesAsString[i][j].Split(' ');
                    string[] v1 = tripplet[0].Split('/'), v2 = tripplet[1].Split('/'), v3 = tripplet[2].Split('/');

                    CustomVertex c1,c2,c3;
                                        
                    c1 = new CustomVertex(
                        points[ Convert.ToInt32(v1[0], nfi)-1], 
                        uvs[ Convert.ToInt32(v1[1], nfi)-1],
                        vns[Convert.ToInt32(v1[2], nfi)-1],
                        Vector3.Zero,
                        Vector3.Zero);

                    c2 = new CustomVertex(
                        points[Convert.ToInt32(v2[0], nfi)-1],
                        uvs[Convert.ToInt32(v2[1], nfi)-1],
                        vns[Convert.ToInt32(v2[2], nfi)-1],
                        Vector3.Zero,
                        Vector3.Zero);

                    c3 = new CustomVertex(
                        points[Convert.ToInt32(v3[0], nfi)-1],
                        uvs[Convert.ToInt32(v3[1], nfi)-1],
                        vns[Convert.ToInt32(v3[2], nfi)-1],
                        Vector3.Zero,
                        Vector3.Zero);
                    
                    faces[i].vertexlist.Add(c1);
                    faces[i].vertexlist.Add(c2);
                    faces[i].vertexlist.Add(c3);                    
                }
            }
            Console.WriteLine("Face created: {0}\n\n", faces.Length);

            
            for (int i = 0; i < faces.Length; i++)
            {
                for (int j = 0; j < faces[i].vertexlist.Count; j++)
                {
                    faces[i].offset += faces[i].vertexlist[j].Position;
                }
                faces[i].offset /= (float)faces[i].vertexlist.Count;
            }

            return faces;
        }

        //Will take a group as string, then convert all objects in that group to seperate faces determined by applied material, then returns it
        private static Obj_Mesh createGroup(OBJ_GROUP_asString ogas, List<Vector3> points, List<Vector2> uvs, List<Vector3> vns)
        {
            Obj_Mesh obj;            

            int facecount = 0;
            string groupname = string.Empty;

            for (int i = 0; i < ogas.groupLines.Count; i++)
            {
                if (ogas.groupLines[i].StartsWith("g "))
                {
                    groupname = ogas.groupLines[i].Substring(2);
                    ogas.groupLines.RemoveAt(i);
                    i = 0;
                }

                if (ogas.groupLines[i].StartsWith("usemtl"))
                {
                    facecount++;
                }
            }
            obj = new Obj_Mesh(facecount);

            obj.groupName = groupname;
            obj.faces = CreateObjectFaces(ogas.groupLines, points,uvs,vns);

            for (int i = 0; i < obj.faces.Length; i++)
            {
                obj.offset += obj.faces[i].offset;

            }
            obj.offset /= (float)obj.faces.Length;

            
            return obj;
        }

        public static ObjGroupsAndMaterialsPack Open_OBJ(string basePath, string filePath)
        {            
            NumberFormatInfo nf = CultureInfo.InvariantCulture.NumberFormat;

            #region Load the mesh
            String relMatPath = String.Empty;
            String[] obj_lines;
            int GroupCount = 0;
            String fullpath = basePath + "//" + filePath + ".obj";
            OBJ_GROUP_asString[] groupAsStrings;
            
            List<Vector3> points_vec = new List<Vector3>();
            List<Vector2> uvs_vec = new List<Vector2>();
            List<Vector3> vns_vec = new List<Vector3>();

            List<String> groupsMtlfaces = new List<String>();
            Obj_Mesh[] obj_groups;
            obj_lines = File.ReadAllLines(fullpath, ASCIIEncoding.ASCII);


            if(obj_lines == null)
            {
                Console.WriteLine("Could not open Obj-File {0}, make sure the path is correct\n and the file is not in use by another process", fullpath);
                throw new FileLoadException();
            }

            //Load the mesh data in lists
            #region creates specific lists from the string version for v, vt, and vn , also counts groups
            for (int i = 0; i < obj_lines.Length; i++)
            {
                if (obj_lines[i].StartsWith("mtllib"))
                {
                    relMatPath = obj_lines[i].Substring(7);
                }
                if (obj_lines[i].StartsWith("v "))
                {
                    String[] splt = obj_lines[i].Substring(2).Split(' ');
                    points_vec.Add(new Vector3(Convert.ToSingle(splt[0], nf), Convert.ToSingle(splt[1], nf), Convert.ToSingle(splt[2], nf)));
                }
                if (obj_lines[i].StartsWith("vn "))
                {
                    String[] splt = obj_lines[i].Substring(3).Split(' ');
                    vns_vec.Add(new Vector3(Convert.ToSingle(splt[0], nf), Convert.ToSingle(splt[1], nf), Convert.ToSingle(splt[2], nf)));
                }
                if (obj_lines[i].StartsWith("vt "))
                {
                    String[] splt = obj_lines[i].Substring(3).Split(' ');
                    uvs_vec.Add(new Vector2(Convert.ToSingle(splt[0], nf), Convert.ToSingle(splt[1], nf)));                
                }
                if (obj_lines[i].StartsWith("f "))                
                    groupsMtlfaces.Add(obj_lines[i].Substring(2));
                
                if (obj_lines[i].StartsWith("g ") && !obj_lines[i].Contains("default"))
                {
                    GroupCount++;
                    groupsMtlfaces.Add(obj_lines[i].Substring(0));
                }
                if (obj_lines[i].StartsWith("usemtl "))                
                    groupsMtlfaces.Add(obj_lines[i].Substring(0));
            }
            #endregion
            obj_lines = null;
            
            
            groupAsStrings = new OBJ_GROUP_asString[GroupCount];

            int index = 0;
            int blockCounter = 0;

            do
            {
                if (null == groupAsStrings[blockCounter])
                    groupAsStrings[blockCounter] = new OBJ_GROUP_asString();

                groupAsStrings[blockCounter].groupLines.Add( groupsMtlfaces[index]);
                
                index++;

                if (index < groupsMtlfaces.Count && groupsMtlfaces[index].StartsWith("g "))
                    blockCounter++;
            } while (index < groupsMtlfaces.Count && blockCounter < groupAsStrings.Length);
            groupsMtlfaces = null;

            obj_groups = new Obj_Mesh[GroupCount];

            for (int i = 0; i < groupAsStrings.Length; i++)
            {
                obj_groups[i] = createGroup(groupAsStrings[i], points_vec, uvs_vec, vns_vec);
                                                
            }
            #endregion

            #region Now load material
            String fullpathmat = basePath + "//" + relMatPath;
            Obj_Material[] materials;
            
            string[] matlines = File.ReadAllLines(fullpathmat);
            
            int matCount = 0;
            foreach (String l in matlines)
            {
                if (l.StartsWith("newmtl "))
                    matCount++;
            }
            
            materials = new Obj_Material[matCount];

            int materialCounter = 0;
            for (int i = 0; i < matlines.Length; i++)
            {                
                
                if (materials[materialCounter] == null)
                    materials[materialCounter] = new Obj_Material();

                if (matlines[i].StartsWith("newmtl ")) // || matlines[i].StartsWith("map_Kd") || matlines[i].StartsWith("illum ") || matlines[i].StartsWith("Tf ") || matlines[i].StartsWith("bump ") || matlines[i].StartsWith("map_Ks "))
                {
                    materials[materialCounter].ShaderName = matlines[i].Substring(7);
                    
                }
                if (matlines[i].StartsWith("map_Kd "))
                {
                    materials[materialCounter].colormap_path = matlines[i].Substring(7);
                    
                }
                if (matlines[i].StartsWith("Tf "))
                {
                    string[] split = matlines[i].Substring(3).Split(' ');

                    materials[materialCounter].tfx = Convert.ToSingle(split[0], nf);
                    materials[materialCounter].tfy = Convert.ToSingle(split[1], nf);
                    materials[materialCounter].tfz = Convert.ToSingle(split[2], nf);
                    
                }
                if (matlines[i].StartsWith("bump "))
                {
                    materials[materialCounter].bumpmap_path = matlines[i].Substring(5);
                    
                }
                if (matlines[i].StartsWith("map_Ks "))
                {
                    materials[materialCounter].specularmap_path = matlines[i].Substring(7);
                    
                }                

                if(i+1 < matlines.Length && matlines[i+1].StartsWith("newmtl ") && !matlines[i].StartsWith("newmtl") && i> 0)
                {
                    materialCounter++;
                }
            }
            #endregion

            for (int i = 0; i < materials.Length; i++)
            {
                char[] hash_c = materials[i].ShaderName.ToCharArray();
                materials[i].shaderindex = i;

                foreach (char c in hash_c)
                    materials[i].textureHash += (int)c;
            }

            #region Find the corresponding shader and apply it
            for (int i = 0; i < obj_groups.Length; i++)
            {
                for (int j = 0; j < obj_groups[i].faces.Length; j++)
                {
                    for (int m = 0; m < materials.Length; m++)
                    {
                        //Set the corresponding shader index to the face, to identify easily in draw call
                        if (obj_groups[i].faces[j].AppliedShader.Equals(materials[m].ShaderName))
                        {
                            obj_groups[i].faces[j].shaderindex = m;
                        }
                    }
                }

            }
            #endregion
            
            //subtract offset from all vertices in faces which belong to the group
            for (int i = 0; i < obj_groups.Length; i++)
            {
                for (int j = 0; j < obj_groups[i].faces.Length; j++)
                {
                    List<CustomVertex> list = new List<CustomVertex>();

                    for (int k = 0; k < obj_groups[i].faces[j].vertexlist.Count; k++)
                    {
                        list.Add(new CustomVertex(
                            obj_groups[i].faces[j].vertexlist[k].Position - obj_groups[i].offset,
                            obj_groups[i].faces[j].vertexlist[k].TextureCoordinate,
                            obj_groups[i].faces[j].vertexlist[k].Normal,
                            obj_groups[i].faces[j].vertexlist[k].BiNormal,
                            obj_groups[i].faces[j].vertexlist[k].Tangent));
                    }
                    obj_groups[i].faces[j].vertexlist = list;
                    
                }
            }



            ObjGroupsAndMaterialsPack pack = new ObjGroupsAndMaterialsPack();
            pack.groups = obj_groups;
            pack.obj_mats = materials;

            return pack;
        }
    }
}
