﻿//CAMERA CLASS AND JOYSTICK INPUT CLASS!  FINALLY WORKING WITH 2 GAMEPADS!

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using SharpDX.DirectInput;
using Microsoft.Xna.Framework.Input;
using SharpDX;
using System.Collections;
using System.Threading;
using SharpDX.Multimedia;
using System.Timers;

namespace ConexGameLibrary
{    
    
    public class CameraGamePad : GameComponent
    {
        public Microsoft.Xna.Framework.Vector3 Position;
        public float original_height;
        public float head_rotation_y, head_rotation_x;
        public float cameraY, cameraX;
        public Microsoft.Xna.Framework.Vector3 floorOffset;
        public Microsoft.Xna.Framework.Matrix View;
        public Microsoft.Xna.Framework.Matrix Projection;
        public Microsoft.Xna.Framework.Vector3 LookAt;
        public Microsoft.Xna.Framework.Matrix RotationMatrix,RotationMatrix2;
        public float RotationSpeed;
        public bool invertY = true;
        short inverter = -1;
        public float walkSpeed;
        public Microsoft.Xna.Framework.Vector3 transformedDirection;
        public Microsoft.Xna.Framework.Vector3 transformedDirection2;
        byte player;
        float zoomFactor = 1;
        Microsoft.Xna.Framework.Input.KeyboardState ks;
        List<Block> blocks_in_class;

        bool enableGamepad = false;

        float left_stick_x_amount, left_stick_y_amount;

        public CameraGamePad(Game game, Microsoft.Xna.Framework.Matrix projection, float rotateSpeed, float walkSpeedParam, byte player)
            : base(game)
        {
            RotationSpeed = rotateSpeed;
            LookAt = new Microsoft.Xna.Framework.Vector3(0, 0, -1);
            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position, LookAt, Microsoft.Xna.Framework.Vector3.Up);
            Projection = projection;
            Position = new Microsoft.Xna.Framework.Vector3(1f, 0, 0);
            walkSpeed = walkSpeedParam;
            
            this.player = player;
        }

        private double angularCounter = 0;

        /// <summary>
        /// Only temporary functioN!
        /// </summary>
        public void recieve_blocks(ref List<Block> blocks_in_class_p)
        {
            this.blocks_in_class = blocks_in_class_p;
        }

        public void UpdateData()
        {
            if (angularCounter < 360)
            {
                angularCounter = angularCounter + 0.008f;
            }
            
            else
            {
                angularCounter = 0;
            }

            cameraX = cameraX += -(float)Math.Cos(angularCounter * 2d) * 0.0002f;
            cameraY = cameraY += (float)Math.Sin(angularCounter) * 0.0001f;
            //camera looks at this direction
            Microsoft.Xna.Framework.Vector3 cameraDirection = new Microsoft.Xna.Framework.Vector3(0, 0, -1);

            //DO the breath move -> up down slowly

            //Rotation matrix for the Y axis of the camear
            RotationMatrix = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY);
            RotationMatrix2 = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY + MathHelper.PiOver2);

            //Creates a vector which points towards the direction that the camera is facing now

            //FOR THE FORWARD BACKWARD MOVEMENT //Normalising will always keep the same speed, even when looking down
            transformedDirection = Microsoft.Xna.Framework.Vector3.Transform(cameraDirection, RotationMatrix);

            Microsoft.Xna.Framework.Vector2 vecNormalised = Microsoft.Xna.Framework.Vector2.Normalize(
                                       new Microsoft.Xna.Framework.Vector2(
                                      transformedDirection.X, transformedDirection.Z
                                      )
                            );
            transformedDirection.X = vecNormalised.X; transformedDirection.Z = vecNormalised.Y;


            //FOR THE SIDEWAYS MOVEMENT
            transformedDirection2 = Microsoft.Xna.Framework.Vector3.Transform(cameraDirection, RotationMatrix2);

            Microsoft.Xna.Framework.Vector2 vecNormalised2 = Microsoft.Xna.Framework.Vector2.Normalize(
                                      new Microsoft.Xna.Framework.Vector2(
                                     transformedDirection2.X, transformedDirection2.Z
                                     )
                           );

            transformedDirection2.X = vecNormalised2.X; transformedDirection2.Z = vecNormalised2.Y;


            Microsoft.Xna.Framework.Vector3 cameraLookAt = Position + Microsoft.Xna.Framework.Vector3.Normalize(transformedDirection);

            //THIS IS WRONG, the camera Looks always a certain position, the position of the camera should
            //be the same as the look at, else it becomes a tracking camera!
            // View = Matrix.CreateLookAt(Position, transformedDirection, Vector3.Up);

            //IT IS IMPORTANT that the Lookat vector is always the same as the position itself, so the camera always rotates around a 0 axis, else the camera simply 
            //tracks the "lookat" and rotation wont work anymroe!

            View = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX);

            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position + new Microsoft.Xna.Framework.Vector3(0,5,0) , cameraLookAt+ new Microsoft.Xna.Framework.Vector3(0,5,0), Microsoft.Xna.Framework.Vector3.Up);

     
            //CAMERA X AND Y ARE TURNED AROUND SINCE IT ROTATES AROUND X AND Y
            
            if (cameraY > 2 * MathHelper.Pi || cameraY < -2 * MathHelper.Pi)
            {
                cameraY = 0;
            }
            if (cameraX > 1.4f)
                cameraX = 1.4f;
            if (cameraX < -1.4f)
                cameraX = -1.4f;
        }
        
        //For camera with Joystick


        //For camera with keyboard
        public void Movement()
        {           
            ks = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            if (invertY == false)
            {
                inverter = 1;
            }
            if (invertY == true)
            {
                inverter = -1;
            }

            if (ks.IsKeyDown(Keys.A))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY + rotateAmount;          
            }

            if (ks.IsKeyDown(Keys.D))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY - rotateAmount;
            }

            if (ks.IsKeyDown(Keys.W))
            {
                float rotateAmount = (RotationSpeed / 60f) * inverter;
                cameraX = cameraX - rotateAmount;
            }
            if (ks.IsKeyDown(Keys.S))
            {
                float rotateAmount = (RotationSpeed / 60f) * inverter;
                cameraX = cameraX + rotateAmount;
            }

            if (ks.IsKeyDown(Keys.Q))
            {
                Position.X = Position.X + transformedDirection.X * walkSpeed;
                Position.Z = Position.Z + transformedDirection.Z * walkSpeed;
                //Console.WriteLine(Position);
            }

            if (ks.IsKeyDown(Keys.E))
            {
                Position.X = Position.X - transformedDirection.X * walkSpeed;
                Position.Z = Position.Z - transformedDirection.Z * walkSpeed;
            }            
        }

        //Call this from outside!
        public void PerformWallCollision(ref List<Block> blocks_param)
        {
            int cnt1, cnt2, cnt3;
            var tri = new Triangle();
            //ATTEMPT NUMBER 1!
            if (blocks_in_class != null)
            {
                Microsoft.Xna.Framework.Ray ray = new Microsoft.Xna.Framework.Ray();
                Microsoft.Xna.Framework.Vector3 averagedNormal;

                #region actual function!
                foreach (Block block in blocks_param)
                {
                    for (int i = 0; i < block.wallModel.vertices.Length; )
                    {
                        
                        cnt1 = i; cnt2 = i + 1; cnt3 = i + 2;

                        tri.p1 = block.wallModel.vertices[cnt1].Position;
                        tri.p2 = block.wallModel.vertices[cnt2].Position;
                        tri.p3 = block.wallModel.vertices[cnt3].Position;

                        float distancePlayerPlane = CollisionDetection.get_distanceFromPlane(Position, tri.p1, tri.p2, tri.p3);
                        Microsoft.Xna.Framework.Vector3 centerOfTriangle = CollisionDetection.getCenterOfTriangle(tri);
                        float maxRadiusAroundTriangle = CollisionDetection.getMaxRadiusAroundTriangle(tri);
                        float distanceFromTriangleCenter = VectorMath.get_length(Microsoft.Xna.Framework.Vector3.Subtract(centerOfTriangle, Position));

                        //if (distancePlayerPlane < maxRadiusAroundTriangle && distanceFromTriangleCenter < maxRadiusAroundTriangle)
                        if (distanceFromTriangleCenter < maxRadiusAroundTriangle)
                        {                            
                            averagedNormal = ( block.wallModel.vertices[cnt1].Normal + block.wallModel.vertices[cnt2].Normal + block.wallModel.vertices[cnt3].Normal) / 3;
                            ray.Position = Position; 
                            ray.Direction = averagedNormal;

                            if (CollisionDetection.checkTriangleIntersect(block.wallModel.vertices[cnt1].Position, block.wallModel.vertices[cnt2].Position,
                                                                                         block.wallModel.vertices[cnt3].Position, ray)  && distancePlayerPlane < 1 )
                            {                                
                                float fac1 = VectorMath.getScalarProductBeforeCosinus(averagedNormal, transformedDirection);
                                float fac2 = VectorMath.getScalarProductBeforeCosinus(averagedNormal, transformedDirection2);
                                Microsoft.Xna.Framework.Vector3.Normalize(averagedNormal);

                                Position += (averagedNormal * ((left_stick_y_amount > 0) ? left_stick_y_amount : left_stick_y_amount * -1) * ((fac1 > 0) ? fac1 : fac1 * -1))  / (walkSpeed * 7f); // *((fac1 > 0) ? fac1 : (-1 * fac1));
                                Position +=(averagedNormal * ((left_stick_x_amount > 0) ? left_stick_x_amount : left_stick_x_amount * -1) * ((fac2 > 0) ? fac2 : fac2 * -1)) / (walkSpeed * 7f); // *((fac2 > 0) ? fac1 : (-1 * fac2));
                            }
                        }
                        i = i + 3;
                    }
                }

                #endregion
            }
        }

        public override void Update(GameTime gameTime)
        {
            Movement();           
            UpdateData();
            PerformWallCollision(ref blocks_in_class);

            base.Update(gameTime);
        }
    }

    public class CameraGamePad2 : GameComponent
    {
        public Microsoft.Xna.Framework.Vector3 Position;
        public float original_height;
        private float cameraY, cameraX;
        public Microsoft.Xna.Framework.Vector3 floorOffset;
        public Microsoft.Xna.Framework.Matrix View;
        public Microsoft.Xna.Framework.Matrix Projection;
        public Microsoft.Xna.Framework.Vector3 LookAt;
        public Microsoft.Xna.Framework.Matrix RotationMatrix, RotationMatrix2;
        public float RotationSpeed;
        public bool invertY = true;
        short inverter = -1;
        public float walkSpeed;
        public Microsoft.Xna.Framework.Vector3 transformedDirection;
        public Microsoft.Xna.Framework.Vector3 transformedDirection2;
     
        byte player;
        Microsoft.Xna.Framework.Input.KeyboardState ks;
        float zoomFactor = 1;
        bool enableGamepad = false;

        List<Block> blocks_in_class;

        float left_stick_x_amount, left_stick_y_amount;

        public CameraGamePad2(Game game, Microsoft.Xna.Framework.Matrix projection, float rotateSpeed, float walkSpeedParam, byte player)
            : base(game)
        {
            RotationSpeed = rotateSpeed;
            LookAt = new Microsoft.Xna.Framework.Vector3(0, 0, -1);
            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position, LookAt, Microsoft.Xna.Framework.Vector3.Up);
            Projection = projection;
            Position = new Microsoft.Xna.Framework.Vector3(0,30, 0);
            walkSpeed = walkSpeedParam;           
            this.player = player;
        }

        double angularCounter = 0d;

        public void recieve_blocks(ref List<Block> blocks_in_class_p)
        {
            this.blocks_in_class = blocks_in_class_p;
        }

        public void Movement()
        {
            ks = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            if (invertY == false)
            {
                inverter = 1;
            }
            if (invertY == true)
            {
                inverter = -1;
            }

            if (ks.IsKeyDown(Keys.J))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY + rotateAmount;
            }


            if (ks.IsKeyDown(Keys.L))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY - rotateAmount;
            }

            if (ks.IsKeyDown(Keys.I))
            {
                float rotateAmount = (RotationSpeed / 60f) * inverter;
                cameraX = cameraX - rotateAmount;
            }
            if (ks.IsKeyDown(Keys.K))
            {
                float rotateAmount = (RotationSpeed / 60f) * inverter;
                cameraX = cameraX + rotateAmount;
            }


            if (ks.IsKeyDown(Keys.U))
            {
                Position.X = Position.X + transformedDirection.X * walkSpeed;
                Position.Z = Position.Z + transformedDirection.Z * walkSpeed;
                //Console.WriteLine(Position);
            }


            if (ks.IsKeyDown(Keys.O))
            {
                Position.X = Position.X - transformedDirection.X * walkSpeed;
                Position.Z = Position.Z - transformedDirection.Z * walkSpeed;
            }
        }
        public void UpdateData()
        {
            if (angularCounter < 360)
            {
                angularCounter = angularCounter + 0.008f;
            }
            else
            {
                angularCounter = 0;
            }

            cameraX = cameraX += -(float)Math.Cos(angularCounter * 2d) * 0.0002f;
            cameraY = cameraY += (float)Math.Sin(angularCounter) * 0.0001f;
            //camera looks at this direction
            Microsoft.Xna.Framework.Vector3 cameraDirection = new Microsoft.Xna.Framework.Vector3(0, 0, -1);

            //DO the breath move -> up down slowly

            //Rotation matrix for the Y axis of the camear
            RotationMatrix = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY);
            RotationMatrix2 = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY + MathHelper.PiOver2);

            //Creates a vector which points towards the direction that the camera is facing now

            //FOR THE FORWARD BACKWARD MOVEMENT //Normalising will always keep the same speed, even when looking down
            transformedDirection = Microsoft.Xna.Framework.Vector3.Transform(cameraDirection, RotationMatrix);

            Microsoft.Xna.Framework.Vector2 vecNormalised = Microsoft.Xna.Framework.Vector2.Normalize(
                                       new Microsoft.Xna.Framework.Vector2(
                                      transformedDirection.X, transformedDirection.Z
                                      )
                            );
            transformedDirection.X = vecNormalised.X; transformedDirection.Z = vecNormalised.Y;


            //FOR THE SIDEWAYS MOVEMENT
            transformedDirection2 = Microsoft.Xna.Framework.Vector3.Transform(cameraDirection, RotationMatrix2);

            Microsoft.Xna.Framework.Vector2 vecNormalised2 = Microsoft.Xna.Framework.Vector2.Normalize(
                                      new Microsoft.Xna.Framework.Vector2(
                                     transformedDirection2.X, transformedDirection2.Z
                                     )
                           );
            transformedDirection2.X = vecNormalised2.X;  transformedDirection2.Z = vecNormalised2.Y;


            Microsoft.Xna.Framework.Vector3 cameraLookAt = Position + Microsoft.Xna.Framework.Vector3.Normalize(transformedDirection);

            //THIS IS WRONG, the camera Looks always a certain position, the position of the camera should
            //be the same as the look at, else it becomes a tracking camera!
            // View = Matrix.CreateLookAt(Position, transformedDirection, Vector3.Up);

            //IT IS IMPORTANT that the Lookat vector is always the same as the position itself, so the camera always rotates around a 0 axis, else the camera simply 
            //tracks the "lookat" and rotation wont work anymroe!

            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position+new Microsoft.Xna.Framework.Vector3(0,5,0), cameraLookAt+new Microsoft.Xna.Framework.Vector3(0,5,0), Microsoft.Xna.Framework.Vector3.Up);

            //CAMERA X AND Y ARE TURNED AROUND SINCE IT ROTATES AROUND X AND Y
            //Console.WriteLine("camera2 is  : " + transformedDirection);
            if (cameraY > 2*MathHelper.Pi || cameraY < - 2*MathHelper.Pi)
            {
                cameraY = 0;
            }
            if (cameraX > 1.4f)
                cameraX = 1.4f;
            if (cameraX < -1.4f)
                cameraX = -1.4f;

        }
        //For camera with Joystick
       
        public void PerformWallCollision(ref List<Block> blocks_param)
        {
            int cnt1, cnt2, cnt3;
            var tri = new Triangle();
            //ATTEMPT NUMBER 1!
            if (blocks_in_class != null)
            {
                Microsoft.Xna.Framework.Ray ray = new Microsoft.Xna.Framework.Ray();
                Microsoft.Xna.Framework.Vector3 averagedNormal;

                #region actual function!
                foreach (Block block in blocks_param)
                {
                    for (int i = 0; i < block.wallModel.vertices.Length; )
                    {

                        cnt1 = i; cnt2 = i + 1; cnt3 = i + 2;

                        tri.p1 = block.wallModel.vertices[cnt1].Position;
                        tri.p2 = block.wallModel.vertices[cnt2].Position;
                        tri.p3 = block.wallModel.vertices[cnt3].Position;

                        float distancePlayerPlane = CollisionDetection.get_distanceFromPlane(Position, tri.p1, tri.p2, tri.p3);
                        Microsoft.Xna.Framework.Vector3 centerOfTriangle = CollisionDetection.getCenterOfTriangle(tri);
                        float maxRadiusAroundTriangle = CollisionDetection.getMaxRadiusAroundTriangle(tri);
                        float distanceFromTriangleCenter = VectorMath.get_length(Microsoft.Xna.Framework.Vector3.Subtract(centerOfTriangle, Position));

                        //if (distancePlayerPlane < maxRadiusAroundTriangle && distanceFromTriangleCenter < maxRadiusAroundTriangle)
                        if (distanceFromTriangleCenter < maxRadiusAroundTriangle)
                        {
                            averagedNormal = (block.wallModel.vertices[cnt1].Normal + block.wallModel.vertices[cnt2].Normal + block.wallModel.vertices[cnt3].Normal) / 3;
                            ray.Position = Position;
                            ray.Direction = averagedNormal;

                            if (CollisionDetection.checkTriangleIntersect(block.wallModel.vertices[cnt1].Position, block.wallModel.vertices[cnt2].Position,
                                                                                         block.wallModel.vertices[cnt3].Position, ray) && distancePlayerPlane < 1)
                            {
                                float fac1 = VectorMath.getScalarProductBeforeCosinus(averagedNormal, transformedDirection);
                                float fac2 = VectorMath.getScalarProductBeforeCosinus(averagedNormal, transformedDirection2);
                                Microsoft.Xna.Framework.Vector3.Normalize(averagedNormal);

                                Position += (averagedNormal * ((left_stick_y_amount > 0) ? left_stick_y_amount : left_stick_y_amount * -1) * ((fac1 > 0) ? fac1 : fac1 * -1)) / (walkSpeed * 7f); // *((fac1 > 0) ? fac1 : (-1 * fac1));
                                Position += (averagedNormal * ((left_stick_x_amount > 0) ? left_stick_x_amount : left_stick_x_amount * -1) * ((fac2 > 0) ? fac2 : fac2 * -1)) / (walkSpeed * 7f); // *((fac2 > 0) ? fac1 : (-1 * fac2));
                            }
                        }
                        i = i + 3;
                    }
                }

                #endregion
            }
        }

        public override void Update(GameTime gameTime)
        {
            Movement();
            UpdateData();
            PerformWallCollision(ref blocks_in_class);
            base.Update(gameTime);
        }
    }

    public class CameraGameMenu : GameComponent
    {

        public Microsoft.Xna.Framework.Vector3 Position;
        private float cameraY, cameraX;
        public Microsoft.Xna.Framework.Vector3 floorOffset;
        public Microsoft.Xna.Framework.Matrix View;
        public Microsoft.Xna.Framework.Matrix Projection;
        public Microsoft.Xna.Framework.Vector3 LookAt;
        public Microsoft.Xna.Framework.Matrix RotationMatrix, RotationMatrix2;
        public float RotationSpeed;

        public float walkSpeed;

        private Microsoft.Xna.Framework.Vector3 transformedDirection;        

        public CameraGameMenu(Game game, Microsoft.Xna.Framework.Matrix projection, float rotateSpeed, float walkSpeedParam)
            : base(game)
        {
            RotationSpeed = rotateSpeed;
            LookAt = new Microsoft.Xna.Framework.Vector3(0, 0, 0);
            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position, LookAt, Microsoft.Xna.Framework.Vector3.Up);
            Projection = projection;
            Position = new Microsoft.Xna.Framework.Vector3(0, 0, 0);
            walkSpeed = walkSpeedParam;
            cameraY = 1.3f;
            cameraX = 0;
        }
        double funcCounter = 0;

        public void Movement()
        {
            Microsoft.Xna.Framework.Input.KeyboardState ks = Microsoft.Xna.Framework.Input.Keyboard.GetState();                        

            if (ks.IsKeyDown(Keys.J))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY + rotateAmount;

            }

            if (ks.IsKeyDown(Keys.L))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY - rotateAmount;
            }

            if (ks.IsKeyDown(Keys.I))
            {
                float rotateAmount = (RotationSpeed / 60f);
                cameraX = cameraX - rotateAmount;
            }
            if (ks.IsKeyDown(Keys.K))
            {
                float rotateAmount = (RotationSpeed / 60f);
                cameraX = cameraX + rotateAmount;
            }

            if (ks.IsKeyDown(Keys.U))
            {
                Position.X = Position.X + transformedDirection.X * walkSpeed;
                Position.Z = Position.Z + transformedDirection.Z * walkSpeed;
                //Console.WriteLine(Position);
            }

            if (ks.IsKeyDown(Keys.O))
            {
                Position.X = Position.X - transformedDirection.X * walkSpeed;
                Position.Z = Position.Z - transformedDirection.Z * walkSpeed;
            }

            //camera looks at this direction
            Microsoft.Xna.Framework.Vector3 cameraDirection = new Microsoft.Xna.Framework.Vector3(0, 0, -1);
            
            Position = new Microsoft.Xna.Framework.Vector3(12, 10, 7);
            
            if (funcCounter > 360)
            {
                funcCounter = 0;
            }
            funcCounter += 0.005d;

            cameraY = cameraY + Convert.ToSingle(Math.Sin(funcCounter) / 2000d);

            cameraX = cameraX + Convert.ToSingle((Math.Cos(funcCounter) / 5000d) + (Math.Sin(funcCounter) / 5000d));

            //Rotation matrix for the Y axis of the camear

            RotationMatrix = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY);
            RotationMatrix2 = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY + MathHelper.PiOver2); ;

            //Creates a vector which points towards the direction that the camera is facing now
            transformedDirection = Microsoft.Xna.Framework.Vector3.Transform(cameraDirection, RotationMatrix);
            
            Microsoft.Xna.Framework.Vector3 cameraLookAt = Position + transformedDirection;

            //Console.WriteLine(Position);

            //THIS IS WRONG, the camera Looks always a certain position, the position of the camera should
            //be the same as the look at, else it becomes a tracking camera!
            // View = Matrix.CreateLookAt(Position, transformedDirection, Vector3.Up);

            //IT IS IMPORTANT that the Lookat vector is always the same as the position itself, so the camera always rotates around a 0 axis, else the camera simply 
            //tracks the "lookat" and rotation wont work anymroe!

            //Right one here
            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position, cameraLookAt, Microsoft.Xna.Framework.Vector3.Up);

            if (cameraX > 359 || cameraX < -359)
            {
                cameraX = 0;
            }
            if (cameraY > 359 || cameraY < -359)
            {
                cameraY = 0;
            }
        }
        
        public override void Update(GameTime gameTime)
        {
            Movement();

            base.Update(gameTime);
        }
    }
    
}
