﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConexGameLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace ConexGameLibrary
{
    public class EffectSprite
    {
        //Sprite Properties
        public float SpriteTimer;
        private float FixedSpriteDuration;
        public float Startsize, Endsize, CurrentSize = 1;
        private float StepSize;

        public Vector3 Startposition;
        public Vector3 CameraPosition, MoveDirection, CurrentDirection;
        public float Movement_Speed;
        public Texture2D Texture;
        public Matrix SpriteMatrix;
        
        //Stuff for shader, like pointLight.
        public float Start_Intensity, End_Intensity, CurrentIntensity;
        private float StepIntensity;
        public Vector4 Color, startColor, EndColor;
        private Vector4 StepColor;

        public EffectVertex[] spritePlane;

        private void Create_SpritePlane()
        {
            spritePlane = new EffectVertex[6];

            spritePlane[0] = new EffectVertex(new Vector3(-1, -1, 0), Vector2.Zero);
            spritePlane[1] = new EffectVertex(new Vector3(1, -1, 0), new Vector2(1, 0));
            spritePlane[2] = new EffectVertex(new Vector3(1, 1, 0), new Vector2(1, 1));

            spritePlane[3] = new EffectVertex(new Vector3(-1, -1, 0), new Vector2(0, 0));
            spritePlane[4] = new EffectVertex(new Vector3(-1, 1, 0), new Vector2(0, 1));
            spritePlane[5] = new EffectVertex(new Vector3(1, 1, 0), new Vector2(1, 1));
        }

        /// <summary>
        /// Set the Sprite effect here, except for movement and PointLight data
        /// </summary>
        /// <param name="Sprite_position"></param>
        /// <param name="camPosition"></param>
        /// <param name="FrameRate"></param>
        public EffectSprite(Vector3 Sprite_position, Vector3 camPosition, int FrameRate, Texture2D tex)
        {
            SpriteTimer = FrameRate * 5;
            Startsize = 1; Endsize = 1;
            MoveDirection = Vector3.Zero;
            Movement_Speed = 0;
            this.Startposition = Sprite_position;
            this.CameraPosition = camPosition;

            Color = Vector4.Zero;
            Start_Intensity = 0; End_Intensity = 0;
            this.SpriteMatrix = Matrix.CreateWorld(Startposition, Vector3.Forward, Vector3.Up);
            this.Texture = tex;
            Create_SpritePlane();
        }

        /// <summary>
        /// Adds startsize, duration, endsize to it
        /// </summary>
        /// <param name="Sprite_position"></param>
        /// <param name="camPosition"></param>
        /// <param name="duration"></param>
        /// <param name="startsize"></param>
        /// <param name="endsize"></param>
        public EffectSprite(
            Vector3 Sprite_position,
            Vector3 camPosition,
            float duration,
            float startsize,
            float endsize,
             Texture2D tex
           )
        {
            this.Texture = tex;
            SpriteTimer = duration;
            Startsize = startsize; 
            Endsize = endsize;
            MoveDirection = Vector3.Zero;
            Movement_Speed = 0;
            this.CameraPosition = camPosition;
            this.Startposition = Sprite_position;

            Color = Vector4.Zero;
            Start_Intensity = 0; End_Intensity = 0;
            this.SpriteMatrix = Matrix.CreateWorld(Startposition, Vector3.Forward, Vector3.Up);
            Create_SpritePlane();
        }

        //Adds the whole bunch of properties to it.
        public EffectSprite(
            Vector3 Sprite_position,
            Vector3 camPosition,
            float duration,
            float startsize,
            float endsize,
            float start_Intensity,
            float end_Intensity,
            Vector4 startColor,
            Vector4 endColor,
            Vector3 moveDirection,
            float movementSpeed,
             Texture2D tex)
        {
            this.Texture = tex;
            SpriteTimer = duration;
            FixedSpriteDuration = duration;
            Startsize = startsize;
            Endsize = endsize;
            Start_Intensity = start_Intensity;
            End_Intensity = end_Intensity;
            this.startColor = startColor;
            this.EndColor = endColor;
            this.CameraPosition = camPosition;
            this.Startposition = Sprite_position;
            this.Movement_Speed = movementSpeed;
            this.SpriteMatrix = Matrix.CreateWorld(Startposition, Vector3.Forward, Vector3.Up);
            this.MoveDirection = moveDirection;
            this.StepColor = Vector4.Divide(Vector4.Subtract(endColor, startColor), FixedSpriteDuration);
            this.StepIntensity = (end_Intensity - start_Intensity) / FixedSpriteDuration;
            this.StepSize = (endsize - startsize) / FixedSpriteDuration;

            Create_SpritePlane();
        }

        //Must be updated in Draw, since the same sprite will render to different cameras
        public void UpdateSprite(Vector3 campos)
        {            
            CurrentSize = (FixedSpriteDuration - SpriteTimer) * StepSize + Startsize;
            CurrentIntensity = (FixedSpriteDuration - SpriteTimer) * StepIntensity + Start_Intensity;
            Color = (FixedSpriteDuration - SpriteTimer) * StepColor + startColor;
            Startposition += (FixedSpriteDuration - SpriteTimer) * MoveDirection;

            if (SpriteTimer > 0)
                SpriteTimer--;


            SpriteMatrix = Matrix.CreateScale(VectorMath.get_length(Vector3.Subtract(campos, Startposition)/5) * CurrentSize) *
                Matrix.CreateWorld(Startposition, Vector3.Normalize(Vector3.Subtract(campos, Startposition)), Vector3.Up);
        }               
    }

    public class EffectSprite_Container
    {
        public List<EffectSprite> Sprites;
        public List<Texture2D> SpriteTextures;
    
        public EffectSprite_Container()
        {
            Sprites = new List<EffectSprite>();
            SpriteTextures = new List<Texture2D>();
            
        }
    }


    public class EffectModel
    {
        //Sprite Properties
        public float Timer;
        private float FixedDuration;
        public float Startsize, Endsize, CurrentSize = 1;
        private float StepSize;

        public Vector3 Position;
        public Vector3 Rotation;
        public Matrix MeshMatrix;

        //Stuff for shader, like pointLight.
        public MeshGroup EffectMesh;
                
        /// <summary>
        /// Set timer to -1 for no change live-time
        /// </summary>
        /// <param name="Sprite_position"></param>
        /// <param name="camPosition"></param>
        /// <param name="FrameRate"></param>
        public EffectModel(Vector3 position, Vector3 rot, float startsize, float endsize, MeshGroup mesh)
        {
            this.Rotation = rot;
            this.Position = position;
            this.Startsize = startsize;
            this.Endsize = endsize;            
            this.FixedDuration = Timer;
            this.EffectMesh = mesh;
        }
        
        public void UpdateSprite()
        {
            CurrentSize = (FixedDuration - Timer) * StepSize + Startsize;
           
            MeshMatrix = 
                Matrix.CreateScale(CurrentSize) * 
                Matrix.CreateRotationX(Rotation.X) *
                Matrix.CreateRotationY(Rotation.Y) * 
                Matrix.CreateRotationZ(Rotation.Z) * 
                Matrix.CreateTranslation(Position);
        }

    }

}
