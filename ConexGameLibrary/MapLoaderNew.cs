﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using System.Globalization;
using System.IO;

namespace ConexGameLibrary
{
    #region MapSplitter Class
    /// <summary>
    /// DO NOT USE DIRECTLY USE MAPLOADERNEW!!!!!!!!!!!!!!    /// 
    /// </summary>
    class MapSplitter
    {
        public ModelBlockFloor extractFloorsFromMeshGroup(MeshGroup meshgroup)
        {
            int counter = 0;

            List<CustomVertex> floorVertices = new List<CustomVertex>();


            while (counter < meshgroup.vertices.Length)
            {
                //Make 3 counter variables to take a triangle per loop
                int cnt1, cnt2, cnt3;
                cnt1 = counter;
                cnt2 = counter + 1;
                cnt3 = counter + 2;

                if (isFloor(meshgroup.vertices[cnt1].Position, meshgroup.vertices[cnt2].Position, meshgroup.vertices[cnt3].Position))
                {
                    floorVertices.Add(meshgroup.vertices[cnt1]);
                    floorVertices.Add(meshgroup.vertices[cnt2]);
                    floorVertices.Add(meshgroup.vertices[cnt3]);

                }

                counter = counter + 3;
            }

            CustomVertex[] floorVerticesAsArray = new CustomVertex[floorVertices.Count];

            for (int i = 0; i < floorVertices.Count; i++)
            {
                floorVerticesAsArray[i] = floorVertices[i];
            }

            var floorBlock = new ModelBlockFloor();
            floorBlock.vertices = floorVerticesAsArray;

            floorBlock.material = meshgroup.material;
            floorBlock.group = meshgroup.GroupName;

            return floorBlock;
        }

        public ModelBlockWall extractWallsFromMeshGroup(MeshGroup meshgroup)
        {
            int counter = 0;

            List<CustomVertex> wallVertices = new List<CustomVertex>();


            while (counter < meshgroup.vertices.Length)
            {
                //Make 3 counter variables to take a triangle per loop
                int cnt1, cnt2, cnt3;
                cnt1 = counter;
                cnt2 = counter + 1;
                cnt3 = counter + 2;

                if (isWall(meshgroup.vertices[cnt1].Position, meshgroup.vertices[cnt2].Position, meshgroup.vertices[cnt3].Position))
                {
                    wallVertices.Add(meshgroup.vertices[cnt1]);
                    wallVertices.Add(meshgroup.vertices[cnt2]);
                    wallVertices.Add(meshgroup.vertices[cnt3]);

                }

                counter = counter + 3;
            }

            CustomVertex[] wallVerticesAsArray = new CustomVertex[wallVertices.Count];

            for (int i = 0; i < wallVertices.Count; i++)
            {
                wallVerticesAsArray[i] = wallVertices[i];
            }

            var wallBlock = new ModelBlockWall();
            wallBlock.vertices = wallVerticesAsArray;

            wallBlock.material = meshgroup.material;
            wallBlock.group = meshgroup.GroupName;

            return wallBlock;
        }

        bool isFloor(Vector3 vec1, Vector3 vec2, Vector3 vec3)
        {
            Vector3 cross;

            Vector3 b = new Vector3(vec1.X - vec2.X, vec1.Y - vec2.Y, vec1.Z - vec2.Z);
            Vector3 a = new Vector3(vec1.X - vec3.X, vec1.Y - vec3.Y, vec1.Z - vec3.Z);

            cross.X = ((a.Y * b.Z) - (a.Z * b.Y));
            cross.Y = ((a.Z * b.X) - (a.X * b.Z));
            cross.Z = ((a.X * b.Y) - (a.Y * b.X));


            if (cross.Y != 0.0f)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool isWall(Vector3 vec1, Vector3 vec2, Vector3 vec3)
        {
            Vector3 cross;

            Vector3 b = new Vector3(vec1.X - vec2.X, vec1.Y - vec2.Y, vec1.Z - vec2.Z);
            Vector3 a = new Vector3(vec1.X - vec3.X, vec1.Y - vec3.Y, vec1.Z - vec3.Z);

            cross.X = ((a.Y * b.Z) - (a.Z * b.Y));
            cross.Y = ((a.Z * b.X) - (a.X * b.Z));
            cross.Z = ((a.X * b.Y) - (a.Y * b.X));
            decimal x, y, z;

            x = Math.Round(Convert.ToDecimal(cross.X), 1);  
            y = Math.Round(Convert.ToDecimal(cross.Y), 1);
            z = Math.Round(Convert.ToDecimal(cross.Z), 1);

            if (Convert.ToSingle(y) == 0.0f)
            {
                return true;
            }
            else
                return false;
        }
    }
    #endregion

    #region MapExtractor Class
    /// <summary>
    /// DO NOT USE DIRECTLY USE MAPLOADERNEW!!!!!!!!!!!!!!
    /// This class is entirely used to extract Modelgroups or meshes, which can have one shader
    /// Creating a scene with say, 10 models... this function will return 10 meshGroups with their shader.
    /// This MapExtractor shall be used to extract maps as in nonphysical models, however using this MapExtractor
    /// With another class called MapSplitter,  Giving the MapSpliter class the meshGroups, will return splitMeshes
    /// , Split in walls and floors !!!!!
    /// </summary>
    public class MapExtractor
    {
        private List<Vector3> vec3List;
        private List<Vector2> uvList;
        private List<Vector3> vnList;
        private List<String> vec3ListString;
        private List<String> uvListString;
        private List<String> vnListString;
        private List<String> FaceGroupMtlStringList;

        private List<BlockAsString> blocksAsString;

        private String gamePath;

        //THose functions are later summarized as mapExtract() 
        #region Later call all the things with one function, not now!

        public MapExtractor(String gamePath)
        {
            this.gamePath = gamePath;
        }

        /// <summary>
        /// This function gets reads all lines from the obj file and returns them
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public string[] GetLinesFromObj(string filePath)
        {
            return File.ReadAllLines(gamePath + "//" + filePath);
        }

        /// <summary>
        /// This function only extracts the mtlLib filename 
        /// </summary>
        /// <param name="WholeLines"></param>
        /// <returns></returns>
        public string getMtlFileName(string[] WholeLines)
        {
            string mtlFileName = string.Empty;
            foreach (string line in WholeLines)
            {
                if (line.StartsWith("mtllib "))
                {
                    mtlFileName = line.Substring(7);
                }
                else
                {
                    mtlFileName = "No MatLib found";
                }
                break;
            }
            return mtlFileName;
        }

        /// <summary>
        /// THis function will only remove comments, and anything else thats not an f,v, vt, vn, g or usemtl, because we dont need them
        /// in the actual mesh data.
        /// </summary>
        ///     
        public string[] filterRelevantMapData(string[] unfilteredLines)
        {
            List<string> filteredLines = new List<String>();

            //Leave out all lines without the relevant beginning letter and add the line to the filteredString list
            foreach (String line in unfilteredLines)
            {
                if (line.StartsWith("g") || line.StartsWith("v") || line.StartsWith("vt") || line.StartsWith("vn") || line.StartsWith("usemtl") || line.StartsWith("f"))
                {
                    filteredLines.Add(line);
                }
            }

            //Fill an Array of the same size as the dynamic List, using a loop
            string[] filteredLinesArrayForm = new string[filteredLines.Count];

            for (int i = 0; i < filteredLines.Count; i++)
            {
                filteredLinesArrayForm[i] = filteredLines[i];
            }

            //Now we can succefully return the natural Array to the call function, of course we clear the List again because of memory reasons
            filteredLines = null;

            return filteredLinesArrayForm;
        }

        /// <summary>
        /// Create Dynamic Lists of v , vn, vt ..  Because f refers to all v's in the obj file, that means we have to use lists of the entire v, vn and vt's ,
        /// else the face wont adress correctly. THE FIRST ELEMENT OF THE LIST WILL BE EMPTY IN THE 3 LISTS
        /// Thats because the faces adress the v, vt and vn starting by 1, not by 0... 
        /// Example, All v go into a big list, the faces adress the specific element of that list, that makes up their face
        /// THIS WILL CREATE STRING LISTS FOR THESE DATAS, not the actual vector3 or vector2 Lists..
        /// It will also return a string list consisting of faces, groups and mtl lines;
        /// </summary>
        /// <param name="filteredData"></param>
        /// <returns></returns>
        public List<String> CreateStringVersionForDataListsAndReturnFaceGroupAndMtl(string[] filteredData)
        {
            List<string> list = new List<string>(filteredData);

            //Initialise the lists before filling them
            vec3ListString = new List<String>();
            uvListString = new List<String>();
            vnListString = new List<String>();
            FaceGroupMtlStringList = new List<String>();


            //Loop throught the entire list of filtered lines and seperate v, vt and vn into their right string version of list
            foreach (string line in list)
            {
                if (line.StartsWith("v "))
                {
                    vec3ListString.Add(line);
                }

                if (line.StartsWith("vt "))
                {
                    uvListString.Add(line);
                }

                if (line.StartsWith("vn "))
                {
                    vnListString.Add(line);
                }

                //Add group to faceGroupMtllist, note, we dont need an offset here, since
                //we dont enumerate throught this list.
                if (line.StartsWith("g "))
                {
                    FaceGroupMtlStringList.Add(line);
                }
                if (line.StartsWith("usemtl "))
                {
                    FaceGroupMtlStringList.Add(line);
                }

                if (line.StartsWith("f "))
                {
                    FaceGroupMtlStringList.Add(line);
                }
            }

            //The String version of v, vt, and vn are filled, only return the face,group and usemtl lines because
            //this will be used to create the Blocks
            return FaceGroupMtlStringList;
        }

        /// <summary>
        /// The function takes in the list of groups, faces and usemtl lines only and seperates them in blockAsString and returns the 
        /// list of BlockAsString..
        /// THis function seperates each model/textured Mesh in its own group basicly
        /// </summary>
        /// 
        public List<BlockAsString> generateBlocksOfStrings(List<String> faceGroupUseMtlStringList)
        {
            Int32 BlockListSize = 0;

            //First we want to know how many models/mesh or Blocks we get, since each block 
            //always has 2 group identifiers, we can count them and divide the result by 2

            #region Loop to get the Group Count
            foreach (string singleLine in faceGroupUseMtlStringList)
            {
                if (singleLine.StartsWith("g "))
                {
                    BlockListSize++;
                }
            }
            #endregion

            //Get the actual amount of blocks we need
            BlockListSize = BlockListSize / 2;

            //Create the list
            blocksAsString = new List<BlockAsString>();

            #region initialises the classes in the BlockAsStringList

            int internalForeachCounter = 0;
            while (internalForeachCounter < BlockListSize)
            {
                blocksAsString.Add(new BlockAsString());

                blocksAsString[internalForeachCounter] = new BlockAsString();
                internalForeachCounter++;
            }

            #endregion Now I can fill them since the Object and the inner List is initialised

            #region Hide this for friendlyness

            //We start by our first list element, 0
            int currentBlock = 0;

            //Loop throught every line!
            for (int i = 0; i < faceGroupUseMtlStringList.Count; i++)
            {
                //This variable looks 1 element further in the list and sees if an g comes after an f 
                //In that case, the first block will be closed!


                //Peak is needed to look one line ahead, see if there is a f line before an g line, this means a new mesh/model is coming -> next block
                int i_Peak = i;
                i_Peak++;


                //Check if the lines contain any f, g or usemtl, the data we need!
                if ((faceGroupUseMtlStringList[i].StartsWith("g ") || faceGroupUseMtlStringList[i].StartsWith("usemtl ") || faceGroupUseMtlStringList[i].StartsWith("f ")))
                {
                    //Add that line to our current block
                    blocksAsString[currentBlock].BlockLines.Add(faceGroupUseMtlStringList[i]);

                    //Check if the peak is still smaller than the actual size of the list, because that ensures that we dont go out of range, if we are in range, do this..
                    if (i_Peak < faceGroupUseMtlStringList.Count)
                    {
                        //And if we are in range, this means the f is not the last line, so we can perform the f/ g check and create a new block
                        if (faceGroupUseMtlStringList[i].Contains("f ") && faceGroupUseMtlStringList[i_Peak].Contains("g "))
                        {
                            currentBlock++;
                        }
                    }
                    else
                        break;
                }

            }

            #endregion

            return blocksAsString;
        }

        /// <summary>
        /// This function takes the string version of v, vt, vn and takes apart the line, turns it into its right datatype and assigns it
        /// To the real lists
        /// </summary>                

        public void turnStringListsIntoRealDatasLists()
        {
            PointStringListToPointList(vec3ListString);
            UvStringListToUvList(uvListString);
            VnStringListToVnList(vnListString);
        }
        /// <summary>
        /// We Only treat a single Block as String here, we later Loop this function to do others
        /// This takes the "block as string" consisting of face, usemtl and group
        /// and turn it into an actual MeshGroup  or Model, this function is called for every BlockAsString
        /// In a list
        /// </summary>
        /// <param name="blockAsStringSingle"></param>
        /// 

        //Run blockAsStringToGroupMesh in a loop, so i dont have to do it in manually
        public List<MeshGroup> blockAsStringToGroupMeshList(List<BlockAsString> blocks)
        {
            List<MeshGroup> meshList = new List<MeshGroup>();

            //Loop throught each element and call "blockAsStringToGroupMesh to convert it into the GroupMeshClass
            foreach (BlockAsString block in blocks)
            {
                meshList.Add(BlockAsStringToGroupMesh(block));
            }

            return meshList;
        }

        //Just modifies the meshgroup which is parsed as   and originally exists on the class MapExtractorNew
        public void assignMaterialAndTexturePath(  List<MeshGroup> meshGroups, String mtl_file_path)
        {
            String[] matLines = File.ReadAllLines(mtl_file_path);

            //Create an Array Of Materials, correspondin to the amount of materials in the file

            int materialCount = 0;
            foreach (String line in matLines)
            {
                if (line.StartsWith("newmtl "))
                {
                    materialCount++;
                }
            }
            Material[] materials = new Material[materialCount];

            //Count the absolute amount of textureNames and materialNames
            string[] matAndKd;
            int matAndKdCounter = 0;
            foreach (String line in matLines)
            {
                if (line.StartsWith("newmtl "))
                {
                    matAndKdCounter++;
                }
                if (line.StartsWith("map_Kd "))
                {
                    matAndKdCounter++;
                }
                if (line.StartsWith("bump "))
                {
                    matAndKdCounter++;
                }
                if (line.StartsWith("map_Ks "))
                {
                    matAndKdCounter++;
                }
            }
            
            //Create a string array, containing the newmtl and map_kd information
            matAndKd = new String[matAndKdCounter];

            //Fills the newMtl and map_kd lines in a row, into the array matAndKd
            int fill_matAndKd_index = 0;
            foreach (string line in matLines)
            {
                if (line.StartsWith("newmtl ") || (line.StartsWith("map_Kd ")) || (line.StartsWith("map_Ks ")) || (line.StartsWith("bump ")))
                {
                  
                    matAndKd[fill_matAndKd_index] = line;
                    fill_matAndKd_index++;
                }
            }

            for (int i = 0; i < materials.Length; i++)
            {
                materials[i] = new Material();
            }

            //Now the first newmtl line goes into the first Material, also the map_kd line goes into the first material,
            //once we have our next newmtl, we go to the next Material, the Material must not contain a textureName

            int put_newmtl_and_kd_into_Material_counter = -1;
            foreach (string line in matAndKd)
            {
                if (line.StartsWith("newmtl "))
                {
                    
                    put_newmtl_and_kd_into_Material_counter++;
                    materials[put_newmtl_and_kd_into_Material_counter].shaderName = line.Substring(7);
                }
                if (line.StartsWith("map_Kd "))
                {
                    materials[put_newmtl_and_kd_into_Material_counter].texturePath = line.Substring(7);
                    
                }

                if (line.StartsWith("bump "))
                {
                    materials[put_newmtl_and_kd_into_Material_counter].bumppath = line.Substring(5);
                    string[] line_segmented = new string[3];
                    line_segmented = materials[put_newmtl_and_kd_into_Material_counter].bumppath.Split(' ');
                    materials[put_newmtl_and_kd_into_Material_counter].bumppath = line_segmented[0];
                    Single.TryParse(line_segmented[2], NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat,out materials[put_newmtl_and_kd_into_Material_counter].bumpfactor);
                }

                if (line.StartsWith("map_Ks "))
                {
                    materials[put_newmtl_and_kd_into_Material_counter].specularpath = line.Substring(7);
                }
            }
            

            //Loop throught meshgroups and compare each shader from meshgroup, to the new material Lists shader name
            //So we can assign the right material to the right meshgroup, since there can be more meshgroups than materials
            //Double use

            int testcount = 0;
            foreach (MeshGroup mesh in meshGroups)
            {
                foreach (Material mat in materials)
                {
                    if (mesh.material.shaderName == mat.shaderName)
                    {
                        testcount++;
                        mesh.material.texturePath = mat.texturePath;
                        mesh.material.bumppath = mat.bumppath;
                        mesh.material.specularpath = mat.specularpath;

                        if (mesh.material.texturePath != null &&( mesh.material.texturePath.Contains(".png") || mesh.material.texturePath.Contains(".tga")))
                        {                            
                            mesh.transparencyFlag = true;
                        }
                        
                    }

                    
                }
            }

        }

        #region BlockAsString to a Single Group Mesh, contains vertices mtl and group name already!!!

        public MeshGroup BlockAsStringToGroupMesh(BlockAsString blockAsStringSingle)
        {
            string groupName = string.Empty;
            string useMaterial = string.Empty;
            String[] faceLines;
            int faceLineCounter = 0;

            #region copy group, mtl name and count the amount of f's here

            //First of all, we get our group Names out of the ( face,Mtl,group ) list
            foreach (String line in blockAsStringSingle.BlockLines)
            {
                #region get Group and Material
                //If the line is a group, and does NOT contain default as name, then copy it!
                if (line.StartsWith("g ") && !line.Contains("default"))
                {
                    groupName = line.Substring(2);
                }

                //get the materialName
                if (line.StartsWith("usemtl "))
                {
                    useMaterial = line.Substring(7);
                }
                #endregion

                //simply counts the amount of faces for array Size
                if (line.StartsWith("f "))
                {
                    faceLineCounter++;
                }
            }

            #endregion

            //after counting the amount of facelines, we can make an array to that size
            faceLines = new String[faceLineCounter];

            #region turn the facelines into lines without the "f "  so they can be used
            int iteratorFaces = 0;
            //Now we copy the face Lines without the "f " to the faceline array
            foreach (String line in blockAsStringSingle.BlockLines)
            {
                //If the line starts with f, add it to the facelist and remove the "f "
                if (line.StartsWith("f "))
                {
                    //check that the iterator cant go out of range, not really needed IMO, but it must work ...
                    if (iteratorFaces < faceLines.Length)
                    {
                        faceLines[iteratorFaces] = line.Substring(2);
                        iteratorFaces++;
                    }
                }
            }
            #endregion
            // At this Point, I have my group name, my material Name... and a List of the face Lines
            //I have to seperate the Face Lines now, and create Triangles from them!

            #region Make the actual Vertices from one Line

            //Turn a faceline into 3 vertex Aka triangle
            List<CustomVertex> vertices = new List<CustomVertex>();
            foreach (String faceline in faceLines)
            {
                //Split the current faceline into an array of seperated Lines
                // 1/1/1 2/2/2 3/3/3  -> to    1/1/1        2/2/2         3/3/3

                string[] seperatedFaceLine = SplitFaceLine(faceline);

                //let the function make the vertex and assign them to a list of vertices

                vertices.Add(makeVertex(seperatedFaceLine[0]));
                vertices.Add(makeVertex(seperatedFaceLine[1]));
                vertices.Add(makeVertex(seperatedFaceLine[2]));
                //This list of vertices basicly goes to the Meshgroup. vertexlist, since its the Model/mesh
                //For one group 
            }
            // Now with our groupname, usemtl name, and our list of vertices, we can create the meshgroup object and return it to the caller

            #endregion

            #region Create the MeshGroup and fill it, then return it

            MeshGroup meshGroup = new MeshGroup();
            meshGroup.GroupName = groupName;
            meshGroup.vertices = vertices.ToArray();
            meshGroup.material = new Material();
            meshGroup.material.shaderName = useMaterial;
            
            // ASSIGN MATERIAL IN A DIFFERENT STEP!!!!                   

            //now our meshGroup has its vertices... basicly the mesh is applied, Now we return our neatly created Meshgroup

            return meshGroup;

            #endregion
        }
        #endregion

        // the both following functions are called during "turnStringListsIntoRealDatasLists()" above!!!!,
        // they make the vertex simply
        #region

        /// <summary>
        /// Takes 1 faceline and splits it into each vertex declaration
        /// </summary>
        /// <param name="faceLine"></param>
        /// <returns></returns>
        /// 
        private string[] SplitFaceLine(string faceLine)
        {
            string[] SplitVersionOfFaceLine = new string[3];
            SplitVersionOfFaceLine = faceLine.Split(' ');

            return SplitVersionOfFaceLine;
        }

        /// <summary>
        /// Give it a string that only describes 1 vertex, basicly a third of the line,  and let it create the vertex, call this function 3 times per line to get a triangle!
        /// </summary>
        /// <param name="trippleOfFaceLine"></param>
        /// <returns></returns>
        /// 
        public CustomVertex makeVertex(string trippleOfFaceLine)
        {
            // turn this  1/1/1
            //into  an array of each number

            //Get the indices of a single vertex
            string[] splitVertexString;
            int[] splitVertex = new int[3];
            splitVertexString = trippleOfFaceLine.Split('/');

            splitVertex[0] = Convert.ToInt32(splitVertexString[0]); //Gives the vec3 Point for the vertex
            splitVertex[1] = Convert.ToInt32(splitVertexString[1]); //Gives the uv coordinate for the vertex
            splitVertex[2] = Convert.ToInt32(splitVertexString[2]); //Gives the vector normal for that vertex

            // if our Third of a face -> a Vertex is made of 1/1/1, our first element in the vec3 list is our point
            // then our first element in the uv List is our uv,   first element in our vn list is our vn!
            //our indices come from the splitVertex elements
           // Console.WriteLine(vec3List[splitVertex[0]] + "    "+  uvList[splitVertex[1]] + "    " + vnList[splitVertex[2]]);
            Vector3 tangent = vnList[splitVertex[2]];
            Vector3 x = Vector3.UnitX; Vector3 y = Vector3.UnitY; Vector3 z = Vector3.UnitZ;

            CustomVertex customvertex = new CustomVertex(vec3List[splitVertex[0]], uvList[splitVertex[1]], vnList[splitVertex[2]], vnList[splitVertex[2]], vnList[splitVertex[2]]);

            return customvertex;
        }

        #endregion

        #region String List to Datatype List  is done with these functions: The Lists in MapExtractor class are filled.
        // Those functions will be called from another function, to make the code cleaner
        /// <summary>
        /// Take the list of point-lines, and convert them into an actual list of Points(Vector3's)
        /// </summary>
        /// <param name="ListOfStringPoints"></param>
        /// <returns></returns>
        public void PointStringListToPointList(List<String> ListOfStringPoints)
        {
            vec3List = new List<Vector3>();
            //Add an empty vector, because our faces tell us the index, where we will find our vertexdata, but that list begins by 1, not by 0    
            vec3List.Add(Vector3.Zero);


            /*
            for (Int32 index = 0; index < ListOfStringPoints.Count; index++)
            {
                //  I can directly assign to the dynamic vec3 lIst since the list is part of the class
                vec3List.Add(LineToPoint(ListOfStringPoints[index])); //We dont need return object for this method                        
            }
            */
            foreach (String line in ListOfStringPoints)
            {
                vec3List.Add(LineToPoint(line));
            }

        }


        /// <summary>
        /// Take the list of uv-lines, and convert them into an actual list of uv Vector2's
        /// </summary>
        /// <param name="ListOfStringPoints"></param>
        /// <returns></returns>
        public void UvStringListToUvList(List<String> ListOfStringUv)
        {
            uvList = new List<Vector2>();
            //Add an empty vector, because our faces tell us the index, where we will find our vertexdata, but that list begins by 1, not by 0    

            uvList.Add(Vector2.Zero);

            for (Int32 index = 0; index < ListOfStringUv.Count; index++)
            {
                //  I can directly assign to the dynamic vec3 lIst since the list is part of the class
                uvList.Add(LineToUv(ListOfStringUv[index])); //We dont need return object for this method                        
            }
        }

        /// <summary>
        /// Take the list of Vn Lines, and convert them into an actual list of Vn's(Vector3's)
        /// </summary>
        /// <param name="ListOfStringPoints"></param>
        /// <returns></returns>
        public void VnStringListToVnList(List<String> ListOfStringVn)
        {
            vnList = new List<Vector3>();
            //Add an empty vector, because our faces tell us the index, where we will find our vertexdata, but that list begins by 1, not by 0    
            vnList.Add(Vector3.Zero);

            for (Int32 index = 0; index < ListOfStringVn.Count; index++)
            {
                //  I can directly assign to the dynamic vec3 lIst since the list is part of the class
                vnList.Add(LineToVn(vnListString[index])); //We dont need return object for this method                        
            }
        }


        /// <summary>
        /// This function is used in PointStringListToPointList to extract a single vector3 , will be used in a loop on PointStringListToPointList
        /// </summary>
        /// <param name="pointLine"></param>
        /// <returns></returns>
        private Vector3 LineToPoint(String pointLine)
        {
            //We have 3 Coordinates in a Vector3, this float array will be filled with the seperated data
            float[] singleDatas = new float[3];

            //from  v 1.00 2.00 2.00  to   1.00 2.00 2.00    The "v "  is removed, so we can seperate it more easily
            string lineCut = pointLine.Substring(2);

            //When we split the cuted line, we have to write each number into a string array for now
            string[] seperatedData = new string[3];

            //Assign the split line to an string array
            seperatedData = lineCut.Split(' ');

            //Convert the numbers in string format, to float format so we can create the Vector3
            Single.TryParse(seperatedData[0], NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out singleDatas[0]);
            Single.TryParse(seperatedData[1], NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out singleDatas[1]);
            Single.TryParse(seperatedData[2], NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out singleDatas[2]);

            seperatedData = null;
            lineCut = null;

            return new Vector3(singleDatas[0], singleDatas[1], singleDatas[2]);
        }

        /// <summary>
        /// This function is used in UvStringListToUvList to turn the line with the uv information into vector2 data and fill the uv list
        /// </summary>
        /// <param name="pointLine"></param>
        /// <returns></returns>
        private Vector2 LineToUv(String uvLine)
        {
            //We have 3 Coordinates in a Vector3, this float array will be filled with the seperated data
            float[] singleDatas = new float[2];

            //from  v 1.00 2.00 2.00  to   1.00 2.00 2.00    The "v "  is removed, so we can seperate it more easily
            string lineCut = uvLine.Substring(3);

            //When we split the cuted line, we have to write each number into a string array for now
            string[] seperatedData = new string[2];

            //Assign the split line to an string array
            seperatedData = lineCut.Split(' ');


            //Convert the numbers in string format, to float format so we can create the Vector3
            Single.TryParse(seperatedData[0], NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out singleDatas[0]);
            Single.TryParse(seperatedData[1], NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out singleDatas[1]);

            seperatedData = null;
            lineCut = null;

            return new Vector2(singleDatas[0], singleDatas[1]);
        }

        /// <summary>
        /// This function is used in VnStringListToVnList to extract a single vector3 , will be used in a loop on VnStringListToVnList
        /// </summary>
        /// <param name="pointLine"></param>
        /// <returns></returns>
        private Vector3 LineToVn(String VnLine)
        {
            //We have 3 Coordinates in a Vector3, this float array will be filled with the seperated data
            float[] singleDatas = new float[3];

            //from  v 1.00 2.00 2.00  to   1.00 2.00 2.00    The "v "  is removed, so we can seperate it more easily
            string lineCut = VnLine.Substring(3);

            //When we split the cuted line, we have to write each number into a string array for now
            string[] seperatedData = new string[3];

            seperatedData = lineCut.Split(' ');


            //Convert the numbers in string format, to float format so we can create the Vector3 (( Use try parse for the right format conversion
            Single.TryParse(seperatedData[0], NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out singleDatas[0]);
            Single.TryParse(seperatedData[1], NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out singleDatas[1]);
            Single.TryParse(seperatedData[2], NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out singleDatas[2]);

            seperatedData = null;
            lineCut = null;

            return new Vector3(singleDatas[0], singleDatas[1], singleDatas[2]);
        }
        // This block of functions is only turning the lines of v, vn and uv into actual lists of the right datatype!
        #endregion
        #endregion
    }
    #endregion

    #region MapLoaderNew class
    public class MapLoaderNew
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gamePath">This is the Path to the game Folder itself</param>
        /// <param name="mapPathAndName">Here enter the MapName and Path, starting inside the gameFolder
        /// for example, ... map01 if the map is inside the game folder, or  maps//map01  if inside a map folder
        /// Dont add extention!</param>
        /// <returns></returns>
        public List<MeshGroup> getMeshGroups(string gamePath, string mapPathAndName)
        {
            String[] Lines;
            MapExtractor mapextractor = new MapExtractor(gamePath + "//");
            Lines = mapextractor.GetLinesFromObj(mapPathAndName + ".obj");
            String MtlName = mapextractor.getMtlFileName(Lines);
            String[] Lines2 = mapextractor.filterRelevantMapData(Lines);
            List<String> groupMtlAndFace = mapextractor.CreateStringVersionForDataListsAndReturnFaceGroupAndMtl(Lines2);
            List<BlockAsString> blocksAsString = mapextractor.generateBlocksOfStrings(groupMtlAndFace);
            mapextractor.turnStringListsIntoRealDatasLists();
            List<MeshGroup> meshGroups = new List<MeshGroup>();
            meshGroups = mapextractor.blockAsStringToGroupMeshList(blocksAsString);

            for (int i = 0; i < meshGroups.Count; i++)
            {
                float radius = 0;
                Vector3 center = Vector3.Zero;
                for (int j = 0; j < meshGroups[i].vertices.Length; j++)
                {
                    center += meshGroups[i].vertices[j].Position;
                }
                center /= meshGroups[i].vertices.Length;

                for (int j = 0; j < meshGroups[i].vertices.Length; j++)
                {
                    float len =VectorMath.get_length(Vector3.Subtract(center, meshGroups[i].vertices[j].Position));
                    if (len > radius)
                        radius = len;
                }
                meshGroups[i].MaxRadius = radius;
                meshGroups[i].Position = center;

                for (int j = 0; j < meshGroups[i].vertices.Length; )
                {
                    meshGroups[i].vertices[j].vertexBinormal = Vector3.Subtract(meshGroups[i].vertices[j].Position, meshGroups[i].vertices[j + 1].Position);
                    meshGroups[i].vertices[j + 1].vertexBinormal = Vector3.Subtract(meshGroups[i].vertices[j].Position, meshGroups[i].vertices[j + 1].Position);
                    meshGroups[i].vertices[j + 2].vertexBinormal = Vector3.Subtract(meshGroups[i].vertices[j].Position, meshGroups[i].vertices[j + 1].Position);

                    meshGroups[i].vertices[j].vertexTangent = Vector3.Subtract(meshGroups[i].vertices[j].Position, meshGroups[i].vertices[j + 2].Position);
                    meshGroups[i].vertices[j + 1].vertexTangent = Vector3.Subtract(meshGroups[i].vertices[j].Position, meshGroups[i].vertices[j + 2].Position);
                    meshGroups[i].vertices[j + 2].vertexTangent = Vector3.Subtract(meshGroups[i].vertices[j].Position, meshGroups[i].vertices[j + 2].Position);

                    j += 3;
                }

            }

            //material step comes here
            mapextractor.assignMaterialAndTexturePath(  meshGroups, gamePath + "//" + mapPathAndName + ".mtl");

            
            return meshGroups;
        }

        /// <summary>
        /// Returns a list of blocks, each block being a divided in floor and wall polygons
        /// </summary>
        /// <param name="mapPath"></param>
        /// <returns></returns>
        public List<Block> get_SplitMapMeshesAsBlock(string gamePath, string mapPathAndName)
        {
            String[] Lines;
            MapExtractor mapextractor = new MapExtractor(gamePath + "//");
            Lines = mapextractor.GetLinesFromObj(mapPathAndName + ".obj");
            String MtlName = mapextractor.getMtlFileName(Lines);
            String[] Lines2 = mapextractor.filterRelevantMapData(Lines);
            List<String> groupMtlAndFace = mapextractor.CreateStringVersionForDataListsAndReturnFaceGroupAndMtl(Lines2);
            List<BlockAsString> blocksAsString = mapextractor.generateBlocksOfStrings(groupMtlAndFace);
            mapextractor.turnStringListsIntoRealDatasLists();
            List<MeshGroup> meshGroups = new List<MeshGroup>();
            meshGroups = mapextractor.blockAsStringToGroupMeshList(blocksAsString);
            //material step comes here
            mapextractor.assignMaterialAndTexturePath(  meshGroups, gamePath + "//" + mapPathAndName + ".mtl");

            MapSplitter splitter = new MapSplitter();

            List<Block> blocks = new List<Block>();

            foreach (MeshGroup meshGroup in meshGroups)
            {
                Block block = new Block();
                block.group = meshGroup.GroupName;
                block.floorModel = splitter.extractFloorsFromMeshGroup(meshGroup);
                block.wallModel = splitter.extractWallsFromMeshGroup(meshGroup);
                //Go throught every vertex 
                Vector3 Position_floor = new Vector3();
                Vector3 Position_wall = new Vector3();
                Single Radius_floor = 0, Radius_wall = 0;

                //For the Floor Mesh
                #region Floor Calculation
                for (int i = 0; i < block.floorModel.vertices.Length; i++)
                {
                    Position_floor = Vector3.Add(Position_floor, block.floorModel.vertices[i].Position);
                }
                Position_floor = Vector3.Divide(Position_floor, (float)block.floorModel.vertices.Length);

                for (int i = 0; i < block.floorModel.vertices.Length; i++)
                {
                    float newRadius = VectorMath.get_length(Vector3.Subtract(Position_floor,block.floorModel.vertices[i].Position));
                    
                    if (newRadius > Radius_floor)
                        Radius_floor = newRadius;
                }
                block.floorModel.RadiusFromCenter = Radius_floor;
                block.floorModel.CenterPosition = Position_floor;
                #endregion


                block.floorModel.triangleCenters = new Vector3[block.floorModel.vertices.Length/3];
                block.floorModel.maxTriangleRadius = new float[block.floorModel.vertices.Length/3];
                for (int i = 0, j = 0; i < block.floorModel.vertices.Length; i+=3, j++)
                {
                    block.floorModel.triangleCenters[j] = (block.floorModel.vertices[i].Position + block.floorModel.vertices[i+1].Position + block.floorModel.vertices[i+2].Position)/3;
                                        
                    float rad = 0;
                    float r1 = VectorMath.get_length(block.floorModel.triangleCenters[j] - block.floorModel.vertices[i].Position);
                    float r2 = VectorMath.get_length(block.floorModel.triangleCenters[j] - block.floorModel.vertices[i + 1].Position);
                    float r3 = VectorMath.get_length(block.floorModel.triangleCenters[j] - block.floorModel.vertices[i + 2].Position);

                    if (r1 > rad)
                        rad = r1;
                    if (r2 > rad)
                        rad = r2;
                    if (r3 > rad)
                        rad = r3;

                    block.floorModel.maxTriangleRadius[j] = rad;
                    
                }

                block.wallModel.triangleCenters = new Vector3[block.wallModel.vertices.Length/3];
                block.wallModel.maxTriangleRadius = new float[block.wallModel.vertices.Length/3];
                for (int i = 0, j = 0; i < block.wallModel.vertices.Length; i += 3, j++)
                {
                    block.wallModel.triangleCenters[j] = (block.wallModel.vertices[i].Position + block.wallModel.vertices[i + 1].Position + block.wallModel.vertices[i + 2].Position) / 3;
                    float rad = 0;
                    float r1 = VectorMath.get_length(block.wallModel.triangleCenters[j] - block.wallModel.vertices[i].Position);
                    float r2 = VectorMath.get_length(block.wallModel.triangleCenters[j] - block.wallModel.vertices[i+1].Position);
                    float r3 = VectorMath.get_length(block.wallModel.triangleCenters[j] - block.wallModel.vertices[i+2].Position);

                    if (r1 > rad)
                        rad = r1;
                    if (r2 > rad)
                        rad = r2;
                    if (r3 > rad)
                        rad = r3;

                    block.wallModel.maxTriangleRadius[j] = rad;
                }

                //For the Wall Mesh
                #region Wall Calculation
                for (int i = 0; i < block.wallModel.vertices.Length; i++)
                {
                    Position_wall = Vector3.Add(Position_wall, block.wallModel.vertices[i].Position);
                }
                Position_wall = Vector3.Divide(Position_wall, (float)block.wallModel.vertices.Length);
                
                for (int i = 0; i < block.wallModel.vertices.Length; i++)
                {
                    float newRadius = VectorMath.get_length(Vector3.Subtract(Position_wall, block.wallModel.vertices[i].Position));
                    if (newRadius > Radius_wall)
                        Radius_wall = newRadius;
                }
                block.wallModel.RadiusFromCenter = Radius_wall;
                block.wallModel.CenterPosition = Position_wall;
                
                #endregion

                blocks.Add(block);
               // Console.WriteLine("Loading splits");
            }

            return blocks;

        }
    }
    #endregion 
  
}
