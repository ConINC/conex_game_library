﻿//CAMERA CLASS AND JOYSTICK INPUT CLASS!  FINALLY WORKING WITH 2 GAMEPADS!

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using SharpDX.DirectInput;
using Microsoft.Xna.Framework.Input;
using SharpDX;
using System.Collections;
using System.Threading;
using SharpDX.Multimedia;
using System.Timers;

namespace ConexGameLibrary
{
    public class JoystickClass
    {
        public int arrow_up1 = -1, arrow_down1 = -1, arrow_left1 = -1, arrow_right1 = -1, arrow_up_right1 = -1, arrow_up_left1 = -1
            , arrow_down_left1 = -1, arrow_down_right1 = -1, left_stick_Y1 = 32767, left_stick_X1 = 32767, right_stick_Y1 = 32767, right_stick_X1 = 32767,
            is_l2_down1 = 0;

        public int arrow_up2 = -1, arrow_down2 = -1, arrow_left2 = -1, arrow_right2 = -1, arrow_up_right2 = -1, arrow_up_left2 = -1
            , arrow_down_left2 = -1, arrow_down_right2 = -1, left_stick_Y2 = 32767, left_stick_X2 = 32767, right_stick_Y2 = 32767, right_stick_X2 = 32767,
            is_l2_down2 = 0;

        public int arrow_up3 = -1, arrow_down3 = -1, arrow_left3 = -1, arrow_right3 = -1, arrow_up_right3 = -1, arrow_up_left3 = -1
            , arrow_down_left3 = -1, arrow_down_right3 = -1, left_stick_Y3 = 32767, left_stick_X3 = 32767, right_stick_Y3 = 32767, right_stick_X3 = 32767;

        public int arrow_up4 = -1, arrow_down4 = -1, arrow_left4 = -1, arrow_right4 = -1, arrow_up_right4 = -1, arrow_up_left4 = -1
            , arrow_down_left4 = -1, arrow_down_right4 = -1, left_stick_Y4 = 32767, left_stick_X4 = 32767, right_stick_Y4 = 32767, right_stick_X4 = 32767;


        //Make Joysticks
        public Joystick[] joysticks_In_Class = new Joystick[4];

        public JoystickUpdate[] joyStickData1;
        public JoystickUpdate[] joyStickData2;
        public JoystickUpdate[] joyStickData3;
        public JoystickUpdate[] joyStickData4;
        public Guid[] joystickGuid;

        /// <summary>
        /// Max 4 gamepads allowed
        /// </summary>
        /// <param name="amountGamepads"></param>
        public void createJoystick(int amountGamepads)
        {
            bool[] foundGamepads = new bool[4]
            {
                false,
                false,
                false,
                false
            };

            // Initialize DirectInput
            DirectInput[] directInputs = new DirectInput[4];
            directInputs[0] = new DirectInput();
            directInputs[1] = new DirectInput();
            directInputs[2] = new DirectInput();
            directInputs[3] = new DirectInput();

             // Create joystick guids for 4 players max
            joystickGuid = new Guid[4];
            joystickGuid[0] = Guid.Empty;
            joystickGuid[1] = Guid.Empty;
            joystickGuid[2] = Guid.Empty;
            joystickGuid[3] = Guid.Empty;

            DeviceInstance[] deviceInstances = new DeviceInstance[4];
            deviceInstances[0] = new DeviceInstance();
            deviceInstances[1] = new DeviceInstance();
            deviceInstances[2] = new DeviceInstance();
            deviceInstances[3] = new DeviceInstance();

            #region Get guids for all 4 gamepads
            try
            {
                deviceInstances[0] = directInputs[0].GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices)[0];
                joystickGuid[0] = deviceInstances[0].InstanceGuid;
                foundGamepads[0] = true;
                Console.WriteLine("Gamepad01  could be found");
            }
            catch
            {
                Console.WriteLine("Gamepad01  could not be found");
            }

            try
            {
                deviceInstances[1] = directInputs[1].GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices)[1];
                joystickGuid[1] = deviceInstances[1].InstanceGuid;
                foundGamepads[1] = true;
                Console.WriteLine("Gamepad02  could be found");
            }
            catch
            {
                Console.WriteLine("Gamepad02  could not be found");
            }

            try
            {
                deviceInstances[2] = directInputs[2].GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices)[2];
                joystickGuid[2] = deviceInstances[2].InstanceGuid;
                foundGamepads[2] = true;
                Console.WriteLine("Gamepad03  could be found");
            }
            catch
            {
                Console.WriteLine("Gamepad02  could not be found");
            }

            try
            {
                deviceInstances[3] = directInputs[3].GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices)[3];
                joystickGuid[3] = deviceInstances[3].InstanceGuid;
                foundGamepads[3] = true;
                Console.WriteLine("Gamepad04  could be found");
            }
            catch
            {
                Console.WriteLine("Gamepad04  could not be found");
            }
            #endregion
                       
            //Guid exists anyway here
            if (foundGamepads[0] && joystickGuid[0] != Guid.Empty)
            {
                   Console.WriteLine("Gamepad Guid 01 is : {0}", joystickGuid[0]);
            }
            
            if (foundGamepads[1] && joystickGuid[1] != Guid.Empty)
            {
                   Console.WriteLine("Gamepad Guid 02 is : {0}", joystickGuid[1]);                
            }
            if (foundGamepads[2] && joystickGuid[2] != Guid.Empty)
            {
                   Console.WriteLine("Gamepad Guid 03 is : {0}", joystickGuid[2]);                
            }
            
            if (foundGamepads[3] && joystickGuid[3] != Guid.Empty)
            {
                   Console.WriteLine("Gamepad Guid 04 is : {0}", joystickGuid[3]);                
            }

            // Instantiate the joystick
            if (foundGamepads[0]) { joysticks_In_Class[0] = new Joystick(directInputs[0], joystickGuid[0]); }
            if (foundGamepads[1]) { joysticks_In_Class[1] = new Joystick(directInputs[1], joystickGuid[1]); }
            if (foundGamepads[2]) { joysticks_In_Class[2] = new Joystick(directInputs[2], joystickGuid[2]); }
            if (foundGamepads[3]) { joysticks_In_Class[3] = new Joystick(directInputs[3], joystickGuid[3]); }

            //The guids are not supposed to be the same, throw an exception here or restart the game, give warning atleast
            for(int i = 0; i < amountGamepads -1 ; i++)
            {                
                    if(joystickGuid[0] == joystickGuid[i+1])
                    {
                    Console.WriteLine("The gamepad guids overlap in someway, can happen if theres only 1 gamepad");
                    
                    }
                    if(amountGamepads < 1)
                    {
                    }                
            }
            
            // Query all suported ForceFeedback effects            
            IList<EffectInfo>[] allEffectsArray = new IList<EffectInfo>[amountGamepads];

            for (int i = 0; i < amountGamepads; i++)
            {
                if (joysticks_In_Class[i] != null)
                {
                    allEffectsArray[i] = joysticks_In_Class[i].GetEffects();
                    joysticks_In_Class[i].Properties.BufferSize = 128;
                    joysticks_In_Class[i].Acquire();
                }
            }
        }        

        /// <summary>
        /// CALL THIS when a gamepad is unplugged to release and reobtain all devices
        /// </summary>
        public void releaseAllJoysticks()
        {
            joystickGuid = null;
            joyStickData1 = null;
            joyStickData2 = null;
            joyStickData3 = null;
            joyStickData4 = null;
        }
    }
    
    public class CameraGamePad : GameComponent
    {
        public Microsoft.Xna.Framework.Vector3 Position;
        public float original_height;
        public float head_rotation_y, head_rotation_x;
        public float cameraY, cameraX;
        public Microsoft.Xna.Framework.Vector3 floorOffset;
        public Microsoft.Xna.Framework.Matrix View;
        public Microsoft.Xna.Framework.Matrix Projection;
        public Microsoft.Xna.Framework.Vector3 LookAt;
        public Microsoft.Xna.Framework.Matrix RotationMatrix,RotationMatrix2;
        public float RotationSpeed;
        public bool invertY = true;
        short inverter = -1;
        public float walkSpeed;
        public Microsoft.Xna.Framework.Vector3 transformedDirection;
        public Microsoft.Xna.Framework.Vector3 transformedDirection2;
        public JoystickClass joystick;
        byte player;
        float zoomFactor = 1;
        Microsoft.Xna.Framework.Input.KeyboardState ks;
        List<Block> blocks_in_class;

        bool enableGamepad = false;

        float left_stick_x_amount, left_stick_y_amount;

        public CameraGamePad(Game game, Microsoft.Xna.Framework.Matrix projection, float rotateSpeed, float walkSpeedParam, byte player)
            : base(game)
        {
            RotationSpeed = rotateSpeed;
            LookAt = new Microsoft.Xna.Framework.Vector3(0, 0, -1);
            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position, LookAt, Microsoft.Xna.Framework.Vector3.Up);
            Projection = projection;
            Position = new Microsoft.Xna.Framework.Vector3(1f, 0, 0);
            walkSpeed = walkSpeedParam;

            joystick = new JoystickClass();
            if(enableGamepad == true)
                joystick.createJoystick(2);

            this.player = player;
        }

        private double angularCounter = 0;

        /// <summary>
        /// Only temporary functioN!
        /// </summary>
        public void recieve_blocks(ref List<Block> blocks_in_class_p)
        {
            this.blocks_in_class = blocks_in_class_p;
        }

        public void UpdateData()
        {
            if (angularCounter < 360)
            {
                angularCounter = angularCounter + 0.008f;
            }
            
            else
            {
                angularCounter = 0;
            }

            cameraX = cameraX += -(float)Math.Cos(angularCounter * 2d) * 0.0002f;
            cameraY = cameraY += (float)Math.Sin(angularCounter) * 0.0001f;
            //camera looks at this direction
            Microsoft.Xna.Framework.Vector3 cameraDirection = new Microsoft.Xna.Framework.Vector3(0, 0, -1);

            //DO the breath move -> up down slowly

            //Rotation matrix for the Y axis of the camear
            RotationMatrix = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY);
            RotationMatrix2 = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY + MathHelper.PiOver2);

            //Creates a vector which points towards the direction that the camera is facing now

            //FOR THE FORWARD BACKWARD MOVEMENT //Normalising will always keep the same speed, even when looking down
            transformedDirection = Microsoft.Xna.Framework.Vector3.Transform(cameraDirection, RotationMatrix);

            Microsoft.Xna.Framework.Vector2 vecNormalised = Microsoft.Xna.Framework.Vector2.Normalize(
                                       new Microsoft.Xna.Framework.Vector2(
                                      transformedDirection.X, transformedDirection.Z
                                      )
                            );
            transformedDirection.X = vecNormalised.X; transformedDirection.Z = vecNormalised.Y;


            //FOR THE SIDEWAYS MOVEMENT
            transformedDirection2 = Microsoft.Xna.Framework.Vector3.Transform(cameraDirection, RotationMatrix2);

            Microsoft.Xna.Framework.Vector2 vecNormalised2 = Microsoft.Xna.Framework.Vector2.Normalize(
                                      new Microsoft.Xna.Framework.Vector2(
                                     transformedDirection2.X, transformedDirection2.Z
                                     )
                           );

            transformedDirection2.X = vecNormalised2.X; transformedDirection2.Z = vecNormalised2.Y;


            Microsoft.Xna.Framework.Vector3 cameraLookAt = Position + Microsoft.Xna.Framework.Vector3.Normalize(transformedDirection);

            //THIS IS WRONG, the camera Looks always a certain position, the position of the camera should
            //be the same as the look at, else it becomes a tracking camera!
            // View = Matrix.CreateLookAt(Position, transformedDirection, Vector3.Up);

            //IT IS IMPORTANT that the Lookat vector is always the same as the position itself, so the camera always rotates around a 0 axis, else the camera simply 
            //tracks the "lookat" and rotation wont work anymroe!

            View = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX);

            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position + new Microsoft.Xna.Framework.Vector3(0,5,0) , cameraLookAt+ new Microsoft.Xna.Framework.Vector3(0,5,0), Microsoft.Xna.Framework.Vector3.Up);

     
            //CAMERA X AND Y ARE TURNED AROUND SINCE IT ROTATES AROUND X AND Y
            
            if (cameraY > 2 * MathHelper.Pi || cameraY < -2 * MathHelper.Pi)
            {
                cameraY = 0;
            }
            if (cameraX > 1.4f)
                cameraX = 1.4f;
            if (cameraX < -1.4f)
                cameraX = -1.4f;
        }
        
        //For camera with Joystick
        public void update_states()
        {
            if (enableGamepad == true)
            {
                try
                {
                    joystick.joyStickData1 = joystick.joysticks_In_Class[0].GetBufferedData();
                }
                catch
                {
                    Console.WriteLine("Gamepad 01 was unplugged! please plug it in again!\n Press enter if its plugged back in!");
                    Console.ReadKey();
                    joystick.releaseAllJoysticks();
                    joystick.createJoystick(2);
                    return;
                }
            }
            
            #region register buttons
            if (joystick.joyStickData1 != null)
            {
                for (int i = 0; i < joystick.joyStickData1.Length; i++)
                {
                    if (joystick.joyStickData1[i].RawOffset == 32 && joystick.joyStickData1[i].Value == 0)
                    {
                        joystick.arrow_up1 = 0;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 32 && joystick.joyStickData1[i].Value == 4500)
                    {
                        joystick.arrow_up_right1 = 4500;
                    } 

                    if (joystick.joyStickData1[i].RawOffset == 32 && joystick.joyStickData1[i].Value == 9000)
                    {
                        joystick.arrow_right1 = 9000;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 32 && joystick.joyStickData1[i].Value == 31500)
                    {
                        joystick.arrow_up_left1 = 31500;
                    } 
                    
                    if (joystick.joyStickData1[i].RawOffset == 32 && joystick.joyStickData1[i].Value == 18000)
                    {
                        joystick.arrow_down1 = 18000;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 32 && joystick.joyStickData1[i].Value == 27000)
                    {
                        joystick.arrow_left1 = 27000;
                    }                   

                    if (joystick.joyStickData1[i].RawOffset == 32 && joystick.joyStickData1[i].Value == -1)
                    {
                        joystick.arrow_up1 = -1;
                        joystick.arrow_down1 = -1;
                        joystick.arrow_left1 = -1;
                        joystick.arrow_right1 = -1;
                        joystick.arrow_down_left1 = -1;
                        joystick.arrow_down_right1 = -1;
                        joystick.arrow_up_left1 = -1;
                        joystick.arrow_up_right1 = -1;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 4 && joystick.joyStickData1[i].Value != 32767)
                    {
                        joystick.left_stick_Y1 = joystick.joyStickData1[i].Value;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 0 && joystick.joyStickData1[i].Value != 32767)
                    {
                        joystick.left_stick_X1 = joystick.joyStickData1[i].Value;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 20 && joystick.joyStickData1[i].Value != 32767)
                    {
                        joystick.right_stick_Y1= joystick.joyStickData1[i].Value;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 8 && joystick.joyStickData1[i].Value != 32767)
                    {
                        joystick.right_stick_X1 = joystick.joyStickData1[i].Value;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 4 && joystick.joyStickData1[i].Value == 32767)
                    {
                        joystick.left_stick_Y1 = 32767;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 0 && joystick.joyStickData1[i].Value == 32767)
                    {
                        joystick.left_stick_X1 = 32767;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 20 && joystick.joyStickData1[i].Value == 32767)
                    {
                        joystick.right_stick_Y1 = 32767;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 8 && joystick.joyStickData1[i].Value == 32767)
                    {
                        joystick.right_stick_X1 = 32767;
                    }

                    if (joystick.joyStickData1[i].RawOffset == 54 && joystick.joyStickData1[i].Value == 128)
                    {
                        joystick.is_l2_down1 = 128;
                    }
                    if (joystick.joyStickData1[i].RawOffset == 54 && joystick.joyStickData1[i].Value == 0)
                    {
                        joystick.is_l2_down1 = 0;
                    }
                }
            }
            #endregion 





            if (joystick.arrow_up1 == 0)
            {
                Position.X = Position.X + transformedDirection.X * walkSpeed;
                Position.Z = Position.Z + transformedDirection.Z * walkSpeed;
            }

            if (joystick.arrow_down1 == 18000)
            {
                Position.X = Position.X - transformedDirection.X * walkSpeed;
                Position.Z = Position.Z - transformedDirection.Z * walkSpeed;
            }

            if (joystick.arrow_right1 == 9000)
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY - rotateAmount;
            }

            if (joystick.arrow_left1 == 27000)
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY + rotateAmount;
            }
            
            if (joystick.arrow_up_right1 == 4500)
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY - rotateAmount;               
            }
            if (joystick.arrow_up_left1 == 31500)
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY + rotateAmount;
            }

            if (joystick.left_stick_Y1 != 32767)
            {
                left_stick_y_amount = ( (float)joystick.left_stick_Y1 - 32767f) / 32000f;

                Position.X = Position.X - transformedDirection.X * walkSpeed * left_stick_y_amount;
                Position.Z = Position.Z - transformedDirection.Z * walkSpeed * left_stick_y_amount;
            }

            if (joystick.left_stick_X1 != 32767)
            {
                left_stick_x_amount = ((float)joystick.left_stick_X1 - 32767f) / 32000f;

                float directionX = transformedDirection.X;
                float directionY = transformedDirection.Y;

                Position.X = Position.X - transformedDirection2.X * walkSpeed * left_stick_x_amount;
                Position.Z = Position.Z - transformedDirection2.Z * walkSpeed * left_stick_x_amount;                
            }

            if (joystick.right_stick_X1 != 32767)
            {
                float val = ((float)joystick.right_stick_X1 - 32767f) / 32000f;

                float rotateAmount = RotationSpeed / 60f;
                cameraY = (cameraY + rotateAmount * val * inverter );
            }

            if (joystick.right_stick_Y1 != 32767)
            {
                float val = ((float)joystick.right_stick_Y1 - 32767f) / 32000f;

                float rotateAmount = RotationSpeed / 60f;
                cameraX = (cameraX + rotateAmount * val * inverter); // is naturally inverted, dats why
            }

            if (joystick.is_l2_down1 == 128)
            {
                if (zoomFactor < 4)
                    zoomFactor = zoomFactor + 0.1f;
                RotationSpeed = 0.5f;
            }
            if (joystick.is_l2_down1 == 0)
            {
                if (zoomFactor > 1)
                    zoomFactor = zoomFactor - 0.1f;
                RotationSpeed = 2.5f;
            }
            

            Projection = Microsoft.Xna.Framework.Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4 / zoomFactor, 4f / 1.5f, 0.5f, 10000.0f);
        }

        //For camera with keyboard
        public void Movement()
        {           
            ks = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            if (invertY == false)
            {
                inverter = 1;
            }
            if (invertY == true)
            {
                inverter = -1;
            }

            if (ks.IsKeyDown(Keys.A))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY + rotateAmount;          
            }

            if (ks.IsKeyDown(Keys.D))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY - rotateAmount;
            }

            if (ks.IsKeyDown(Keys.W))
            {
                float rotateAmount = (RotationSpeed / 60f) * inverter;
                cameraX = cameraX - rotateAmount;
            }
            if (ks.IsKeyDown(Keys.S))
            {
                float rotateAmount = (RotationSpeed / 60f) * inverter;
                cameraX = cameraX + rotateAmount;
            }

            if (ks.IsKeyDown(Keys.Q))
            {
                Position.X = Position.X + transformedDirection.X * walkSpeed;
                Position.Z = Position.Z + transformedDirection.Z * walkSpeed;
                //Console.WriteLine(Position);
            }

            if (ks.IsKeyDown(Keys.E))
            {
                Position.X = Position.X - transformedDirection.X * walkSpeed;
                Position.Z = Position.Z - transformedDirection.Z * walkSpeed;
            }            
        }

        //Call this from outside!
        public void PerformWallCollision(ref List<Block> blocks_param)
        {
            int cnt1, cnt2, cnt3;
            var tri = new Triangle();
            //ATTEMPT NUMBER 1!
            if (blocks_in_class != null)
            {
                Microsoft.Xna.Framework.Ray ray = new Microsoft.Xna.Framework.Ray();
                Microsoft.Xna.Framework.Vector3 averagedNormal;

                #region actual function!
                foreach (Block block in blocks_param)
                {
                    for (int i = 0; i < block.wallModel.vertices.Length; )
                    {
                        
                        cnt1 = i; cnt2 = i + 1; cnt3 = i + 2;

                        tri.p1 = block.wallModel.vertices[cnt1].Position;
                        tri.p2 = block.wallModel.vertices[cnt2].Position;
                        tri.p3 = block.wallModel.vertices[cnt3].Position;

                        float distancePlayerPlane = Collision.get_distanceFromPlane(Position, tri.p1, tri.p2, tri.p3);
                        Microsoft.Xna.Framework.Vector3 centerOfTriangle = Collision.getCenterOfTriangle(tri);
                        float maxRadiusAroundTriangle = Collision.getMaxRadiusAroundTriangle(tri);
                        float distanceFromTriangleCenter = VectorMath.get_length(Microsoft.Xna.Framework.Vector3.Subtract(centerOfTriangle, Position));

                        //if (distancePlayerPlane < maxRadiusAroundTriangle && distanceFromTriangleCenter < maxRadiusAroundTriangle)
                        if (distanceFromTriangleCenter < maxRadiusAroundTriangle)
                        {                            
                            averagedNormal = ( block.wallModel.vertices[cnt1].Normal + block.wallModel.vertices[cnt2].Normal + block.wallModel.vertices[cnt3].Normal) / 3;
                            ray.Position = Position; 
                            ray.Direction = averagedNormal;

                            if (Collision.checkTriangleIntersect_static( block.wallModel.vertices[cnt1].Position, block.wallModel.vertices[cnt2].Position,
                                                                                         block.wallModel.vertices[cnt3].Position, ray)  && distancePlayerPlane < 1 )
                            {                                
                                float fac1 = VectorMath.getScalarProductBeforeCosinus(averagedNormal, transformedDirection);
                                float fac2 = VectorMath.getScalarProductBeforeCosinus(averagedNormal, transformedDirection2);
                                Microsoft.Xna.Framework.Vector3.Normalize(averagedNormal);

                                Position += (averagedNormal * ((left_stick_y_amount > 0) ? left_stick_y_amount : left_stick_y_amount * -1) * ((fac1 > 0) ? fac1 : fac1 * -1))  / (walkSpeed * 7f); // *((fac1 > 0) ? fac1 : (-1 * fac1));
                                Position +=(averagedNormal * ((left_stick_x_amount > 0) ? left_stick_x_amount : left_stick_x_amount * -1) * ((fac2 > 0) ? fac2 : fac2 * -1)) / (walkSpeed * 7f); // *((fac2 > 0) ? fac1 : (-1 * fac2));
                            }
                        }
                        i = i + 3;
                    }
                }

                #endregion
            }
        }

        public override void Update(GameTime gameTime)
        {
            Movement();
            update_states();
            UpdateData();
            PerformWallCollision(ref blocks_in_class);

            base.Update(gameTime);
        }
    }

    public class CameraGamePad2 : GameComponent
    {
        public Microsoft.Xna.Framework.Vector3 Position;
        public float original_height;
        private float cameraY, cameraX;
        public Microsoft.Xna.Framework.Vector3 floorOffset;
        public Microsoft.Xna.Framework.Matrix View;
        public Microsoft.Xna.Framework.Matrix Projection;
        public Microsoft.Xna.Framework.Vector3 LookAt;
        public Microsoft.Xna.Framework.Matrix RotationMatrix, RotationMatrix2;
        public float RotationSpeed;
        public bool invertY = true;
        short inverter = -1;
        public float walkSpeed;
        public Microsoft.Xna.Framework.Vector3 transformedDirection;
        public Microsoft.Xna.Framework.Vector3 transformedDirection2;
        public JoystickClass joystick;
        byte player;
        Microsoft.Xna.Framework.Input.KeyboardState ks;
        float zoomFactor = 1;
        bool enableGamepad = false;

        List<Block> blocks_in_class;

        float left_stick_x_amount, left_stick_y_amount;

        public CameraGamePad2(Game game, Microsoft.Xna.Framework.Matrix projection, float rotateSpeed, float walkSpeedParam, byte player)
            : base(game)
        {
            RotationSpeed = rotateSpeed;
            LookAt = new Microsoft.Xna.Framework.Vector3(0, 0, -1);
            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position, LookAt, Microsoft.Xna.Framework.Vector3.Up);
            Projection = projection;
            Position = new Microsoft.Xna.Framework.Vector3(0,30, 0);
            walkSpeed = walkSpeedParam;
            joystick = new JoystickClass();
            if(enableGamepad == true)
                joystick.createJoystick(2);
            this.player = player;
        }

        double angularCounter = 0d;

        public void recieve_blocks(ref List<Block> blocks_in_class_p)
        {
            this.blocks_in_class = blocks_in_class_p;
        }

        public void Movement()
        {
            ks = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            if (invertY == false)
            {
                inverter = 1;
            }
            if (invertY == true)
            {
                inverter = -1;
            }

            if (ks.IsKeyDown(Keys.J))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY + rotateAmount;
            }


            if (ks.IsKeyDown(Keys.L))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY - rotateAmount;
            }

            if (ks.IsKeyDown(Keys.I))
            {
                float rotateAmount = (RotationSpeed / 60f) * inverter;
                cameraX = cameraX - rotateAmount;
            }
            if (ks.IsKeyDown(Keys.K))
            {
                float rotateAmount = (RotationSpeed / 60f) * inverter;
                cameraX = cameraX + rotateAmount;
            }


            if (ks.IsKeyDown(Keys.U))
            {
                Position.X = Position.X + transformedDirection.X * walkSpeed;
                Position.Z = Position.Z + transformedDirection.Z * walkSpeed;
                //Console.WriteLine(Position);
            }


            if (ks.IsKeyDown(Keys.O))
            {
                Position.X = Position.X - transformedDirection.X * walkSpeed;
                Position.Z = Position.Z - transformedDirection.Z * walkSpeed;
            }
        }
        public void UpdateData()
        {
            if (angularCounter < 360)
            {
                angularCounter = angularCounter + 0.008f;
            }
            else
            {
                angularCounter = 0;
            }

            cameraX = cameraX += -(float)Math.Cos(angularCounter * 2d) * 0.0002f;
            cameraY = cameraY += (float)Math.Sin(angularCounter) * 0.0001f;
            //camera looks at this direction
            Microsoft.Xna.Framework.Vector3 cameraDirection = new Microsoft.Xna.Framework.Vector3(0, 0, -1);

            //DO the breath move -> up down slowly

            //Rotation matrix for the Y axis of the camear
            RotationMatrix = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY);
            RotationMatrix2 = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY + MathHelper.PiOver2);

            //Creates a vector which points towards the direction that the camera is facing now

            //FOR THE FORWARD BACKWARD MOVEMENT //Normalising will always keep the same speed, even when looking down
            transformedDirection = Microsoft.Xna.Framework.Vector3.Transform(cameraDirection, RotationMatrix);

            Microsoft.Xna.Framework.Vector2 vecNormalised = Microsoft.Xna.Framework.Vector2.Normalize(
                                       new Microsoft.Xna.Framework.Vector2(
                                      transformedDirection.X, transformedDirection.Z
                                      )
                            );
            transformedDirection.X = vecNormalised.X; transformedDirection.Z = vecNormalised.Y;


            //FOR THE SIDEWAYS MOVEMENT
            transformedDirection2 = Microsoft.Xna.Framework.Vector3.Transform(cameraDirection, RotationMatrix2);

            Microsoft.Xna.Framework.Vector2 vecNormalised2 = Microsoft.Xna.Framework.Vector2.Normalize(
                                      new Microsoft.Xna.Framework.Vector2(
                                     transformedDirection2.X, transformedDirection2.Z
                                     )
                           );
            transformedDirection2.X = vecNormalised2.X;  transformedDirection2.Z = vecNormalised2.Y;


            Microsoft.Xna.Framework.Vector3 cameraLookAt = Position + Microsoft.Xna.Framework.Vector3.Normalize(transformedDirection);

            //THIS IS WRONG, the camera Looks always a certain position, the position of the camera should
            //be the same as the look at, else it becomes a tracking camera!
            // View = Matrix.CreateLookAt(Position, transformedDirection, Vector3.Up);

            //IT IS IMPORTANT that the Lookat vector is always the same as the position itself, so the camera always rotates around a 0 axis, else the camera simply 
            //tracks the "lookat" and rotation wont work anymroe!

            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position+new Microsoft.Xna.Framework.Vector3(0,5,0), cameraLookAt+new Microsoft.Xna.Framework.Vector3(0,5,0), Microsoft.Xna.Framework.Vector3.Up);

            //CAMERA X AND Y ARE TURNED AROUND SINCE IT ROTATES AROUND X AND Y
            //Console.WriteLine("camera2 is  : " + transformedDirection);
            if (cameraY > 2*MathHelper.Pi || cameraY < - 2*MathHelper.Pi)
            {
                cameraY = 0;
            }
            if (cameraX > 1.4f)
                cameraX = 1.4f;
            if (cameraX < -1.4f)
                cameraX = -1.4f;

        }
        //For camera with Joystick
        public void update_states()
        {
            if (enableGamepad == true)
            {
                try
                {
                    joystick.joyStickData2 = joystick.joysticks_In_Class[1].GetBufferedData();
                }

                catch
                {
                    Console.WriteLine("Gamepad 02 was unplugged! please plug it in again!\n Press enter if its plugged back in!");
                    Console.ReadKey();
                    joystick.releaseAllJoysticks();
                    joystick.createJoystick(2);
                    return;
                }
            }
            #region register buttons
            if (joystick.joyStickData2 != null && joystick.joystickGuid[1] != joystick.joystickGuid[0])
            {
                for (int i = 0; i < joystick.joyStickData2.Length; i++)
                {
                    if (joystick.joyStickData2[i].RawOffset == 32 && joystick.joyStickData2[i].Value == 0)
                    {
                        joystick.arrow_up2 = 0;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 32 && joystick.joyStickData2[i].Value == 4500)
                    {
                        joystick.arrow_up_right2 = 4500;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 32 && joystick.joyStickData2[i].Value == 9000)
                    {
                        joystick.arrow_right2 = 9000;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 32 && joystick.joyStickData2[i].Value == 31500)
                    {
                        joystick.arrow_up_left2 = 31500;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 32 && joystick.joyStickData2[i].Value == 18000)
                    {
                        joystick.arrow_down2 = 18000;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 32 && joystick.joyStickData2[i].Value == 27000)
                    {
                        joystick.arrow_left2 = 27000;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 32 && joystick.joyStickData2[i].Value == -1)
                    {
                        joystick.arrow_up2 = -1;
                        joystick.arrow_down2 = -1;
                        joystick.arrow_left2 = -1;
                        joystick.arrow_right2 = -1;
                        joystick.arrow_down_left2 = -1;
                        joystick.arrow_down_right2 = -1;
                        joystick.arrow_up_left2 = -1;
                        joystick.arrow_up_right2 = -1;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 4 && joystick.joyStickData2[i].Value != 32767)
                    {
                        joystick.left_stick_Y2 = joystick.joyStickData2[i].Value;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 0 && joystick.joyStickData2[i].Value != 32767)
                    {
                        joystick.left_stick_X2 = joystick.joyStickData2[i].Value;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 20 && joystick.joyStickData2[i].Value != 32767)
                    {
                        joystick.right_stick_Y2 = joystick.joyStickData2[i].Value;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 8 && joystick.joyStickData2[i].Value != 32767)
                    {
                        joystick.right_stick_X2 = joystick.joyStickData2[i].Value;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 4 && joystick.joyStickData2[i].Value == 32767)
                    {
                        joystick.left_stick_Y2 = 32767;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 0 && joystick.joyStickData2[i].Value == 32767)
                    {
                        joystick.left_stick_X2 = 32767;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 20 && joystick.joyStickData2[i].Value == 32767)
                    {
                        joystick.right_stick_Y2 = 32767;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 8 && joystick.joyStickData2[i].Value == 32767)
                    {
                        joystick.right_stick_X2 = 32767;
                    }

                    if (joystick.joyStickData2[i].RawOffset == 54 && joystick.joyStickData2[i].Value == 128)
                    {
                        joystick.is_l2_down2 = 128;
                    }
                    if (joystick.joyStickData2[i].RawOffset == 54 && joystick.joyStickData2[i].Value == 0)
                    {
                        joystick.is_l2_down2 = 0;
                    }       
            
                }
            }
            #endregion

            /*
            if (joystick.arrow_up2 == 0)
            {
                Position.X = Position.X + transformedDirection.X * walkSpeed;
                Position.Z = Position.Z + transformedDirection.Z * walkSpeed;
            }

            if (joystick.arrow_down2 == 18000)
            {
                Position.X = Position.X - transformedDirection.X * walkSpeed;
                Position.Z = Position.Z - transformedDirection.Z * walkSpeed;
            }

            if (joystick.arrow_right2 == 9000)
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY - rotateAmount;
            }

            if (joystick.arrow_left2 == 27000)
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY + rotateAmount;
            }

            if (joystick.arrow_up_right2 == 4500)
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY - rotateAmount;
            }
            if (joystick.arrow_up_left2 == 31500)
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY + rotateAmount;
            }
            */
            if (joystick.left_stick_Y2 != 32767)
            {
                left_stick_y_amount = ((float)joystick.left_stick_Y2 - 32767f) / 32000f;

                Position.X = Position.X - transformedDirection.X * walkSpeed * left_stick_y_amount;
                Position.Z = Position.Z - transformedDirection.Z * walkSpeed * left_stick_y_amount;
            }

            if (joystick.left_stick_X2 != 32767)
            {
                left_stick_x_amount = ((float)joystick.left_stick_X2 - 32767f) / 32000f;
                
                Position.X = Position.X - transformedDirection2.X * walkSpeed * left_stick_x_amount;
                Position.Z = Position.Z - transformedDirection2.Z * walkSpeed * left_stick_x_amount;
            }

            if (joystick.right_stick_X2 != 32767)
            {
                float val = ((float)joystick.right_stick_X2 - 32767f) / 32000f;

                float rotateAmount = RotationSpeed / 60f;
                cameraY = (cameraY + rotateAmount * val * inverter);
            }

            if (joystick.right_stick_Y2 != 32767)
            {
                float val = ((float)joystick.right_stick_Y2 - 32767f) / 32000f;

                float rotateAmount = RotationSpeed / 60f;
                cameraX = (cameraX + rotateAmount * val * inverter); // is naturally inverted, dats why
            }

            if (joystick.is_l2_down2 == 128)
            {
                if (zoomFactor < 4)
                    zoomFactor = zoomFactor + 0.1f;
                RotationSpeed = 0.5f;
            }
            if (joystick.is_l2_down2 == 0)
            {
                if (zoomFactor > 1)
                    zoomFactor = zoomFactor - 0.1f;
                RotationSpeed = 2.5f;
            }
            Projection = Microsoft.Xna.Framework.Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4 / zoomFactor, 4f / 1.5f, 0.5f, 10000.0f);
        }

        public void PerformWallCollision(ref List<Block> blocks_param)
        {
            int cnt1, cnt2, cnt3;
            var tri = new Triangle();
            //ATTEMPT NUMBER 1!
            if (blocks_in_class != null)
            {
                Microsoft.Xna.Framework.Ray ray = new Microsoft.Xna.Framework.Ray();
                Microsoft.Xna.Framework.Vector3 averagedNormal;

                #region actual function!
                foreach (Block block in blocks_param)
                {
                    for (int i = 0; i < block.wallModel.vertices.Length; )
                    {

                        cnt1 = i; cnt2 = i + 1; cnt3 = i + 2;

                        tri.p1 = block.wallModel.vertices[cnt1].Position;
                        tri.p2 = block.wallModel.vertices[cnt2].Position;
                        tri.p3 = block.wallModel.vertices[cnt3].Position;

                        float distancePlayerPlane = Collision.get_distanceFromPlane(Position, tri.p1, tri.p2, tri.p3);
                        Microsoft.Xna.Framework.Vector3 centerOfTriangle = Collision.getCenterOfTriangle(tri);
                        float maxRadiusAroundTriangle = Collision.getMaxRadiusAroundTriangle(tri);
                        float distanceFromTriangleCenter = VectorMath.get_length(Microsoft.Xna.Framework.Vector3.Subtract(centerOfTriangle, Position));

                        //if (distancePlayerPlane < maxRadiusAroundTriangle && distanceFromTriangleCenter < maxRadiusAroundTriangle)
                        if (distanceFromTriangleCenter < maxRadiusAroundTriangle)
                        {
                            averagedNormal = (block.wallModel.vertices[cnt1].Normal + block.wallModel.vertices[cnt2].Normal + block.wallModel.vertices[cnt3].Normal) / 3;
                            ray.Position = Position;
                            ray.Direction = averagedNormal;

                            if (Collision.checkTriangleIntersect_static(block.wallModel.vertices[cnt1].Position, block.wallModel.vertices[cnt2].Position,
                                                                                         block.wallModel.vertices[cnt3].Position, ray) && distancePlayerPlane < 1)
                            {
                                float fac1 = VectorMath.getScalarProductBeforeCosinus(averagedNormal, transformedDirection);
                                float fac2 = VectorMath.getScalarProductBeforeCosinus(averagedNormal, transformedDirection2);
                                Microsoft.Xna.Framework.Vector3.Normalize(averagedNormal);

                                Position += (averagedNormal * ((left_stick_y_amount > 0) ? left_stick_y_amount : left_stick_y_amount * -1) * ((fac1 > 0) ? fac1 : fac1 * -1)) / (walkSpeed * 7f); // *((fac1 > 0) ? fac1 : (-1 * fac1));
                                Position += (averagedNormal * ((left_stick_x_amount > 0) ? left_stick_x_amount : left_stick_x_amount * -1) * ((fac2 > 0) ? fac2 : fac2 * -1)) / (walkSpeed * 7f); // *((fac2 > 0) ? fac1 : (-1 * fac2));
                            }
                        }
                        i = i + 3;
                    }
                }

                #endregion
            }
        }

        public override void Update(GameTime gameTime)
        {
            Movement();
            update_states();
            UpdateData();
            PerformWallCollision(ref blocks_in_class);
            base.Update(gameTime);
        }
    }

    public class CameraGameMenu : GameComponent
    {

        public Microsoft.Xna.Framework.Vector3 Position;
        private float cameraY, cameraX;
        public Microsoft.Xna.Framework.Vector3 floorOffset;
        public Microsoft.Xna.Framework.Matrix View;
        public Microsoft.Xna.Framework.Matrix Projection;
        public Microsoft.Xna.Framework.Vector3 LookAt;
        public Microsoft.Xna.Framework.Matrix RotationMatrix, RotationMatrix2;
        public float RotationSpeed;

        public float walkSpeed;

        private Microsoft.Xna.Framework.Vector3 transformedDirection;        

        public CameraGameMenu(Game game, Microsoft.Xna.Framework.Matrix projection, float rotateSpeed, float walkSpeedParam)
            : base(game)
        {
            RotationSpeed = rotateSpeed;
            LookAt = new Microsoft.Xna.Framework.Vector3(0, 0, 0);
            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position, LookAt, Microsoft.Xna.Framework.Vector3.Up);
            Projection = projection;
            Position = new Microsoft.Xna.Framework.Vector3(0, 0, 0);
            walkSpeed = walkSpeedParam;
            cameraY = 1.3f;
            cameraX = 0;
        }
        double funcCounter = 0;

        public void Movement()
        {
            Microsoft.Xna.Framework.Input.KeyboardState ks = Microsoft.Xna.Framework.Input.Keyboard.GetState();                        

            if (ks.IsKeyDown(Keys.J))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY + rotateAmount;
                Console.WriteLine("Takes input");
            }

            if (ks.IsKeyDown(Keys.L))
            {
                float rotateAmount = RotationSpeed / 60f;
                cameraY = cameraY - rotateAmount;
            }

            if (ks.IsKeyDown(Keys.I))
            {
                float rotateAmount = (RotationSpeed / 60f);
                cameraX = cameraX - rotateAmount;
            }
            if (ks.IsKeyDown(Keys.K))
            {
                float rotateAmount = (RotationSpeed / 60f);
                cameraX = cameraX + rotateAmount;
            }

            if (ks.IsKeyDown(Keys.U))
            {
                Position.X = Position.X + transformedDirection.X * walkSpeed;
                Position.Z = Position.Z + transformedDirection.Z * walkSpeed;
                //Console.WriteLine(Position);
            }

            if (ks.IsKeyDown(Keys.O))
            {
                Position.X = Position.X - transformedDirection.X * walkSpeed;
                Position.Z = Position.Z - transformedDirection.Z * walkSpeed;
            }

            //camera looks at this direction
            Microsoft.Xna.Framework.Vector3 cameraDirection = new Microsoft.Xna.Framework.Vector3(0, 0, -1);
            
            Position = new Microsoft.Xna.Framework.Vector3(12, 10, 7);
            
            if (funcCounter > 360)
            {
                funcCounter = 0;
            }
            funcCounter += 0.005d;

            cameraY = cameraY + Convert.ToSingle(Math.Sin(funcCounter) / 2000d);
            Console.WriteLine(funcCounter);
            cameraX = cameraX + Convert.ToSingle((Math.Cos(funcCounter) / 5000d) + (Math.Sin(funcCounter) / 5000d));

            //Rotation matrix for the Y axis of the camear

            RotationMatrix = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY);
            RotationMatrix2 = Microsoft.Xna.Framework.Matrix.CreateRotationX(cameraX) * Microsoft.Xna.Framework.Matrix.CreateRotationY(cameraY + MathHelper.PiOver2); ;

            //Creates a vector which points towards the direction that the camera is facing now
            transformedDirection = Microsoft.Xna.Framework.Vector3.Transform(cameraDirection, RotationMatrix);
            
            Microsoft.Xna.Framework.Vector3 cameraLookAt = Position + transformedDirection;

            //Console.WriteLine(Position);

            //THIS IS WRONG, the camera Looks always a certain position, the position of the camera should
            //be the same as the look at, else it becomes a tracking camera!
            // View = Matrix.CreateLookAt(Position, transformedDirection, Vector3.Up);

            //IT IS IMPORTANT that the Lookat vector is always the same as the position itself, so the camera always rotates around a 0 axis, else the camera simply 
            //tracks the "lookat" and rotation wont work anymroe!

            //Right one here
            View = Microsoft.Xna.Framework.Matrix.CreateLookAt(Position, cameraLookAt, Microsoft.Xna.Framework.Vector3.Up);

            if (cameraX > 359 || cameraX < -359)
            {
                cameraX = 0;
            }
            if (cameraY > 359 || cameraY < -359)
            {
                cameraY = 0;
            }
        }
        
        public override void Update(GameTime gameTime)
        {
            Movement();

            base.Update(gameTime);
        }
    }

    public class Player
    {
 
    }
}
