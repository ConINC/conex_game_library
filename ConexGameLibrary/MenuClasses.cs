﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ConexGameLibrary;
using Microsoft.Xna.Framework.Input;

namespace ConexGameLibrary
{
    public class MenuPage
    {
        public String pageName;
        public int pageIndex;
        public List<MenuElement> menuElements;

        public MenuPage(string pagename, int pageIndex)
        {
            this.pageIndex = pageIndex;
            this.pageName = pagename;
            menuElements = new List<MenuElement>();
        }

        public void AddMenuElement(MenuElement element)
        {
            menuElements.Add(element);
        }
    }

    public class MenuElement
    {
        public WindowPolygon polygon;

        public String menuText;
        public Vector2 textPosition;
        public int textureChoice = -1;
        public int textureChoiceClicked = -1;
        public int currentTexturIndex = -1;
        public Rectangle MenuRectangle;
        public bool IsButton = false;
        public int fontChoice = 0;
        
        /// <summary>
        /// The menu is position in a factor of 0 to 1  left to right  or 0 to 1 from bottom to top
        /// The menu also always stays inside the frame
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="effect"></param>
        /// <param name="menuText_p"></param>
        /// <param name="textPosition_p"></param>
        /// <param name="menuPositon_p"></param>
        /// <param name="menuScale_p"></param>
        public MenuElement(int textureIndex, int textureIndexClicked,Point resolution, BasicEffect effect, String menuText_p, Vector2 textPosition_p,
                            Rectangle menuRectangle_p, bool isbutton_p)
        {
            this.textureChoice = textureIndex;
            this.textureChoiceClicked = textureIndexClicked;
            this.currentTexturIndex = textureIndex;

            polygon = new WindowPolygon(menuRectangle_p, resolution);
            this.menuText = menuText_p;
            effect.TextureEnabled = true;            
            this.menuText = menuText_p;
            this.textPosition = textPosition_p;
            this.MenuRectangle = menuRectangle_p;
            this.IsButton = isbutton_p;
            this.fontChoice = 1;
            
        }

        public bool updateMenuElement()
        {            
            MouseState mouse = Mouse.GetState();
            
            if (mouse.X > MenuRectangle.X && mouse.X < (MenuRectangle.X + MenuRectangle.Width))
            {
                if (mouse.Y > MenuRectangle.Y && mouse.Y < (MenuRectangle.Y + MenuRectangle.Height))
                {
                    if (!(textureChoiceClicked < 0))
                    {
                        this.currentTexturIndex = textureChoiceClicked;
                        return true;
                    }
                }
            }
            this.currentTexturIndex = textureChoice;
            return false;            
        }

        public bool menuClickedLeft()
        {
            MouseState mouse = Mouse.GetState();

            if (mouse.X > MenuRectangle.X && mouse.X < (MenuRectangle.X + MenuRectangle.Width))
            {
                if (mouse.Y > MenuRectangle.Y && mouse.Y < (MenuRectangle.Y + MenuRectangle.Height))
                {
                    //If element was clicked
                    if (mouse.LeftButton == ButtonState.Pressed && IsButton)
                        return true;
                }
            }            
            return false;
        }

        public bool menuClickedRight()
        {
            MouseState mouse = Mouse.GetState();

            if (mouse.X > MenuRectangle.X && mouse.X < (MenuRectangle.X + MenuRectangle.Width))
            {
                if (mouse.Y > MenuRectangle.Y && mouse.Y < (MenuRectangle.Y + MenuRectangle.Height))
                {
                    //If element was clicked
                    if (mouse.RightButton == ButtonState.Pressed && IsButton)
                        return true;
                }
            }
            return false;
        }
    }

    public class WindowPolygon
    {        
        private CustomVertex[] plane;
        Rectangle windowRect;

        public WindowPolygon(Rectangle rect, Point res)
        {
            this.windowRect = rect;
            makeVertices(rect,res);
        }

        public CustomVertex[] getWindowPlane()
        {
            return plane;
        }

        /// <summary>
        /// Rectangle will be given in pixels.
        /// </summary>
        /// <param name="rect"></param>
        private void makeVertices(Rectangle rect, Point resolution)
        {
            Vector2 point1; //As polygon Coordinate -1 to 1

            point1.X = (float)rect.X / (float)resolution.X; //Convert from pixels to a space of 0 to 1
            point1.X = point1.X * 2 - 1;                    //Convert from pixels to a space of -1 to 1

            point1.Y = (float)rect.Y / (float)resolution.Y;
            point1.Y = point1.Y * 2 - 1;
            
            Vector2 point2;
            point2.X = point1.X + ((float)rect.Width / (float)resolution.X) * 2;
            point2.Y = point1.Y + ((float)rect.Height / (float)resolution.Y) * 2;


            //Example Pos 100,100 size   100,500
            //First convert the pixels to a space of 0 to 1...  the top left is 0,0 
            
            plane = new CustomVertex[6];
            //Top 2 Vertices: Triangle 1;     -1 is left on the screen  1 top
            plane[0] = new CustomVertex(new Vector3(point1.X, -point1.Y, 0), new Vector2(0, 0), Vector3.Zero, Vector3.Zero, Vector3.Zero);
            plane[1] = new CustomVertex(new Vector3(point2.X, -point1.Y, 0), new Vector2(1, 0), Vector3.Zero, Vector3.Zero, Vector3.Zero);

            plane[2] = new CustomVertex(new Vector3(point1.X, -point2.Y, 0), new Vector2(0, 1), Vector3.Zero, Vector3.Zero, Vector3.Zero);

            plane[3] = new CustomVertex(new Vector3(point2.X, -point2.Y, 0), new Vector2(1, 1), Vector3.Zero, Vector3.Zero, Vector3.Zero);
            plane[4] = new CustomVertex(new Vector3(point2.X, -point1.Y, 0), new Vector2(1, 0), Vector3.Zero, Vector3.Zero, Vector3.Zero);
            plane[5] = new CustomVertex(new Vector3(point1.X, -point2.Y, 0), new Vector2(0, 1), Vector3.Zero, Vector3.Zero, Vector3.Zero);
        }
    }
}
