﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Text;
using ConexGameLibrary;
using System.IO;
using ConexGameLibrary;
using System.Drawing;

namespace ConexGameLibrary
{
    public class MenuSegment
    {
        //To render the text
        public SpriteBatch spritebatch;
        public List<SpriteFont> fonts;

        public BasicEffect Effect;
        public Effect Render_Effect;
        public List<MenuPage> menuPages;
        public List<Texture2D> menu_textures;
        
        public String currentPageName;
        public int currentPageIndex;

        public Viewport[] viewports_menu;

        //Some Animated Models to display in the menu "playermodels"
        public AnimatedModelMesh[] previewModels;

        public RenderTarget2D[] previewRenderTargets;

        public int menuInactiveTimer = 0; //To prevent doubleclick 
        public RasterizerState rast;

        public MenuSegment()
        {
            this.rast = new RasterizerState();
            rast.CullMode = CullMode.None;

            this.previewModels = new AnimatedModelMesh[10];
            fonts = new List<SpriteFont>();
            menuPages = new List<MenuPage>();
            menu_textures = new List<Texture2D>();
        }
    }

    public class GameSegment
    {
        public SpriteBatch spritebatch;
        public List<SpriteFont> fonts;

        public Single[] PlayerHeights;
        public Level Level;
        public List<Effect> Effects;
        public List<Player> Players;
        public List<Camera> Cameras;
        public Viewport[] viewports;
        public List<Matrix> Perspectives;

        public List<PhysicsModel> PhysicsModels;
        public List<Vehicle> Vehicles;
        public Texture2D normal_basic, specular_basic;

        public List<Mirror> Mirrors;
        public List<Shadow> Shadows;
        public Texture2D FlashLightTexture;

        public EffectSprite_Container spriteContainer;
       
        public List<Sound_Container> Sound_Collection; //TESTING
        public List<Sound_Container> GeneralSound_Effects;

        public List<I_GameItems> itemsInMap;
        public Hud_Settings_Game hudsettings;
        public Joystick_Input joystickinput = new Joystick_Input();

        public AnimationContainer shared_animation_human;
        public AnimationContainer shared_animation_guns;

        public List<AnimatedModelMesh> playerModels = new List<AnimatedModelMesh>();
        public List<AnimatedModelMesh> hitboxModels = new List<AnimatedModelMesh>();
        public List<AnimatedModelMesh> modelAttachments = new List<AnimatedModelMesh>();

        public List<AnimatedModelMesh> weaponModels_World = new List<AnimatedModelMesh>();
        public List<AnimatedModelMesh> weaponModels_View = new List<AnimatedModelMesh>();

        public List<GunLoadInfo> gunLoadInfos = new List<GunLoadInfo>();
        public List<PlayerLoadInfo> playerLoadInfos = new List<PlayerLoadInfo>();
        public List<Generic_Firearm> generic_firearms = new List<Generic_Firearm>();
        public List<Firearm_Property> gunproperties = new List<Firearm_Property>();

        public _Model Testmodel;
        public VertexBuffer testmodel_vb;

        public NPC_Level npc_level;

        public Camera externalCam;

        public GameSegment()
        {
            this.GeneralSound_Effects = new List<Sound_Container>();           
            hudsettings = new Hud_Settings_Game();
            fonts = new List<SpriteFont>();
            itemsInMap = new List<I_GameItems>();
            
            spriteContainer = new EffectSprite_Container();
            //firearmSprites = new EffectSprite_ContainerArray(4);

            Shadows = new List<Shadow>();
            Mirrors = new List<Mirror>();
            Level = new Level();
            Effects = new List<Effect>();
            
            Players = new List<Player>();

            Cameras = new List<Camera>();
            this.viewports = new Viewport[4];
            this.Perspectives = new List<Matrix>();
            PlayerHeights = new Single[4];
            PlayerHeights[0] = 0; PlayerHeights[1] = 0; PlayerHeights[2] = 0; PlayerHeights[3] = 0;

            PhysicsModels = new List<PhysicsModel>();
            Vehicles = new List<Vehicle>();


           
            this.Sound_Collection = new List<Sound_Container>();
            this.npc_level = new NPC_Level();
        }        
    }
    
}
