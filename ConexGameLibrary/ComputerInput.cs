﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace ConexGameLibrary
{
    public static class KeyBoardInput
    {
                
    }

    public static class MouseInput
    {
        private static MouseState current;

        public static Vector2 PositionDifference = Vector2.Zero;
        
        public static bool LeftMousePressed()
        {
            current = Mouse.GetState();
            if (current.LeftButton.Equals(KeyState.Down))
                return true;

            else
                return false;
            
        }

        public static bool RightMousePressed()
        {
            current = Mouse.GetState();
            if (current.RightButton.Equals(KeyState.Down))
                return true;

            else
                return false;

        }

        //Will return a value between -1 and 1 for the max mouse offset
        public static Vector2 getMousePositionAsFloat(int width, int height)
        {

            current = Mouse.GetState();
            Vector2 value;
            value.X = current.X;
            value.Y = current.Y;
            
            value.X = (float)(current.X - (width / 2)) / (float)(width / 2);
            value.Y = (float)(current.Y - (height / 2)) / (float)(height / 2);
            return value;
        }
        public static Vector2 getMouseDifference(int width, int height)
        {

            Vector2 pos = getMousePositionAsFloat(width, height);

            KeyboardState k = Keyboard.GetState();

            if(k.IsKeyDown(Keys.Space))
                Mouse.SetPosition(width / 2, height / 2);

            Vector2 pos2 = getMousePositionAsFloat(width, height);

            return Vector2.Subtract(pos, pos2);
        }

        
    }

    
}
