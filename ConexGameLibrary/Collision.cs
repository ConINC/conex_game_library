using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace ConexGameLibrary
{    
    public class SphereVolume
    {
        public Vector3 Position;
        public Single Radius;

        public SphereVolume(Vector3 position, Single radius)
        {   
            this.Radius = radius;
            this.Position = position;
        }
    }

    public class Triangle
    {
        public Triangle()
        {
        }        
        public Vector3 p1;        
        public Vector3 p2;
        public Vector3 p3;     
    }    
  
    /// <summary>
    /// This is the structure of a Plane in Hess-normal-form
    /// </summary>
   
    public struct Plane_normal
    {
        public Single distanceOrig;        
        public Vector3 rVector;
        public Vector3 nVector;
        public Vector3 randomPoint;
                                                                                                        // direction on plane
        public Plane_normal(Single distanceOrig, Vector3 nVector, Vector3 rVector, Vector3 randomPointonPlane)
        {
            this.distanceOrig = distanceOrig; //Simply one point on the plane
            this.nVector = nVector; //The cross product of 2 directional vectors in the plane (p3-p2) X (p3-p1) or d1 X d2
            this.rVector = rVector; //This is the distance of 2 points on the plane
            this.randomPoint = randomPointonPlane;
        }
    }

    /// <summary>
    /// This class contains vector3 related math functions, including a function to create a plane from a triangle
    /// </summary>
    public static class VectorMath //Describes math for vector operations
    {
        public static Vector3 vector3_round(Vector3 vec, int digits)
        {
            vec.X = (float)Math.Round((double)vec.X, digits);
            vec.Y = (float)Math.Round((double)vec.Y, digits);
            vec.Z = (float)Math.Round((double)vec.Z, digits);
            return vec;
        }

        public static Vector3 getCrossProduct(Vector3 vec1, Vector3 vec2)
        {
            Vector3 cross;
            cross.X = ((vec1.Y * vec2.Z) - (vec1.Z * vec2.Y));
            cross.Y = ((vec1.Z * vec2.X) - (vec1.X * vec2.Z));
            cross.Z = ((vec1.X * vec2.Y) - (vec1.Y * vec2.X));

            return cross;
        }
        
        //Is probaly outdated now, was used in wall collision detection, but now not used.
        /// <summary>
        /// Outdated!!! gets the clamped angle 0 = 90degree  1= 180 or 0 degree
        /// </summary>
        /// <param name="cross"></param>
        /// <param name="camDir"></param>
        /// <returns></returns>
        public static Single returnClampedAngleDegree(Vector3 cross, Vector3 camDir)
        {
            return (Single)(                
                    Math.Acos(
                        (double)               
                            ((VectorMath.getScalarProduct(cross, camDir) / (VectorMath.getAbsoluteVector(cross) * VectorMath.getAbsoluteVector(camDir)))
                    )
                )
            );
        }

        public static float getScalarProduct(Vector3 vec1, Vector3 vec2)
        {
            float scalar;
            scalar = (vec1.X * vec2.X) + (vec1.Y * vec2.Y) + (vec1.Z * vec2.Z);
            return scalar;
        }

        /// <summary>
        /// Returns scalar / |a| * |b|
        /// </summary>
        /// <param name="vec1"></param>
        /// <param name="vec2"></param>
        /// <returns></returns>
        public static float getScalarProductBeforeCosinus(Vector3 vec1, Vector3 vec2)
        {
            float scalar;
            scalar = ((vec1.X * vec2.X) + (vec1.Y * vec2.Y) + (vec1.Z * vec2.Z)) / (get_length(vec1) * get_length(vec2));
            return scalar;
        }
             
        public static float getAbsoluteVector(Vector3 vec3)
        {   
            //Returns the absoltue of an vector!  by formula:   a = squareRoot(a�+b�+c�) 
            float absVec3 = (float)Math.Sqrt(Math.Pow(vec3.X, 2) + Math.Pow(vec3.Y, 2) + Math.Pow(vec3.Z, 2) );   
            return absVec3;
        }

        /// <summary>
        /// Redirection to the get Absolute vector method!
        /// </summary>
        /// <param name="vec3"></param>
        /// <returns></returns>
        public static float get_length(Vector3 vec3)
        {
            return getAbsoluteVector(vec3);
        }

        public static double getAbsoluteVector(Vector2 vec2)
        {
            //Returns the absoltue of an vector!  by formula:   a = squareRoot(a�+b�+c�) 
            double absVec2 = Math.Sqrt(Math.Pow(vec2.X, 2) + Math.Pow(vec2.Y, 2));
            return absVec2;
        }

        public static Vector3 ReturnStep(Vector3 start, Vector3 end, Int32 currentFrame, Int32 maxFrame)
        {
            Vector3 difference = Vector3.Subtract(end, start);
            Single stepMultiplicator = currentFrame / (Single)maxFrame;
            Vector3 Step = Vector3.Multiply(difference, stepMultiplicator);
            var newStep = Vector3.Add(start, Step);

            return newStep;
        }

        /// <summary>
        /// Use this for Center, get the radius with the other function, use both !
        /// </summary>
        /// <param name="tri"></param>
        /// <returns></returns>
        public static Vector3 getCenterOfTriangle(Triangle tri)
        {
            float x = (tri.p1.X + tri.p2.X + tri.p3.X) / 3;
            float y = (tri.p1.Y + tri.p2.Y + tri.p3.Y) / 3;
            float z = (tri.p1.Z + tri.p2.Z + tri.p3.Z) / 3;
            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Overloaded function 
        /// </summary>
        /// <param name="tri"></param>
        /// <returns></returns>
        public static Vector3 getCenterOfTriangle(Vector3 a, Vector3 b, Vector3 c)
        {
            float x = (a.X + b.X + c.X) / 3;
            float y = (a.Y + b.Y + c.Y) / 3;
            float z = (a.Z + b.Z + c.Z) / 3;
            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Is used with the other function (getCenterOfTriangle)
        /// </summary>
        /// <param name="tri"></param>
        /// <returns></returns>
        public static Single getMaxRadiusAroundTriangle(Triangle tri)
        {
            float x = (tri.p1.X + tri.p2.X + tri.p3.X) / 3;
            float y = (tri.p1.Y + tri.p2.Y + tri.p3.Y) / 3;
            float z = (tri.p1.Z + tri.p2.Z + tri.p3.Z) / 3;
            var center = new Vector3(x, y, z);

            float[] distances = new float[3];
            distances[0] = Vector3.Distance(center, tri.p1);
            distances[1] = Vector3.Distance(center, tri.p2);
            distances[2] = Vector3.Distance(center, tri.p3);

            return distances.Max();
        }

        public static Single getMaxRadiusAroundTriangle(Vector3 a, Vector3 b, Vector3 c)
        {
            float x = (a.X + b.X + c.X) / 3;
            float y = (a.Y + b.Y + c.Y) / 3;
            float z = (a.Z + b.Z + c.Z) / 3;
            var center = new Vector3(x, y, z);

            float[] distances = new float[3];
            distances[0] = Vector3.Distance(center, a);
            distances[1] = Vector3.Distance(center, b);
            distances[2] = Vector3.Distance(center, c);

            return distances.Max();
        }

        public static Vector3 InterpolateLinear(Vector3 start, Vector3 end, float factor)
        {
            return Vector3.Add(start, Vector3.Multiply(Vector3.Subtract(end, start), factor));
        }               
    }

    public static class Collision
    {       
        ///<summary>
        /// This Function takes a whole Model and returns true if a ray collides with any face of the model
        /// </summary> 
        public static bool CheckFaceRay(ref VertexListModel vlm, ref Ray ray)
        {            
            bool listContinue = true;
            
            while (listContinue)
            {
                //We have 3 p=(x,y,z) Points and a ray by the form r:x = a1x+a0                
                Vector3 A, B, C;
                A = vlm.customVertex[0].Position;
                B = vlm.customVertex[1].Position;
                C = vlm.customVertex[2].Position;

                //Do colision
                #region Collision Code blabla
                //Getting the difference or distance
                Vector3 AB, AC;
                AB = Vector3.Subtract(B, A);
                AC = Vector3.Subtract(C, A);

                //For simplicity, I create extra vectors for the ray, instead of using it directly
                Vector3 rayOrigin, rayDirection;
                rayOrigin = ray.Position;
                rayDirection = ray.Direction;              

                //now getting the crossproduct from ray direction and a distance from 2 points
                //Cross product : An orthogonal to the level that 
                //vector a and b create "pen standing on paper example"
                Vector3 CrossProduct = VectorMath.getCrossProduct(rayDirection, AC);
                
                //Now scalar of the crossProduct and the first distance AB
                float ScalarProduct = VectorMath.getScalarProduct(CrossProduct, AB);

                //divide 1 by scalar product
                float f = 1 / ScalarProduct;
                
                Vector3 s = Vector3.Subtract(rayOrigin, A);

                float u = f * (VectorMath.getScalarProduct(s, CrossProduct));

                Vector3 Q = VectorMath.getCrossProduct(s, AB);

                float v = f * VectorMath.getScalarProduct(rayDirection, Q);
                #endregion

                //if u < 0 or u > 1 -> No collision
                #region
                if (u < 0 || u > 1)
                {
                    return false;
                }
                #endregion
                //if v < 0 or (u+v) > 1 -> No collision
                #region
                else if (v < 0 || (u + v) > 1)
                {
                    //No collision still
                    return false;
                }
                #endregion

                else {
                    //Have colision, leave here                    
                    listContinue = false;
                    return true;
                }
            }
            return false;            
        }
 
        /// <summary>
        /// This is the simplest version, it takes a triangle and ray, returns true if intersects can be used in bigger functions for more
        /// </summary>        
        public static bool checkTriangleIntersect(Vector3 A, Vector3 B, Vector3 C, Ray ray)
        {
                //Getting the difference or distance
                Vector3 AB, AC;
                AB = Vector3.Subtract(B, A);
                AC = Vector3.Subtract(C, A);

                //For simplicity, I create extra vectors for the ray, instead of using it directly
                Vector3 rayOrigin, rayDirection;
                rayOrigin = ray.Position;
                rayDirection = ray.Direction;
                //now getting the crossproduct from ray direction and a distance from 2 points
                //Cross product : An orthogonal to the level that 
                //vector a and b create "pen standing on paper example"
                Vector3 CrossProduct = VectorMath.getCrossProduct(rayDirection, AC);
                //Now scalar of the crossProduct and the first distance AB
                float ScalarProduct = VectorMath.getScalarProduct(CrossProduct, AB);
                //divide 1 by scalar product
                float f = 1 / ScalarProduct;
                Vector3 s = Vector3.Subtract(rayOrigin, A);
                float u = f * (VectorMath.getScalarProduct(s, CrossProduct));
                Vector3 Q = VectorMath.getCrossProduct(s, AB);
                float v = f * VectorMath.getScalarProduct(rayDirection, Q);
                //if u < 0 or u > 1 -> No collision
                if (u < 0 || u > 1)
                {
                    return false;
                }
                //if v < 0 or (u+v) > 1 -> No collision
                else if (v < 0 || (u + v) > 1)
                {
                    //No collision still
                    return false;
                }
                else
                {
                    //Have colision, leave here
                    return true;
                }
        }

        public static bool checkTriangleIntersect_static(Vector3 A, Vector3 B, Vector3 C, Ray ray)
        {
            //Getting the difference or distance
            Vector3 AB, AC;
            AB = Vector3.Subtract(B, A);
            AC = Vector3.Subtract(C, A);

            //For simplicity, I create extra vectors for the ray, instead of using it directly
            Vector3 rayOrigin, rayDirection;
            rayOrigin = ray.Position;
            rayDirection = ray.Direction;
            //now getting the crossproduct from ray direction and a distance from 2 points
            //Cross product : An orthogonal to the level that 
            //vector a and b create "pen standing on paper example"
            Vector3 CrossProduct = VectorMath.getCrossProduct(rayDirection, AC);
            //Now scalar of the crossProduct and the first distance AB
            float ScalarProduct = VectorMath.getScalarProduct(CrossProduct, AB);
            //divide 1 by scalar product
            float f = 1 / ScalarProduct;
            Vector3 s = Vector3.Subtract(rayOrigin, A);
            float u = f * (VectorMath.getScalarProduct(s, CrossProduct));
            Vector3 Q = VectorMath.getCrossProduct(s, AB);
            float v = f * VectorMath.getScalarProduct(rayDirection, Q);
            //if u < 0 or u > 1 -> No collision
            if (u < 0 || u > 1)
            {
                return false;
            }
            //if v < 0 or (u+v) > 1 -> No collision
            else if (v < 0 || (u + v) > 1)
            {
                //No collision still
                return false;
            }
            else
            {
                //Have colision, leave here
                return true;
            }
        }
        
        /// <summary>
        /// This function returns a vector4,  W= distance from wall, X = the x direction in which i cant move anymore since the wall is there
        ///  Y  = the y direction in which i cant move anymore since the wall is there ,  Z = shows 0 if my sphere radius is smaller than the distance from the
        ///  sphere center to the wall plane and shows 1 if my radius is greater than the  distance from the wall plane, if 1, we actually intersect with the wall.
        /// </summary>
        /// <param name="sphereVolume"></param>
        /// <param name="triangle"></param>
        /// <returns></returns>
        public static Vector4 SpherePlaneIntersectionWall(SphereVolume sphereVolume, Triangle triangle)
        {            
            Plane_normal plane;
            Vector3 d1, d2, cross;
            d1 = Vector3.Subtract(triangle.p3, triangle.p2);
            d2 = Vector3.Subtract(triangle.p3, triangle.p1);
            cross = VectorMath.getCrossProduct(d1, d2);

            plane = new Plane_normal(Convert.ToSingle(VectorMath.getAbsoluteVector(triangle.p1)),cross,d1, triangle.p1);
            //This is the plane of the Wall, also there is an additional Point on the plane //For the collision math!
            
            // Now I have the Collider Sphere and the walls plane
            //Calculate the distance of a given point and the plane with this formula 
            float distance = Math.Abs(
                Convert.ToSingle(VectorMath.getScalarProduct(
                        Vector3.Divide(plane.nVector, Convert.ToSingle(VectorMath.getAbsoluteVector(plane.nVector))),
                        Vector3.Subtract( sphereVolume.Position, triangle.p1))
                      )
                );

            //Now that i know the distance from the Point or sphere center, we need the direction vector in world-coordinates,
            //because we want to know, in which direction the wall lies. Simply invert the normal vector, since the normal vector
            //points towards the direction that i can move

            //I cannot move towards this direction, this is the direction towards the wall, once i m close enough, i will collide with the wall!
            var normal = Vector3.Normalize(VectorMath.getCrossProduct(Vector3.Subtract(triangle.p3, triangle.p1), Vector3.Subtract(triangle.p3, triangle.p2)));

            /*
             * At last, we need to check, if our collision Sphere is close enough, meaning the radius 
             * of the sphere is bigger or equal with the distance from the wall plane
            */
            Single intersecting = 0;
            if (sphereVolume.Radius >= distance)
            {
                intersecting = 1;
            }

            if (sphereVolume.Radius < distance)
            {
                intersecting = 0;
            }

            Vector4 vec4data = new Vector4();
            vec4data.W = distance;
            vec4data.X = normal.X * 1f;
            vec4data.Y = normal.Z * 1f;
            vec4data.Z = intersecting;

            return vec4data;
        }

        public static float get_distanceFromPlane(Vector3 camPosition, Vector3 a, Vector3 b, Vector3 c)
        {
            //gives me the distance from a given plane spun by triangle
            Vector3 d1, d2, cross;
            d1 = Vector3.Subtract(c, b);
            d2 = Vector3.Subtract(c, a);
            cross = VectorMath.getCrossProduct(d1, d2);

            float distance = Math.Abs(
                Convert.ToSingle(VectorMath.getScalarProduct(
                        Vector3.Divide(cross,VectorMath.getAbsoluteVector(cross)),
                        Vector3.Subtract(camPosition, a))
                      )
                );
            return distance;
        }

        /// <summary>
        /// Use this for Center, get the radius with the other function, use both !
        /// </summary>
        /// <param name="tri"></param>
        /// <returns></returns>
        public static Vector3 getCenterOfTriangle(Triangle tri)
        {            
            float x = (tri.p1.X + tri.p2.X + tri.p3.X) / 3;
            float y =(tri.p1.Y + tri.p2.Y + tri.p3.Y) / 3;
            float z = (tri.p1.Z + tri.p2.Z + tri.p3.Z) / 3;
            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Is used with the other function (getCenterOfTriangle)
        /// </summary>
        /// <param name="tri"></param>
        /// <returns></returns>
        public static Single getMaxRadiusAroundTriangle(Triangle tri)
        {
            float x = (tri.p1.X + tri.p2.X + tri.p3.X) / 3;
            float y = (tri.p1.Y + tri.p2.Y + tri.p3.Y) / 3;
            float z = (tri.p1.Z + tri.p2.Z + tri.p3.Z) / 3;
            var center = new Vector3(x, y, z);

            float[] distances = new float[3];
            distances[0] = Vector3.Distance(center,tri.p1);
            distances[1] = Vector3.Distance(center,tri.p2);
            distances[2] = Vector3.Distance(center,tri.p3);

            return distances.Max(); 

        }

        /// <summary>
        /// This function is also in Collision, not only in terainphysics class
        /// </summary>
        /// <param name="tri"></param>
        /// <param name="ray"></param>
        /// <returns></returns>
        public static Vector3 intersectionPointTriangleRay(Triangle tri, Ray ray)
        {
            Vector3 a = Vector3.Subtract(tri.p3, tri.p1);
            Vector3 b = Vector3.Subtract(tri.p2, tri.p1);

            Vector3 nVector = VectorMath.getCrossProduct(a, b);

            float d = VectorMath.getScalarProduct(nVector, tri.p1);

              
            float tempScalarProduct = VectorMath.getScalarProduct(nVector, ray.Direction);

            float t = -((-d + VectorMath.getScalarProduct(nVector, ray.Position)) / tempScalarProduct);
            
            Vector3 qPoint = ray.Position + ray.Direction * t;

            return qPoint;
        }
        
        public static Vector3 intersectionPointTriangleRay_static(Triangle tri, Ray ray) 
        {
            Vector3 a = Vector3.Subtract(tri.p3, tri.p1);
            Vector3 b = Vector3.Subtract(tri.p2, tri.p1);

            Vector3 nVector = VectorMath.getCrossProduct(a, b);

            float d = VectorMath.getScalarProduct(nVector, tri.p1);
            
            float tempScalarProduct = VectorMath.getScalarProduct(nVector, ray.Direction);

            float t = -((-d + VectorMath.getScalarProduct(nVector, ray.Position)) / tempScalarProduct);

            Vector3 qPoint = ray.Position + ray.Direction * t;

            return qPoint;
        }

        public static float planeRayAngle(Vector3 down_direction, Vector3 a, Vector3 b, Vector3 c)
        {
            Vector3 normal = VectorMath.getCrossProduct(Vector3.Subtract(a, b), Vector3.Subtract(a, c));

            return VectorMath.getScalarProductBeforeCosinus(normal, down_direction);
        }

    }

    public class PointInsideVolume
    {
        public bool isInsideRadius(Vector3 position, SphereVolume sphere)
        {            
            if(VectorMath.getAbsoluteVector(Vector3.Subtract(position,sphere.Position)) < sphere.Radius )
            {
                return true;
            }
            else
                return false;
        }                
    }

    public class TerrainPhysics
    {
        public TerrainPhysics()
        {
        }

        public Vector3 intersectionPointTriangleRay(Triangle tri, Ray ray)
        {
            Vector3 a = Vector3.Subtract(tri.p3, tri.p1);
            Vector3 b = Vector3.Subtract(tri.p2, tri.p1);
            Vector3 nVector = Vector3.Normalize(VectorMath.getCrossProduct(a, b));
            float d = VectorMath.getScalarProduct(nVector, tri.p1);
            Plane_normal plane = new Plane_normal(d, Vector3.Normalize(VectorMath.getCrossProduct(a, b)), Vector3.Subtract(b, a), tri.p1);
            float t = -((-d + VectorMath.getScalarProduct(nVector, ray.Position)) / VectorMath.getScalarProduct(nVector, ray.Direction));
            return ray.Position + ray.Direction * t; ;
        }
                
        public float getObjectHeight(ref List<Block> blocks, ref Vector3 Position, float orig_height)
        {
            
            float height = 0;
            foreach (Block block in blocks)
            {
                for (int i = 0; i < block.floorModel.vertices.Length; )
                {
                    int cnt1, cnt2, cnt3;
                    cnt1 = i; cnt2 = i + 1; cnt3 = i + 2;
                    var tri = new Triangle();
                  
                    tri.p1 = block.floorModel.vertices[cnt1].Position;
                    tri.p2 = block.floorModel.vertices[cnt2].Position;
                    tri.p3 = block.floorModel.vertices[cnt3].Position;
                    
                    Vector3 point = intersectionPointTriangleRay(tri, new Ray(Position, Vector3.Down));

                    if (Collision.checkTriangleIntersect(tri.p1, tri.p2, tri.p3, new Ray(Position, Vector3.Down)))
                    {
                        if ((point.Y >= Position.Y - 1) && !(point.Y > Position.Y + 1))
                        {
                            height = point.Y;
                        }
                    }
                    
                    i += 3;
                }
            }
            return height;
        }

        public static class AnimClass
        {
            /// <summary>
            /// Returns the current frame, divided by the max frames between the keyframes and returns
            /// the step of a vector 
            /// </summary>
            /// <param name="start"></param>
            /// <param name="end"></param>
            /// <param name="currentFrame"></param>
            /// <param name="maxFrame"></param>
            /// <returns></returns>
            public static Vector3 ReturnStep(Vector3 start, Vector3 end, Int32 currentFrame, Int32 maxFrame)
            {
                Vector3 difference = Vector3.Subtract(end, start);
                Single stepMultiplicator = currentFrame / (Single)maxFrame;
                Vector3 Step = Vector3.Multiply(difference, stepMultiplicator);
                var newStep = Vector3.Add(start, Step);

                return newStep;
            }
        }        
    }
}
