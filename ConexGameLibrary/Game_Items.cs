﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ConexGameLibrary;

namespace ConexGameLibrary
{
    public interface I_GameItems
    {
        void UpdateItem();
        Matrix Get_ItemMatrix();
        StaticModelMesh Get_Item_Mesh();
        void UpdateCollider();
        SphereConvexHull getCollider();
        bool ComparePowerupEffect(string eff);
        float Get_Value();
        void PickeUpItem();
        bool ItemSpawned();
    }

    #region Standard Health/BodyArmor Item
    public class HealthPack : I_GameItems
    {
        // Graphics part
        public Vector3 Position;
        public Vector3 Rotation;
        public Matrix meshMatrix;
        private Vector3 _rotate_addition;
        private Vector3 _dynamicPos;

        public StaticModelMesh mesh;
        private SphereConvexHull _collider;

        private int _respawnTimeLimit;
        private int _respawnTimer;
        public bool IsSpawned;

        private int _health;

        public int getHealth()
        {
            this._respawnTimer = _respawnTimeLimit;
            this.IsSpawned = false;
            return _health;
        }

        /// <summary>
        /// Set start properties, rotate will run the whole time in update
        /// </summary>
        /// <param name="position"></param>
        /// <param name="give_Health"></param>
        public HealthPack(Vector3 position, int setHealth, int respawnTime, Vector3 rotate, StaticModelMesh meshp, SphereConvexHull col)
        {
            this._collider = col;
            this.mesh = meshp;
            this._respawnTimeLimit = respawnTime;
            this._rotate_addition = rotate;
            this.Position = position;
            this._health = setHealth;
        }

        public Matrix Get_ItemMatrix()
        {
            meshMatrix = Matrix.CreateRotationX(Rotation.X) *
                Matrix.CreateRotationY(Rotation.Y) *
                Matrix.CreateRotationZ(Rotation.Z) *
                Matrix.CreateTranslation(_dynamicPos);

            return meshMatrix;
        }
        public bool ItemSpawned()
        {
            return IsSpawned;
        }

        public void PickeUpItem()
        {
            this._respawnTimer = this._respawnTimeLimit;
            Console.WriteLine("Picked up health");
        }
        public StaticModelMesh Get_Item_Mesh()
        {
            return mesh;
        }

        public float Get_Value()
        {
            return 0;
        }

        public bool ComparePowerupEffect(String eff)
        {
            return false;
        }

        public void UpdateItem()
        {
            if (_respawnTimer > 0)
            {
                IsSpawned = false;
                _respawnTimer--;
            }
            if (_respawnTimer == 0)
            {
                IsSpawned = true;
            }
            //This updates it constantly
            this.Rotation += _rotate_addition;
            this._dynamicPos = new Vector3(Position.X, Position.Y + (Single)Math.Sin((double)Rotation.Y) / 2.0f, Position.Z);

            #region UpdateRotation
            if (Rotation.X >= MathHelper.TwoPi)
                Rotation.X = 0;
            else if (Rotation.Y >= MathHelper.TwoPi)
                Rotation.Y = 0;
            else if (Rotation.Z >= MathHelper.TwoPi)
                Rotation.Z = 0;
            else
                return;
            #endregion
        }

        public SphereConvexHull getCollider()
        {
            return _collider;
        }

        public void UpdateCollider()
        {
            this._collider.Position = _dynamicPos;
        }
    }

    public class BodyArmor : I_GameItems
    {
        // Graphics part
        public Vector3 Position;
        public Vector3 Rotation;
        public Matrix meshMatrix;
        private Vector3 _rotate_addition;
        private Vector3 _dynamicPos;

        public StaticModelMesh mesh;
        private SphereConvexHull _collider;
        private int _respawnTimeLimit;
        private int _respawnTimer;
        public bool IsSpawned;

        private int _Armor;

        public int getArmor()
        {
            this._respawnTimer = _respawnTimeLimit;
            this.IsSpawned = false;
            return _Armor;
        }

        public float Get_Value()
        { 
            return 0; 
        }

        public bool ItemSpawned()
        {
            return IsSpawned;
        }

        public void PickeUpItem()
        {
            this._respawnTimer = this._respawnTimeLimit;
        }

        public bool ComparePowerupEffect(String eff)
        {           
            return false;
        }

        /// <summary>
        /// Set start properties, rotate will run the whole time in update
        /// </summary>
        /// <param name="position"></param>
        /// <param name="give_Health"></param>
        public BodyArmor(Vector3 position, int startArmor, int respawnTime, Vector3 rotate, StaticModelMesh meshp,SphereConvexHull col)
        {
            this._collider = col;
            this.mesh = meshp;
            this._respawnTimeLimit = respawnTime;
            this._rotate_addition = rotate;
            this.Position = position;
            this._Armor = startArmor;
        }
        
        public StaticModelMesh Get_Item_Mesh()
        {
            return mesh;
        }

        public Matrix Get_ItemMatrix()
        {
            meshMatrix = Matrix.CreateRotationX(Rotation.X) *
                Matrix.CreateRotationY(Rotation.Y) *
                Matrix.CreateRotationZ(Rotation.Z) *
                Matrix.CreateTranslation(_dynamicPos);

            return meshMatrix;
        }

        public void UpdateItem()
        {
            if (_respawnTimer > 0)
            {
                IsSpawned = false;
                _respawnTimer--;
            }
            if (_respawnTimer == 0)
            {
                IsSpawned = true;
            }
            //This updates it constantly            
            this._dynamicPos = new Vector3(Position.X, Position.Y + (Single)Math.Sin((double)Rotation.Y)/2.0f,Position.Z);

            this.Rotation += _rotate_addition;
            #region UpdateRotation
            if (Rotation.X >= MathHelper.TwoPi)
                Rotation.X = 0;
            else if (Rotation.Y >= MathHelper.TwoPi)
                Rotation.Y = 0;
            else if (Rotation.Z >= MathHelper.TwoPi)
                Rotation.Z = 0;
            else
                return;
            #endregion
        }

        public void UpdateCollider()
        {
            this._collider.Position = Position;
        }

        public SphereConvexHull getCollider()
        {
            return _collider;
        }
    }

    public class Powerup : I_GameItems
    {
        // Graphics part
        public Vector3 Position;
        public Vector3 Rotation;
        public Matrix meshMatrix;
        private Vector3 _rotate_addition;
        private Vector3 _dynamicPos;

        public StaticModelMesh mesh;
        private SphereConvexHull _collider;
        private int _respawnTimeLimit;
        private int _respawnTimer;
        public bool IsSpawned;

        private float _powerupValue;
        public string PowerUpEffect;

        public float Get_Value()
        {
            return _powerupValue;
        }

        public bool ComparePowerupEffect(String eff)
        {
            if (eff.Equals(PowerUpEffect))
                return true;
            else
                return false;
        }

        public bool ItemSpawned()
        {
            return IsSpawned;
        }

        public void PickeUpItem()
        {
            Console.WriteLine("item " + PowerUpEffect + "picked up");
            this._respawnTimer = this._respawnTimeLimit;
        }

        /// <summary>
        /// speed, shotfactor,teleport,jump,jetpack,health_double,xray
        /// </summary>
        /// <param name="effect"></param>
        /// <returns></returns>
        public bool isEffect(String effect)
        {
            if (PowerUpEffect.Equals(effect))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Set start properties, rotate will run the whole time in update
        /// </summary>
        /// <param name="position"></param>
        /// <param name="give_Health"></param>
        public Powerup(Vector3 position, float value, string effect, int respawnTime, Vector3 rotate, StaticModelMesh meshp, SphereConvexHull col)
        {
            this.PowerUpEffect = effect;
            this._powerupValue = value;
            this._collider = col;
            this.mesh = meshp;
            this._respawnTimeLimit = respawnTime;
            this._rotate_addition = rotate;
            this.Position = position;
        }

        public StaticModelMesh Get_Item_Mesh()
        {
            return mesh;
        }

        public Matrix Get_ItemMatrix()
        {
            meshMatrix = Matrix.CreateRotationX(Rotation.X) *
                Matrix.CreateRotationY(Rotation.Y) *
                Matrix.CreateRotationZ(Rotation.Z) *
                Matrix.CreateTranslation(_dynamicPos);

            return meshMatrix;
        }

        public void UpdateItem()
        {
            if (_respawnTimer > 0)
            {
                IsSpawned = false;
                _respawnTimer--;
            }
            if (_respawnTimer == 0)
            {
                IsSpawned = true;
            }
            //This updates it constantly            
            this._dynamicPos = new Vector3(Position.X, Position.Y + (Single)Math.Sin((double)Rotation.Y) / 2.0f, Position.Z);

            this.Rotation += _rotate_addition;
            #region UpdateRotation
            if (Rotation.X >= MathHelper.TwoPi)
                Rotation.X = 0;
            else if (Rotation.Y >= MathHelper.TwoPi)
                Rotation.Y = 0;
            else if (Rotation.Z >= MathHelper.TwoPi)
                Rotation.Z = 0;
            else
                return;
            #endregion
        }

        public void UpdateCollider()
        {
            this._collider.Position = Position;
        }

        public SphereConvexHull getCollider()
        {
            return _collider;
        }
    }

    #endregion

    
}
