﻿using System;
using System.Collections.Generic;
using ConexGameLibrary;
using System.Threading;

namespace AnimImportTest
{
    class Program
    {
        static void Main(string[] args)
        {
            
            MoveImport move_obj = new MoveImport();
            move_obj.Lines = move_obj.returnLines("C://Users//Conex//Desktop//gamefolder//","test.mov");
            List<Floatx9[]> test_animation = move_obj.get_frames_from_lines(ref move_obj.Lines, 6);

            foreach (Floatx9[] line in test_animation)
            {
                Floatx9[] line_c = line;
                showTrippletData(ref line_c);

                Thread.Sleep(1000 / 30);
            }
            
            Console.ReadKey();
        }

        static void showTrippletData(ref Floatx9[] tripplets_line)
        {
            for (int i = 0; i < tripplets_line.Length; i++)
            {
                Console.Write(tripplets_line[i].translation[0] + " " + tripplets_line[i].translation[1] + " " + tripplets_line[i].translation[2] + "     ");
                Console.Write(tripplets_line[i].rotation[0] + " " + tripplets_line[i].rotation[1] + " " + tripplets_line[i].rotation[2] + "     ");
                Console.Write(tripplets_line[i].scale[0] + " " + tripplets_line[i].scale[1] + " " + tripplets_line[i].scale[2] + "     ");
                
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
